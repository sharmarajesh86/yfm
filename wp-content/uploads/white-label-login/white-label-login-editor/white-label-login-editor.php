<?php

/**
Plugin Name: CSS Editor for White Label Login
Plugin URI: http://white-label-login.righthere.com
Description: CSS Editor for White Label Login
Version: 1.0.2.63683
Author: Alberto Lau (RightHere LLC)
Author URI: http://white-label-login.righthere.com
 **/

if(defined('SPLE_PATH')) throw new Exception( __('A duplicate of this addon/plugin is already active.','spl') );
 
if(defined('SPL_ADDON_PATH')){
	define('SPLE_PATH', trailingslashit(SPL_ADDON_PATH . dirname($addon)) ); 
	define("SPLE_URL", trailingslashit(SPL_ADDON_URL . dirname($addon)) );
}else{
	define('SPLE_PATH', plugin_dir_path(__FILE__) ); 
	define("SPLE_URL", plugin_dir_url(__FILE__) );
} 
 
class plugin_spl_editor {
	var $id = 'sple';
	var $righthere_css_version='1.1.1';
	var $options_varname = 'spl_options';
	function plugin_spl_editor(){		
		// Integration point #1
		// Register the bundled righthere_css module.
		require_once SPLE_PATH.'righthere-css/load.php';//this file contains the same as load.pop.php from options panel, so only one needs be loaded.
		rh_register_php('righthere-css',SPLE_PATH.'righthere-css/class.module_righthere_css.php', $this->righthere_css_version);		
		rh_register_php('righthere-css-frontend',SPLE_PATH.'righthere-css/class.righthere_css_frontend.php', $this->righthere_css_version);	
		rh_register_php('rh-google-fonts-admin',SPLE_PATH.'righthere-css/class.google_web_fonts_admin.php', '1.0.0');
		rh_register_php('rh-functions', SPLE_PATH.'righthere-css/rh-functions.php', '1.0.0');
		rh_register_php('rh-edit-admin-bar',SPLE_PATH.'righthere-css/class.admin_bar_editor_access.php', $this->righthere_css_version);	
		rh_register_php('rhcss-options',SPLE_PATH.'righthere-css/class.rhcss_pop_options.php', $this->righthere_css_version);		
		//-----------
		$this->options = get_option($this->options_varname);
		$this->options = is_array($this->options)?$this->options:array();				
		//-----------
		
		add_action( 'after_setup_theme', array(&$this,'after_setup_theme'), 9 );//before main theme loads modules.
		
		add_filter( 'spl_form' , array( &$this,'spl_form'), 99, 1);
		
		
	}
	
	function spl_form( $classes ){	
		if( isset($_REQUEST['spl_edit']) ){			
			//force the layout type that is being edited.
			if( false!==strpos( $_REQUEST['spl_edit'], 'spl_dialog' ) ){
				$classes = 'spl-modal spl-modal-effect-9';
			}else if( false!==strpos($_REQUEST['spl_edit'],'spl_vertical') ){
				$classes = 'splogin-vertical splogin-right';
			}else if( false!==strpos($_REQUEST['spl_edit'],'spl_horizontal') ){
				$classes = 'splogin-horizontal splogin-top	';
			}else{		
			}	
		}
		return $classes;
	}
	
	function after_setup_theme(){
		if(!defined('SPL_VERSION'))return '';//calendarize-it is not active. do nothing.
		global $spl_plugin;
		// Integration point #2
		//usually por loading pop-panel, but now also loads the css editor module.
		do_action('rh-php-commons');	
	
		new google_web_fonts_admin(array(
			'path' 	=> SPLE_PATH.'righthere-css/',
			'url'	=> SPLE_URL.'righthere-css/'		
		));
		
		//load the frontend output
		new righthere_css_frontend();	
	
		$editor_enabled = $spl_plugin->get_option('enable_css_editor','1',true);
		$editor_debug = $spl_plugin->get_option('enable_css_editor_debug','',true);
		$editor_debug = '1'==$editor_debug?true:false;
		
		$bootstrap_in_footer = $spl_plugin->get_option('bootstrap_in_footer','',true);
		$bootstrap_in_footer = '1'==$bootstrap_in_footer ? true : false;
		
		$alternate_accordion = $spl_plugin->get_option('alternate_accordion','',true);
		$alternate_accordion = '1'==$alternate_accordion ? true : false;
		
		if($editor_enabled){
			// Integration point #3
			// Include the integration class by the current plugin
			require_once SPLE_PATH.'includes/class.spl_editor_backgrounds.php';		
			$settings = array(
				'url'						=> SPLE_URL.'righthere-css/',
				'path'						=> SPLE_PATH.'righthere-css/',
				'plugin_id'					=> 'spl',
				'version'					=> '1.0.9',
				'capability'				=> 'manage_options',
				'options_varname'			=> $this->options_varname,
				'cb_get_option'				=> array(&$this,'get_option'),
				'resources_path'			=> 'slide-login',			
				'file_queue_options_name' 	=> 'spl_queue',
				'upload_limit_per_index'	=> 20,		
				'debug'						=> $editor_debug,
				'detect_selector'			=> '.spl-background-container',
				//--
				'id'						=> 'spl',
				'trigger_var'				=> 'spl_edit',
				'trigger_val'				=> 'spl_bg_xxx',
				'bootstrap_in_footer'		=> $bootstrap_in_footer,
				'alternate_accordion'		=> $alternate_accordion,
				'in_login'					=> true,
				'queue'						=> 'spl_backgrounds'  
			);
			
			foreach( apply_filters('spl-css-editor-login-forms', array(
				'login'			=> __('Login','spl'),
				'register'		=> __('Register','spl'),		
				'lostpassword'	=> __('Lost password','spl')
			)) as $action => $label ){
				$settings['action']		= $action;
				$settings['section']	='spl_bg_'.$action;
				$settings['trigger_val']='spl_bg_'.$action;
				new spl_editor_backgrounds($settings);				
			}
			
			$action = 'login';	
			require_once SPLE_PATH.'includes/class.spl_closed_backgrounds.php';					
			//----------------------
					
			$settings['action']		= 'default';
			$settings['section']	='spl_closed_bg_default';
			$settings['trigger_val']='spl_closed_bg_default';
			$settings['selector']	= '.spl-slide-login.wp-login';
			new spl_closed_backgrounds($settings);	
			
			//----------------------	
			foreach( apply_filters('spl-css-editor-login-forms', array(
				'login'			=> __('Login','spl'),
				'register'		=> __('Register','spl'),		
				'lostpassword'	=> __('Lost password','spl'),
				'rp'			=> __('Reset Password','spl')
			)) as $action => $label ){
				$settings['action']		= $action;
				$settings['section']	= 'spl_closed_bg_'.$action;
				$settings['trigger_val']= 'spl_closed_bg_'.$action;
				$settings['selector']	= '';
				new spl_closed_backgrounds($settings);				
			}			
	


			
			//----- Include a second editable content
			require_once SPLE_PATH.'includes/class.spl_form_logout.php';	
			$settings['section']	='spl_logout';
			$settings['trigger_val']='spl_logout';
			$settings['detect_selector']='.rhcss-spl_login_body_class';
			new spl_form_logout($settings);

			require_once SPLE_PATH.'includes/class.spl_wp_login.php';	
			$settings['section']		='spl_wp_login';
			$settings['trigger_val']	='spl_wp_login';
			$settings['detect_selector']='.rhcss-spl_login_body_class';
			new spl_wp_login($settings);

			
			$action='login';			
			require_once SPLE_PATH.'includes/class.spl_layout_dialog.php';	
			$settings['section']		= sprintf('spl_dialog_%s', $action);
			$settings['trigger_val']	= sprintf('spl_dialog_%s', $action);;
			$settings['detect_selector']= '.rhcss-spl_login_body_class';
			new spl_layout_dialog($settings);	
			
			require_once SPLE_PATH.'includes/class.spl_layout_dialog_fields.php';	
			$settings['section']		= sprintf('spl_dialog_fields_%s', $action);
			$settings['trigger_val']	= sprintf('spl_dialog_fields_%s', $action);;
			$settings['detect_selector']= '.rhcss-spl_login_body_class';
			new spl_layout_dialog_fields($settings);		

			$action='login';			
			require_once SPLE_PATH.'includes/class.spl_layout_horizontal.php';	
			$settings['section']		= sprintf('spl_horizontal_%s', $action);
			$settings['trigger_val']	= sprintf('spl_horizontal_%s', $action);;
			$settings['detect_selector']= '.rhcss-spl_login_body_class';
			new spl_layout_horizontal($settings);	

			$action='login';			
			require_once SPLE_PATH.'includes/class.spl_layout_horizontal_fields.php';	
			$settings['section']		= sprintf('spl_horizontal_fields_%s', $action);
			$settings['trigger_val']	= sprintf('spl_horizontal_fields_%s', $action);;
			$settings['detect_selector']= '.rhcss-spl_login_body_class';
			new spl_layout_horizontal_fields($settings);	

			$action='login';			
			require_once SPLE_PATH.'includes/class.spl_layout_vertical.php';	
			$settings['section']		= sprintf('spl_vertical_%s', $action);
			$settings['trigger_val']	= sprintf('spl_vertical_%s', $action);;
			$settings['detect_selector']= '.rhcss-spl_login_body_class';
			new spl_layout_vertical($settings);		

			$action='login';			
			require_once SPLE_PATH.'includes/class.spl_layout_vertical_fields.php';	
			$settings['section']		= sprintf('spl_vertical_fields_%s', $action);
			$settings['trigger_val']	= sprintf('spl_vertical_fields_%s', $action);;
			$settings['detect_selector']= '.rhcss-spl_login_body_class';
			new spl_layout_vertical_fields($settings);				
			
			
			require_once SPLE_PATH.'includes/class.spl_top_message.php';	

			$settings['section']		= 'spl_ns_error';
			$settings['trigger_val']	= 'spl_ns_error';
			$settings['detect_selector']= '.ns-box';
			$settings['namespace']		= '.ns-box.ns-bar.ns-effect-slidetop.ns-error';
			new spl_top_message($settings);		

			$settings['section']		= 'spl_ns_success';
			$settings['trigger_val']	= 'spl_ns_success';
			$settings['detect_selector']= '.ns-box';
			$settings['namespace']		= '.ns-box.ns-bar.ns-effect-slidetop.ns-success';
			new spl_top_message($settings);				
			
			$settings['section']		= 'spl_ns_lp';
			$settings['trigger_val']	= 'spl_ns_lp';
			$settings['detect_selector']= '.ns-box';
			$settings['namespace']		= '.ns-box.ns-effect-slidetop.spl-notice-action-lostpassword';
			new spl_top_message($settings);	
			
			require_once SPLE_PATH.'includes/class.spl_maintanance.php';	
			$settings['section']		= 'spl_maintanance';
			$settings['trigger_val']	= 'spl_maintanance';
			$settings['detect_selector']= '.ns-box';
			$settings['namespace']		= '.splogin.splogin-maintanance';
			new spl_maintanance($settings);	
			
			require_once SPLE_PATH.'includes/class.spl_countdown.php';	
			$settings['section']		= 'spl_countdown';
			$settings['trigger_val']	= 'spl_countdown';
			$settings['detect_selector']= '.spl-countdown-holder';
			$settings['namespace']		= '';
			new spl_countdown($settings);	
			
			require_once SPLE_PATH.'includes/class.spl_countdown_dlg.php';	
			$settings['section']		= 'spl_countdown_dlg';
			$settings['trigger_val']	= 'spl_countdown_dlg';
			$settings['detect_selector']= '.spl-countdown-holder';
			$settings['namespace']		= '.splogin.splogin-maintanance.splogin-countdown';
			new spl_countdown_dlg($settings);				
			
		}	

		//option fields and admin bar links
		require_once SPLE_PATH.'includes/class.spl_css_options.php';
		
		new spl_css_options(array('plugin_id'=>'spl-css','admin_bar'=>$editor_enabled));			


		
		// Integration point #5
		// add tab to options panel
		if(is_admin()){
			//Creates the CSS Editor tab in the calendarize-it options
			new rhcss_pop_options('spl-css','manage_options',true);
			
			//--- create a separate menu for CSS Editor
			$settings = array(				
				'id'					=> 'spl-css',
				'plugin_id'				=> 'spl',
				'capability'			=> 'manage_options',
				'options_varname'		=> $spl_plugin->options_varname,
				'menu_id'				=> 'spl-css-options',
				'page_title'			=> __('CSS Editor','spl'),
				'menu_text'				=> __('CSS Editor','spl'),
				'option_menu_parent'	=> 'spl',
				//'option_menu_parent'	=> $this->id,
				'notification'			=> (object)array(
					'plugin_version'=> SPL_VERSION,
					'plugin_code' 	=> 'SPL',
					'message'		=> __('Slide Login plugin update %s is available! <a href="%s">Please update now</a>','spl')
				),
				'theme'					=> false,
				'stylesheet'			=> 'spl-options',
				'option_show_in_metabox'=> true,
				'path'			=> SPL_PATH.'options-panel/',
				'url'			=> SPL_URL.'options-panel/',
				'pluginslug'	=> SPL_SLUG,
				//'api_url' 		=> "http://localhost"
				'api_url' 		=> "http://plugins.righthere.com",
				'layout'		=> 'horizontal'
			);			
			new PluginOptionsPanelModule($settings);			
		}
		
	}

	function get_option($name,$default='',$default_if_empty=false){
		$value = isset($this->options[$name])?$this->options[$name]:$default;
		if($default_if_empty){
			$value = ''==$value?$default:$value;
		}
		return $value;
	}	
	
	function update_options($options){
		update_option($this->options_varname,$options);
	}		
}

new plugin_spl_editor();
?>