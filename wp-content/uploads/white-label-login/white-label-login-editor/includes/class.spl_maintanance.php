<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/

class spl_maintanance extends module_righthere_css{
	var $action='custom';
	var $namespace = '';
	function spl_maintanance($args=array()){
		//------------
		$defaults = array(
			'action'			=> 'maintanance',
			'namespace'			=> '.splogin.splogin-maintanance',
			'capability'		=> 'manage_options',
			'trigger_var'		=> 'spl_edit'
		);
		foreach($defaults as $property => $default){
			$this->$property = isset($args[$property])?$args[$property]:$default;
		}
		//-----------	
		if( isset( $_REQUEST[$args['trigger_var']] ) && $_REQUEST[$args['trigger_var']]==$args['trigger_val'] ){
			add_filter('enable_forced_login', array(&$this,'enable_forced_login'), 10, 1);
			add_filter('enable_spl', array(&$this,'enable_forced_login'), 10, 1);
		}		
		//-----------
		
		add_filter('spl_show_maintanance', array( &$this,'spl_show_maintanance'), 10 , 1 );
		add_filter('spl_maintanance_content', array( &$this, 'spl_maintanance_content'), 10, 1);
		return $this->module_righthere_css($args);
	}
	
	function enable_forced_login(){
		return '1';
	}
	
	function spl_maintanance_content( $content ){
		if( ''==trim($content) && isset($_REQUEST[$this->trigger_var]) && $_REQUEST[$this->trigger_var]==$this->trigger_val && current_user_can( $this->capability ) ){
			$content = __('Some dummy content so that it is not empty in the CSS Editor.  You can change this text in Options -> Maintanance (tab)','spl');
		}
		return $content;
	}
	
	function spl_show_maintanance( $val ){
		if( isset($_REQUEST[$this->trigger_var]) && $_REQUEST[$this->trigger_var]==$this->trigger_val && current_user_can( $this->capability ) ){
			return true;
		}	
		return $val;
	}
	
	function options($t=array()){
		$this->footer();
		//----
//-- Dialog		
		$id = sprintf('spl-layout-%s-%s-dlg', $this->layout, $this->action);		

		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Dialog','spl');
		$t[$i]->options = array();	
				
		$t[$i]->options[] =(object)array(
				'id'				=> $id.'width',
				'type'				=> 'css',
				'label'				=> __('Width','rhc'),
				'input_type'		=> 'number',
				'unit'				=> 'px',
				'class'				=> 'input-mini',
				'min'				=> '500',
				'max'				=> '2048',
				'step'				=> '1',
				'selector'	=> implode(',',array(
					$this->namespace
				)),	
				'property'			=> 'width',
				'real_time'			=> true
			);
			
		$t[$i]->options[] =(object)array(
				'id'				=> $id.'dlg_shadow',
				'type'				=> 'css',
				'label'				=> __('Box shadow','rhc'),
				'input_type'		=> 'textshadow',
				'opacity'			=> true,
				'selector'			=> $this->namespace,//make sure this is more specific than the background derived one.
				'property'			=> 'box-shadow',
				'real_time'			=> true,
				'btn_clear'			=> true
			);				
		/*	
		$t[$i]->options = $this->add_border_options($t[$i]->options,array(
			'prefix'	=> $id.'-border_',
			'selector'	=> implode(',',array(
				$this->namespace
			))
		));		
		
		$t[$i]->options = $this->add_border_radius_options($t[$i]->options,array(
			'prefix'	=> $id.'-rad_',
			'selector'	=> implode(',',array(
				$this->namespace
			))
		));	
		*/									
//-- HEADER ----
		$id = sprintf('spl-layout-%s-%s-header', $this->layout, $this->action);
		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Header','spl');
		$t[$i]->options = array();		
			
		$t[$i]->options = $this->add_font_options( $t[$i]->options, array(
			'prefix'	=> $id.'-font_',
			'selector'	=> implode(',',array(
				$this->namespace.' h3.action-section'
			)),			
			'labels'	=> (object)array(
				'family'	=> __('Font','rhc'),
				'size'		=> __('Size','rhc'),
				'color'		=> __('Color','rhc')				
			)
		));			
			
		$t[$i]->options = $this->add_backgroud_options( $t[$i]->options, array(
			'label'		=> __('Background','rhc'),
			'prefix'	=> $id.'-bg_',
			'selector'	=> implode(',',array(
				$this->namespace.' h3.action-section'
			))
		));		
		
		$t[$i]->options = $this->add_padding_options($t[$i]->options,array(
			'prefix'	=> $id.'_pad',
			'selector'	=> implode(',',array(
				$this->namespace.' h3.action-section'
			))
		));			
//-- BODY		
		$id = sprintf('spl-layout-%s-%s-body', $this->layout, $this->action);
		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Body','spl');
		$t[$i]->options = array();		
		
		$t[$i]->options = $this->add_backgroud_options( $t[$i]->options, array(
			'label'		=> __('Background','rhc'),
			'prefix'	=> $id.'-bg_',
			'selector'	=> implode(',',array(
				$this->namespace.' .spl-content'
			))
		));	

		$t[$i]->options = $this->add_padding_options($t[$i]->options,array(
			'prefix'	=> $id.'_pad',
			'selector'	=> implode(',',array(
				$this->namespace.' .splogin-body'
			))
		));			
			
							
//-- LINKS
		$id = sprintf('spl-layout-%s-%s-links', $this->layout, $this->action);
		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Links','spl');
		$t[$i]->options = array(
			/*
			(object)array(//This is needed because we are unable to read the css value from :after and :before
				'input_type' => 'raw_html',
				'html'=> '<div id="'.$id.'-border-top_helper'.'" ></div>'
			)				
			*/
		);				

		$t[$i]->options = $this->add_font_options( $t[$i]->options, array(
			'prefix'	=> $id.'-font',
			'selector'	=> implode(',',array(
				$this->namespace.' .spl_custom_links > a'
			)),			
			'labels'	=> (object)array(
				'family'	=> __('Font','rhc'),
				'size'		=> __('Size','rhc'),
				'color'		=> __('Color','rhc')				
			)
		));		
		
		$t[$i]->options = $this->add_backgroud_options( $t[$i]->options, array(
			'label'		=> __('Background','rhc'),
			'prefix'	=> $id.'-bg_',
			'selector'	=> implode(',',array(
				$this->namespace.' .spl_custom_links > a'
			))
		));				

		$t[$i]->options = $this->add_border_options($t[$i]->options,array(
			'prefix'	=> $id.'-border-top_',
			'label'			=> array(
				'color'	=> __('Border top color','rhc'),
				'style' => __('Border top style','rhc'),
				'size'	=> __('Width top','rhc')
			),			
			'property'	=>array(
				'color'	=> 'border-top-color',
				'style' => 'border-top-style',
				'size'	=> 'border-top-width'
			),
			'selector'	=> implode(',',array(
				'#'.$id.'-border-top_helper',
				$this->namespace.' .spl_custom_links > a:first-child'
			))
		));	
		

		$t[$i]->options = $this->add_border_options($t[$i]->options,array(
			'prefix'	=> $id.'-border-btm_',
			'label'			=> array(
				'color'	=> __('Border bottom color','rhc'),
				'style' => __('Border bottom style','rhc'),
				'size'	=> __('Width bottom','rhc')
			),			
			'property'	=>array(
				'color'	=> 'border-bottom-color',
				'style' => 'border-bottom-style',
				'size'	=> 'border-bottom-width'
			),
			'selector'	=> implode(',',array(
				$this->namespace.' .spl_custom_links > a'
			))
		));		

		$t[$i]->options[] = (object)array(
			'input_type'  	=> 'raw_html',
			'html'			=> '<div style="height:85px;display:block;"></div>'
		);			
//--- LINKS HOVER		
		$id = sprintf('spl-layout-%s-%s-linksh', $this->layout, $this->action);
		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Links Hover','spl');
		$t[$i]->options = array(
			(object)array(//This is needed because we are unable to read the css value from :after and :before
				'input_type' => 'raw_html',
				'html'=> '<div id="'.$id.'-hover-top_helper'.'" ></div>'
			)				
		);		
		
		$t[$i]->options = $this->add_font_options( $t[$i]->options, array(
			'prefix'	=> $id.'-fonth',
			'selector'	=> implode(',',array(
				$this->namespace.' .spl_custom_links > a:hover',
				'#'.$id.'-hover-top_helper'
			)),			
			'labels'	=> (object)array(
				'family'	=> __('Font','rhc'),
				'size'		=> __('Size','rhc'),
				'color'		=> __('Color','rhc')				
			)
		));		
		
		$t[$i]->options = $this->add_backgroud_options( $t[$i]->options, array(
			'label'		=> __('Background','rhc'),
			'prefix'	=> $id.'-bg-h_',
			'selector'	=> implode(',',array(
				$this->namespace.' .spl_custom_links > a:hover',
				'#'.$id.'-hover-top_helper'
			))
		));					
		//-- Saved and DC  -----------------------		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'rh-saved-list'; 
		$t[$i]->label 		= __('Templates','spl');
		$t[$i]->options = array(
			(object)array(
				'id'				=> 'rh_saved_settings',
				'input_type'		=> 'backup_list'
			)			
		);			
//----------------------------------------------------------------------
		return $t;
	}
	
	function footer(){
?>
<script>
jQuery('BODY').addClass('spla-<?php echo $this->action ?>');
</script>
<?php 	
	}
}
?>