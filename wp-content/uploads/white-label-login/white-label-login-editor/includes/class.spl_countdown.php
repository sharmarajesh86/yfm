<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/

class spl_countdown extends module_righthere_css{
	var $action='custom';
	var $namespace = '';
	function spl_countdown($args=array()){
		//------------
		$defaults = array(
			'action'			=> 'countdown',
			'namespace'			=> '',
			'capability'		=> 'manage_options',
			'trigger_var'		=> 'spl_edit'
		);
		foreach($defaults as $property => $default){
			$this->$property = isset($args[$property])?$args[$property]:$default;
		}
		//-----------	
		if( isset( $_REQUEST[$args['trigger_var']] ) && $_REQUEST[$args['trigger_var']]==$args['trigger_val'] ){
			add_filter('enable_forced_login', array(&$this,'enable_forced_login'), 10, 1);
			add_filter('enable_spl', array(&$this,'enable_forced_login'), 10, 1);
		}		
		//-----------
		add_filter('spl_show_maintanance', array( &$this,'spl_show_maintanance'), 10 , 1 );
		add_filter('spl_maintanance_content', array( &$this, 'spl_maintanance_content'), 10, 1);
		return $this->module_righthere_css($args);
	}
	
	function enable_forced_login(){
		return '1';
	}
	
	function spl_maintanance_content( $content ){
		if( false===strpos($content,'[spl_countdown') && isset($_REQUEST[$this->trigger_var]) && $_REQUEST[$this->trigger_var]==$this->trigger_val && current_user_can( $this->capability ) ){
			//countdown shortcoe is not present, add one just for css editor.
			$content.=sprintf('[spl_countdown date="%s-01-01 00:00"]',
				(date('Y')+1)
			);
		}
		return $content;
	}
	
	function spl_show_maintanance( $val ){
		if( isset($_REQUEST[$this->trigger_var]) && $_REQUEST[$this->trigger_var]==$this->trigger_val && current_user_can( $this->capability ) ){
			return true;
		}	
		return $val;
	}
	
	function options($t=array()){
		$this->footer();
		
		$derived = array(
						array(
							'type'	=> 'same',
							'val'	=> '',
							'sel'	=> '.spl-countdown-tpl,.for-css-editor.spl-countdown-tpl',
							'arg'	=> array(
								(object)array(
									'name' => 'z-index',
									'tpl'	=>'1;'
								)
							)
						)	
					)	;
		//----
		$id = sprintf('spl-countdown-%s', $this->action);	
		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Countdown','spl');
		$t[$i]->options = array();		

		$t[$i]->options = $this->add_font_options( $t[$i]->options, array(
			'prefix'	=> $id.'-font_',
			'selector'	=> implode(',',array(
				'.spl-countdown-tpl',
				'.for-css-editor.spl-countdown-tpl'
			)),		
			'labels'	=> (object)array(
				'family'	=> __('Font','rhc'),
				'size'		=> __('Size','rhc'),
				'color'		=> __('Color','rhc')				
			),
			'enable_size'	=> false
		));			
			
		$t[$i]->options[] = (object)array(
				'id'				=> $id.'-bgcolor2',
				'type'				=> 'css',
				'label'				=> __('Background color','rhc'),
				'input_type'		=> 'color_or_something_else',
				'selector'	=> implode(',',array(
					'.spl-countdown-tpl',
					'.for-css-editor.spl-countdown-tpl'
				)),
				'other_options'		=> array(
					'transparent'	=> 'transparent'
				),								
				'property'			=> 'background-color',			
				'btn_clear'			=> true,
				'opacity'			=> true,
				'real_time'			=> true,
				'derived'			=> $derived
			);			
			
		$t[$i]->options[] = (object)array(
				'id'				=> $id.'-bdr-color',
				'type'				=> 'css',
				'label'				=> __('Bar color','rhc'),
				'input_type'		=> 'color_or_something_else',
				'holder_class'		=> '',
				'opacity'			=> true,
				'btn_clear'			=> true,
				'selector'	=> implode(',',array(
					'.spl-countdown-tpl',
					'.for-css-editor.spl-countdown-tpl'
				)),
				'other_options'		=> array(
					'transparent'	=> 'transparent'
				),					
				'property'			=> 'border-color',			
				'real_time'			=> true,
				'derived'			=> $derived				
			);	
			
		$t[$i]->options[] =(object)array(
				'id'				=> $id.'thick',
				'type'				=> 'css',
				'label'				=> __('Bar thickness','rhc'),
				'input_type'		=> 'number',
				'unit'				=> 'px',
				'class'				=> 'input-mini',
				'min'				=> '0',
				'max'				=> '100',
				'step'				=> '10',
				'selector'	=> implode(',',array(
					'.spl-countdown-tpl',
					'.for-css-editor.spl-countdown-tpl'
				)),	
				'property'			=> 'line-height',
				'real_time'			=> true
			);						
			
		$t[$i]->options[] = (object)array(
			'input_type'  	=> 'raw_html',
			'html'			=> '<div style="height:145px;display:block;"></div>'
		);		
		
		
		//----
		$id = sprintf('spl-countdown-label-%s', $this->action);	
		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Countdown label','spl');
		$t[$i]->options = array();				

		$t[$i]->options = $this->add_font_options( $t[$i]->options, array(
			'prefix'	=> $id.'-font_',
			'selector'	=> implode(',',array(
				'.spl-css-edit-1 .spl-countdown-item-label'
			)),		
			'labels'	=> (object)array(
				'family'	=> __('Font','rhc'),
				'size'		=> __('Size','rhc'),
				'color'		=> __('Color','rhc')				
			)
		));		

		$t[$i]->options[] = (object)array(
			'input_type'  	=> 'raw_html',
			'html'			=> '<div style="height:145px;display:block;"></div>'
		);								
		//-- Saved and DC  -----------------------		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'rh-saved-list'; 
		$t[$i]->label 		= __('Templates','spl');
		$t[$i]->options = array(
			(object)array(
				'id'				=> 'rh_saved_settings',
				'input_type'		=> 'backup_list'
			)			
		);			
//----------------------------------------------------------------------
		return $t;
	}
	
	function footer(){
?>
<script>
jQuery('BODY').addClass('spla-<?php echo $this->action ?>');

function _init_listen_for_changes(){
	jQuery(document).ready(function($){
		$('.real-time').bind('change',function(e){
			//init_spl_countdown();
			countdown_apply_css();
			
			holder = $(".spl-countdown-item").parents('.spl-countdown-holder');
			if( holder.find('.spl-placeholder').length==0 ){
				holder.append('<div class="spl-placeholder" style="display:none;"></div>');
			}else{
				holder.find('.spl-placeholder').empty();
			}
			
			holder.find('.spl-countdown canvas').remove();
			holder.find('.spl-countdown .spl-countdown-item').unwrap();
			
			holder.find('.spl-placeholder').append( holder.find('.spl-countdown') );
			holder.find('.spl-countdown-cont').empty();
			
			holder.find('.spl-placeholder .spl-countdown-item').each(function(i,item){
				//to remove knob initialization.
				$(item).replaceWith( $(item).clone() ); 
			});

			holder.find('.spl-placeholder .spl-countdown').each(function(i,item){
				$(item).appendTo( holder.find('.spl-countdown-cont') );
			});
			
			holder.find('.spl-countdown').find(".spl-countdown-item").knob({});
		});
	});
}

setTimeout('_init_listen_for_changes()',1000);//Maybe race condition.

</script>
<?php 	
	}
	//-- temporar
	function add_font_options($options,$args){
		$defaults = array(
			'prefix' 			=> 'undefined',
			'selector'			=> 'undefined',
			'labels'			=> array(
				'family'	=> __('Font family','rhcss'),
				'size'		=> __('Size','rhcss'),
				'color'		=> __('Color','rhcss'),
				'weight'	=> __('Weight','rhcss')
			),
			'derived_color' => false,
			'enable_size'	=> true
		);
		
		foreach($defaults as $varname => $default){
			$$varname = isset($args[$varname])?$args[$varname]:$default;
		}	
		
		$options[] =(object)array(
				'id'				=> $prefix.'-font-family',
				'type'				=> 'css',
				'label'				=> $labels->family,
				'input_type'		=> 'font',
				'bold'				=> false,
				'class'				=> '',
				'holder_class'		=> '',
				//'class'				=> 'input-mini pop_rangeinput',
				'selector'			=> $selector,
				'property'			=> 'font-family',
				'real_time'			=> true
			);

		$options[] =(object)array(
				'id'				=> $prefix.'-font-weight',
				'type'				=> 'css',
				'label'				=> @$labels->weight,
				'input_type'		=> 'select',
				'options'	=> array(
					'initial'	=> __('Initial','rhcss'),
					'normal'	=> __('Normal','rhcss'),
					'bold'		=> __('Bold','rhcss'),
					'100'		=> '100',
					'200'		=> '200',
					'300'		=> '300',
					'400'		=> '400',
					'500'		=> '500',
					'600'		=> '600',
					'700'		=> '700',
					'800'		=> '800',
					'900'		=> '900'
				),
				'class'				=> '',
				'holder_class'		=> '',
				//'class'				=> 'input-mini pop_rangeinput',
				'selector'			=> $selector,
				'property'			=> 'font-weight',
				'real_time'			=> true
			);
		
		$options[] =(object)array('input_type'		=> 'grid_start');
			
		$tmp =(object)array(
				'id'				=>  $prefix.'-font-color',
				'type'				=> 'css',
				'label'				=> $labels->color,
				'input_type'		=> 'colorpicker',
				'holder_class'		=> 'span8 input-no-label',
				'opacity'			=> true,
				'btn_clear'			=> true,
				'selector'			=> $selector,
				'property'			=> 'color',
				'real_time'			=> true
			);
		if(false!==$derived_color){
			$tmp->derived = $derived_color;
		}
		$options[] = $tmp;	
		
		if($enable_size):
		$options[] =(object)array(
				'id'				=> $prefix.'-font-size',
				'type'				=> 'css',
				'label'				=> $labels->size,
				'input_type'		=> 'number',
				//'input_type'		=> 'element_size',
				'unit'				=> 'px',
				'class'				=> 'input-font-size',
				'holder_class'		=> 'span4 input-no-label',
				//'class'				=> 'input-mini pop_rangeinput',
				'min'				=> '6',
				'max'				=> '144',
				'step'				=> '1',
				'selector'			=> $selector,
				'property'			=> 'font-size',
				'real_time'			=> true
			);
		endif;
			
		$options[] =(object)array( 'input_type'		=> 'grid_end');
				
		return $options;
	}	
	
}
?>