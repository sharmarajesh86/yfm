<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/
class spl_css_options {
	function spl_css_options($args=array()){
		add_filter('rhcss-editor-options-'.$args['plugin_id'], array(&$this,'pop_tab_css_editor_options'),10,1);
		if($args['admin_bar']){
			add_action('init',array(&$this,'init'),9999 );
		}
		add_action('admin_head-events_page_spl-css-options',array(&$this,'admin_head_options'));
	}

	function admin_head_options(){
?>
<style>
.pt-option-yesno .pt-label {
display:inline-block;
min-width:220px;
}
</style>
<?php
	}
	
	
	function pop_tab_css_editor_options($options){
		//add more options to the CSS Editor Tab
		//calendar page
		//venue page
		//organizer page
		//upcoming events widget url
		global $wpdb,$spl_plugin;
		$options[]=(object)array(
						'type'			=> 'clear'
					);	
		$options[]=(object)array(
						'type'			=> 'subtitle',
						'label'			=> __('Editable sections urls','rhc')
					);	
		//-- Calendar url ----------------------------------------------------------------------		
		$options[]=	(object)array(
				'id'	=> 'rhc_css_calendar_url',
				'type'	=>'text',
				'label'	=> __('Page with login button URL','rhc'),
				'el_properties'	=> array('class'=>'widefat'),
				'description' => __('Specify the url of a page where the login button shortcode is implemented.  If left blank the editor will try to determine it automatically.','spl'),
				'default' => $this->get_spl_shortcode_url(),
				'save_option'=>true,
				'load_option'=>true
			);
		//------------------------------------------------------------------------

	
		return $options;
	}

	function get_spl_shortcode_url(){
		return site_url('/');
	}
		
	function init(){
		global $spl_plugin;
		if( '1'!=$spl_plugin->get_option('enable_css_editor','1',true) )return;//editor is not enabled. do not add items to the menu.
	
		// Provide access to the css editor
		//-- create editor quick access links
		
		$args = array(
			'nodes'=>array(
				array(
					'id' => 'spl', 
					'title' => __('White Label Login','spl'), 
					'parent' => 'rh-css-editor-root',
					'href'		=> '#',
					'meta'		=> array('onclick'=>'javascript:jQuery(this).parent().toggleClass("hover");')
				)
				
			)
		);
		
		$args['nodes'][]=array(
					'id' 		=> 'spl-layouts', 
					'title' 	=> __('Layouts','spl'), 
					'parent' 	=> 'spl',
					'href'		=> '#'
				);		
		
		$action='login';
		$layouts = array(
			(object)array(
				'label'				=> __('Dialog','spl'),
				'layout' 			=> 'dialog',
				'detect_selector'	=> '.rhcss-spl_login_body_class'
			),
			(object)array(
				'label'				=> __('Dialog fields','spl'),
				'layout' 			=> 'dialog_fields',
				'detect_selector'	=> '.rhcss-spl_login_body_class'
			),
			(object)array(
				'label'				=> __('Horizontal','spl'),
				'layout' 			=> 'horizontal',
				'detect_selector'	=> '.rhcss-spl_login_body_class'
			),
			(object)array(
				'label'				=> __('Horizontal fields','spl'),
				'layout' 			=> 'horizontal_fields',
				'detect_selector'	=> '.rhcss-spl_login_body_class'
			),
			(object)array(
				'label'				=> __('Vertical','spl'),
				'layout' 			=> 'vertical',
				'detect_selector'	=> '.rhcss-spl_login_body_class'
			),
			(object)array(
				'label'				=> __('Vertical fields','spl'),
				'layout' 			=> 'vertical_fields',
				'detect_selector'	=> '.rhcss-spl_login_body_class'
			)
		);
		
		foreach( $layouts as $lo ){
			$trigger_var = sprintf('spl_%s_%s', $lo->layout, $action);
			
			$args['nodes'][]=array(
						'id' 		=> sprintf('spl-layouts-%s', $lo->layout), 
						'title' 	=> $lo->label, 
						'parent' 	=> 'spl-layouts',
						'href'		=> $this->addURLParameter( $this->get_login_url( $action ), 'spl_edit', $trigger_var ),
						'meta'		=> array(
							'class'	=> 'not-spl'
						)						
					);			
		}
				
				 		
		$args['nodes'][]=array(
					'id' 		=> 'spl-messages', 
					'title' 	=> __('Top Message','spl'), 
					'parent' 	=> 'spl',
					'href'		=> '#'
				);	

		$args['nodes'][]=array(
					'id' 		=> 'spl-messages-success', 
					'title' 	=> __('Success','spl'), 
					'parent' 	=> 'spl-messages',
					'href'		=> $this->addURLParameter( $this->get_login_url( 'login' ), 'spl_edit', 'spl_ns_success' ).'#spl_ns_success',
					'meta'		=> array(
						'class'	=> 'not-spl'
					)	
				);		
				
		$args['nodes'][]=array(
					'id' 		=> 'spl-messages-error', 
					'title' 	=> __('Error','spl'), 
					'parent' 	=> 'spl-messages',
					'href'		=> $this->addURLParameter( $this->get_login_url( 'login' ), 'spl_edit', 'spl_ns_error' ).'#spl_ns_error',
					'meta'		=> array(
						'class'	=> 'not-spl'
					)	
				);		
				
		$args['nodes'][]=array(
					'id' 		=> 'spl-messages-lp', 
					'title' 	=> __('Lost password','spl'), 
					'parent' 	=> 'spl-messages',
					'href'		=> $this->addURLParameter( $this->get_login_url( 'lostpassword' ), 'spl_edit', 'spl_ns_lp' ),
					'meta'		=> array(
						'class'	=> 'not-spl'
					)	
				);						
	/*			
		$args['nodes'][]=array(
					'id' 		=> 'spl-layouts-dialog', 
					'title' 	=> __('Modal','spl'), 
					'parent' 	=> 'spl-layouts',
					'href'		=> '#'
				);	
				
		$args['nodes'][]=array(
					'id' 		=> 'spl-layouts-vertical', 
					'title' 	=> __('Vertical','spl'), 
					'parent' 	=> 'spl-layouts',
					'href'		=> '#'
				);							
				
		$args['nodes'][]=array(
					'id' 		=> 'spl-layouts-horizontal', 
					'title' 	=> __('Horizontal','spl'), 
					'parent' 	=> 'spl-layouts',
					'href'		=> '#'
				);						
*/
					
		
		$mod_frontend = ( '1' == apply_filters( 'enable_spl', $spl_plugin->get_option('enable_spl', '1', true) ) ) ? true : false ;
		$mod_backgrounds = ( '1'==$spl_plugin->get_option( 'enable_backgrounds', '1', true) ) ? true : false ;
		
		if( $mod_frontend || $mod_backgrounds ){
			$args['nodes'][]=array(
						'id' 		=> 'spl-bg', 
						'title' 	=> __('Backgrounds','spl'), 
						'parent' 	=> 'spl',
						'href'		=> '#'
					);		
		}
		
		if( $mod_backgrounds ){					
			foreach( apply_filters('spl-css-editor-login-forms', array(
				'login'			=> __('Login','spl'),
				'register'		=> __('Register','spl'),		
				'lostpassword'	=> __('Lost password','spl')
			)) as $action => $label ){
				$args['nodes'][]=array(
							'id' 		=> 'spl-bg-'.$action, 
							'title' 	=> $label, 
							'parent' 	=> 'spl-bg',
							'href'		=> $this->addURLParameter( $this->get_login_url( $action ), 'spl_edit', 'spl_bg_'.$action ),
							'meta'		=> array(
								'class'	=> 'not-spl'
							)
						);				
			}
		}
		
		
		//---- closed form backgrounds----- this apply to the background behind the form in wp-login.php wich is visible on form submit success.
		if($mod_frontend){
			$args['nodes'][]=array(
						'id' 		=> 'spl-form-closed', 
						'title' 	=> __('Backgrounds (closed form)','spl'), 
						'parent' 	=> 'spl',
						'href'		=> '#'
					);	
					
			foreach( apply_filters('spl-css-editor-login-forms', array(
				'default'		=> __('Default','spl'),
				'login'			=> __('Login','spl'),
				'register'		=> __('Register','spl'),		
				'lostpassword'	=> __('Lost password','spl'),
				'rp'			=> __('Reset Password','spl')
			)) as $action => $label ){
				$args['nodes'][]=array(
							'id' 		=> 'spl-closed-'.$action, 
							'title' 	=> $label, 
							'parent' 	=> 'spl-form-closed',
							'href'		=> $this->addURLParameter( $this->get_login_url( $action ), 'spl_edit', 'spl_closed_bg_'.$action ).'#spl_close',
							'meta'		=> array(
								'class'	=> 'not-spl'
							)							
						);	
			}	
			
			$args['nodes'][]=array(
						'id' 		=> 'spl-form-logout', 
						'title' 	=> __('Logout form','spl'), 
						'parent' 	=> 'spl',
						'href'		=> $this->addURLParameter( $this->get_login_url(), 'spl_edit', 'spl_logout' ).'#spl_logout',
						'meta'		=> array(
							'class'	=> 'not-spl'
						)		
					);									
		}
		//---- END closed form backgrounds-----
		$args['nodes'][]=array(
					'id' 		=> 'spl-maintanance-dlg', 
					'title' 	=> __('Maintanance','spl'), 
					'parent' 	=> 'spl',
					'href'		=> $this->addURLParameter( $this->get_login_url( 'maintanance' ), 'spl_edit', 'spl_maintanance' ),
					'meta'		=> array(
						'class'	=> 'not-spl'
					)	
				);			
		//--- countdown
		$args['nodes'][]=array(
					'id' 		=> 'spl-countdown', 
					'title' 	=> __('Countdown','spl'), 
					'parent' 	=> 'spl',
					'href'		=> '#'
				);			
		
		$args['nodes'][]=array(
					'id' 		=> 'spl-countdown-dlg', 
					'title' 	=> __('Dialog','spl'), 
					'parent' 	=> 'spl-countdown',
					'href'		=> $this->addURLParameter( $this->get_login_url( 'maintanance' ), 'spl_edit', 'spl_countdown_dlg' ),
					'meta'		=> array(
						'class'	=> 'not-spl'
					)	
				);		
		
		$args['nodes'][]=array(
					'id' 		=> 'spl-countdown-dial', 
					'title' 	=> __('Count down','spl'), 
					'parent' 	=> 'spl-countdown',
					'href'		=> $this->addURLParameter( $this->get_login_url( 'maintanance' ), 'spl_edit', 'spl_countdown' ),
					'meta'		=> array(
						'class'	=> 'not-spl'
					)	
				);			
		
		$args['nodes'][]=array(
					'id' 		=> 'spl-wp-login', 
					'title' 	=> __('Default WP Login','spl'), 
					'parent' 	=> 'spl',
					'href'		=> $this->addURLParameter( $this->get_login_url(), 'spl_edit', 'spl_wp_login' ),
					'meta'		=> array(
						'class'	=> 'not-spl'
					)		
				);				
		
		
		new admin_bar_editor_access($args);		
	}
	
	function get_login_url( $action='login' ){
		return $this->addURLParameter( wp_login_url(), 'action', $action );
	}
	
	
	function addURLParameter($url, $paramName, $paramValue) {
	     if(trim($url)=='')return '';
		 $url_data = parse_url($url);
	     if(!isset($url_data["query"])){
		 	$url_data["query"]="";
		 }
	     $params = array();
	     parse_str($url_data['query'], $params);
	     $params[$paramName] = $paramValue;
	     $url_data['query'] = http_build_query($params);
	     return $this->build_url($url_data);
	}

	function build_url($url_data) {
	    $url="";
	    if(isset($url_data['host']))
	    {
	        $url .= $url_data['scheme'] . '://';
	        if (isset($url_data['user'])) {
	            $url .= $url_data['user'];
	                if (isset($url_data['pass'])) {
	                    $url .= ':' . $url_data['pass'];
	                }
	            $url .= '@';
	        }
	        $url .= $url_data['host'];
	        if (isset($url_data['port'])) {
	            $url .= ':' . $url_data['port'];
	        }
	    }
	    $url .= @$url_data['path'];
	    if (isset($url_data['query'])) {
	        $url .= '?' . $url_data['query'];
	    }
	    if (isset($url_data['fragment'])) {
	        $url .= '#' . $url_data['fragment'];
	    }
	    return $url;
	}		
}

?>