<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/

class spl_top_message extends module_righthere_css{
	var $action='login';
	var $namespace = '';
	function spl_top_message($args=array()){
		//------------
		$defaults = array(
			'action'			=> 'lostpassword',
			'namespace'			=> '.ns-box.spl-notice-action-lostpassword'
		);
		foreach($defaults as $property => $default){
			$this->$property = isset($args[$property])?$args[$property]:$default;
		}
		//-----------	
	
		return $this->module_righthere_css($args);
	}
	
	function options($t=array()){
		$this->footer();
		//----
		$id = sprintf('spl-ns-%s', $this->action);	
		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Font','spl');
		$t[$i]->options = array();		

		$t[$i]->options = $this->add_font_options( $t[$i]->options, array(
			'prefix'	=> $id.'-font',
			'selector'	=> implode(',',array(
				$this->namespace.' .ns-notice'
			)),			
			'labels'	=> (object)array(
				'family'	=> __('Font','rhc'),
				'size'		=> __('Size','rhc'),
				'color'		=> __('Color','rhc')				
			)
		));		
		
		$t[$i]->options[] = (object)array(
			'input_type'  	=> 'raw_html',
			'html'			=> '<div style="height:145px;display:block;"></div>'
		);
//-- BG
		$id = sprintf('spl-bg-%s', $this->action);
		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Background','spl');
		$t[$i]->options = array();	

		$t[$i]->options = $this->add_backgroud_options( $t[$i]->options, array(
			'label'		=> __('Background','rhc'),
			'prefix'	=> $id.'-bg_',
			'selector'	=> implode(',',array(
				$this->namespace
			))
		));		
		
		
//-- BG2
		$id = sprintf('spl-lbox-%s', $this->action);
		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Left Box','spl');
		$t[$i]->options = array();		
		
		$t[$i]->options[] = (object)array(
				'id'				=> $id.'-bgcolor',
				'type'				=> 'css',
				'label'				=> __('Background color','rhc'),
				'input_type'		=> 'color_or_something_else',
				'holder_class'		=> '',
				'opacity'			=> true,
				'btn_clear'			=> true,
				'selector'	=> implode(',',array(
					$this->namespace
				)),
				'property'			=> 'border-color',			
				'real_time'			=> true,
				'derived'			=> array(
						array(
							'type'	=> 'same',
							'val'	=> '5',
							'sel'	=> $this->namespace,
							'arg'	=> array(
								(object)array(
									'name' => 'box-shadow',
									'tpl'	=>'inset 5.3em 0 __value__;'
								)
							)
						)
					)					
			);			

		$t[$i]->options[] = (object)array(
			'input_type'  	=> 'raw_html',
			'html'			=> '<div id="'.$id.'-color-helper'.'" ></div>'
		);				
			
		$t[$i]->options[] = (object)array(
				'id'				=> $id.'-color',
				'type'				=> 'css',
				'label'				=> __('Icon color','rhc'),
				'input_type'		=> 'color_or_something_else',
				'holder_class'		=> '',
				'opacity'			=> true,
				'btn_clear'			=> true,
				'selector'	=> implode(',',array(
					$this->namespace.' .ns-box-inner .icon:before',
					'#'.$id.'-color-helper'
				)),
				'property'			=> 'color',			
				'real_time'			=> true				
			);		
			
		$t[$i]->options[] = (object)array(
			'input_type'  	=> 'raw_html',
			'html'			=> '<div style="height:145px;display:block;"></div>'
		);						
//-- CLOSE
		$id = sprintf('spl-close-%s', $this->action);
		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Close Icon','spl');
		$t[$i]->options = array();	

		$t[$i]->options[] = (object)array(
			'input_type'  	=> 'raw_html',
			'html'			=> '<div id="'.$id.'-color-helper'.'" ></div>'
		);				
			
		$t[$i]->options[] = (object)array(
				'id'				=> $id.'-color',
				'type'				=> 'css',
				'label'				=> __('Icon color','rhc'),
				'input_type'		=> 'color_or_something_else',
				'holder_class'		=> '',
				'opacity'			=> true,
				'btn_clear'			=> true,
				'selector'	=> implode(',',array(
					$this->namespace.' .ns-close:before',
					'#'.$id.'-color-helper'
				)),
				'property'			=> 'color',			
				'real_time'			=> true				
			);	
			
		$t[$i]->options[] = (object)array(
			'input_type'  	=> 'raw_html',
			'html'			=> '<div style="height:145px;display:block;"></div>'
		);
		//-- Saved and DC  -----------------------		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'rh-saved-list'; 
		$t[$i]->label 		= __('Templates','spl');
		$t[$i]->options = array(
			(object)array(
				'id'				=> 'rh_saved_settings',
				'input_type'		=> 'backup_list'
			)			
		);			
//----------------------------------------------------------------------
		return $t;
	}
	
	function footer(){
?>
<script>
jQuery('BODY').addClass('spla-<?php echo $this->action ?>');
</script>
<?php 	
	}
}
?>