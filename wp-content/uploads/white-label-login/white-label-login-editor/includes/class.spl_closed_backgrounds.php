<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/

class spl_closed_backgrounds extends module_righthere_css{
	var $action='login';
	var $selector = '';
	var $queue = 'spl_backgrounds';
	function spl_closed_backgrounds($args=array()){
		$this->action = isset( $args['action'] ) ? $args['action'] : $this->action ;
		$this->selector = isset( $args['selector'] ) && ''!=$args['selector'] ? $args['selector'] : $this->selector ;
		$this->queue = isset( $args['queue'] ) ? $args['queue'] : $this->queue ;
		return $this->module_righthere_css($args);
	}
	
	function options($t=array()){
		$this->footer();
		//----
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'spl-closed-bg-'.$this->action; 
		$t[$i]->label 		= __('Background (on form close)','spl');
		$t[$i]->options = array();	
		
		$t[$i]->options = $this->add_backgroud_options( $t[$i]->options, array(
			'label'		=> __('Background','spl'),
			'prefix'	=> 'spl_closed_bg_'.$this->action,
			'selector'	=> ( empty($this->selector) ? sprintf('.spla-%s.spl-slide-login.wp-login', $this->action ) : $this->selector ) ,
			'queue'		=> $this->queue
		));		
		
			
		//-- Saved and DC  -----------------------		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'rh-saved-list'; 
		$t[$i]->label 		= __('Templates','spl');
		$t[$i]->options = array(
			(object)array(
				'id'				=> 'rh_saved_settings',
				'input_type'		=> 'backup_list'
			)			
		);			
//----------------------------------------------------------------------
		return $t;
	}
	
	function footer(){
?>
<script>
jQuery('BODY').addClass('spla-<?php echo $this->action ?>');
</script>
<?php 	
	}
}
?>