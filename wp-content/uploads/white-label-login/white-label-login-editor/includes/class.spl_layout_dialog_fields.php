<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/

class spl_layout_dialog_fields extends module_righthere_css{
	var $action='login';
	var $namespace = '';
	function spl_layout_dialog_fields($args=array()){
		//------------
		$defaults = array(
			'action'			=> 'login',
			'layout'			=> 'dialog_fields',
			'namespace'			=> '.splogin.spl-modal'
		);
		foreach($defaults as $property => $default){
			$this->$property = isset($args[$property])?$args[$property]:$default;
		}
		//-----------	
		if( isset( $_REQUEST[$args['trigger_var']] ) && $_REQUEST[$args['trigger_var']]==$args['trigger_val'] ){
			add_filter('enable_spl', array(&$this,'enable_spl'), 10, 1);
		}		
		//-----------
	
		return $this->module_righthere_css($args);
	}
	
	function enable_spl(){	
		return '1';
	}
	
	function options($t=array()){
		$this->footer();
		//----
//-- LABELS		
		$id = sprintf('spl-layout-%s-%s-labels', $this->layout, $this->action);						

		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Labels','spl');
		$t[$i]->options = array();		
		
		$t[$i]->options = $this->add_font_options( $t[$i]->options, array(
			'prefix'	=> $id.'-font',
			'selector'	=> implode(',',array(
				$this->namespace.' label'
			)),			
			'labels'	=> (object)array(
				'family'	=> __('Font','rhc'),
				'size'		=> __('Size','rhc'),
				'color'		=> __('Color','rhc')				
			)
		));	
		
		$t[$i]->options[] = (object)array(
			'input_type'  	=> 'raw_html',
			'html'			=> '<div style="height:145px;display:block;"></div>'
		);		
						
//-- INPUT		
		$id = sprintf('spl-layout-%s-%s-inp', $this->layout, $this->action);						

		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Input fields','spl');
		$t[$i]->options = array();		

		$t[$i]->options = $this->add_font_options( $t[$i]->options, array(
			'prefix'	=> $id.'-font',
			'selector'	=> implode(',',array(
				$this->namespace.' input.input',
				$this->namespace.' input.input:-webkit-autofill'
			)),			
			'labels'	=> (object)array(
				'family'	=> __('Font','rhc'),
				'size'		=> __('Size','rhc'),
				'color'		=> __('Color','rhc')				
			)
		));		

		$t[$i]->options[] = (object)array(
				'id'				=> $id.'-bgcolor',
				'type'				=> 'css',
				'label'				=> __('Background color','rhc'),
				'input_type'		=> 'color_or_something_else',
				'holder_class'		=> '',
				'opacity'			=> true,
				'btn_clear'			=> true,
				'selector'	=> implode(',',array(
					$this->namespace.' input.input'
				)),
				'property'			=> 'background-color',			
				'real_time'			=> true
			);	

		$t[$i]->options[] = (object)array(
				'id'				=> $id.'-outline-color',
				'type'				=> 'css',
				'label'				=> __('Outline color','rhc'),
				'input_type'		=> 'color_or_something_else',
				'holder_class'		=> '',
				'opacity'			=> true,
				'btn_clear'			=> true,
				'selector'	=> implode(',',array(
					$this->namespace.' input.input'
				)),
				'property'			=> 'outline-color',			
				'real_time'			=> true
			);				
		
		$t[$i]->options = $this->add_border_options($t[$i]->options,array(
			'prefix'	=> $id.'-border_',
			'selector'	=> implode(',',array(
				$this->namespace.' input.input'
			))
		));			

		$t[$i]->options = $this->add_border_radius_options($t[$i]->options,array(
			'prefix'	=> $id.'-rad_',
			'selector'	=> implode(',',array(
				$this->namespace.' input.input'
			))
		));					

		//-- REMEMBER ME ---------------
		$id = sprintf('spl-layout-%s-%s-chk', $this->layout, $this->action);
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Checkbox','spl');
		$t[$i]->options = array();

		$t[$i]->options[] = (object)array(
			'input_type'  	=> 'raw_html',
			'html'			=> '<div class="chk-helper-color-'.$id.'" style="display:none;"></div>'
		);			
		
		$t[$i]->options[] = (object)array(
				'id'				=> $id.'-bgcolor',
				'type'				=> 'css',
				'label'				=> __('Background color','rhc'),
				'input_type'		=> 'color_or_something_else',
				'opacity'			=> true,
				'btn_clear'			=> true,
				'selector'	=> implode(',',array(
					'.chk-helper-color-'.$id,
					$this->namespace.' .checkbox-4-2.rh-checkbox+label'
				)),
				'property'			=> 'background-color',		
				'other_options'		=> array(
					'transparent'	=> 'transparent'
				),					
				'real_time'			=> true
			);	
			
		$t[$i]->options[] = (object)array(
				'id'				=> $id.'-bord-color',
				'type'				=> 'css',
				'label'				=> __('Border color','rhc'),
				'input_type'		=> 'color_or_something_else',
				'opacity'			=> true,
				'btn_clear'			=> true,
				'selector'	=> implode(',',array(
					'.chk-helper-color-'.$id,
					$this->namespace.' .checkbox-4-2.rh-checkbox+label'
				)),
				'property'			=> 'border-color',		
				'other_options'		=> array(
					'transparent'	=> 'transparent'
				),					
				'real_time'			=> true
			);				

		$t[$i]->options[] = (object)array(
				'id'				=> $id.'-chk-color',
				'type'				=> 'css',
				'label'				=> __('Check color','rhc'),
				'input_type'		=> 'color_or_something_else',
				'opacity'			=> true,
				'btn_clear'			=> true,
				'selector'	=> implode(',',array(
					'.chk-helper-color-'.$id,
					$this->namespace.' .checkbox-4-2.rh-checkbox+label:after'
				)),
				'property'			=> 'color',		
				'other_options'		=> array(
					'transparent'	=> 'transparent'
				),					
				'real_time'			=> true
			);	

		$marker_options = array(
			''	=> __('Default','spl'),
		);	
		foreach( array(
			'\2713',
			'\2714',
			'\2715',
			'\2716',
			'\2717',
			'\2718',
			'\2730',
			'\2731'
		) as $j => $value ){
			$marker_options[$value] = sprintf( __('Option %s','spl'), ($j+1) );
		}
		
		
		$t[$i]->options[] = (object)array(
				'id'				=> $id.'-chk-icon',
				'type'				=> 'css',
				'label'				=> __('Marker','rhcss'),
				'input_type'		=> 'select',			
				'selector'	=> implode(',',array(
					'.chk-helper-color-'.$id,
					$this->namespace.' .checkbox-4-2.rh-checkbox+label:after'
				)),
				'class'				=> 'css-icon-font',
				'options'			=> apply_filters('spl_marker_options', $marker_options),
				'property'			=> 'content',
				'real_time'			=> true
			);
			
	

		$t[$i]->options = $this->add_font_options( $t[$i]->options, array(
			'prefix'	=> $id.'-font',
			'selector'	=> implode(',',array(
				$this->namespace.' .splogin-rememberme label'
			)),			
			'labels'	=> (object)array(
				'family'	=> __('Font','rhc'),
				'size'		=> __('Size','rhc'),
				'color'		=> __('Color','rhc')				
			)
		));	
		
		$t[$i]->options[] = (object)array(
			'input_type'  	=> 'raw_html',
			'html'			=> '<div style="height:145px;display:block;"></div>'
		);				
//-- BUTTONS	
		$id = sprintf('spl-layout-%s-%s-btn-def', $this->layout, $this->action);
		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Button','spl');
		$t[$i]->options = array();

		$t[$i]->options = $this->add_font_options( $t[$i]->options, array(
			'prefix'	=> $id.'-font',
			'selector'	=> implode(',',array(
				$this->namespace.' .btn-splogin label'
			)),			
			'labels'	=> (object)array(
				'family'	=> __('Font','rhc'),
				'size'		=> __('Size','rhc'),
				'color'		=> __('Color','rhc')				
			)
		));				

		$t[$i]->options = $this->add_backgroud_options( $t[$i]->options, array(
			'label'		=> __('Background','rhc'),
			'prefix'	=> $id.'-bg_',
			'selector'	=> implode(',',array(
				$this->namespace.' .btn-splogin'
			))
		));		
		
		$t[$i]->options = $this->add_border_options($t[$i]->options,array(
			'prefix'	=> $id.'-border_',
			'selector'	=> implode(',',array(
				$this->namespace.' .btn-splogin'
			))
		));			

		$t[$i]->options = $this->add_border_radius_options($t[$i]->options,array(
			'prefix'	=> $id.'-rad_',
			'selector'	=> implode(',',array(
				$this->namespace.' .btn-splogin'
			))
		));	
//-- BUTTONS HOVER			
		$id = sprintf('spl-layout-%s-%s-btnh-def', $this->layout, $this->action);	
		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Button Hover','spl');
		$t[$i]->options = array();	
		
		$t[$i]->options = $this->add_font_options( $t[$i]->options, array(
			'prefix'	=> $id.'-font',
			'selector'	=> implode(',',array(
				$this->namespace.' .btn-splogin label:hover',
				$this->namespace.' .btn-splogin label:focus',
				$this->namespace.' .btn-splogin label:active',
				$this->namespace.' .btn-splogin label.active'
			)),			
			'labels'	=> (object)array(
				'family'	=> __('Font','rhc'),
				'size'		=> __('Size','rhc'),
				'color'		=> __('Color','rhc')				
			)
		));		
		
		$t[$i]->options = $this->add_backgroud_options( $t[$i]->options, array(
			'label'		=> __('Background','rhc'),
			'prefix'	=> $id.'-bg_',
			'selector'	=> implode(',',array(
				$this->namespace.' .btn-splogin:hover',
				$this->namespace.' .btn-splogin:focus',
				$this->namespace.' .btn-splogin:active',
				$this->namespace.' .btn-splogin.active'
			))
		));		
		
		$t[$i]->options = $this->add_border_options($t[$i]->options,array(
			'prefix'	=> $id.'-border_',
			'selector'	=> implode(',',array(
				$this->namespace.' .btn-splogin:hover',
				$this->namespace.' .btn-splogin:focus',
				$this->namespace.' .btn-splogin:active',
				$this->namespace.' .btn-splogin.active'
			))
		));			

		$t[$i]->options = $this->add_border_radius_options($t[$i]->options,array(
			'prefix'	=> $id.'-rad_',
			'selector'	=> implode(',',array(
				$this->namespace.' .btn-splogin:hover',
				$this->namespace.' .btn-splogin:focus',
				$this->namespace.' .btn-splogin:active',
				$this->namespace.' .btn-splogin.active'
			))
		));											
			
		//-- Saved and DC  -----------------------		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'rh-saved-list'; 
		$t[$i]->label 		= __('Templates','spl');
		$t[$i]->options = array(
			(object)array(
				'id'				=> 'rh_saved_settings',
				'input_type'		=> 'backup_list'
			)			
		);			
//----------------------------------------------------------------------
		return $t;
	}
	
	function footer(){
?>
<script>
jQuery('BODY').addClass('spla-<?php echo $this->action ?>');
</script>
<?php 	
	}
}
?>