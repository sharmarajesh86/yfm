<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/

class spl_layout_horizontal extends module_righthere_css{
	var $action='login';
	var $namespace = '';
	function spl_layout_horizontal($args=array()){
		//------------
		$defaults = array(
			'action'			=> 'login',
			'layout'			=> 'horizontal',
			'namespace'			=> '.splogin.splogin-horizontal'
		);
		foreach($defaults as $property => $default){
			$this->$property = isset($args[$property])?$args[$property]:$default;
		}
		//-----------	
		if( isset( $_REQUEST[$args['trigger_var']] ) && $_REQUEST[$args['trigger_var']]==$args['trigger_val'] ){
			add_filter('enable_spl', array(&$this,'enable_spl'), 10, 1);
		}		
		//-----------
	
		return $this->module_righthere_css($args);
	}
	
	function enable_spl(){	
		return '1';
	}
	
	function options($t=array()){
		$this->footer();
		//----
//-- Dialog		
		$id = sprintf('spl-layout-%s-%s-dlg', $this->layout, $this->action);		

		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Dialog','spl');
		$t[$i]->options = array();	
				
		$t[$i]->options[] =(object)array(
				'id'				=> $id.'height',
				'type'				=> 'css',
				'label'				=> __('Height','rhc'),
				'input_type'		=> 'number',
				'unit'				=> 'px',
				'class'				=> 'input-mini',
				'min'				=> '100',
				'max'				=> '750',
				'step'				=> '1',
				'selector'	=> implode(',',array(
					$this->namespace,
					$this->namespace.' .spl-content'
				)),	
				'property'			=> 'height',
				'real_time'			=> true
			);		
			
		$t[$i]->options = $this->add_padding_options($t[$i]->options,array(
			'prefix'	=> $id.'_pad',
			'selector'	=> implode(',',array(
				$this->namespace.' .splogin-body'
			))
		));	

//-- HEADER ----
		$id = sprintf('spl-layout-%s-%s-header', $this->layout, $this->action);
		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Header','spl');
		$t[$i]->options = array();		
			
		$t[$i]->options = $this->add_font_options( $t[$i]->options, array(
			'prefix'	=> $id.'-font_',
			'selector'	=> implode(',',array(
				$this->namespace.' h3.action-section'
			)),			
			'labels'	=> (object)array(
				'family'	=> __('Font','rhc'),
				'size'		=> __('Size','rhc'),
				'color'		=> __('Color','rhc')				
			)
		));			
			
		$t[$i]->options = $this->add_backgroud_options( $t[$i]->options, array(
			'label'		=> __('Background','rhc'),
			'prefix'	=> $id.'-bg_',
			'selector'	=> implode(',',array(
				$this->namespace.' h3.action-section'
			))
		));		
		
		$t[$i]->options = $this->add_padding_options($t[$i]->options,array(
			'prefix'	=> $id.'_pad',
			'selector'	=> implode(',',array(
				$this->namespace.' h3.action-section'
			))
		));			
//-- BODY		
		$id = sprintf('spl-layout-%s-%s-body', $this->layout, $this->action);
		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Body','spl');
		$t[$i]->options = array();		
		
		$t[$i]->options = $this->add_backgroud_options( $t[$i]->options, array(
			'label'		=> __('Background','rhc'),
			'prefix'	=> $id.'-bg_',
			'selector'	=> implode(',',array(
				$this->namespace.' .spl-content'
			))
		));	
												
//-- LINKS
		$id = sprintf('spl-layout-%s-%s-links', $this->layout, $this->action);
		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Links','spl');
		$t[$i]->options = array(
			/*
			(object)array(//This is needed because we are unable to read the css value from :after and :before
				'input_type' => 'raw_html',
				'html'=> '<div id="'.$id.'-border-top_helper'.'" ></div>'
			)				
			*/
		);				

		$t[$i]->options = $this->add_font_options( $t[$i]->options, array(
			'prefix'	=> $id.'-font',
			'selector'	=> implode(',',array(
				$this->namespace.' .spl_custom_links > a'
			)),			
			'labels'	=> (object)array(
				'family'	=> __('Font','rhc'),
				'size'		=> __('Size','rhc'),
				'color'		=> __('Color','rhc')				
			)
		));		
		
		$t[$i]->options = $this->add_backgroud_options( $t[$i]->options, array(
			'label'		=> __('Background','rhc'),
			'prefix'	=> $id.'-bg_',
			'selector'	=> implode(',',array(
				$this->namespace.' .spl_custom_links > a'
			))
		));				

		$t[$i]->options = $this->add_border_options($t[$i]->options,array(
			'prefix'	=> $id.'-border_',
			'label'			=> array(
				'color'	=> __('Border color','rhc'),
				'style' => __('Border style','rhc'),
				'size'	=> __('Width','rhc')
			),			
			'selector'	=> implode(',',array(
				$this->namespace.' .spl_custom_links > a'
			))
		));	

		$t[$i]->options[] = (object)array(
			'input_type'  	=> 'raw_html',
			'html'			=> '<div style="height:85px;display:block;"></div>'
		);		
//--- LINKS HOVER		
		$id = sprintf('spl-layout-%s-%s-linksh', $this->layout, $this->action);
		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= $id; 
		$t[$i]->label 		= __('Links Hover','spl');
		$t[$i]->options = array(
			(object)array(//This is needed because we are unable to read the css value from :after and :before
				'input_type' => 'raw_html',
				'html'=> '<div id="'.$id.'-hover-top_helper'.'" ></div>'
			)				
		);		
		
		$t[$i]->options = $this->add_font_options( $t[$i]->options, array(
			'prefix'	=> $id.'-fonth',
			'selector'	=> implode(',',array(
				$this->namespace.' .spl_custom_links > a:hover',
				'#'.$id.'-hover-top_helper'
			)),			
			'labels'	=> (object)array(
				'family'	=> __('Font','rhc'),
				'size'		=> __('Size','rhc'),
				'color'		=> __('Color','rhc')				
			)
		));		
		
		$t[$i]->options = $this->add_backgroud_options( $t[$i]->options, array(
			'label'		=> __('Background','rhc'),
			'prefix'	=> $id.'-bg-h_',
			'selector'	=> implode(',',array(
				$this->namespace.' .spl_custom_links > a:hover',
				'#'.$id.'-hover-top_helper'
			))
		));		
		
		$t[$i]->options = $this->add_border_options($t[$i]->options,array(
			'prefix'	=> $id.'-border-h_',
			'label'			=> array(
				'color'	=> __('Border color','rhc'),
				'style' => __('Border style','rhc'),
				'size'	=> __('Width','rhc')
			),			
			'selector'	=> implode(',',array(
				$this->namespace.' .spl_custom_links > a:hover'
			))
		));			

		$t[$i]->options[] = (object)array(
			'input_type'  	=> 'raw_html',
			'html'			=> '<div style="height:85px;display:block;"></div>'
		);					
		//-- Saved and DC  -----------------------		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'rh-saved-list'; 
		$t[$i]->label 		= __('Templates','spl');
		$t[$i]->options = array(
			(object)array(
				'id'				=> 'rh_saved_settings',
				'input_type'		=> 'backup_list'
			)			
		);			
//----------------------------------------------------------------------
		return $t;
	}
	
	function footer(){
?>
<script>
jQuery('BODY').addClass('spla-<?php echo $this->action ?>');
</script>
<?php 	
	}
}
?>