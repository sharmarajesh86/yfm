<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/

class spl_editor_backgrounds extends module_righthere_css{
	var $action='login';
	var $queue = 'spl_backgrounds';
	function spl_editor_backgrounds($args=array()){
		$this->action = isset( $args['action'] ) ? $args['action'] : $this->action ;
		$this->queue = isset( $args['queue'] ) ? $args['queue'] : $this->queue ;
		return $this->module_righthere_css($args);
	}
	
	function options($t=array()){
		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'spl-bg3-'.$this->action; 
		$t[$i]->label 		= __('Top Layer','spl');
		$t[$i]->options = array();	
		$t[$i]->options[]=	(object)array(
				'id'				=> 'spl_bg_layer3_opacity_'.$this->action,
				'type'				=> 'css',
				'label'				=> __('Opacity','spl'),
				'input_type'		=> 'number',
				//'unit'				=> '',
				//'class'				=> 'input-font-size',
				'holder_class'		=> '',
				'class'				=> 'input-mini',
				'min'				=> '0',
				'max'				=> '1',
				'step'				=> '0.1',
				'selector'			=> '.spl-open.spl-action-'.$this->action.' .spl-bg-layer3',
				'property'			=> 'opacity',
				'real_time'			=> true
			);			
		$t[$i]->options = $this->add_backgroud_options( $t[$i]->options, array(
			'label'		=> __('Background','spl'),
			'prefix'	=> 'spl_bg_layer3_'.$this->action,
			'selector'	=> '.spl-action-'.$this->action.' .spl-bg-layer3',
			'queue'		=> $this->queue
		));		
		
		//-----
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'spl-bg2-'.$this->action; 
		$t[$i]->label 		= __('Middle Layer','spl');
		$t[$i]->options = array();	
		$t[$i]->options[]=	(object)array(
				'id'				=> 'spl_bg_layer2_opacity_'.$this->action,
				'type'				=> 'css',
				'label'				=> __('Opacity','spl'),
				'input_type'		=> 'number',
				//'unit'				=> '',
				//'class'				=> 'input-font-size',
				'holder_class'		=> '',
				'class'				=> 'input-mini',
				'min'				=> '0',
				'max'				=> '1',
				'step'				=> '0.1',
				'selector'			=> '.spl-open.spl-action-'.$this->action.' .spl-bg-layer2',
				'property'			=> 'opacity',
				'real_time'			=> true
			);			
		$t[$i]->options = $this->add_backgroud_options( $t[$i]->options, array(
			'label'		=> __('Background','spl'),
			'prefix'	=> 'spl_bg_layer2_'.$this->action,
			'selector'	=> '.spl-action-'.$this->action.' .spl-bg-layer2',
			'queue'		=> $this->queue	
		));			
		
		//-- Background
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'spl-bg1-'.$this->action; 
		$t[$i]->label 		= __('Bottom Layer','spl');
		$t[$i]->options = array();	
		$t[$i]->options[]=	(object)array(
				'id'				=> 'spl_bg_layer1_opacity_'.$this->action,
				'type'				=> 'css',
				'label'				=> __('Opacity','spl'),
				'input_type'		=> 'number',
				//'unit'				=> '',
				//'class'				=> 'input-font-size',
				'holder_class'		=> '',
				'class'				=> 'input-mini',
				'min'				=> '0',
				'max'				=> '1',
				'step'				=> '0.1',
				'selector'			=> '.spl-open.spl-action-'.$this->action.' .spl-bg-layer1',
				'property'			=> 'opacity',
				'real_time'			=> true
			);	
		$t[$i]->options = $this->add_backgroud_options( $t[$i]->options, array(
			'label'		=> __('Background','spl'),
			'prefix'	=> 'spl_bg_layer1_'.$this->action,
			'selector'	=> '.spl-action-'.$this->action.' .spl-bg-layer1',
			'queue'		=> $this->queue	
		));	
					
		//-- Saved and DC  -----------------------		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'rh-saved-list'; 
		$t[$i]->label 		= __('Templates','spl');
		$t[$i]->options = array(
			(object)array(
				'id'				=> 'rh_saved_settings',
				'input_type'		=> 'backup_list'
			)			
		);			
//----------------------------------------------------------------------
		return $t;
	}
}
?>