<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/

class spl_form_logout extends module_righthere_css{
	function spl_form_logout($args=array()){
		return $this->module_righthere_css($args);
	}
	
	function options($t=array()){
		$this->footer();
		//----
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'spl-form-logout-general'; 
		$t[$i]->label 		= __('Display','spl');
		$t[$i]->options = array();			

		$t[$i]->options[] =(object)array(
				'id'				=> 'spl-logout-display',
				'type'				=> 'css',
				'label'				=> __('Display','spl'),
				'input_type'		=> 'select',
				'options'	=> array(
					''		=> __('--choose--','rhcss'),
					'block'	=> __('Block','rhcss'),
					'none'	=> __('None (hide logout form)','rhcss')
				),
				'selector'			=> implode(',',array(
					"[data-action='logout']"
				)),
				'property'			=> 'display',
				'real_time'			=> true
			);		
		//----
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'spl-form-logout'; 
		$t[$i]->label 		= __('Spinner','spl');
		$t[$i]->options = array();	
		
		$t[$i]->options[] =(object)array(
			'input_type'=>'raw_html',
			'html' => implode('',array(
					'<div class="spl_logout_spinner"></div>'						
				)
			)
		);
		
		$t[$i]->options[] =(object)array(
				'id'				=> 'spl-logout-spinner',
				'type'				=> 'css',
				'label'				=> __('Spinner','spl'),
				'input_type'		=> 'select',
/*
				'options'	=> array(
					"'\\e000'"	=> __('Spinner 1','rhcss'),
					"'\\e001'"	=> __('Spinner 2','rhcss'),
					"'\\e002'"	=> __('Spinner 3','rhcss'),
					"'\\e003'"	=> __('Spinner 4','rhcss')
				),
*/
				'options'	=> array(
					'\e000'	=> __('Spinner 1','rhcss'),
					'\e001'	=> __('Spinner 2','rhcss'),
					'\e002'	=> __('Spinner 3','rhcss'),
					'\e003'	=> __('Spinner 4','rhcss'),
					'\e004'	=> __('Spinner 5','rhcss'),
					'\e005'	=> __('Spinner 6','rhcss'),
					'\e006'	=> __('Spinner 7','rhcss'),
					'\e007'	=> __('Spinner 8','rhcss'),
					'\e008'	=> __('Spinner 9','rhcss'),
					'\e009'	=> __('Spinner 10','rhcss'),
					'\e010'	=> __('Spinner 11','rhcss'),
					'\e011'	=> __('Spinner 12','rhcss'),
					'\e012'	=> __('Spinner 13','rhcss'),
					'\e013'	=> __('Spinner 14','rhcss'),
					'\e014'	=> __('Spinner 15','rhcss'),
					'\e015'	=> __('Spinner 16','rhcss'),
					'\e016'	=> __('Spinner 17','rhcss'),
					'\e017'	=> __('Spinner 18','rhcss')
				),


				'class'				=> 'css-icon-font',
				'holder_class'		=> '',
				//'class'				=> 'input-mini pop_rangeinput',
				'selector'			=> implode(',',array(
					'.spl_logout_spinner',
					'body .splogin .xspinner:before'
				)),
				'property'			=> 'content',
				'real_time'			=> true
			);
			
		$t[$i]->options[] = (object)array(
				'id'				=> 'spl-logout-spinner-color',
				'type'				=> 'css',
				'label'				=> __('Spinner color','rhc'),
				'input_type'		=> 'colorpicker',
				'holder_class'		=> '',
				'opacity'			=> true,
				'btn_clear'			=> true,
				'selector'			=> implode(',',array(
					'.spl_logout_spinner',
					'body .splogin .xspinner:before'
				)),
				'property'			=> 'color',
				'real_time'			=> true
			);
//-----------------------

		//-- Saved and DC  -----------------------		
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'rh-saved-list'; 
		$t[$i]->label 		= __('Templates','spl');
		$t[$i]->options = array(
			(object)array(
				'id'				=> 'rh_saved_settings',
				'input_type'		=> 'backup_list'
			)			
		);			
//----------------------------------------------------------------------
		return $t;
	}
	
	function footer(){

	}
}
?>