<?php


add_action('set_current_user', 'csstricks_hide_admin_bar');
function csstricks_hide_admin_bar() {
  if (!current_user_can('edit_posts')) {
    show_admin_bar(false);
  }
}


function chk_active_user($user,$username) {
	$user_data = $user->data;
    $user_id = $user_data->ID;
    $user_email = $user_data->user_email;

	$userStatus = get_user_meta($user_id, 'user_disabled', true);
	$active_acc = get_user_meta($user_id, 'account_inactive', true);

    // for testing $userStatus = 1;
    $login_page  = home_url('/login/');

    global $wpdb;
	$result = $wpdb->get_results("SELECT * FROM yfm_wp_AT_contacts WHERE Email='$user_email'");
	if(!empty($user_id)){
		if(!empty($userStatus)){
		    if($userStatus == 0){
		        //return new WP_Error( 'disabled_account','Your account is no longer active, Please contact the administrator.');
		        //exit;
		    }
		}
	    if( $result[0]->Position == 'New User' || $result[0]->Position == ''){
	    	//return new WP_Error( 'disabled_account','Account pending please contact administrator.');
	    }else{
	       return $user;
	    }

	}
	return $user;

    //return $status;
}
add_filter( 'authenticate', 'chk_active_user',100,2);

function admin_default_page() {
  return site_url().'/dashboard';
 }
add_filter('login_redirect', 'admin_default_page');
