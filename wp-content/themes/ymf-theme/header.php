<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/custom-style.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/custom-style-responsive.css" rel="stylesheet" type="text/css" />

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/fontawesome.css" rel="stylesheet">
	<?php wp_head(); ?>
	<style>
<?php
if ( is_user_logged_in() ) { ?>

    #menu-item-100, #menu-item-101{display:none;}
<?php } else {?>
    #menu-item-120,#menu-item-130, #menu-item-124,#menu-item-142,#menu-item-147,#menu-item-154{display:none;}
<?php } ?>
</style>
</head>

<body <?php body_class(); ?>>
	<header>
        <div class="top-bar">
            <div class="container">
                <div class="top-navigation-left pull-left">
                    <div class="top-social-wrapper">
                        <div class="social-icon">
                            <a href="mailto:tom@yourfleetmedia.com" target="_blank">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/email.png" alt="Email" width="32" height="32">
                            </a>
                            <a href="https://www.linkedin.com/company/your-fleet-media" target="_blank">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/linkedin.png" alt="Linkedin" width="32" height="32">
                            </a>
                        </div>
                        <div class="clear"></div>
                  </div>
                </div>
            	<?php
									wp_nav_menu( array(
										'theme_location' => 'primary',
										'menu_class'     => 'list-inline',
									 ) );
								?>
			</div>
        </div>
        <div class="main-header">
            <div class="container">
                <div class="logo-sec">									
                    <a href="<?php echo home_url('/');?>"><img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/small-logo.png" alt="yfm-logo" /></a>
                </div>
            </div>
        </div>
    </header>
