<?php /*
* Template Name: Home
*/
get_header();?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- <link rel="stylesheet" href="/styles.css">
      <link rel="stylesheet" href="/syntax.css">
      <link rel="stylesheet" href="/github-markdown.css"> -->
      
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<!--       <script type="text/javascript" src="toc.js"></script> -->
      <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri();?>/css/sol.css">
      <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/sol.js"></script>
      <title>1wayit</title>
   </head>
   <body>
      <div class="wrapper">
         <aside>
            <div class="box">
               <header>Table of contents</header>
               <div id="toc"></div>
            </div>
         </aside>
         <div id="main-content-wrapper">
            <div id="main-content" class="markdown-body">

                  <select id="demonstration" name="character0" style="width: 250px" multiple="multiple">
                                       <option value="Evil Monkey">Evil Monkey</option>
                     <option value="Herbert">John Herbert</option>
                     <option value="Adam">Adam West</option>
                     <option value="Buzz Killington">Buzz Killington</option>
                     <option value="Tricia">Tricia Takanawa</option>
                     <option value="Tom">Tom Tucker</option>
                     <option value="Jake">Jake Tucker</option>
                     <option value="Diane">Diane Simmons</option>
                     <option value="Ollie">Ollie Williams</option>
                     <option value="Dr Hartmann">Dr. Elmer Hartmann</option>
                     <option value="Barbara">Barbara Pewterschmidt</option>
                     <option value="Carter">Carter Pewterschmidt</option>
                     <option value="Neil">Neil Goldmann</option>
                     <option value="Mort">Mort Goldmann</option>
                     <option value="Muriel">Muriel Goldmann</option>
                     <option value="Consuela">Consuela</option>
                     <option value="Jillian">Jillian Russel</option>
                     <option value="Phineas and Barnaby">Phineas and Barnaby</option>
                     <option value="Vern and Johnny">Vern and Johnny</option>
                     <option value="Ernie">Ernie The Giant Chicken</option>
                     <option value="Bruce">Bruce</option>
                     <option value="Jeffrey">Jeffrey</option>
                     <option value="Carl">Carl</option>
                     <br>
                  </select>
            
             
               <script type="text/javascript">
                    $('#demonstration').searchableOptionList({ maxHeight: '300px', showSelectAll: true });
                 
               </script>
            </div>
         </div>
           </div>
      <script type="text/javascript">
         /*$(function() {
             $('#toc').toc({
                 'selectors': 'h2,h3,h4',
                 'container': '#main-content',
                 'smoothScrolling': true
             });
         });
*/      </script>
   </body>
</html>
<?php get_footer();?>