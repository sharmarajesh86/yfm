<?php /*
* Template Name: Register
*/
get_header();
?>
<?php // Get company details from airtable
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "https://api.airtable.com/v0/apprTuWAjlI7FBBPb/Companies");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

$headers = array();
$headers[] = "Authorization: Bearer keyoA5KNgdkmd3iDG";
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}

$result = curl_exec($ch);

$data =json_decode($result);

curl_close ($ch);


// Get locations details from airtable
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "https://api.airtable.com/v0/apprTuWAjlI7FBBPb/Locations?maxRecords=200");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

$headers = array();
$headers[] = "Authorization: Bearer keyoA5KNgdkmd3iDG";
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}

$Locations = curl_exec($ch);

$Location =json_decode($Locations);


curl_close ($ch);
?>
<?php if(!is_user_logged_in()){ ?>

<h1>New User</h1>

<form name="registerform" id="registerform" method="post">
    
    <p>
        <label for="user_email">E-mail Login</label>
        <input type="email" name="user_email" id="user_email" value="" data-validation="email" >
    </p>
    <p>
        <label for="Credentials">Credentials xxxx</label>
        <input type="password" name="credentials" id="credentials" value="" data-validation="length alphanumeric" data-validation-length="min4">
    </p>
    <p>
        <label for="full_name">Full Name</label>
        <input type="text" name="full_name" value="" >
    </p>
     <p>
        <label for="company">Company</label>
        <select name="company" id="company">
        	<option></option>
        	<option value="other">Other</option>
        	<?php
			for($i=0; $i<count($data->records)-1;$i++) {
				echo '<option value="'.$data->records[$i]->fields->CompanyCode.'" data-id="'.$data->records[$i]->id.'">'.$data->records[$i]->fields->CompanyName.'</option>';
			}			
			?>
			
        </select>


  
    </p>
    <div class="other-company" style="display: none">
    	<p>
	        <label for="street_line">Street Address</label>
	        <input type="text" name="street_line" value="" >
    	</p>

    	<p>
	        <label for="street_line_2">Street Address Line 2</label>
	        <input type="text" name="street_line_2" value="" >
    	</p>

    	<p>
	        <label for="suburb">Suburb</label>
	        <input type="text" name="suburb" value="" >
    	</p>

    	<p>
	        <label for="state">State</label>
	        <input type="text" name="state" value="" >
    	</p>

    	<p>
	        <label for="post_code">Post Code</label>
	        <input type="text" name="post_code" value="" >
    	</p>

    </div>
    <p>
        <label for="location">Location</label>
        <select name="location">
        	<option></option>
        	<?php
			for($i=0; $i<count($Location->records)-1;$i++) {
				if(!empty($Location->records[$i]->fields->ID1))
				{
					echo '<option value="'.$Location->records[$i]->fields->ID1.'">'.$Location->records[$i]->fields->ID1.'</option>';
				}
				
			}			
			?>
			
        </select>
    </p>
    <p>
        <label for="phone">Phone</label>
        <input type="tel" name="phone" value="">
    </p>
    <p style="display:none">
        <label for="confirm_email">Please leave this field empty</label>
        <input type="text" name="confirm_email" id="confirm_email" value="">
    </p>

  
   
    <p class="submit"><input type="submit" name="wp-submit" id="wp-submit" value="Register" /></p>
</form>
<?php } ?>
 <script src="<?php echo get_stylesheet_directory_uri();?>/js/custom1.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
  $.validate({
    lang: 'en'
  });
</script> 
<?php get_footer();?>


