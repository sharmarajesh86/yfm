<?php // Start a session for the oauth session storage
@ob_start();
session_start(); /*
* Template Name: Auth
*/
    get_header();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


include("vendor/autoload.php");

//include('../src/XeroPHP/Application/PartnerApplication.php');
use XeroPHP\Application\PublicApplication;
use XeroPHP\Remote\Request;
use XeroPHP\Remote\URL;



//These are the minimum settings - for more options, refer to examples/config.php
$config = [
    'oauth' => [
        'callback'    => 'localhost', 
        'consumer_key'    => 'SGBKNCWCEGQLWNLCDRO54IN90KFODB',
        'consumer_secret' => '5PVY28J0PCXD9ZRYCR9ZSJUI6FJCMP',
    ],
    'curl' => [
        CURLOPT_CAINFO => __DIR__.'/certs/ca-bundle.crt',
    ],
];

$xero = new PublicApplication($config);
echo '<pre>';
//if no session or if it is expired
if (null === $oauth_session = getOAuthSession()) {
    $url = new URL($xero, URL::OAUTH_REQUEST_TOKEN);
    $request = new Request($xero, $url);

    //Here's where you'll see if your keys are valid.
    //You can catch a BadRequestException.
    try {
        $request->send();
    } catch (Exception $e) {
        print_r($e);
        if ($request->getResponse()) {

            print_r($request->getResponse()->getOAuthResponse());
            print_r($error_code = $oauth_response['oauth_problem']);
            print_r($error_reason = $oauth_response['oauth_problem_advice']);
        }
    }

    $oauth_response = $request->getResponse()->getOAuthResponse();

    setOAuthSession(
        $oauth_response['oauth_token'],
        $oauth_response['oauth_token_secret']
    );

    printf(
        '<a href="%s" target="_blank"><img src="'.get_stylesheet_directory_uri().'/connect_xero_button_blue_2x.png" alt="connect"></a>',
        $xero->getAuthorizeURL($oauth_response['oauth_token'])
    );
    exit;
} else {
    $xero->getOAuthClient()
        ->setToken($oauth_session['token'])
        ->setTokenSecret($oauth_session['token_secret']);

    if (isset($_REQUEST['oauth_verifier'])) {
        $xero->getOAuthClient()->setVerifier($_REQUEST['oauth_verifier']);

        $url = new URL($xero, URL::OAUTH_ACCESS_TOKEN);
        $request = new Request($xero, $url);

        $request->send();
        $oauth_response = $request->getResponse()->getOAuthResponse();

        setOAuthSession(
            $oauth_response['oauth_token'],
            $oauth_response['oauth_token_secret'],
            $oauth_response['oauth_expires_in']
        );

        //drop the qs
        $uri_parts = explode('?', $_SERVER['REQUEST_URI']);

        //Just for demo purposes
        header(
            sprintf(
                'Location: http%s://%s%s',
                (isset($_SERVER['HTTPS']) ? 's' : ''),
                $_SERVER['HTTP_HOST'],
                $uri_parts[0]
            )
        );
        exit;
    }
}

//Otherwise, you're in.
print_r($xero->load('Accounting\\Organisation')->execute());

//The following two functions are just for a demo
//you should use a more robust mechanism of storing tokens than this!
function setOAuthSession($token, $secret, $expires = null)
{
    // expires sends back an int
    if ($expires !== null) {
        $expires = time() + intval($expires);
    }

    $_SESSION['oauth'] = [
        'token' => $token,
        'token_secret' => $secret,
        'expires' => $expires
    ];
}

function getOAuthSession()
{
    //If it doesn't exist or is expired, return null
    if (!isset($_SESSION['oauth'])
        || ($_SESSION['oauth']['expires'] !== null
        && $_SESSION['oauth']['expires'] <= time())
    ) {
        return null;
    }
    return $_SESSION['oauth'];
}

 get_footer(); ?>