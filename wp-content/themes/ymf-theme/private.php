<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


include("../vendor/autoload.php");

use XeroPHP\Application\PrivateApplication;

//These are the minimum settings - for more options, refer to examples/config.php
$config = [
    'oauth' => [
        'callback'    => 'http://localhost/', 
        'consumer_key'    => 'QALU6AWYVA3B2RTQFYFXXHFEITG1IC',
        'consumer_secret' => 'VWXPSZSYVZIYLDXAJPT5JAMIVDDYUL',
        'rsa_private_key'  => 'file://certs/private.pem',
    ],
];

$xero = new PrivateApplication($config);

print_r($xero->load('Accounting\\Organisation')->execute());
