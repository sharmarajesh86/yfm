/**
 *  This file is used to initialize the editor button above the editor. 
 */
var securityKey = aiButtonSettings.securityKey;  
var additionalSettings = aiButtonSettings.additionalSettings;
 
jQuery(document).ready(function(){
	 jQuery("#insert-iframe-button").click(function() {
	 if (securityKey !== '') {
	   send_to_editor("[advanced_iframe securitykey=\"" + securityKey +  "\""  + additionalSettings + "]");
	 } else {
	   send_to_editor("[advanced_iframe" + additionalSettings + "]");
	 }
	 return false;
	 });
});
