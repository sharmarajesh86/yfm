<?php ob_start();
/**************************************************************************************
*
* This file consists of the code to create the shortcode that is used in this plugin to create the dasboard page.
* It will further links to other forms after user logged in.
*
*********************************************************************************************/
/********* Create Dashboard Shortcode********/
if (!session_id()) {
    session_start();
}

function dashboard_creation(){
	require_once(ABSPATH.'/wp-config.php');
	require_once(ABSPATH.'/wp-load.php');
	require_once(ABSPATH.'/wp-includes/wp-db.php');
	require_once( ABSPATH.'wp-admin/includes/user.php' );
	global $wpdb;
	$userid= get_current_user_id();
	$user = get_user_by('id',$userid);
	$queryUsers = "SELECT * FROM yfm_wp_AT_contacts where Contact_id= '".$user->data->airtable_id."'";
	$contact = $wpdb->get_results($queryUsers, OBJECT);
	if(!empty($contact)){
		$position = strtolower(str_replace(' ','',$contact[0]->Position));





 


		/*if(strcmp($position,'newuser')==0){
			 wp_logout();
			 wp_redirect( site_url().'?acc=inprogress' );

		} else{
      update_user_meta( $userid, 'account_inactive', ' ');
    }*/
	}

  /*if('yes' === get_user_meta( get_current_user_id(), sanitize_key( 'user_disabled' ), true ) ){
                $error_message = get_option( 'baba_locked_message' );
                 echo "Your account is no longer active, Please contact the administrator.";
                wp_logout();
                wp_redirect( site_url().'?acc=disabled' );
        }

    if('yes' === get_user_meta( get_current_user_id(), sanitize_key( 'account_inactive' ), true ) ){
                    $error_message = get_option( 'baba_locked_message' );
                     echo "Your registration is in progress.";
                    wp_logout();
                    wp_redirect( site_url().'?acc=inprogress' );
    } */

    if(is_user_logged_in()){
    	include( plugin_dir_path( __FILE__ ) .'../includes/include.php'); // include scripts and styles
  //
	// if (!session_id()) {
	//     session_start();
	// }

?>

<div class="login-panel">
        <div class="container">
            <div class="dashboard-sec">
            <h4>Dashboard</h4>
            <div class="row ">
                <div class="col-sm-4">
                    <div class="dashboard-bx">
                        <img src="<?php echo get_stylesheet_directory_uri();?>/img/order-ic.png" alt="order-ic" />
                        <span>ORDER PARTS<em>(Body shops)</em></span>
                        <div class="btn-sec">
                          <a href="<?php echo home_url('overview-order');?>">View Order Details <i class="fa fa-arrow-right"></i></a>
                         </div>
                    </div>
                </div>
                <div class="col-sm-4">
                      <div class="dashboard-bx blue">
                        <img src="<?php echo get_stylesheet_directory_uri();?>/img/report-ic.png" alt="report-ic" />
                        <span>REPORT ISSUES<em>(Clients)</em></span>
                        <div class="btn-sec">
                          <a href="<?php echo home_url('truck-report');?>">View Truck Report <i class="fa fa-arrow-right"></i></a>
                         </div>
                    </div>
                </div>
                <div class="col-sm-4">
                <div class="dashboard-bx pink">
                        <img src="<?php echo get_stylesheet_directory_uri();?>/img/job-ic.png" alt="job-ic" />
                        <span>JOB COMPLETION<em>(Installers)</em></span>
                        <div class="btn-sec">
                          <a href="<?php echo home_url('job-completion');?>">View Job Completion <i class="fa fa-arrow-right"></i></a>
                         </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>

<?php }else{
	echo do_shortcode('[login_form]');
}
}
add_shortcode('dashboard', 'dashboard_creation'); // EO Register Form shortcode
?>
