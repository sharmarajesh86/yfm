<?php /**************************************************************************************
*
* This file consists of the code to create the shortcode that is used in this plugin to create the dasboard page.
* It will further links to other forms after user logged in.
*
*********************************************************************************************/
/********* Create apilogs Shortcode********/

function users(){
	require_once(ABSPATH.'/wp-config.php');
	require_once(ABSPATH.'/wp-load.php');
	require_once(ABSPATH.'/wp-includes/wp-db.php');
	require_once( ABSPATH.'wp-admin/includes/user.php' );
	global $wpdb;

	$userid= get_current_user_id();
	$user = get_user_by('id',$userid);
	$queryUsers = "SELECT * FROM yfm_wp_AT_contacts where Contact_id= '".$user->data->airtable_id."'";
	$contact = $wpdb->get_results($queryUsers, OBJECT);
	/*if(!empty($contact)){
		$position = strtolower(str_replace(' ','',$contact[0]->Position));
		if(strcmp($position,'newuser')==0){
			 wp_logout();
			 wp_redirect( home_url().'?acc=inprogress' );
		}
	}
	if('yes' === get_user_meta( get_current_user_id(), sanitize_key( 'user_disabled' ), true ) ){
            $error_message = get_option( 'baba_locked_message' );
             echo "Your account is no longer active, Please contact the administrator.";
            wp_logout();
            wp_redirect( home_url().'?acc=disabled' );
	}*/
	if(!is_user_logged_in()){ $loginLink = site_url()."/login"; ?>
		<script type="text/javascript"> window.location="<?php echo $loginLink;?>"; </script>
	<?php }

	if(is_user_logged_in()){
	include( plugin_dir_path( __FILE__ ) .'../includes/include.php'); // include scripts and styles
	require_once( ABSPATH.'wp-admin/includes/user.php' );
	global $wpdb;
	$queryUsers = "SELECT * FROM wp_users ORDER BY id DESC";
	$Users = $wpdb->get_results($queryUsers, OBJECT);
	//echo "<prE>"; print_r($logs); ///die;
?>


<div class="login-panel">

        <div class="container">
            <div class="dashboard-sec">
            <h4>Users</h4>
            <div class="row">
<table id="example" class="table table-striped table-bordered userlist" style="width:100%">
        <thead>
            <tr>
            	<th>S.No.</th>
                <th>Name</th>
                <th class="email">Email</th>
                <th>Company Name</th>
                <th>Location</th>
                <th>Registered Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        	<?php  $i=1; foreach ($Users as $key => $User) { ?>
        		<?php  $user = get_userdata( $User->ID );

	            	if(empty($user->caps['administrator'])){
	            		// echo "<pre>";print_r($user->caps);?>
	            <tr>
	                <td><?php echo $i; ?></td>
	                <td><?php echo $User->user_nicename; ?></td>
	              	<td><?php echo $User->user_email; ?></td>
	              	<td><?php $CompanyDetail = "SELECT * FROM yfm_wp_AT_contacts Where Contact_id= '".$User->airtable_id."' ";
					$Details = $wpdb->get_results($CompanyDetail, OBJECT);
					if(!empty($Details))
					{
						$CompanyName = "SELECT * FROM yfm_wp_AT_companies Where company_id= '".json_decode($Details[0]->Companies)[0]."' ";
						$Company = $wpdb->get_results($CompanyName, OBJECT);
						print_r($Company[0]->CompanyName);
					}
					// else{
					// 		$user = wp_delete_user($User->ID);
					// }
					 ?>    	</td>
	              	<td><?php if(!empty($Details))
					{
						$LocationsName = "SELECT * FROM yfm_wp_AT_locations Where Location_id= '".json_decode($Details[0]->Locations)[0]."' ";
						$Locations = $wpdb->get_results($LocationsName, OBJECT);
						print_r($Locations[0]->Fulladdress);
					}	?></td>
	              	<td><?php echo $User->user_registered; ?></td>
	              	<td><?php
	              	$Userstatus = "SELECT * FROM wp_usermeta Where user_id= '".$User->ID."' ORDER BY umeta_id DESC ";
					$status = $wpdb->get_results($Userstatus, OBJECT);
					if($status)
					{				//print_r($status);
						$size =  sizeof($status);
						if($size==1)
						{
							if($status[0]->meta_key === 'user_disabled' && $status[0]->meta_value === '1')
							{

								echo ' <a class="enable_user" data-id="'.$User->ID.'" data-status="0"><span class="btn btn-sm btn-info">Enable</span></a>';
							}
							else
							{
								echo '<a class="enable_user btn btn-sm btn-danger" data-id="'.$User->ID.'" data-status="1">Disable</a>';
			              	}
						}
						else
						{
							if($status[0]->meta_key === 'user_disabled' && $status[0]->meta_value === '1'||$status[1]->meta_key === 'user_disabled' && $status[1]->meta_value === '1')
							{
								echo ' <a class="enable_user btn btn-sm btn-info" data-id="'.$User->ID.'" data-status="0">Enable</a>';
							}
							else
							{
								echo '<a class="enable_user btn btn-sm btn-danger" data-id="'.$User->ID.'" data-status="1">Disable</a>';
			              	}
			            }

					}
					else
							{
								echo '<a class="enable_user btn btn-sm btn-danger" data-id="'.$User->ID.'" data-status="1">Disable</a>';
			              	}
					?> 	</td>
					</tr>
					<?php } ?>
            <?php  $i++; } ?>
                <div id="loaders" style="display: none;"></div>
    <div id="overlays" style="display: none;"></div>
        </tbody>

    </table>
</div>
</div>
</div>
</div>

    <!-- Modal -->
	<div class="modal fade" id="erroLogModal" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Detail Response Message</h4>
				</div>
				<div class="errorlog-modal-body">
					<?php echo "<prE>"; print_r($log->response_data); echo "</prE>"; ?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>


<?php }else{
	echo do_shortcode('[login_form]');
}
}
add_shortcode('users', 'users'); // EO Register Form shortcode


add_action('wp_ajax_user_status', 'user_status');
add_action('wp_ajax_nopriv_user_status', 'user_status');


function user_status()
{
	global $wpdb;
	$uid 	= $_POST['uid'];
	$status 	= $_POST['status'];

	$update = update_user_meta( $uid, 'user_disabled', $status );
	if($update)
	{
		$data['msg']=  "update meta";
	}
	else{
		$data['msg']=  "not updated";
	}

	return wp_send_json($data);
}
?>
