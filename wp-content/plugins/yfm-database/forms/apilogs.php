<?php /**************************************************************************************
*
* This file consists of the code to create the shortcode that is used in this plugin to create the dasboard page.
* It will further links to other forms after user logged in.
*
*********************************************************************************************/
/********* Create apilogs Shortcode********/

function api_logs(){
	require_once(ABSPATH.'/wp-config.php');
	require_once(ABSPATH.'/wp-load.php');
	require_once(ABSPATH.'/wp-includes/wp-db.php');
	require_once( ABSPATH.'wp-admin/includes/user.php' );
	global $wpdb;

	$userid= get_current_user_id();
	$user = get_user_by('id',$userid);
	$queryUsers = "SELECT * FROM yfm_wp_AT_contacts where Contact_id= '".$user->data->airtable_id."'";
	$contact = $wpdb->get_results($queryUsers, OBJECT);
	if(!empty($contact))
	{
		$position = strtolower(str_replace(' ','',$contact[0]->Position));
		if(strcmp($position,'newuser')==0)
		{
			 wp_logout();
			 wp_redirect( site_url().'?acc=inprogress' );
		}
	}
	if('yes' === get_user_meta( get_current_user_id(), sanitize_key( 'user_disabled' ), true ) ){
            $error_message = get_option( 'baba_locked_message' );
             echo "Your account is no longer active, Please contact the administrator.";
            wp_logout();
            wp_redirect( site_url().'?acc=disabled' );


}else{

	if(!is_user_logged_in()){ $loginLink = site_url()."/login"; ?>
		<script type="text/javascript"> window.location="<?php echo $loginLink;?>"; </script>
	<?php }

	if(is_user_logged_in()){
	include( plugin_dir_path( __FILE__ ) .'../includes/include.php'); // include scripts and styles

	global $wpdb;
	$queryLogs = "SELECT * FROM ymf_wp_API_log ORDER BY id DESC";
	$logs = $wpdb->get_results($queryLogs, OBJECT);
	//echo "<prE>"; print_r($logs); ///die;
?>


<div class="login-panel">

        <div class="container">
            <div class="dashboard-sec">
            <h4>API Logs</h4>
            <div class="row">
<div class="table-responsive">
<table id="apilogs" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
            	<th>Refrence</th>
                <th>User Name</th>
                <th>Status</th>
                <th>Request</th>
                <th>Response</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
        	<?php foreach ($logs as $key => $log) { ?>
	            <tr>	            	
	                <td><?php echo $log->log_reference; ?></td>
	              	<td><?php echo $log->sent_by; ?></td>
	                <td><?php echo $log->status; ?></td>
	              	<td>
	<div class="request">
		<?php echo $log->request; ?>
	</div>
	</td>
	              	<td >
  	<div class="request">
	              		<?php echo $log->response;?>
	            	<?php if($log->status == 'fail'){ ?>
		              		<!--a href="#" data-toggle="modal" data-id="<?php echo $log->log_reference; ?>" data-target="#myModal"> View more..</a-->
		              		<!-- <a href="#" data-toggle="modal" data-id="<?php echo $log->log_reference; ?>" class="ViewErrorLog" data-target="#erroLogModal"> View more..</a> -->
		              		<span style="display: none;" id="span-<?php echo $log->log_reference; ?>"> <?php echo "<prE>"; print_r(json_decode($log->response_data)); echo "</prE>"; ?> </span>
	              		<?php } ?>
	              		</div>
	              	</td>
	              	<td><?php echo $log->created_date; ?></td>

	            </tr>
            <?php } ?>
        </tbody>

    </table>
</div>
</div>
</div>
</div>
</div>

    <!-- Modal -->
	<div class="modal fade" id="erroLogModal" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Detail Response Message</h4>
				</div>
				<div class="errorlog-modal-body">
					<?php echo "<prE>"; print_r($log->response_data); echo "</prE>"; ?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>


<?php }else{
	echo do_shortcode('[login_form]');
} } ?>
<script type="text/javascript">
	jQuery(document).ready(function() {
    jQuery('#apilogs').DataTable( {
        "order": [[ 7, "desc" ]]
    } );
} );
</script>
<?php }
add_shortcode('apilogs', 'api_logs'); // EO Register Form shortcode
?>
