<?php ob_start();
/**************************************************************************************
*
* This file consists of the code to create the shortcode that is used in this plugin to create the Overview Order form.
* It will further links to other forms after user logged in.
*
*********************************************************************************************/
/********* Create Dashboard Shortcode********/

if (!session_id()) {
    session_start();
}

function jobCompletionForm_creation(){
  require_once(ABSPATH.'/wp-config.php');
	require_once(ABSPATH.'/wp-load.php');
	require_once(ABSPATH.'/wp-includes/wp-db.php');
	require_once( ABSPATH.'wp-admin/includes/user.php' );
	global $wpdb;

	$userid= get_current_user_id();
	$user = get_user_by('id',$userid);
  $company = $user->data->airtable_company_id;
  if($userid!='1'){
    $companyDetail = "SELECT * from yfm_wp_AT_companies where company_id='$company'";
    $companyDel = $wpdb->get_results($companyDetail, OBJECT);
  }
	$queryUsers = "SELECT * FROM yfm_wp_AT_contacts where Contact_id= '".$user->data->airtable_id."'";
	$contact = $wpdb->get_results($queryUsers, OBJECT);
/*	if(!empty($contact)){
		$position = strtolower(str_replace(' ','',$contact[0]->Position));
		if(strcmp($position,'newuser')==0)
		{
			 wp_logout();
			 wp_redirect( site_url().'?acc=inprogress' );
		}
	}
	if('yes' === get_user_meta( get_current_user_id(), sanitize_key( 'user_disabled' ), true ) ){
            $error_message = get_option( 'baba_locked_message' );
             echo "Your account is no longer active, Please contact the administrator.";
            wp_logout();
            wp_redirect( site_url().'?acc=disabled' );


} */

	if(!is_user_logged_in()){ $loginLink = site_url()."/login"; ?>
		<script type="text/javascript"> window.location="<?php echo $loginLink;?>"; </script>
	<?php }

	//include( plugin_dir_path( __FILE__ ) . '../airtables/get-trucks.php'); // get companies list
	// Fetc truck details
	global $wpdb;
	$wlud = wp_get_current_user();
	$user_company_id = $wlud->data->airtable_company_id;
	$user_contact_id = $wlud->data->airtable_id;

	//$queryTruck = "SELECT ID1,ID2,Truck_id FROM yfm_wp_AT_trucks ORDER BY id ASC";

	$queryTruck1 = "SELECT DISTINCT yfm_wp_AT_tasks.Truck_id,yfm_wp_AT_trucks.ID1,yfm_wp_AT_trucks.ID2,yfm_wp_AT_tasks.Task_id FROM yfm_wp_AT_trucks INNER JOIN yfm_wp_AT_tasks ON yfm_wp_AT_trucks.Truck_id = yfm_wp_AT_tasks.Truck_id Where yfm_wp_AT_tasks.Status = 'In Progress' ORDER BY yfm_wp_AT_trucks.Truck_id ASC";

	//$queryTruck = "SELECT DISTINCT yfm_wp_AT_tasks.Truck_id,yfm_wp_AT_trucks.ID1,yfm_wp_AT_trucks.ID2,yfm_wp_AT_tasks.Task_id FROM yfm_wp_AT_trucks INNER JOIN yfm_wp_AT_tasks ON yfm_wp_AT_trucks.Truck_id = yfm_wp_AT_tasks.Truck_id Where yfm_wp_AT_tasks.Status != 'Unassigned' AND yfm_wp_AT_tasks.Status != 'Complete'AND yfm_wp_AT_tasks.Status != 'Unassigned' AND yfm_wp_AT_tasks.Installer LIKE '%$user_contact_id%'  ORDER BY yfm_wp_AT_trucks.Truck_id ASC";

	$queryTruck = "SELECT DISTINCT yfm_wp_AT_tasks.Truck_id,yfm_wp_AT_trucks.ID1,yfm_wp_AT_trucks.ID2,yfm_wp_AT_tasks.Task_id FROM yfm_wp_AT_trucks INNER JOIN yfm_wp_AT_tasks ON yfm_wp_AT_trucks.Truck_id = yfm_wp_AT_tasks.Truck_id Where yfm_wp_AT_tasks.Status != 'Unassigned' AND yfm_wp_AT_tasks.Status != 'Complete' AND yfm_wp_AT_tasks.Installer LIKE '%$user_contact_id%'  ORDER BY yfm_wp_AT_trucks.Truck_id ASC";

	$trucks = $wpdb->get_results($queryTruck, OBJECT);

	// echo '<pre>';
	// print_r($trucks);
	include( plugin_dir_path( __FILE__ ) .'../includes/include.php'); // include scripts and styles
	?>
 <div class="login-panel">
    <div class="container">
        <div class="dashboard-sec overview-sec truck-sec job-sec">
            <h4>Job Completion</h4>
            <div class="overview-order-detail">
               <div id="msg"></div>
               <?php
               if($companyDel[0]->Type != 'Installation'){
                 echo '<div class="alert alert-danger text-center">You are not eligible to submit a Job Completion. Please contact YFM administrator</div>';
               } else {?>
               <form id="jobCompletion"  method="POST" enctype="multipart/form-data">
              		<input type="hidden" name="action" value="job_completion">
              		<input type="hidden" name="hiddenTruckId" id="hiddenTruckId" value="">
                  <input type="hidden" name="hiddenTaskId" id="hiddenTaskId" value="">
              		<div class="form-group">
                    <div class="row">
                    	<div class="col-md-3">
                    		<label>Truck Identification</label>
                    	</div>
                    	<div class="col-md-9 with-select">
                    		<select class="form-control chosen-select" name="truck_identification" id="truck_identification" required="true">
                                	<?php if(empty($trucks)){ ?>
                                		<option value=""> No record found </option>
                                	<?php }else{ ?>
                                		<option value="">--Select Truck--</option>
                                	<?php
                    				foreach ($trucks as $key => $truck) {
                    					echo '<option value="'.$truck->ID1.'" data-id="'.$truck->Truck_id.'" data-taskid="'.$truck->Task_id.'">'.$truck->ID1.' - '.$truck->ID2.'</option>';
                    				}
                    			}
                    			?>
                        </select>
                    	</div>
                    </div>
              			</div>
			             <div class="form-group" id="job_completion_images">
            				<div class="row">
          						<div class="col-md-3">
          							<label>Image 1<sup></sup></label>
          						</div>
          						<div class="col-md-4">
          							<input type="file" class="form-control" name="work_image[]" id="">
          							<!-- <span>Attach Photograph</span> -->
          						</div>
          						<div class="col-md-1">
          							<i class="far fa-plus-square addimagejob" id="body_internal_image"></i>
          							<button type="button" class="btn btn-info addimagejob" id="body_internal_image">
          							<span class="glyphicon glyphicon-plus"></span>
          							</button>
          						</div>
            					</div>
            				</div>
            				<div class="form-group">
            					<div class="row">
            						<div class="col-md-3">
            							<label>Comments</label>
            						</div>
            						<div class="col-md-9">
            							<textarea class="form-control" name="comments_box" id="comments_box"></textarea>
            						</div>
            					</div>
            				</div>
            				<div class="form-group text-center">
            					<input type="submit" class="btn btn-orange job-completion" name="submit" value="Submit" >
            				</div>
                  	<div id="loader2" style="display: none;"></div>
                		<div id="overlay" style="display: none;"></div>
          		</form>
            <?php } ?>
          </div>
      </div>
  </div>
</div>

	<?php /*}else{
		echo do_shortcode('[login_form]');
	}*/


}
add_shortcode('job_completion', 'jobCompletionForm_creation'); // EO Register Form shortcode

add_action('wp_ajax_job_completion', 'job_completion');
add_action('wp_ajax_nopriv_job_completion', 'job_completion');


function job_completion(){
	if(!is_user_logged_in()){
		$loginLink = '<a href="'.site_url().'/login" target="_blank" > login </a>';
		$ajaxResponse['msg'] = __('Login Required. Clieck here to '.$loginLink);
		$ajaxResponse['status'] = __('fail');
		echo wp_json_encode($ajaxResponse); exit;
	}

    global $wpdb;
    $ajaxResponse = array();
    $ajaxResponse['status'] = 'success';

	$task_id = $_POST['hiddenTaskId'];
	$tid1 = $_POST['hiddenTruckId'];

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Tasks/".$task_id);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");


	$headers = array();
	$headers[] = "Authorization: Bearer keyw3cJfiAFXmYjJf";
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	$data =json_decode($result);
	curl_close($curl);
	// echo '<pre>';
	// print_r($data->fields->P_Side_Artwork);
	//die;


 	$queryTruck = "SELECT Truck_id,ID1,ID2,Truck_Type,D_Side_Art_code,P_Side_Art_code,Rear_Art_code,Client FROM yfm_wp_AT_trucks WHERE Truck_id = '".$tid1."'";
	$truck_data = $wpdb->get_results($queryTruck, OBJECT);
	// echo '<pre>';
	// print_r($truck_data);

	$partCodeId = str_replace('["', '', $truck_data[0]->P_Side_Art_code);
	$pSideArtCodeId = str_replace('"]', '', $partCodeId);
	//$queryPSide = "SELECT * FROM yfm_wp_AT_side_artwork WHERE Side_Artwork_id = '$pSideArtCodeId'";
	$queryPSide = "SELECT * FROM yfm_wp_AT_side_artwork WHERE Artwork_code = '".$data->fields->P_side_Artwork."' or Artwork_Transition ='".$data->fields->P_side_Artwork."'";
	$truckPSideArtData = $wpdb->get_results($queryPSide, OBJECT);
	 // echo '<pre>';
	 // print_r($truckPSideArtData);

		/*$dartCodeId = str_replace('["', '', $truck_data[0]->D_Side_Art_code);
		$dSideArtCodeId = str_replace('"]', '', $dartCodeId);
		$queryDSide = "SELECT Artwork_Code,Artwork,Artwork_Transition,Artwork_Transition FROM yfm_wp_AT_side_artwork WHERE Side_Artwork_id = '$dSideArtCodeId'";
		$truckDSideArtData = $wpdb->get_results($queryDSide, OBJECT);*/
		//print_r($truck_data[0]->Rear_Art_code);

		$rartCodeId = str_replace('["', '', $truck_data[0]->Rear_Art_code);
		//$rartCodeId = str_replace('["', '', $data->fields->Rear_Art_code);
		$rSideArtCodeId = str_replace('"]', '', $rartCodeId);
		//$queryRSide = "SELECT * FROM yfm_wp_AT_rear_artwork WHERE Rear_Artwork_id = '$rSideArtCodeId'";
		$queryRSide = "SELECT * FROM yfm_wp_AT_rear_artwork WHERE Artwork_code ='".$data->fields->Rear_Artwork."' or Artwork_Transition ='".$data->fields->Rear_Artwork."'";
		$truckRSideArtData = $wpdb->get_results($queryRSide, OBJECT);
		 // echo '<pre>';
		 //  print_r($truckRSideArtData);
		// die;

	//print_r($newArtWorkId); die;
	//$newRArtWorkId = json_decode($truckRSideArtData[0]->Artwork_Transition);
/*
	if(!empty(json_decode($truckPSideArtData[0]->Artwork_Transition)))
	{
	$newArtWorkId = json_decode($truckPSideArtData[0]->Artwork_Transition);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Trucks/".$tid1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n  \"fields\": {\n  \"D_Side_Art_code\": [\n \"$newArtWorkId[0]\"\n ] }\n}");
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, PATCH);

			$headers = array();
			$headers[] = "Authorization: Bearer keyw3cJfiAFXmYjJf";
			$headers[] = "Content-Type: application/json";
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			//echo '<pre>';
			$data =json_decode($result);
			//print_r($data);
			if (curl_errno($ch)) {
			    echo 'Error:' . curl_error($ch);
			}
			curl_close ($ch);
	}
else{

	$newArtWorkId = $truckPSideArtData[0]->Side_Artwork_id;
				$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Trucks/".$tid1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n  \"fields\": {\n    \"D_Side_Art_code\":  [\n      \"$newArtWorkId\"\n    ] }\n}");
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, PATCH);

	$headers = array();
	$headers[] = "Authorization: Bearer keyw3cJfiAFXmYjJf";
	$headers[] = "Content-Type: application/json";
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	//print_r($result);
	if (curl_errno($ch)) {
	    echo 'Error:' . curl_error($ch);
	}
	curl_close ($ch);
}
if(!empty(json_decode($truckPSideArtData[0]->Artwork_Transition)))
{
	$newArtWorkId = json_decode($truckPSideArtData[0]->Artwork_Transition);

				$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Trucks/".$tid1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n  \"fields\": {\n    \"P_Side_Art_code\":  [\n      \"$newArtWorkId[0]\"\n    ] }\n}");
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, PATCH);

	$headers = array();
	$headers[] = "Authorization: Bearer keyw3cJfiAFXmYjJf";
	$headers[] = "Content-Type: application/json";
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	//print_r($result);
	if (curl_errno($ch)) {
	    echo 'Error:' . curl_error($ch);
	}
	curl_close ($ch);
}
else{

	$newArtWorkId = $truckPSideArtData[0]->Side_Artwork_id;
				$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Trucks/".$tid1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n  \"fields\": {\n    \"P_Side_Art_code\":  [\n      \"$newArtWorkId\"\n    ] }\n}");
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, PATCH);

	$headers = array();
	$headers[] = "Authorization: Bearer keyw3cJfiAFXmYjJf";
	$headers[] = "Content-Type: application/json";
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	//print_r($result);
	if (curl_errno($ch)) {
	    echo 'Error:' . curl_error($ch);
	}
	curl_close ($ch);
}
if(json_decode($truckRSideArtData[0]->Artwork_Transition) != 'N'){

		$newRArtWork = $truckRSideArtData[0]->Rear_Artwork_id;
		  // print_r($newRArtWork);
		  // echo '<br>';
		$newTArtWork = json_decode($truckRSideArtData[0]->Artwork_Transition);
		//print_r($newTArtWork);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Trucks/".$tid1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n  \"fields\": {\n    \"Rear_Art_code\":  [\n \"$newRArtWork\"\n    ] }\n}");
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, PATCH);

	$headers = array();
	$headers[] = "Authorization: Bearer keyw3cJfiAFXmYjJf";
	$headers[] = "Content-Type: application/json";
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	//print_r($result);
	if (curl_errno($ch)) {
	    echo 'Error:' . curl_error($ch);
	}
	curl_close ($ch);
}

else{
		//$ArtworkArray['Rear_Art_Work']   = json_decode($truckRSideArtData[0]->Artwork);
		$newRArtWork = $truckRSideArtData[0]->Rear_Artwork_id;
		$newTArtWork = json_decode($truckRSideArtData[0]->Artwork_Transition);
		//print_r($newTArtWork);

		$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Trucks/".$tid1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n  \"fields\": {\n    \"Rear_Art_code\":  [\n \"$newRArtWork\"\n    ] }\n}");
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, PATCH);

	$headers = array();
	$headers[] = "Authorization: Bearer keyw3cJfiAFXmYjJf";
	$headers[] = "Content-Type: application/json";
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	//print_r($result);
	if (curl_errno($ch)) {
	    echo 'Error:' . curl_error($ch);
	}
	curl_close ($ch);
} */


	/*echo '<pre>';
	print_r($data->fields->Images);*/
	// Upload images
	$upload_overrides = array( 'test_form' => false );

    $taskImagesArr = [];
	if(!empty($_FILES['work_image'])){
		$files = $_FILES['work_image'];
		foreach ($files['name'] as $key => $value) {
			if ($files['name'][$key]) {
				$file = array(
					'name'     => $files['name'][$key],
					'type'     => $files['type'][$key],
					'tmp_name' => $files['tmp_name'][$key],
					'error'    => $files['error'][$key],
					'size'     => $files['size'][$key]
				);
				//wp_handle_upload($file);
				$movefile = wp_handle_upload($file, $upload_overrides );
				if ( $movefile && ! isset( $movefile['error'] ) ) {
				   $url = stripslashes($movefile['url']);
				   $taskImagesArr[] =['url'=>$url];

				} else {
				    // Error generated by _wp_handle_upload()
				    // @see _wp_handle_upload() in wp-admin/includes/file.php
				    //echo $movefile['error'];
				}
			}
		}
	}


	$panel_type = implode(',', $panel_type); //die;
	if(!empty($data->fields->Images))
	{
		$images=array_merge($data->fields->Images,$taskImagesArr);
	}
	else{
		$images=$taskImagesArr;
	}

	 // echo '<pre>';
	 // print_r($images);
	//die;
	//print_r($taskImagesArr);die;

	$queryTruck = "SELECT Truck_id,ID1,ID2,Truck_Type,D_Side_Art_code,P_Side_Art_code,Rear_Art_code,Client FROM yfm_wp_AT_trucks WHERE Truck_id = '".$tid1."'";
	$truck_data = $wpdb->get_results($queryTruck, OBJECT);
	//echo "<pre>"; print_r($truck_data); die;
	$trukId1 = $truck_data[0]->ID1;
	$trukId2 = $truck_data[0]->ID2;


	$ccomments = $_POST['comments_box'];

	// Wp Login user Data
	$wlud = wp_get_current_user();

	$taskData =[];
	$taskData=[
	    'fields'=>[
	   	/*'Truck_ID1'=>[$truck_data[0]->Truck_id],
	   	'Task_Type'=>'Alert',*/
	   	/*'Images'=>[
	   		['url'=>$url1],
	        ['url'=>$url2]
	   	 ],*/
	   	'Images'=>$images,
	   /*	'Job Type'=>['Job Complete'],*/
	    'Status'=>['Pending Approval'],
	    /*"Ordered_by"=> [$wlud->data->airtable_id],*/
	   // "Notes (client)" =>  $data->fields->Notes.' '.$ccomments
	    "Task_Notes_1" => $data->fields->Task_Notes_1.' '.$ccomments
		]
	];
	// echo '<pre>';
	// print_r($taskData);
	// echo '</pre>';
	//die;
	/*
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Tasks/".$task_id);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");


	$headers = array();
	$headers[] = "Authorization: Bearer keyw3cJfiAFXmYjJf";
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	//echo '<pre>';
	$data =json_decode($result);*/
	//print_r($data);

 	$curl = curl_init();
	$airtableTaskUrl = "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Tasks/".$task_id;
	curl_setopt_array($curl, array(
	CURLOPT_URL => $airtableTaskUrl,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "PATCH",
	//CURLOPT_POSTFIELDS => "{\n  \"fields\": {\n    \"Truck_ID1\": [\n      \"rec0z32nKnCHIQhzu\"\n    ],\n    \"Task_Type\": \"Repairs (Parts & Labour)\",\n    \"Images\": [\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC.jpg\"\n      },\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC-1.jpg\"\n      }\n    ],\n    \"Job Type\": [\n      \"Order\"\n    ],\n    \"Status\": \"Client response?\"\n  }\n}",
	CURLOPT_POSTFIELDS => json_encode($taskData),
	CURLOPT_HTTPHEADER => array(
	"authorization: Bearer keyw3cJfiAFXmYjJf",
	"cache-control: no-cache",
	"content-type: application/json",
	"postman-token: d6aa4070-c2cd-df5f-cd62-0f25e59be5e2"
	),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);

	$airtableResponse = json_decode($response);

	//echo "<pre>"; print_r($airtableResponse); echo "</pre>"; die;

	if ($err) {

		$request['url'] = $airtableTaskUrl;
		$request['data'] = $taskData;

		$ajaxResponse['log']['request'] = wp_json_encode($request);
		$ajaxResponse['log']['status'] = "fail";
		$ajaxResponse['log']['response'] = wp_json_encode($err);
		$ajaxResponse['log']['sent_by'] = $wlud->data->user_email;
		$ajaxResponse['log']['log_reference'] = strtotime(date('Y-m-d h:i:s'));
		$ajaxResponse['log']['created_date'] = date('Y-m-d h:i:s');
		//Update error log
		$api = $wpdb->insert('ymf_wp_API_log', $ajaxResponse['log']);

		if($api)
		{
			//echo 'logs inserted';
			$data['msg']= 'something went wrong.Contact with administrator with following reference : {'.$ajaxResponse['log']['log_reference'].'}';
			$data['status'] = 'fail';
		}
		echo wp_json_encode($data); exit;

	}
	else if(isset($airtableResponse->error)){

		$request['url'] = $airtableTaskUrl;
		$request['data'] = $taskData;

		$ajaxResponse['log']['request'] = wp_json_encode($request);
		$ajaxResponse['log']['status'] = "fail";
		$ajaxResponse['log']['response'] = wp_json_encode($airtableResponse);
		$ajaxResponse['log']['sent_by'] = $wlud->data->user_email;
		$ajaxResponse['log']['log_reference'] = strtotime(date('Y-m-d h:i:s'));
		$ajaxResponse['log']['created_date'] = date('Y-m-d h:i:s');
		//Update error log
		$api = $wpdb->insert('ymf_wp_API_log', $ajaxResponse['log']);

		if($api)
		{
			//echo 'logs inserted';
			$data['msg']= 'something went wrong.Contact with administrator with following reference : {'.$ajaxResponse['log']['log_reference'].'}';
			$data['status'] = 'fail';
			echo wp_json_encode($data); exit;
		}

	} else {

			$request['url'] = $airtableTaskUrl;
			$request['data'] = $taskData;

			$ajaxResponse['log']['request'] = wp_json_encode($request);
			$ajaxResponse['log']['status'] = "success";
			$ajaxResponse['log']['response'] = wp_json_encode($airtableResponse);
			$ajaxResponse['log']['sent_by'] = $wlud->data->user_email;
			$ajaxResponse['log']['log_reference'] = strtotime(date('Y-m-d h:i:s'));
			$ajaxResponse['log']['created_date'] = date('Y-m-d h:i:s');
			//Update error log
			$api = $wpdb->insert('ymf_wp_API_log', $ajaxResponse['log']);
			//echo '<pre>'; print_r($airtableResponse); die;

			$Task_Type=$airtableResponse->fields->Task_Type;
			$Status=$airtableResponse->fields->Status;

			//$D_side_Artwork=$airtableResponse->fields->D_side_Artwork;
			//$P_side_Artwork=$airtableResponse->fields->P_side_Artwork;
			//$Rear_Artwork=$airtableResponse->fields->Rear_Artwork;
			//$Panels=$airtableResponse->fields->Panels;

			$Truck_ID1=wp_json_encode($airtableResponse->fields->Truck_ID1);
			$Truck_ID2=wp_json_encode($airtableResponse->fields->Truck_ID2);
			$Client=wp_json_encode($airtableResponse->fields->Client);
			$Location=wp_json_encode($airtableResponse->fields->Location);
			$Job_No=$airtableResponse->fields->Job_No;
			$Order_Date=$airtableResponse->fields->Order_Date;
			$Images=wp_json_encode($airtableResponse->fields->Images);
			$Task_id=wp_json_encode($airtableResponse->id);
			$Ordered_by=wp_json_encode($airtableResponse->fields->Ordered_by);
			$Truck_id=$airtableResponse->fields->Truck_ID1[0];
			//echo "<pre>"; print_r($wpArray); echo "</pre>"; die;

		  	$table_name = 'yfm_wp_AT_tasks';

			$checkDataInsert = $wpdb->insert($table_name, array(
			    'Job_No' => $Job_No,
			    'Truck_ID1' => $Truck_ID1,
				'Truck_ID2' => $Truck_ID2,
			    'Task_Type'=> $Task_Type,
			    'Status'=>$Status,
			    'Location'=> $Location,
			    'Client'=>$Client,
			    'Order_Date'=>$Order_Date,
			    'Images'=>$Images,
			   	'Task_id'=>$Task_id,
			   	'Ordered_by'=> $Ordered_by,
			   	'Truck_id'=> $Truck_id
			));

			//echo $wpdb->last_query;die;

			if($checkDataInsert)
			{
				$tastLink = '<a href="https://airtable.com/tbla2QBLhnUjtZ2nx/viw8im6rKKLvvdRMI/'.$airtableResponse->id.'"> click here </a>';
				$contactLink = '<a href="https://airtable.com/tblfJ923QFFjr89w3/viwAXRAip5DGKfT5M/'.$wlud->data->airtable_id.'"> click here </a>';
				$consignmentAddress = 'Consignment Address : '.$wlud->data->airtable_address.'.';


				$subject = 'Your Fleet Media Job Complete';
				$to = 'service@yourfleetmedia.com';
				$body = '<div style="width:100%;background:#eee;margin: 0;padding:50px 0;height:100%;">';
				$body .= '<table style="margin:0 auto;padding:20px;width: 700px;background:#fff;border: 1px solid #ddd;">';

				$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">Truck identification :</td><td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;"> '.$trukId1.' '.$trukId2.'</td></tr>';
				$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">Task link :</td> <td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.$tastLink.'</td></tr>';
				$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">Contact raised the request :</td><td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.$contactLink.'</td></tr>';
				//$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">Consignment Address </td><td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.$consignmentAddress.'</td></tr>';
				$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;"> Comments: </td><td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.$ccomments.'</td></tr>';
				$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;"></td> <td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;"> </td></tr>';
				$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 0px solid #ddd;"> Thanks, </td> ';
				$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 0px solid #ddd;"> Your Fleet Media </td> ';

				$body .= '</table></div>';

				$headers = array('Content-Type: text/html; charset=UTF-8');
				$a = wp_mail( $to, $subject, $body, $headers);
				//print_r($a); die;
				if($a){

					$mail['msg'] = __('Form Submitted Successfully.');
					$mail['status'] = __('success');
					//$_SESSION['form-message'] = $data;
					echo wp_json_encode($mail); exit;

				}else{

					$request['mail']['request'] = "mail sending";
					$request['mail']['response'] = "issue with mail sending";

					$ajaxResponse['log']['request'] = wp_json_encode($request['mail']['request']);
					$ajaxResponse['log']['status'] = "fail";
					$ajaxResponse['log']['response'] = wp_json_encode($request['mail']['response']);
					$ajaxResponse['log']['sent_by'] = $wlud->data->user_email;
					$ajaxResponse['log']['log_reference'] = strtotime(date('Y-m-d h:i:s'));
					$ajaxResponse['log']['created_date'] = date('Y-m-d h:i:s');
					//Update error log
					$api = $wpdb->insert('ymf_wp_API_log', $ajaxResponse['log']);

					if($api)
					{
						//echo 'logs inserted';
						$data['msg']= 'something went wrong.Contact with administrator with following reference : {'.$ajaxResponse['log']['log_reference'].'}';
						$data['status'] = 'fail';
						echo wp_json_encode($data); exit;
					}

				}

			}else{
				$request['wp']['response'] = $wpdb->last_query;
				$request['wp']['request'] = "Wordpress task entry";

				$ajaxResponse['log']['request'] = wp_json_encode($request['wp']['request'] );
				$ajaxResponse['log']['status'] = "fail";
				$ajaxResponse['log']['response'] = wp_json_encode($request['wp']['response']);
				$ajaxResponse['log']['sent_by'] = $wlud->data->user_email;
				$ajaxResponse['log']['log_reference'] = strtotime(date('Y-m-d h:i:s'));
				$ajaxResponse['log']['created_date'] = date('Y-m-d h:i:s');
				//Update error log
				$api = $wpdb->insert('ymf_wp_API_log', $ajaxResponse['log']);

				if($api)
				{
					//echo 'logs inserted';
					$data['msg']= 'something went wrong.Contact with administrator with following reference : {'.$ajaxResponse['log']['log_reference'].'}';
					$data['status'] = 'fail';
					echo wp_json_encode($data); exit;
				}


			}

	}

}
   ?>
