<?php /**************************************************************************************
*
* This file consists of the code to create the shortcode that is used in this plugin to create the Overview Order form.
* It will further links to other forms after user logged in.
*
*********************************************************************************************/
/********* Create Dashboard Shortcode********/

if (!session_id()) {
    session_start();
}

function truckReport_creation(){
  require_once(ABSPATH.'/wp-config.php');
	require_once(ABSPATH.'/wp-load.php');
	require_once(ABSPATH.'/wp-includes/wp-db.php');
	require_once( ABSPATH.'wp-admin/includes/user.php' );
	global $wpdb;

	$userid= get_current_user_id();
	$user = get_user_by('id',$userid);
	$queryUsers = "SELECT * FROM yfm_wp_AT_contacts where Contact_id= '".$user->data->airtable_id."'";
	$contact = $wpdb->get_results($queryUsers, OBJECT);
	/*if(!empty($contact)){
		$position = strtolower(str_replace(' ','',$contact[0]->Position));
		if(strcmp($position,'newuser')==0)
		{
			 wp_logout();
			 wp_redirect( site_url().'?acc=inprogress' );
		}
	}
	if('yes' === get_user_meta( get_current_user_id(), sanitize_key( 'user_disabled' ), true ) ){
            $error_message = get_option( 'baba_locked_message' );
             echo "Your account is no longer active, Please contact the administrator.";
            wp_logout();
            wp_redirect( site_url().'?acc=disabled' );
  }*/

	if(!is_user_logged_in()){ $loginLink = site_url()."/login"; ?>
		<script type="text/javascript"> window.location="<?php echo $loginLink;?>"; </script>
	<?php }

	//include( plugin_dir_path( __FILE__ ) . '../airtables/get-trucks.php'); // get companies list
	// Fetc truck details
	global $wpdb;


   $company = $user->data->airtable_id;
    $contactDetail = "SELECT * from yfm_wp_AT_contacts where Contact_id='$company'";
    $contactDel = $wpdb->get_results($contactDetail, OBJECT);

    $com = $contactDel[0]->Companies;
    $contact_com = str_replace('["','',$contactDel[0]->Companies);
    $contact_com = str_replace('"]','',$contact_com);

    $loc = $contact[0]->Locations;
  $contact_loc = str_replace('["','',$contact[0]->Locations);
  $contact_loc = str_replace('"]','',$contact_loc);

  //if($userid!='24'){
    $companyDetail = "SELECT * from yfm_wp_AT_companies where company_id='$contact_com'";
    $companyDel = $wpdb->get_results($companyDetail, OBJECT);
  //}


	//$queryTruck = "SELECT ID1,ID2,Truck_id FROM yfm_wp_AT_trucks where ID1 != ''  and Client_code='$contact_com' and Ops_Status !='Retired' ORDER BY id ASC";

  if($companyDel[0]->Type == 'Client'){

    $queryTruck = "SELECT ID1,ID2,Truck_id FROM yfm_wp_AT_trucks where ID1 != ''  and Client_code='$contact_com' and Ops_Status !='Retired' ORDER BY id ASC";

	}
	else{
		$queryTruck = "SELECT ID1,ID2,Truck_id,Ops_Status FROM yfm_wp_AT_trucks where ID1 != '' and Ops_Status !='Retired' ORDER BY id ASC";
		$trucks = $wpdb->get_results($queryTruck, OBJECT);

	}



	include( plugin_dir_path( __FILE__ ) .'../includes/include.php'); // include scripts and styles ?>

	 <div class="login-panel">
        <div class="container">
            <div class="dashboard-sec overview-sec truck-sec">
                <h4>Truck Report</h4>
                <div class="overview-order-detail">

	<div id="msg"></div>
    <div class="row">
    	<div class="col-md-12">
        <?php

        $type = array("Body Repairs", "Client", "Installation");
      if(in_array($companyDel[0]->Type, $type)){?>
    		<form id="truckReport" method="post" enctype="multipart/form-data">
    			<input type="hidden" name="action" value="truckReport">
    			<input type="hidden" name="hiddenTruckId" id="hiddenTruckId" value="">

					<div class="form-group">
						<div class="row">
							<div class="col-md-3">
								<label>Truck Identification</label>
							</div>
							<div class="col-md-9 with-select">
								<select class="form-control chosen-select" name="truck_identification" id="truck_identification" required="true">
									<option value="">--Select Truck--</option>
									<?php
									/*for($i=0; $i<count($trucks->records)-1;$i++) {
									echo '<option value="'.$trucks->records[$i]->fields->ID1.'" data-id="'.$trucks->records[$i]->id.'">'.$trucks->records[$i]->fields->ID1.' - '.$trucks->records[$i]->fields->ID2.'</option>';
									}*/
									foreach ($trucks as $key => $truck) {
									echo '<option value="'.$truck->ID1.'" data-id="'.$truck->Truck_id.'">'.$truck->ID1.' - '.$truck->ID2.'</option>';
									}
									?>
								</select>
							</div>
						</div>
					</div>
		<!-- <div class="form-group">
		 <div class="row">
			<div class="col-md-3">
				<label>Purchase Order Number<sup></sup></label>
			</div>
			<div class="col-md-9">
				<input type="text" class="form-control" name="purchase_order_num" id="purchase_order_num">
			</div>
			</div>
		</div> -->
		<div class="form-group">
		 <div class="row">
			<div class="col-md-3">
				<label>Panel Selection</label>
			</div>
			<div class="col-md-9">
				<h5>Truck Body</h5>
				<div class="form-group panels-list">
                	<label class="Check_box">Driver Side
                      <input type="checkbox" name="panel_selection[]" class="panelchk123" value="driver_side" panel_type="BD">
                      <span class="checkmark"></span>
                    </label>
                    <label class="Check_box">Passenger Side
                      <input type="checkbox" name="panel_selection[]" class="panelchk123" value="passenger_side" panel_type="BP">
                      <span class="checkmark"></span>
                    </label>
                    <label class="Check_box">Rear
                      <input type="checkbox" name="panel_selection[]" class="panelchk123" value="rear" panel_type="BR">
                      <span class="checkmark"></span>
                    </label>
                    <label class="Check_box">Front
                      <input type="checkbox" name="panel_selection[]" value="front" panel_type="BF">
                      <span class="checkmark"></span>
                    </label>
                    <label class="Check_box">Internal
                      <input type="checkbox" name="panel_selection[]" value="body_internal" panel_type="BI">
                      <span class="checkmark"></span>
                    </label>
                    <h5>Truck Cabin</h5>
                    <label class="Check_box">Driver Side
                      <input type="checkbox" name="panel_selection[]" value="cabin_driver" panel_type="CD">
                      <span class="checkmark"></span>
                    </label>
                    <label class="Check_box">Passenger Side
                      <input type="checkbox" name="panel_selection[]" value="cabin_passenger" panel_type="CP">
                      <span class="checkmark"></span>
                    </label>
                     <label class="Check_box">Front
                      <input type="checkbox" name="panel_selection[]" value="cabin_front" panel_type="CF">
                      <span class="checkmark"></span>
                    </label>
              		<label class="Check_box">Internal
                      <input type="checkbox" name="panel_selection[]" value="cabin_internal" panel_type="CI">
                      <span class="checkmark"></span>
                    </label>

                </div>
			</div>
			</div>
		</div>
		<div class="form-group" id="job_completion_images">
			<div class="row">
				<div class="col-md-3">
					<label>Image 1</label>
				</div>
				<div class="col-md-4">
					<input type="file" class="form-control truck_report_image" name="work_image[]" id="">
					<!-- <span>Attach Photograph</span> -->
				</div>
				<div class="col-md-1">
					<i class="far fa-plus-square addimagejob" id="body_internal_image" data-toggle="tooltip" data-placement="top" title="Max of 5 images can be uploaded"></i>
					<button type="button" class="btn btn-info addimagejob" id="body_internal_image" data-toggle="tooltip" data-placement="top" title="Max of 5 images can be uploaded">
					<span class="glyphicon glyphicon-plus"></span>
					</button>
				</div>
			</div>
		</div>
        <div class="form-group">
			<div class="row">
				<div class="col-md-3">
					<label>Comments<sup></sup></label>
				</div>
				<div class="col-md-9">
					<textarea class="form-control" name="ccomments" id="ccomments"></textarea>
				</div>
			</div>
		</div>
		<div class="form-group text-center">
			<input type="submit" class="btn btn-orange" id="truck-submit" value="Submit" name="submit">
		</div>

</div>
<div id="loader" style="display: none;"></div>
<div id="overlay" style="display: none;"></div>
</form>
<?php }else{
  echo '<div class="alert alert-danger text-center">You are not eligible to submit truck report. Kindly contact administrator</div>';
}  ?>
</div>
</div>
</div>
</div>


	<?php /*}else{
		echo do_shortcode('[login_form]');
	}*/
 }
add_shortcode('truck_report_form', 'truckReport_creation'); // EO Register Form shortcode
add_action('wp_ajax_parts_list', 'part_list1');
add_action('wp_ajax_nopriv_parts_list', 'part_list1');

function part_list1()
{


	$panel_type = $_POST['panel_type'];

	$querystr = "SELECT * FROM yfm_wp_AT_parts_list WHERE Panel = '$panel_type' ORDER BY id DESC";

 	$parts_data = $wpdb->get_results($querystr, OBJECT);
 	if(!empty($parts_data))
 	{
 		echo wp_json_encode($parts_data);exit;
 	}
 	else{

 		$querystr = "SELECT * FROM yfm_wp_AT_parts_list WHERE Panel = 'Gen' ORDER BY id DESC";
 		$parts_data = $wpdb->get_results($querystr, OBJECT);
 		echo wp_json_encode($parts_data);exit;
 	}


}

add_action('wp_ajax_truckReport', 'truckReport');
add_action('wp_ajax_nopriv_truckReport', 'truckReport');

// define( 'RESOURCES_PLUGIN_URL',plugin_dir_url(__FILE__) );
// if ( ! defined( 'RESOURCES_PLUGIN_DIR' ) )
// define( 'RESOURCES_PLUGIN_DIR', untrailingslashit( dirname( __FILE__ ) ) );
function truckReport(){

	//echo "<prE>"; print_r($_POST); echo "</prE>";
	//echo "<prE>"; print_r($_FILES); echo "</prE>";
	//die;

	if(!is_user_logged_in()){
		$loginLink = '<a href="'.site_url().'/login" target="_blank" > login </a>';
		$ajaxResponse['msg'] = __('Login Required. Clieck here to '.$loginLink);
		$ajaxResponse['status'] = __('fail');
		echo wp_json_encode($ajaxResponse); exit;
	}

	global $wpdb;
	$ajaxResponse = array();
    $ajaxResponse['status'] = 'success';

    $postComments = $_POST['ccomments'];
    //$ponum = $_POST['purchase_order_num'];


    $panelCodes = [];
	foreach ($_POST['panel_selection'] as $key => $value) {
		$panelCode = '';
    	if($value == 'driver_side'){
			$panelCode = "BD";
		}else if($value == 'passenger_side'){
			$panelCode = "BP";
		}else if($value == 'rear'){
			$panelCode = "BR";
		}else if($value == 'front'){
			$panelCode = "BF";
		}else if($value == 'cabin_front'){
			$panelCode = "CF";
		}else if($value == 'cabin_driver'){
			$panelCode = "CD";
		}else if($value == 'cabin_passenger'){
			$panelCode = "CP";
		}else if($value == 'cabin_internal'){
			$panelCode = "CI";
		}else if($value == 'body_internal'){
			$panelCode = "BI";
		}
		$panelCodes[] = $panelCode;
	}

	$mailPanelCodes = implode(',', $panelCodes);

    // Upload images
	$upload_overrides = array( 'test_form' => false );

    $taskImagesArr = [];
	if(!empty($_FILES['work_image'])){
		$mailImageLinks = "";
		$files = $_FILES['work_image'];
		foreach ($files['name'] as $key => $value) {
			if ($files['name'][$key]) {
				$file = array(
					'name'     => $files['name'][$key],
					'type'     => $files['type'][$key],
					'tmp_name' => $files['tmp_name'][$key],
					'error'    => $files['error'][$key],
					'size'     => $files['size'][$key]
				);
				//wp_handle_upload($file);
				$movefile = wp_handle_upload($file, $upload_overrides );
				if ( $movefile && ! isset( $movefile['error'] ) ) {
				   $url = stripslashes($movefile['url']);
				   $taskImagesArr[] =['url'=>$url];
				   $mailImageLinks .= '<a href="'.$url.'"> Photograph '.($key+1).' </a><br>';
				} else {
				    // Error generated by _wp_handle_upload()
				    // @see _wp_handle_upload() in wp-admin/includes/file.php
				    //echo $movefile['error'];
				}
			}
		}

	}else{
		$mailImageLinks = 'No Images.';
	}


	$truckid = explode('-', $_POST['truck_identification']);
	$ccomments = $postComments;
	$tid1 = trim($truckid[0]);

	$tid1 = $_POST['hiddenTruckId'];

	$queryTruck = "SELECT Truck_id,ID1,ID2,Truck_Type,D_Side_Art_code,P_Side_Art_code,Rear_Art_code FROM yfm_wp_AT_trucks WHERE Truck_id = '".$tid1."'";
	$truck_data = $wpdb->get_results($queryTruck, OBJECT);

	$trukId1 = $truck_data[0]->ID1;
	$trukId2 = $truck_data[0]->ID2;

	#Get Side Art/Rear Art Data
	//if ($panel_type == 'BP' && $truck_data[0]->P_Side_Art_code != '""') {
		$partCodeId = str_replace('["', '', $truck_data[0]->P_Side_Art_code);
		$pSideArtCodeId = str_replace('"]', '', $partCodeId);
		$queryPSide = "SELECT Artwork_Code,Artwork_Transition FROM yfm_wp_AT_side_artwork WHERE Side_Artwork_id = '$pSideArtCodeId'";
		$truckPSideArtData = $wpdb->get_results($queryPSide, OBJECT);
	//}

	//if ($panel_type == 'BD' && $truck_data[0]->D_Side_Art_code != '""') {
		$dartCodeId = str_replace('["', '', $truck_data[0]->D_Side_Art_code);
		$dSideArtCodeId = str_replace('"]', '', $dartCodeId);
		$queryDSide = "SELECT Artwork_Code,Artwork_Transition FROM yfm_wp_AT_side_artwork WHERE Side_Artwork_id = '$dSideArtCodeId'";
		$truckDSideArtData = $wpdb->get_results($queryDSide, OBJECT);
	//}
	//if ($panel_type == 'BR' && $truck_data[0]->Rear_Art_code != '""') {
		$rartCodeId = str_replace('["', '', $truck_data[0]->Rear_Art_code);
		$rSideArtCodeId = str_replace('"]', '', $rartCodeId);
		$queryRSide = "SELECT Artwork_Code,Artwork_Transition FROM yfm_wp_AT_rear_artwork WHERE Rear_Artwork_id = '$rSideArtCodeId'";
		$truckRSideArtData = $wpdb->get_results($queryRSide, OBJECT);
	//}

	$calData['D_Side_Art_code'] = $truckDSideArtData[0]->Artwork_Code;
	$calData['P_Side_Art_code'] = $truckPSideArtData[0]->Artwork_Code;
	$calData['Rear_Art_code'] = $truckRSideArtData[0]->Artwork_Code;

	$newArtWorkId = json_decode($truckDSideArtData[0]->Artwork_Transition);


	$ArtworkArray = array();
	if(!empty(json_decode($truckDSideArtData[0]->Artwork_Transition))){
		$newArtWorkId = json_decode($truckDSideArtData[0]->Artwork_Transition);
		$queryDSide2 = "SELECT Artwork_Code,Artwork,Artwork_Transition,Artwork_Transition FROM yfm_wp_AT_side_artwork WHERE Side_Artwork_id = '$newArtWorkId[0]'";
		$truckDSideArtData2 = $wpdb->get_results($queryDSide2, OBJECT);
		$ArtworkArray['D_Side_Art_Work'] = json_decode($truckDSideArtData2[0]->Artwork);
		$calData['D_Side_Art_code'] = $truckDSideArtData2[0]->Artwork_Code;
	}else{
		$ArtworkArray['D_Side_Art_Work'] = json_decode($truckDSideArtData[0]->Artwork);
	}

	if(!empty(json_decode($truckPSideArtData[0]->Artwork_Transition))){
		$newArtWorkId = json_decode($truckPSideArtData[0]->Artwork_Transition);
		$queryPSide2 = "SELECT Artwork_Code,Artwork,Artwork_Transition,Artwork_Transition FROM yfm_wp_AT_side_artwork WHERE Side_Artwork_id = '$newArtWorkId[0]'";
		$truckPSideArtData2 = $wpdb->get_results($queryPSide2, OBJECT);
		$ArtworkArray['P_Side_Art_Work'] = json_decode($truckPSideArtData2[0]->Artwork);
		$calData['P_Side_Art_code'] = $truckPSideArtData2[0]->Artwork_Code;
	}else{
		$ArtworkArray['P_Side_Art_Work'] = json_decode($truckPSideArtData[0]->Artwork);
	}

	if(json_decode($truckRSideArtData[0]->Artwork_Transition) != 'N'){
		$newArtWorkId = json_decode($truckRSideArtData[0]->Artwork_Transition);
		/*$queryRSide2 = "SELECT Artwork_Code,Artwork,Artwork_Transition,Artwork_Transition FROM yfm_wp_AT_rear_artwork WHERE Artwork_Code = '$newArtWorkId'";
		$truckRSideArtData2 = $wpdb->get_results($queryRSide2, OBJECT);
		$ArtworkArray['Rear_Art_Work']   = json_decode($truckRSideArtData2[0]->Artwork);
		$calData['Rear_Art_code']   = $truckRSideArtData2[0]->Artwork_Code;*/
		$ArtworkArray['Rear_Art_Work']   = $newArtWorkId;
		$calData['Rear_Art_code']   = $truckRSideArtData[0]->Artwork_Code;
	}else{
		$ArtworkArray['Rear_Art_Work']   = json_decode($truckRSideArtData[0]->Artwork);
	}

	$panelCode = [];
	foreach ($_POST['panel_selection'] as $key => $value) {
		if($value == 'driver_side'){
			$panelCode[] = "BD";
		}else if($value == 'passenger_side'){
			$panelCode[] = "BP";
		}else if($value == 'rear'){
			$panelCode[] = "BR";
		}else if($value == 'front'){
			$panelCode[] = "BF";
		}else if($value == 'cabin_front'){
			$panelCode[] = "CF";
		}else if($value == 'cabin_driver'){
			$panelCode[] = "CD";
		}else if($value == 'cabin_passenger'){
			$panelCode[] = "CP";
		}else if($value == 'cabin_internal'){
			$panelCode[] = "CI";
		}else if($value == 'body_internal'){
			$panelCode[] = "BI";
		}
	}



	// Wp Login user Data
	$wlud = wp_get_current_user();
  // echo '<pre>';
  // print_r($wlud);

	$taskData =[];
	$taskData=[
	    'fields'=>[
	   	'Truck_ID1'=>[$truck_data[0]->Truck_id],
	   	'Task_Type'=>'Alert',
	   		/*'Images'=>[
	   		['url'=>$url1],
	        ['url'=>$url2]
	   	 ],*/
	   	'Images'=>$taskImagesArr,
	   	 'Job_Type'=>'Report',
	     'Status'=>['Unassigned'],
	     'D_side_Artwork'=>$calData['D_Side_Art_code'],
	     'P_side_Artwork'=>$calData['P_Side_Art_code'],
	     'Rear_Artwork'=>$calData['Rear_Art_code'],
	     "Panels"=>$panelCode,
	     "Ordered_by"=> [$wlud->data->airtable_id],
	     "Task_Notes_1" =>  $ccomments
		]
	];
  // echo '<pre>';
  // print_r($taskData);

	$curl = curl_init();
	$airtableTaskUrl = "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Tasks";
	curl_setopt_array($curl, array(
	CURLOPT_URL => $airtableTaskUrl,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	//CURLOPT_POSTFIELDS => "{\n  \"fields\": {\n    \"Truck_ID1\": [\n      \"rec0z32nKnCHIQhzu\"\n    ],\n    \"Task_Type\": \"Repairs (Parts & Labour)\",\n    \"Images\": [\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC.jpg\"\n      },\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC-1.jpg\"\n      }\n    ],\n    \"Job Type\": [\n      \"Order\"\n    ],\n    \"Status\": \"Client response?\"\n  }\n}",
	CURLOPT_POSTFIELDS => json_encode($taskData),
	CURLOPT_HTTPHEADER => array(
	"authorization: Bearer keyw3cJfiAFXmYjJf",
	"cache-control: no-cache",
	"content-type: application/json",
	"postman-token: d6aa4070-c2cd-df5f-cd62-0f25e59be5e2"
	),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	$airtableResponse = json_decode($response);

	//echo "<prE>"; print_r($airtableResponse); echo "</pre>"; //die;

	if ($err) {

		$request['url'] = $airtableTaskUrl;
		$request['data'] = $taskData;

		$ajaxResponse['log']['request'] = "Create Task";
		$ajaxResponse['log']['request_data'] = wp_json_encode($request);
		$ajaxResponse['log']['status'] = "fail";
		$ajaxResponse['log']['response'] = "Curl request fail.";
		$ajaxResponse['log']['response_data'] = wp_json_encode($err);
		$ajaxResponse['log']['sent_by'] = $wlud->data->user_email;
		$ajaxResponse['log']['log_reference'] = strtotime(date('Y-m-d h:i:s'));
		$ajaxResponse['log']['created_date'] = date('Y-m-d h:i:s');
		//Update error log
		$api = $wpdb->insert('ymf_wp_API_log', $ajaxResponse['log']);

		if($api)
		{
			//echo 'logs inserted';
			$data['msg']= 'something went wrong.Contact with administrator with following reference : {'.$ajaxResponse['log']['log_reference'].'}';
			$data['status'] = 'fail';
		}
		echo wp_json_encode($data); exit;

	}else if(isset($airtableResponse->error)){

		$request['url'] = $airtableTaskUrl;
		$request['data'] = $taskData;

		$ajaxResponse['log']['request'] = "Create Task";
		$ajaxResponse['log']['request_data'] = wp_json_encode($request);
		$ajaxResponse['log']['status'] = "fail";
		$ajaxResponse['log']['response'] = "Airtable request error";
		$ajaxResponse['log']['response_data'] = wp_json_encode($airtableResponse);
		$ajaxResponse['log']['sent_by'] = $wlud->data->user_email;
		$ajaxResponse['log']['log_reference'] = strtotime(date('Y-m-d h:i:s'));
		$ajaxResponse['log']['created_date'] = date('Y-m-d h:i:s');
		//Update error log
		$api = $wpdb->insert('ymf_wp_API_log', $ajaxResponse['log']);

		if($api)
		{
			//echo 'logs inserted';
			$data['msg']= 'something went wrong.Contact with administrator with following reference : {'.$ajaxResponse['log']['log_reference'].'}';
			$data['status'] = 'fail';
		}
		echo wp_json_encode($data); exit;
	} else {

		$request['url'] = $airtableTaskUrl;
		$request['data'] = $taskData;
		$ajaxResponse['log']['request'] = "Create Task";
		$ajaxResponse['log']['request_data'] = wp_json_encode($request);
		$ajaxResponse['log']['status'] = "success";
		$ajaxResponse['log']['response'] = 'Tas created. <a href="https://airtable.com/tbla2QBLhnUjtZ2nx/viw8im6rKKLvvdRMI/'.$airtableResponse->id.'" target="_blank"> click here </a>';
		$ajaxResponse['log']['response_data'] = '';
		$ajaxResponse['log']['sent_by'] = $wlud->data->user_email;
		$ajaxResponse['log']['log_reference'] = strtotime(date('Y-m-d h:i:s'));
		$ajaxResponse['log']['created_date'] = date('Y-m-d h:i:s');
		//Update error log
		$api = $wpdb->insert('ymf_wp_API_log', $ajaxResponse['log']);
	}


	// Create Purchase order From Truck Report Form
	// Code Removed. No Need to Create Order With This Form

	if(isset($airtableResponse->id)){
    // echo '<pre>';
    // print_r($airtableResponse->fields);
    // echo '---------';
		$Task_Type=$airtableResponse->fields->Task_Type;
		$Status=$airtableResponse->fields->Status;
		//$D_side_Artwork=$response->fields->D_side_Artwork;
		//$P_side_Artwork=$response->fields->P_side_Artwork;
		//$Rear_Artwork=$response->fields->Rear_Artwork;
		//$Panels=$response->fields->Panels;
		$Truck_ID1=wp_json_encode($airtableResponse->fields->Truck_ID1);
		$Truck_ID2=wp_json_encode($airtableResponse->fields->Truck_ID2);
		$Client=wp_json_encode($airtableResponse->fields->Client);
		$Location=wp_json_encode($airtableResponse->fields->Location);
		$Job_No = $airtableResponse->fields->Job_No;
		$Order_Date=$airtableResponse->fields->Order_Date;
		$Images=wp_json_encode($airtableResponse->fields->Images);
		$Task_id=wp_json_encode($airtableResponse->id);
		$Ordered_by=wp_json_encode($airtableResponse->fields->Ordered_by);
		$Truck_id=$airtableResponse->fields->Truck_ID1[0];

		$table_name = 'yfm_wp_AT_tasks';
		$checkDataInsert=$wpdb->insert($table_name, array(
		    'Job_No' => $Job_No,
		    'Truck_ID1' => $Truck_ID1,
			'Truck_ID2' => $Truck_ID2,
		    'Task_Type'=> $Task_Type,
		    'Status'=>$Status,
		    'Location'=> $Location,
		    'Client'=>$Client,
		    'Order_Date'=>$Order_Date,
		    'Images'=>$Images,
		   	'Task_id'=>$Task_id,
		   	'Ordered_by'=> $Ordered_by,
		   	'Truck_id'=> $Truck_id
		));

		//echo $wpdb->last_query;
		if($checkDataInsert)
		{

			$tastLink = '<a href="https://airtable.com/tbla2QBLhnUjtZ2nx/viw8im6rKKLvvdRMI/'.$airtableResponse->id.'"> click here </a>';
			$contactLink = '<a href="https://airtable.com/tblfJ923QFFjr89w3/viwAXRAip5DGKfT5M/'.$wlud->data->airtable_id.'"> click here </a>';
			$consignmentAddress = 'Consignment Address : '.$wlud->data->airtable_address.'.';

			$subject = 'Your Fleet Media Truck Report';
			$to = 'service@yourfleetmedia.com';

			$body = '<div style="width:100%;background:#eee;margin: 0;padding:50px 0;height:100%;">';
			$body .= '<table style="margin:0 auto;padding:20px;width: 700px;background:#fff;border: 1px solid #ddd;">';

			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">Truck identification :</td><td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;"> '.$trukId1.' '.$trukId2.'</td></tr>';
			/*$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">Purchase Order Number :</td><td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;"> '.$ponum.'</td></tr>';*/

			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;"> Panels :</td> <td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.$mailPanelCodes.'</td></tr>';

			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;"> Truck Report Images :</td> <td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.$mailImageLinks.'</td></tr>';

			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;"> Task link <td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.$tastLink.'</td></tr>';
			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">Contact raised the request :</td><td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.$contactLink.'</td></tr>';
			//$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">Consignment Address </td><td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.$consignmentAddress.'</td></tr>';

			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;"> Comments: </td><td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.$postComments.'</td></tr>';

			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;"></td> <td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;"> </td></tr>';
			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 0px solid #ddd;"> Thanks, </td> </tr>';
			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 0px solid #ddd;"> Your Fleet Media </td> <tr>';

			$body .= '</table></div>';

			$headers = array('Content-Type: text/html; charset=UTF-8');
			$a = wp_mail( $to, $subject, $body, $headers);

			if($a){
				$data['msg']= 'Truck report submitted successfully';
				$data['status'] = 'success';
				$_SESSION['form-message'] = $data;
				echo wp_json_encode($data); exit;
			}else{
				$request['mail']['request'] = "mail sending";
				$request['mail']['response'] = "issue with mail sending";

				$ajaxResponse['log']['request'] = wp_json_encode($request['mail']['request']);
				$ajaxResponse['log']['status'] = "fail";
				$ajaxResponse['log']['response'] = wp_json_encode($request['mail']['response']);
				$ajaxResponse['log']['sent_by'] = $wlud->data->user_email;
				$ajaxResponse['log']['log_reference'] = strtotime(date('Y-m-d h:i:s'));
				$ajaxResponse['log']['created_date'] = date('Y-m-d h:i:s');
				//Update error log
				$api = $wpdb->insert('ymf_wp_API_log', $ajaxResponse['log']);

				if($api)
				{
					//echo 'logs inserted';
					$data['msg']= 'something went wrong.Contact with administrator with following reference : {'.$ajaxResponse['log']['log_reference'].'}';
					$data['status'] = 'fail';
				}
				echo wp_json_encode($data); exit;
			}

		}else
		{
			$request['wp']['response'] = $wpdb->last_query;
			$request['wp']['request'] = "Wordpress task entry";

			$ajaxResponse['log']['request'] = wp_json_encode($request['wp']['request'] );
			$ajaxResponse['log']['status'] = "fail";
			$ajaxResponse['log']['response'] = wp_json_encode($request['wp']['response']);
			$ajaxResponse['log']['sent_by'] = $wlud->data->user_email;
			$ajaxResponse['log']['log_reference'] = strtotime(date('Y-m-d h:i:s'));
			$ajaxResponse['log']['created_date'] = date('Y-m-d h:i:s');
			//Update error log
			$api = $wpdb->insert('ymf_wp_API_log', $ajaxResponse['log']);

			if($api)
			{
				//echo 'logs inserted';
				$data['msg']= 'something went wrong.Contact with administrator with following reference : {'.$ajaxResponse['log']['log_reference'].'}';
				$data['status'] = 'fail';
			}
			echo wp_json_encode($data); exit;
		}

    }

}
?>
