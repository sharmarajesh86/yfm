<?php /**************************************************************************************
*
* This file consists of the code to create the shortcode that is used in this plugin 
* to create the login forms.
*
*********************************************************************************************/
/********* Create Login Form Shortcode********/

function forgetpassForm_creation(){
	
	if(!is_user_logged_in()){ 
	
	include( plugin_dir_path( __FILE__ ) .'../includes/include.php'); // include scripts and styles
		?>

	    	<div class="login-form">
	      		<div class="main-div">
			        <div class="panel">
			          <h2>Forget Password</h2>
			          <?php if(isset($_GET['action'])&&$_GET['action'] == 'forget')
			          {
			          	echo '<p class="confirm-msg">Check your email for the confirmation link.</p>';
			          	$headers = 'From: YFM <myname@example.com>' . "\r\n";
						$mail = wp_mail( 'ritika@1wayit.com', 'subject', 'message', $headers );
						if($mail)
						{
							echo "mail sent";
						}
						else{
							echo "mail not sent";
						}
			          } else { ?>
			          <p class="msg">Please enter your username or email address. You will receive a link to create a new password via email.</p>
			      <?php } ?>
			        </div>
			        <form name="lostpasswordform" id="lostpasswordform" action="<?php echo wp_lostpassword_url(); ?>" method="post">
			          <div class="form-group">
			            <input type="text" class="form-control input" name="user_login" id="user_login" placeholder="Username or Email Address">
			          </div>
			       
			          <button type="submit" name="wp-submit" id="wp-submit" class="forgetPass btn btn-primary btn-block">Get New Password</button>
			          <input type="hidden" name="redirect_to" value="<?php echo esc_url(home_url('/forget-password?action=forget')); ?>">
			        </form>
	      		</div>
	    	</div>

<?php	}
else{
	echo "You are already logged in! Click here to <a href='".wp_logout_url( home_url() )."'>logout</a>";
}


}
add_shortcode('forget_password', 'forgetpassForm_creation'); // EO Register Form shortcode
?>
