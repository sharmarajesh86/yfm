<?php /**************************************************************************************
*
* This file consists of the code to create the shortcode that is used in this plugin to create the Overview Order form.
* It will further links to other forms after user logged in.
*
*********************************************************************************************/
/********* Create Dashboard Shortcode********/

function orderForm_creation(){
	
	//if(is_user_logged_in()){

	include( plugin_dir_path( __FILE__ ) . '../airtables/get-trucks.php'); // get companies list
	include( plugin_dir_path( __FILE__ ) .'../includes/include.php'); // include scripts and styles ?>
<?php    $taskData =[];
        //$taskData['fields']['Truck_ID1'][] = 'rec0z32nKnCHIQhzu';
        //$taskData['fields']['Task_Type'][] = 'Repairs(Parts only)';
        //$taskData['fields']['Task_Type'] = 'Repairs(Parts only)';
        //$taskData['fields']['Job Type'][] = 'Order';
        //$taskData['fields']['Status'] = 'Unassigned';
        //$taskData['fields']['Images']['url'][] = 'http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC.jpg';
        //$taskData['fields']['Images']['url'][] = 'http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC-1.jpg';

	    //echo "<pre>";	print_r(json_encode($taskData)); die;

          $taskData=[
               'fields'=>[
               	'Truck_ID1'=>['rec0z32nKnCHIQhzu'],
               	'Task_Type'=>'Repairs (Parts & Labour)',
               	'Images'=>[
               		['url'=>'http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC.jpg'],
                    ['url'=>'http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC-1.jpg']
               	 ],
               	 'Job Type'=>['Order'],
                 'Status'=>'Unassigned'        
            ]
       ];

       echo json_encode( $taskData); die;


          $curl = curl_init();

		  curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.airtable.com/v0/apprTuWAjlI7FBBPb/Tasks",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  //CURLOPT_POSTFIELDS => "{\n  \"fields\": {\n    \"Truck_ID1\": [\n      \"rec0z32nKnCHIQhzu\"\n    ],\n    \"Task_Type\": \"Repairs (Parts & Labour)\",\n    \"Images\": [\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC.jpg\"\n      },\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC-1.jpg\"\n      }\n    ],\n    \"Job Type\": [\n      \"Order\"\n    ],\n    \"Status\": \"Client response?\"\n  }\n}",
		  CURLOPT_POSTFIELDS => json_encode($taskData),
		  CURLOPT_HTTPHEADER => array(
		    "authorization: Bearer keyoA5KNgdkmd3iDG",
		    "cache-control: no-cache",
		    "content-type: application/json",
		    "postman-token: d6aa4070-c2cd-df5f-cd62-0f25e59be5e2"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		}

        print_r(wp_json_encode($response)); die;

 ?>
    <section id="main-content">  
    	<div id="msg"></div>
		<div class="notes">
			<h4>Fields marked with <sup>*</sup> are mandatory.</h4>
		</div>
    	<form id="overviewOrder" name="overviewOrder" action="#" method="post">
    		<?php //echo "<pre>"; print_r($trucks->records); echo "</pre>"; ?>
	    	<div class="row">
	    		<div class="col-md-12">
	        		<div class="form_panel">
						<div class="form-group">
							<div class="row">
								<div class="col-md-3">
									<label>Truck Identification<sup>*</sup></label>
								</div>
								<div class="col-md-9">	
									<select class="form-control success" name="truck_identification" id="truck_identification">
					                	<option>--Select Truck--</option>           
							        	<?php
										for($i=0; $i<count($trucks->records)-1;$i++) {
											echo '<option value="'.$trucks->records[$i]->fields->ID1.'" data-id="'.$trucks->records[$i]->id.'">'.$trucks->records[$i]->fields->ID1.' - '.$trucks->records[$i]->fields->ID2.'</option>';
										}			
										?>
					                </select>
								</div>			
							</div>			
						</div>
						<div class="form-group">
						 	<div class="row">
								<div class="col-md-3">
									<label>Purchase Order Number<sup>*</sup></label>
								</div>
								<div class="col-md-9">
									<input type="text" class="form-control" name="purchase_order_num" id="purchase_order_num" >
								</div>			
							</div>			
						</div>
						<div class="form-group">
						 	<div class="row">
								<div class="col-md-3">
									<label>Panel Selection<sup>*</sup></label>
								</div>
								<div class="col-md-9">
									<div class="form-group">
					                	<label class="Check_box">Driver Side
					                      <input type="checkbox" name="panel_selection[]" class="panelchk" value="driver_side" panel_type="BD">
					                      <span class="checkmark"></span>
					                    </label>
					                    <label class="Check_box">Passenger Side
					                      <input type="checkbox" name="panel_selection[]" class="panelchk" value="passenger_side" panel_type="BP">
					                      <span class="checkmark"></span>
					                    </label>
					                    <label class="Check_box">Rear
					                      <input type="checkbox" name="panel_selection[]" class="panelchk" value="rear" panel_type="BR">
					                      <span class="checkmark"></span>
					                    </label>
					                    <!--label class="Check_box">Front
					                      <input type="checkbox" name="panel_selection[]" value="front" panel_type="BF">
					                      <span class="checkmark"></span>
					                    </label>
					                    <label class="Check_box">Cabin Front
					                      <input type="checkbox" name="panel_selection[]" value="cabin_front" panel_type="CF">
					                      <span class="checkmark"></span>
					                    </label>
					                    <label class="Check_box">Cabin Driver
					                      <input type="checkbox" name="panel_selection[]" value="cabin_driver" panel_type="CD">
					                      <span class="checkmark"></span>
					                    </label>
					                    <label class="Check_box">Cabin Passenger
					                      <input type="checkbox" name="panel_selection[]" value="cabin_passenger" panel_type="CP">
					                      <span class="checkmark"></span>
					                    </label>
					              		<label class="Check_box">Cabin Internal
					                      <input type="checkbox" name="panel_selection[]" value="cabin_internal" panel_type="CI">
					                      <span class="checkmark"></span>
					                    </label>
										<label class="Check_box">Body Internal
					                      <input type="checkbox" name="panel_selection[]" value="body_internal" panel_type="BI">
					                      <span class="checkmark"></span>
					                    </label-->
					                </div>
								</div>			
							</div>			
						</div>
						<div class="form-group">
						 	<div class="row">
								<div class="col-md-3">
									<label>Photograph damage-1<sup>*</sup></label>
								</div>
								<div class="col-md-9">
									<input type="file" class="form-control" name="damage_1" />
					                <span>Attach Photograph</span>
								</div>			
							</div>			
						</div>
						<div class="form-group">
						 	<div class="row">
								<div class="col-md-3">
									<label>Photograph damage-2<sup>*</sup></label>
								</div>
								<div class="col-md-9">
									<input type="file" class="form-control" name="damage_2" />
					                <span>Attach Photograph</span>
								</div>			
							</div>			
						</div>
						<hr>
						<div class="form-group hide" id="driver_side">
						 	<div class="row">
								<div class="col-md-3">
									<label>Part Names - Driver Side<sup>*</sup></label>
								</div>
								<div class="col-md-9">
									<select name="driver_side" id="driver_side_part" multiple>						
									</select>									
								</div>			
							</div>			
						</div>	
						<div class="form-group hide" id="driver_side_parts">
						 	<div class="row">
								<div class="col-md-3">
									<label>Driver Side Parts Quantity<sup>*</sup></label>
								</div>
								<div class="col-md-9" id="driver_side_parts_qty">								
										
								</div>			
							</div>			
						</div>	
						<hr>				
						<div class="form-group hide" id="passenger_side">
						 	<div class="row">
								<div class="col-md-3">
									<label>Part Names - Passenger Side<sup>*</sup></label>
								</div>
								<div class="col-md-9">
									<select name="passenger_side" id="passenger_side_part" multiple>						
									</select>									
								</div>			
							</div>			
						</div>
						<div class="form-group hide" id="passenger_side_parts">
						 	<div class="row">
								<div class="col-md-3">
									<label>Passenger Side Parts Quantity<sup>*</sup></label>
								</div>
								<div class="col-md-9" id="passenger_side_parts_qty">								
										
								</div>			
							</div>			
						</div>	
						<hr>
				        <div class="form-group hide" id="rear">
						 	<div class="row">
								<div class="col-md-3">
									<label>Part Names - Rear<sup>*</sup></label>
								</div>
								<div class="col-md-9">
									<select name="rear" id="rear_part" multiple>						
									</select>									
								</div>			
							</div>			
						</div>
						<div class="form-group hide" id="rear_side_parts">
						 	<div class="row">
								<div class="col-md-3">
									<label>Rear Side Parts Quantity<sup>*</sup></label>
								</div>
								<div class="col-md-9" id="rear_side_parts_qty">								
										
								</div>			
							</div>			
						</div>	
						<hr>
						<!--div class="form-group hide" id="front">
						 	<div class="row">
								<div class="col-md-3">
									<label>Part Names - Front<sup>*</sup></label>
								</div>
								<div class="col-md-9">
									<select name="front" id="front_part" multiple>						
									</select>									
								</div>			
							</div>			
						</div>
						<div class="form-group hide" id="cabin_front">
						 	<div class="row">
								<div class="col-md-3">
									<label>Part Names - Cabin Front<sup>*</sup></label>
								</div>
								<div class="col-md-9">
									<select name="cabin_front" id="cabin_front_part" multiple>						
									</select>									
								</div>			
							</div>			
						</div>
						<div class="form-group hide" id="cabin_driver">
						 	<div class="row">
								<div class="col-md-3">
									<label>Part Names - Cabin Driver<sup>*</sup></label>
								</div>
								<div class="col-md-9">
									<select name="cabin_driver" id="cabin_driver_part" multiple>						
									</select>									
								</div>			
							</div>			
						</div>
						<div class="form-group hide" id="cabin_passenger">
						 	<div class="row">
								<div class="col-md-3">
									<label>Part Names - Cabin Passenger<sup>*</sup></label>
								</div>
								<div class="col-md-9">
									<select name="cabin_passenger" id="cabin_passenger_part" multiple>						
									</select>									
								</div>			
							</div>			
						</div>
						<div class="form-group hide" id="cabin_internal">
						 	<div class="row">
								<div class="col-md-3">
									<label>Part Names - Cabin Internal<sup>*</sup></label>
								</div>
								<div class="col-md-9">
									<select name="cabin_internal" id="cabin_internal_part" multiple>						
									</select>									
								</div>			
							</div>			
						</div>
						<div class="form-group hide" id="body_internal">
						 	<div class="row">
								<div class="col-md-3">
									<label>Part Names - Body Internal<sup>*</sup></label>
								</div>
								<div class="col-md-9">
									<select name="body_internal" id="body_internal_part" multiple>						
									</select>									
								</div>			
							</div>			
						</div-->

				        <div class="form-group">
						 	<div class="row">
								<div class="col-md-3">
									<label>Do you require installation?<sup>*</sup></label>
								</div>
								<div class="col-md-9">
									<div class="radio radio-primary">
					                    <input type="radio" name="installation" id="radio1" value="option1">
					                    <label for="radio1">Yes</label>
					                </div>
					                <div class="radio radio-primary">
					                    <input type="radio" name="installation" id="radio2" value="option2">
					                    <label for="radio2">No</label>
					                </div>
								</div>			
							</div>			
						</div>
						
						<div id="qtyhtml">							

						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-md-9" id="caldata">
								</div>
								<div class="col-md-3">
								</div>
							</div>
						</div>

						<div class="form-group text-right">
							<input type="submit" class="btn btn-primary overview-order" value="Submit" style="display:none;">
							<input type="button" class="btn btn-primary show-qty-popup" value="Submit">							
						</div>
					</div>
	        	</div>
	       	</div>
		</form>
	</section>

	<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#qtyModal">Open Modal</button>

	<!-- Modal -->
	<div id="qtyModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Parts Quantity</h4>
	      </div>
	      <div class="modal-body">
	    		<div class="row">
		    		<div class="col-md-12">
		        		<div class="form_panel" id="partqtysection">
		        			<!--
		        			<h3> BP PArts </h3>
							<div class="form-group">
							 	<div class="row">
									<div class="col-md-6">
										<label>Tension Profile - Horizontal<sup>*</sup></label>
									</div>
									<div class="col-md-6">
										<input type="text" class="form-control" name="purchase_order_num" id="purchase_order_num" >
									</div>			
								</div>			
							</div>														
							-->
						</div>
		        	</div>
	       		</div>
	      </div>
	      <div class="modal-footer">
	        <!--button type="button" class="btn btn-default" data-dismiss="modal">Close</button-->
	        <input type="button" class="btn btn-primary popup-submit" value="Submit">							
	      </div>
	    </div>

	  </div>
	</div>

	<?php /*}else{
		echo do_shortcode('[login_form]');
	}*/
}
add_shortcode('order_form', 'orderForm_creation'); // EO Register Form shortcode

add_action('wp_ajax_parts_list', 'part_list');
add_action('wp_ajax_nopriv_parts_list', 'part_list');

function part_list()
{	

	global $wpdb;

	$panel_type = $_POST['panel_type'];
	$truck = $_POST['truck'];

	$querystr = "SELECT * FROM yfm_wp_AT_parts_list WHERE Panel = '$panel_type' ORDER BY id DESC";          
	$parts_data = $wpdb->get_results($querystr, OBJECT);

	$queryTruck = "SELECT Truck_Type,D_Side_Art_code,P_Side_Art_code,Rear_Art_code FROM yfm_wp_AT_trucks WHERE ID1 = '$truck'";          
	$truck_data = $wpdb->get_results($queryTruck, OBJECT);

	//echo "<pre>"; print_r($truck_data); die;

	$truckType = str_replace('["', '', $truck_data[0]->Truck_Type);	
	$truckType = str_replace('"]', '', $truckType);	
	
	$queryTruckType = "SELECT Truck_Type,$panel_type,Bodies FROM yfm_wp_AT_truck_types WHERE Truck_Types_id = '$truckType'";
	$truckTypeData = $wpdb->get_results($queryTruckType, OBJECT);

	//echo "<pre>"; print_r($truckTypeData); //die;

	#Get Boddy Data
	$truckBody = str_replace('["', '', $truckTypeData[0]->Bodies);	
	$truckBody = str_replace('"]', '', $truckBody);	

	$queryTruckBody = "SELECT * FROM yfm_wp_AT_bodies WHERE Bodies_id = '$truckBody'";
	$truckBodyData = $wpdb->get_results($queryTruckBody, OBJECT);

	#Get Side Art/Rear Art Data
	if ($panel_type == 'BP' && $truck_data[0]->P_Side_Art_code != '""') {
		$partCodeId = str_replace('["', '', $truck_data[0]->P_Side_Art_code);	
		$pSideArtCodeId = str_replace('"]', '', $partCodeId);
		$queryPSide = "SELECT Artwork_Code,Artwork_Transition FROM yfm_wp_AT_side_artwork WHERE Side_Artwork_id = '$pSideArtCodeId'";
		$truckPSideArtData = $wpdb->get_results($queryPSide, OBJECT);
	}

	if ($panel_type == 'BD' && $truck_data[0]->D_Side_Art_code != '""') {
		$dartCodeId = str_replace('["', '', $truck_data[0]->D_Side_Art_code);	
		$dSideArtCodeId = str_replace('"]', '', $dartCodeId);	
		$queryDSide = "SELECT Artwork_Code,Artwork_Transition FROM yfm_wp_AT_side_artwork WHERE Side_Artwork_id = '$dSideArtCodeId'";
		$truckDSideArtData = $wpdb->get_results($queryDSide, OBJECT);
	}
	if ($panel_type == 'BR' && $truck_data[0]->Rear_Art_code != '""') {
		$rartCodeId = str_replace('["', '', $truck_data[0]->Rear_Art_code);	
		$rSideArtCodeId = str_replace('"]', '', $rartCodeId);	
		$queryRSide = "SELECT Artwork_Code,Artwork_Transition FROM yfm_wp_AT_rear_artwork WHERE Rear_Artwork_id = '$rSideArtCodeId'";
		$truckRSideArtData = $wpdb->get_results($queryRSide, OBJECT);
	}
	

	//echo $artCodeId; die;

	/*
	$calData['BP_Length'] = $truckBodyData[0]->BP_Length;
	$calData['BP_Height'] = $truckBodyData[0]->BP_Length;
	$calData['BD_Length'] = $truckBodyData[0]->BD_Length;
	$calData['BD_Height'] = $truckBodyData[0]->BD_Height;
	$calData['BR_Length'] = $truckBodyData[0]->BR_Length;
	$calData['BR_Height'] = $truckBodyData[0]->BR_Height;
	*/

	#Calculation Data 
	$calData = array();
	$calData['truckCode'] = $truckTypeData[0]->Truck_Type;
	$calData['D_Side_Art_code'] = $truckDSideArtData[0]->Artwork_Code;
	$calData['P_Side_Art_code'] = $truckPSideArtData[0]->Artwork_Code;
	$calData['Rear_Art_code'] = $truckRSideArtData[0]->Artwork_Code;
	$calData['panelCode'] = $panel_type;
	$calData['signage'] = $truckTypeData[0]->$panel_type;

	$l = $panel_type.'_Length';
	$h = $panel_type.'_Height';
	$calData[$l] = $truckBodyData[0]->$l;
	$calData[$h] = $truckBodyData[0]->$h;


	$queryPanelCals = "SELECT * FROM `yfm_wp_AT_panel_calcs` WHERE `Signage` LIKE '".$truckTypeData[0]->$panel_type."' ";
	$truckPanelCals = $wpdb->get_results($queryPanelCals, OBJECT);

	//echo "<pre>"; print_r($truckPanelCals); die;

	#PanelCals
	$calculation = array();
	foreach ($truckPanelCals as $key => $value) {
		$panelCals[$value->Profile_Code] = $value->Calculation;
		$customeKey = $panel_type.$value->Profile_Code;
		#Calculation
		if($value->Profile_Code == 'TP'){
			$calculation[$customeKey] = $truckBodyData[0]->$l + $value->Calculation;
		}else if($value->Profile_Code == 'HBP'){
			$calculation[$customeKey] = $truckBodyData[0]->$l + $value->Calculation;
		}else if($value->Profile_Code == 'VBP'){
			$calculation[$customeKey] = $truckBodyData[0]->$h + $value->Calculation;
		}else if($value->Profile_Code == 'HTP'){
			$calculation[$customeKey] = $truckBodyData[0]->$l + $value->Calculation;
		}else if($value->Profile_Code == 'VTP'){
			$calculation[$customeKey] = $truckBodyData[0]->$h + $value->Calculation;
		}else if($value->Profile_Code == 'BL'){
			$calculation[$customeKey] = $truckBodyData[0]->$l + $value->Calculation;
		}else if($value->Profile_Code == 'BH'){
			$calculation[$customeKey] = $truckBodyData[0]->$h + $value->Calculation;
		}else{
			$calculation[$customeKey] = 'NA';
		}
	}
	 
	//echo "<pre>"; print_r($calculation); die;	
 	if(empty($parts_data))
 	{
 		$querystr = "SELECT * FROM yfm_wp_AT_parts_list WHERE Panel = 'Gen' ORDER BY id DESC"; 
 		$parts_data = $wpdb->get_results($querystr, OBJECT);
 	}
 	
 	$res = array();
	$res['calData']   = $calData;
	//$res['calDimensions']   = $calDimensions;
	$res['panelCals'] = $panelCals;
	$res['partsData'] = $parts_data;
	$res['calculation'] = $calculation;

	//echo "<pre>"; print_r($res); die;

 	echo wp_json_encode($res);exit;
 	

 	
}

add_action('wp_ajax_overviewOrder', 'overviewOrder');
add_action('wp_ajax_nopriv_overviewOrder', 'overviewOrder');

	function overviewOrder(){       


        global $wpdb;

        //$myrows = $wpdb->get_results( "SELECT Job_No FROM `yfm_wp_AT_tasks` ORDER BY `Job_No` DESC LIMIT 1" );
		//print_r($myrows);die('Test');

		//$Job_Num = $myrows[0]->Job_No;
		  
		//$Truck_ID1 = $_POST['truck_identification'];
	
		//print_r($Truck_ID1); die();
		//$Job_No = $_POST['purchase_order_num'];
		//$t=explode(" - ",$Truck_ID1);
		//$truckId1=$t[0];
		//$truckId2=$t[1];

		//$truckid1 = wp_json_encode($truckId1);
		//$truckid2 = wp_json_encode($truckId2);
		

        $taskData = array();
        $taskData['fields']['Truck_ID1'] = 'rec0z32nKnCHIQhzu';
        $taskData['fields']['Task_Type'] = 'Repairs(Parts only)';
        $taskData['fields']['Job Type'] = 'Order';
        $taskData['fields']['Status'] = 'Unassigned';
        $taskData['fields']['Images']['url'] = 'http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC.jpg';
        $taskData['fields']['Images']['url'] = 'http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC-1.jpg';

		
		if ( ! function_exists( 'wp_handle_upload' ) ) {
    		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}

			$upload_overrides = array( 'test_form' => false );

			if(!empty($_FILES['damage_1'])){
				$uploadedfile = $_FILES['damage_1'];
				$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );	
			}

			if(!empty($_FILES['damage_2'])){
				$uploadedfile2 = $_FILES['damage_2'];				
				$movefile = wp_handle_upload( $uploadedfile2, $upload_overrides );	
			}

			

			if ( $movefile && ! isset( $movefile['error'] ) ) {
			    echo "File is valid, and was successfully uploaded.\n";
			    var_dump( $movefile );
			} else {
			    /**
			     * Error generated by _wp_handle_upload()
			     * @see _wp_handle_upload() in wp-admin/includes/file.php
			     */
			    echo $movefile['error'];
			}


		$Task_Type=$_POST['task_Type'];

		$Status=$_POST['status'];
		$D_side_Artwork=$_POST['D_side_Artwork'];
		$P_side_Artwork=$_POST['P_side_Artwork'];
		$Rear_Artwork=$_POST['Rear_Artwork'];
		$Panels=$_POST['Panels'];
		$Installer=$_POST['radioVal'];
		//$Installer2=$_POST['Installer1'];

		// $Installer=$_POST['Installer2'];
		$date=$_POST['Install_Date'];
		

		

		  $table_name = 'yfm_wp_AT_tasks';

					$sql=$wpdb->insert($table_name, array(
					   'Job_No' => $Job_Num+1,
					    'Truck_ID1' => $truckid1,
						 'Truck_ID2' => $truckid2,
					    'Task_Type'=> $Task_Type,
					    'Status'=>$Status,
					    'D_side_Artwork'=> wp_json_encode($D_side_Artwork),
					     'P_side_Artwork'=> wp_json_encode($P_side_Artwork),
					    'Rear_Artwork'=> wp_json_encode($Rear_Artwork),
					    'Panels'=> wp_json_encode($Panels),
					  'Installer'=>wp_json_encode($Installer),
					    //'Installer1'=>wp_json_encode($Installer2),
					    'Install_Date'=>$date,
					   
					   'Images'=>wp_json_encode($movefile),
					   
					));

					if($wpdb->query($sql)) 
		               {
		              echo "yes";
		               }
		               else
		               {
		               	echo "no";
		               }
		
}

?>