<?php /**************************************************************************************
*
* This file consists of the code to create the shortcode that is used in this plugin to create the registration forms.
* It is used to create the registration form, create users in wordpress admin and update user metadata like firstname and lastname etc.
*
*********************************************************************************************/

/********* Create Register Form Shortcode********/

function registerform_creation(){
	if(!is_user_logged_in()){
		//include( plugin_dir_path( __FILE__ ) . '../airtables/get-companies.php'); // get companies list
		global $wpdb;
		$companyDataQry = "SELECT CompanyCode,CompanyName,company_id FROM yfm_wp_AT_companies where CompanyCode != ''";
		$companyData = $wpdb->get_results($companyDataQry, OBJECT);

		//echo "<prE>"; print_r($companyData); die;

		//include( plugin_dir_path( __FILE__ ) .'../airtables/get-locations.php'); // get locations list
		//$locationDataQry = "SELECT Location_id,Fulladdress FROM yfm_wp_AT_locations";
		//$locationData = $wpdb->get_results($locationDataQry, OBJECT);
		$locationData = array();
		include( plugin_dir_path( __FILE__ ) .'../includes/include.php'); // include scripts and styles

		// echo "<pre>";
		// 	print_r($locationData );
		// echo "</pre>";
	?>
	<div class="maskArea">
		<div class="popup">
			<p>Please enter OTP sent to your mobile number.</p>
			<form action="" method="post">
				<div class="form-group">
					<label style="font-weight: bold;margin-top: 12px;">Enter OTP</label>
					<input type="text" name="verify_otp" id="verify_otp" value="" class="form-control success" placeholder="Enter OTP" required>
					<span class="invalidOtp">Invalid OTP.</span>
					<span class="otpIssue"></span>
					<span class="optEmpty">Please enter your OTP.</span>
				</div>
				<div class="text-center">
					<button type="button" id="verify_now" class="btn btn-orange btn-primary submitUser">Verify and Register</button>
				</div>
			</form>

		</div>
	</div>
		<div class="login-panel">
		  <div class="container">
		    <div class="row ">
		    	<div class="col-sm-5">
		        <div class="login-detail register">
		        	<h1>Register Now</h1>
							<div id="msg"></div>
							<!-- <div class="notes">
								<h4>Fields marked with <sup>*</sup> are mandatory.</h4>
							</div> -->
							<form name="registerform" id="registerform" method="post" action="#">
								<input type="hidden" name="action" value="register_form">
								<div class="form-group" style="position:relative;">
									<label>Username (email)</label>
									<input type="email" name="user_email" id="user_email" value="" class="form-control success req" placeholder="Email Address">
									<span class="emailAE">Email id already exists.</span>
								</div>
								<div class="form-group">
									<label>Password</label>
									<input type="password" name="credentials" id="credentials" value="" class="form-control success req" placeholder="Password">
								</div>
								<div class="form-group">
									<label>Full Name</label>
									<input type="text" name="full_name" id="full_name" class="form-control success req" placeholder="Full Name">
								</div>
								<div class="form-group">
									<label>Company</label>
									<select class="form-control success chosen-select req" name="company" id="company">
					          <option>--Select Company--</option>
					          <option value="other">Other - register a new company</option>
							      <?php
											/*for($i=0; $i<count($data->records)-1;$i++) {
												echo '<option value="'.$data->records[$i]->fields->CompanyCode.'" data-id="'.$data->records[$i]->id.'">'.$data->records[$i]->fields->CompanyName.'</option>';
											}*/
											foreach ($companyData as $key => $value) {
												echo '<option value="'.$value->CompanyCode.'" data-id="'.$value->company_id.'">'.$value->CompanyName.'</option>';
											}
										?>
					        </select>
								</div>
								<div class="other-company" style="display: none">
									<div class="form-group">
										<label>Company Name</label>
										<input type="text" class="form-control" placeholder="Company Name" name="company_name" id="company_name">
										<input type="hidden" name="company_id" id="company_id" value="">
										<input type="hidden" name="company_code" id="company_code" value="">
										<input type="hidden" name="company_type" id="company_type" value="">
										<input type="hidden" name="company_comments" id="company_comments" value="">
										<input type="hidden" name="company_contacts" id="company_contacts" value="">
										<input type="hidden" name="company_rating" id="company_rating" value="">
										<input type="hidden" name="web_link_co_code" id="web_link_co_code" value="">
									</div>
									<div class="form-group">
										<label>Website (Optional)</label>
										<label for="website" class="website-label">
											<select name="link_type" id="link_type">
												<option value="0" data-title="http">http://</option>
												<option value="1" data-title="https">https://</option>
											</select>
										</label>
										<input type="text" class="form-control" name="Website" placeholder="www.example.com" id="website">
									</div>
									<div class="form-group">
										<!-- <label>Address Details</label>	 -->
										<label><span>Street Address</span></label>
										<input type="text" class="form-control" placeholder="Street Address" name="street_address" id="street_address">
									</div>
								  <div class="form-group">
										<label><span>Street Address Line 2</span>	</label>
										<input type="text" class="form-control" placeholder="Street Address Line 2" id="street_address_2">
									</div>
								  <div class="form-group">
										<label><span>Suburb</span></label>
									  <input type="text" class="form-control" placeholder="Suburb" name="suburb" id="suburb">
									</div>
									<div class="form-group">
									  <label><span>State</span></label>
									  <select class="form-control" id="state" name="state">
											<option>Select State</option>
											<option value="SA">SA</option>
											<option value="NSW">NSW</option>
											<option value="VIC">VIC</option>
											<option value="WA">WA</option>
											<option value="NT">NT</option>
											<option value="QLD">QLD</option>
											<option value="TAS">TAS</option>
											<option value="ACT">ACT</option>
										</select>
									</div>
									<div class="form-group">
										<label><span>Post Code</span></label>
										<input type="text" class="form-control" placeholder="Post Code" name="post_code" id="post_code">
									</div>
								</div>
								<div class="form-group" id="locationsection">
									<label>Location</label>
									<select class="form-control success" name="locations" id="locations"> <!-- chosen-select-location -->
						        <option>--Select Location--</option>
						        <option value="other">Other - Add a new location</option>
										<?php
												/*for($i=0; $i<count($get_locations->records)-1;$i++) {
												if(!empty($get_locations->records[$i]->fields->ID1))
													{
														echo '<option value="'.$get_locations->records[$i]->id.'">'.$get_locations->records[$i]->fields->Fulladdress.'</option>';
													}
												}*/
												foreach ($locationData as $key => $value) {
													echo '<option value="'.$value->Location_id.'">'.$value->Fulladdress.'</option>';
												}
										?>
						      </select>
						      <input type="hidden" name="location_id" id="location_id" value="">
								</div>
								<div class="location-data" style="display: none;">
									<div class="form-group">
										<label class="lbl-bold"><!-- Address Details <br>-->Street Address</label>
										<input type="text" class="form-control" placeholder="Street Address" name="street_address1" id="street_address1">
									</div>
					        <div class="form-group">
										<label class="lbl-bold"><span>Street Address Line 2</span></label>
										<input type="text" class="form-control" placeholder="Street Address Line 2" id="street_address_2_1" name="street_address_2_1">
									</div>
								  <div class="form-group">
										<label class="lbl-bold">Suburb</label>
									  <input type="text" class="form-control" placeholder="Suburb" name="suburb1" id="suburb1">
									</div>
							    <div class="form-group">
						        <label class="lbl-bold">State</label>
						        <select class="form-control" id="state1" name="state1">
					            <option>Select State</option>
					            <option value="SA">SA</option>
											<option value="NSW">NSW</option>
											<option value="VIC">VIC</option>
											<option value="WA">WA</option>
											<option value="NT">NT</option>
											<option value="QLD">QLD</option>
											<option value="TAS">TAS</option>
											<option value="ACT">ACT</option>
										</select>
							    </div>
									<div class="form-group">
										<label class="lbl-bold">Post Code</label>
										<input type="text" class="form-control" placeholder="Post Code" name="post_code1" id="post_code1">
									</div>
								</div> <?php /*****EO location-data*******/?>
								<div class="form-group">
										<label>Phone</label>
										<input type="tel" name="phone" value="" maxlength="16" id="phone" class="form-control success req mbCheckNm" placeholder="">
										<span id="valid-msg" class="hide">✓ Valid</span>
									<span id="error-msg" class="hide"></span>
								</div>
						    <div class="form-group">
									<label>Phone</label>
									<input type="tel" name="phone21" value="" maxlength="16" id="phone2" class="form-control phoneNo success" placeholder="Landline">
								</div>
								<!--<div class="text-center submitBtn">
									<input type="submit" name="wp-submit" id="wp-submit" class="btn btn-orange btn-primary rgisterUser" value="Register">
								</div>-->
								<div class="text-center submitBtn">
									<button type="button" class="btn btn-orange btn-primary rgisterUser" id="veryfyMobile">Next Step</button>
								</div>
						    <div id="loader" style="display: none;"></div>
						    <div id="overlay" style="display: none;"></div>
							</form>
						</div>
					</div>
					<div class="col-sm-7">
		        <div class="register-right">
		        	<img src="<?php echo get_stylesheet_directory_uri();?>/img/truck-img.jpg" alt="truck-img" />
		          <h2>Unleash the power of your fleet with high impact truck advertising campaigns</h2>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
	<?php
	} else{
		echo "You are already logged in! Click here to <a href='".wp_logout_url( home_url() )."'>logout</a>";
	}
}
add_shortcode('find_locations', 'registerform_creation'); // EO Register Form shortcode

add_action('wp_ajax_find_locations', 'find_locations');
add_action('wp_ajax_nopriv_find_locations', 'find_locations');

function find_locations()
{
	global $wpdb;
	$cid = $_POST['companyid'];

	$queryLocation = "SELECT Locations FROM yfm_wp_AT_companies where company_id = '$cid'";
	$locations = $wpdb->get_results($queryLocation, OBJECT);

	$locationids = json_decode($locations[0]->Locations);

	$queryLocationData = "SELECT * FROM yfm_wp_AT_locations where Location_id IN ($locationids)";
	$locationData = $wpdb->get_results($queryLocationData, OBJECT);
	//echo "<pre>"; print_r($locationData); echo "</prE>"; die;
}


add_shortcode('register_form', 'registerform_creation'); // EO Register Form shortcode

add_action('wp_ajax_register_form', 'register_form');
add_action('wp_ajax_nopriv_register_form', 'register_form');


function register_form_old(){
	global $wpdb;

	/***** User Information **********/

	$user_email 	= $_POST['user_email'];
	$credentials 	= $_POST['credentials'];
	$firname 		= $_POST['firname'];
	$lasname 		= $_POST['lasname'];
	$phone 			= $_POST['phone'];
	$phone2 		= $_POST['phone2'];
	$user_emails = $_POST['user_email'];
	$send_request = $_POST['send_request'];
	$received_request = $_POST['received_request'];
	$contac_id = $_POST['contac_id'];

	$exists = email_exists( $user_email );
	if($exists)
	{
		if ( $user = get_user_by( 'email', $user_email) ) {
       	 return $user_id = $user->ID;
	    }
	    $data['msg'] = __('Email already exists!');
		$data['status'] = __('danger');
		$data['id'] = $user->ID;
		$user_id = $user->ID;


	}
	else
	{

		if($lasname != "")
		{
			$fullname = strtolower($firname.'_'.$lasname);
		}
		else
		{
			$fullname = strtolower($firname);
		}

		if(!empty($user_email) && !empty($credentials))
		{
			if ( email_exists($user_email) == false ) {
			$user_id = wp_create_user( $fullname, $credentials, $user_email );

			if($user_id)
			{
				$data['msg'] = __('User created Successfully.');
				$data['status'] = __('success');
				$data['id'] = $user_id;

				//Update user with id
				$updateContactId = $wpdb->update('wp_users', array('airtable_id' => $contac_id));

			}
			else{

				$data['msg'] = __('User Not Created!');
				$data['status'] = __('danger');
			}
			} else {
				$user_id = __('User email already exists.');
			}
		}

	}

	if(isset($send_request))
	{
		//error with the query
		$api = $wpdb->insert('ymf_wp_API_logs', array(
	    'sent_request' => wp_json_encode($send_request),
	    'receive_request' => wp_json_encode($received_request),
	    'sent_by' => wp_json_encode($user_emails),
	    'created_date'=>date('Y-m-d h:i:s'),
		));

		if($api)
		{
			//echo 'logs inserted';
			$data['msg']= 'logs inserted';
		}
		else
		{
			//echo 'logs not inserted';
			$data['msg']= 'logs not inserted';
		}

	}


	$data['msg'] = __('User created Successfully.');
				$data['status'] = __('success');
				$data['id'] = $user_id;

	return wp_send_json($data);
			echo $user_id;


}


function register_form(){
	global $wpdb;

	/***** User Information **********/

	$full_name 		= $_POST['full_name'];
	$location_id 	= $_POST['location_id'];
	$company_id 	= $_POST['company_id'];
	$user_email 	= $_POST['user_email'];
	$credentials 	= $_POST['credentials'];
	$firname 		= $_POST['firname'];
	$lasname 		= $_POST['lasname'];
	$phone 			= $_POST['phone'];
	$phone2 		= $_POST['phone2'];
	$phone21 		= $_POST['phone21'];
	$user_emails    = $_POST['user_emails'];

	$user_address = $_POST['company_name'].", ".$_POST['street_address'].", ".$_POST['suburb'].", ".$_POST['state'].", ".$_POST['post_code'];
	//$contac_id = $_POST['contac_id'];
	$exists = email_exists($user_email);
	if($exists){
		if ( $user = get_user_by( 'email', $user_email) ) {
  		$user_id = $user->ID;
   		//return $user_id;
	  }

		$data['msg'] = __('Email already exists!');
		$data['status'] = __('danger');
		$data['id'] = $user->ID;
		$user_id = $user->ID;

		return wp_send_json($data);
			//echo $user_id;
	} else {
		if($_POST['company_id'] == ''){
			// Create location and company at airtable
			$locationData =[];
			if($phone2==''){
				$phone2 = $phone21;
			}
			$locationData=[
			    	'fields'=>[
				   		'ID1'=>$_POST['suburb'],
					   	'Street'=>$_POST['street_address'],
					    //'street_address_2'=>$_POST['street_address'],
					    'Suburb'=>$_POST['suburb'],
					    'State'=> $_POST['state'],
					    'Postcode'=> $_POST['post_code'],
					    'Email'=> $_POST['user_email'],
					    'Phone1'=> $_POST['phone'],
					    'Phone2'=> $phone2
						]
			];
			// echo '===========';
			// print_r($locationData);
			// echo '==========';
			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Locations",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			//CURLOPT_POSTFIELDS => "{\n  \"fields\": {\n    \"Truck_ID1\": [\n      \"rec0z32nKnCHIQhzu\"\n    ],\n    \"Task_Type\": \"Repairs (Parts & Labour)\",\n    \"Images\": [\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC.jpg\"\n      },\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC-1.jpg\"\n      }\n    ],\n    \"Job Type\": [\n      \"Order\"\n    ],\n    \"Status\": \"Client response?\"\n  }\n}",
			CURLOPT_POSTFIELDS => json_encode($locationData),
			CURLOPT_HTTPHEADER => array(
				"authorization: Bearer keyw3cJfiAFXmYjJf",
				"cache-control: no-cache",
				"content-type: application/json",
				"postman-token: d6aa4070-c2cd-df5f-cd62-0f25e59be5e2"
				),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);

			$airtableLocationResponse = json_decode($response);
			//echo "<pre>"; print_r($airtableLocationResponse); echo "</pre>"; die;
			$location_id = $airtableLocationResponse->id;

			if($_POST['link_type']==0){
				$website = 'http://'.$_POST['Website'];
			} else if($_POST['link_type']==1) {
				$website = 'https://'.$_POST['Website'];
			}
			$companyData=[
			    'fields'=>[
				   	'CompanyCode'=>'Pending Company',
				   	'CompanyName'=>$_POST['company_name'],
				    'Locations'=> [$airtableLocationResponse->id],
				    'Type'=>'NA',
				    'Website'=> $website
					]
			];

			//print_r($companyData);

			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Companies",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			//CURLOPT_POSTFIELDS => "{\n  \"fields\": {\n    \"Truck_ID1\": [\n      \"rec0z32nKnCHIQhzu\"\n    ],\n    \"Task_Type\": \"Repairs (Parts & Labour)\",\n    \"Images\": [\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC.jpg\"\n      },\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC-1.jpg\"\n      }\n    ],\n    \"Job Type\": [\n      \"Order\"\n    ],\n    \"Status\": \"Client response?\"\n  }\n}",
			CURLOPT_POSTFIELDS => json_encode($companyData),
			CURLOPT_HTTPHEADER => array(
			"authorization: Bearer keyw3cJfiAFXmYjJf",
			"cache-control: no-cache",
			"content-type: application/json",
			"postman-token: d6aa4070-c2cd-df5f-cd62-0f25e59be5e2"
			),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);

			$airtablCompanyResponse = json_decode($response);
			//echo "<pre>"; print_r($airtablCompanyResponse); echo "</pre>"; die;
			$company_id = $airtablCompanyResponse->id;
		}

		if($location_id == ''){
			// Create location at airtable
			$locationData =[];
			$locationData=[
			    'fields'=>[
				   	'ID1'=>$_POST['suburb1'],
				   	'Street'=>$_POST['street_address1'].' '.$_POST['street_address_2_1'],
				    //'street_address_2'=>$_POST['street_address_2_1'],
				    'Suburb'=>$_POST['suburb1'],
				    'State'=> $_POST['state1'],
				    'Postcode'=> $_POST['post_code1'],
				    'Email'=> $user_emails,
				    'Phone1'=> $_POST['phone'],
				    'Phone2'=> $_POST['phone2'],
				    "Company"=>[$company_id],
					]
			];

			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Locations",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			//CURLOPT_POSTFIELDS => "{\n  \"fields\": {\n    \"Truck_ID1\": [\n      \"rec0z32nKnCHIQhzu\"\n    ],\n    \"Task_Type\": \"Repairs (Parts & Labour)\",\n    \"Images\": [\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC.jpg\"\n      },\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC-1.jpg\"\n      }\n    ],\n    \"Job Type\": [\n      \"Order\"\n    ],\n    \"Status\": \"Client response?\"\n  }\n}",
			CURLOPT_POSTFIELDS => json_encode($locationData),
			CURLOPT_HTTPHEADER => array(
				"authorization: Bearer keyw3cJfiAFXmYjJf",
				"cache-control: no-cache",
				"content-type: application/json",
				"postman-token: d6aa4070-c2cd-df5f-cd62-0f25e59be5e2"
				),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);

			$airtableLocationResponse = json_decode($response);
			//echo "<pre>"; print_r($airtableLocationResponse); echo "</pre>";
			$location_id = $airtableLocationResponse->id;

		}


		if($lasname != ""){
			$fullname = strtolower($firname.'_'.$lasname);
		}else{
			$fullname = strtolower($firname);
		}

		if(!empty($user_email) && !empty($credentials)){
			if ( email_exists($user_email) == false ) {
				// Create Contact At Airtable
				$contactData=[
				  'fields'=>[
			    	'ContactName'=>$full_name,
			    	"Locations"=>[$location_id],
			    	// "Company"=>[$company_id],
			    	"Position"=> "New User",
			    	"Phone"=> $phone,
			    	"Email"=> $user_email
					]
				];
				//echo "<pre>"; print_r($contactData); echo "</prE>"; //die;
				$curl = curl_init();
				curl_setopt_array($curl, array(
				CURLOPT_URL => "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Contacts",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				//CURLOPT_POSTFIELDS => "{\n  \"fields\": {\n    \"Truck_ID1\": [\n      \"rec0z32nKnCHIQhzu\"\n    ],\n    \"Task_Type\": \"Repairs (Parts & Labour)\",\n    \"Images\": [\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC.jpg\"\n      },\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC-1.jpg\"\n      }\n    ],\n    \"Job Type\": [\n      \"Order\"\n    ],\n    \"Status\": \"Client response?\"\n  }\n}",
				CURLOPT_POSTFIELDS => json_encode($contactData),
				CURLOPT_HTTPHEADER => array(
					"authorization: Bearer keyw3cJfiAFXmYjJf",
					"cache-control: no-cache",
					"content-type: application/json",
					"postman-token: d6aa4070-c2cd-df5f-cd62-0f25e59be5e2"
				),
				));

				$response = curl_exec($curl);
				$err = curl_error($curl);
				curl_close($curl);

				$airtableResponse = json_decode($response);
				//echo "<pre>"; print_r($airtableResponse); echo "</pre>";
				$contac_id = $airtableResponse->id;
				//echo "contact:".$contac_id;	//die;

				// Wp Insert Contact Data
				$wpAtContact = array(
				    'Contact_id'  =>  $airtableResponse->id,
				    'ContactName' =>  $airtableResponse->fields->ContactName,
						'Position'    =>  $airtableResponse->fields->Position,
				    'Companies'   =>  wp_json_encode($airtableResponse->fields->Companies),
				    'Locations'   =>  wp_json_encode($airtableResponse->fields->Locations),
				    'Phone'       =>  $airtableResponse->fields->Phone,
				    'Email'       =>  $airtableResponse->fields->Email,
				);

				$c_id = $wpdb->insert($table_name, $wpAtContact);
				//$user_id = wp_create_user( $fullname, $credentials, $user_email );
				$table_name = 'wp_users';

				$user_id = $wpdb->insert($table_name, array(
				    'user_login' => $user_email,
				    'user_pass' => wp_hash_password($credentials),
						'user_nicename' => $full_name,
				    'user_email'=> $user_email,
				    'airtable_id'=> $airtableResponse->id,
				    'airtable_ContactName'=> $airtableResponse->fields->ContactName,
				    'airtable_company_id'=> $company_id,
				    'airtable_address'=>$user_address
				));

				if($user_id){
					//print_r($wpdb->insert_id);
					$uid = $wpdb->insert_id;
					add_user_meta( $uid, 'account_inactive', '1');

					#mail send to user
					$subject = 'YFM Account created';
					$to = $user_email;
					$body  = '<div style="width:100%; margin:0 auto;">';
					$body .= '<table style="width:100%; background:#eee; padding:10px;">';
					$body .= '<tr><th style="text-align:left;">Hello '.$full_name.'</th></tr>';
					$body .= '<tr><th style="text-align:left;"> Your YFM account has been created. Your login details are:</th></tr>';
					$body .= '<tr><th style="text-align:left;"></th></tr>';
					$body .= '<tr><th style="text-align:left;">User Name: '.$user_email.'</th></tr>';
					$body .= '<tr><th style="text-align:left;">Password: '.$credentials.'</th></tr>';
					$body .= '<tr><th style="text-align:left;"></th></tr>';
					$body .= '<tr><th style="text-align:left;"></th></tr>';
					$body .= '<tr><th style="text-align:center;"> Your Fleet Media – Specialists in Truck Signage Solutions </th></tr>';
					$body .= '</table></div>';
					$headers = array('Content-Type: text/html; charset=UTF-8');
					$a = wp_mail( $to, $subject, $body, $headers);
					if($a){
						$ajaxResponse['mail'] = 'success';
					}else{
						$ajaxResponse['mail'] = 'fail';
						$ajaxResponse['status'] = 'fail';
					}

					#mail send to user
					$subject = 'YFM Account created';
					$to = 'service@yourfleetmedia.com';
					$body  = '<div style="width:100%; margin:0 auto;">';
					$body .= '<table style="width:100%; background:#eee; padding:10px;">';
					$body .= '<tr><th style="text-align:left;">Hello '.$full_name.'</th></tr>';
					$body .= '<tr><th style="text-align:left;"> New registration is done on YFM. Login details are:</th></tr>';
					$body .= '<tr><th style="text-align:left;"></th></tr>';
					$body .= '<tr><th style="text-align:left;">User Name: '.$user_email.'</th></tr>';
					$body .= '<tr><th style="text-align:left;">Password: Choosen by user</th></tr>';
					$body .= '<tr><th style="text-align:left;"></th></tr>';
					$body .= '<tr><th style="text-align:left;"></th></tr>';
					$body .= '<tr><th style="text-align:center;"> Your Fleet Media – Specialists in Truck Signage Solutions </th></tr>';
					$body .= '</table></div>';
					$headers = array('Content-Type: text/html; charset=UTF-8');
					$a = wp_mail( $to, $subject, $body, $headers);

					if($a){
						$ajaxResponse['mail'] = 'success';
					}else{
						$ajaxResponse['mail'] = 'fail';
						$ajaxResponse['status'] = 'fail';
					}
					$data['msg'] = __('User created Successfully.');
					$data['status'] = __('success');
					$data['id'] = $user_id;
				} else{
					$data['msg'] = __('User Not Created!');
					$data['status'] = __('danger');
				}
			} else {
				$user_id = __('User email already exists.');
			}
		} else {
			$user_id = __('User mail  or password should not empty.');
		}
	}


	if(isset($send_request)){
		//error with the query
		$api = $wpdb->insert('ymf_wp_API_logs', array(
	    'sent_request' => wp_json_encode($send_request),
	    'receive_request' => wp_json_encode($received_request),
	    'sent_by' => wp_json_encode($user_emails),
	    'created_date'=>date('Y-m-d h:i:s'),
		));

		if($api){
			//echo 'logs inserted';
			$data['msg']= 'logs inserted';
		}else{
			//echo 'logs not inserted';
			$data['msg']= 'logs not inserted';
		}
	}

	$data['msg'] = __('User created Successfully.');
	$data['status'] = __('success');
	$data['id'] = $user_id;

	return wp_send_json($data);
			//echo $user_id;
}


function user_metadata( $user_id ){
	if( !empty( $_POST['firname'] ) && !empty( $_POST['lasname'] ) ){
		update_user_meta( $user_id, 'first_name', trim($_POST['firname']) );
	 	update_user_meta( $user_id, 'last_name', trim($_POST['lasname']) );
	}
	update_user_meta( $user_id, 'show_admin_bar_front', false );
}
add_action( 'user_register', 'user_metadata' );
add_action( 'profile_update', 'user_metadata' );



function validateMail(){
	$exists = email_exists( $_POST['usrMail'] );
	if($exists == ''){
		die('success');
	}else{
		die('Already exists');
	}
}
add_action('wp_ajax_validateMail', 'validateMail');
add_action('wp_ajax_nopriv_validateMail', 'validateMail');
