<?php

/*********************************************************************************************
*
* This file consists of the code to create the shortcode that is used in this plugin to create the Overview Order form.
* It will further links to other forms after user logged in.
*
*********************************************************************************************/

if (!session_id()) {
    session_start();
}

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

function orderForm_creation(){
  require_once(ABSPATH.'/wp-config.php');
	require_once(ABSPATH.'/wp-load.php');
	require_once(ABSPATH.'/wp-includes/wp-db.php');
	require_once( ABSPATH.'wp-admin/includes/user.php' );
	global $wpdb;

	$userid= get_current_user_id();
	$user = get_user_by('id',$userid);
	// echo '<pre>';
	// print_r($user->data->airtable_ContactName); die;
	$queryUsers = "SELECT * FROM yfm_wp_AT_contacts where Contact_id= '".$user->data->airtable_id."'";
	$contact = $wpdb->get_results($queryUsers, OBJECT);

  $com = $contact[0]->Companies;
  $contact_com = str_replace('["','',$contact[0]->Companies);
  $contact_com = str_replace('"]','',$contact_com);

  $loc = $contact[0]->Locations;
  $contact_loc = str_replace('["','',$contact[0]->Locations);
  $contact_loc = str_replace('"]','',$contact_loc);

	/*if(!empty($contact)){
		$position = strtolower(str_replace(' ','',$contact[0]->Position));
		if(strcmp($position,'newuser')==0){
			 wp_logout();
			 wp_redirect( site_url().'?acc=inprogress' );
		}
	}

	if('yes' === get_user_meta( get_current_user_id(), sanitize_key( 'user_disabled' ), true ) ){
            $error_message = get_option( 'baba_locked_message' );
             echo "Your account is no longer active, Please contact the administrator.";
            wp_logout();
            wp_redirect( site_url().'?acc=disabled' );


}*/
	if(!is_user_logged_in()){
    $loginLink = site_url()."/login"; ?>
		<script type="text/javascript"> window.location="<?php echo $loginLink;?>"; </script>
	<?php }

	//include( plugin_dir_path( __FILE__ ) . '../airtables/get-trucks.php'); // get companies list
	// Fetc truck details
	global $wpdb;
	$company = $user->data->airtable_company_id;
  if($userid!='24'){
    $companyDetail = "SELECT * from yfm_wp_AT_companies where company_id='$contact_com'";
    $companyDel = $wpdb->get_results($companyDetail, OBJECT);
  }

	//$queryTruck = "SELECT ID1,ID2,Truck_id,Ops_Status FROM yfm_wp_AT_trucks where ID1 != '' and Client_code='$contact_com' and Ops_Status !='Retired' ORDER BY id ASC";


	if($companyDel[0]->Type == 'Client'){
		$queryTruck = "SELECT ID1,ID2,Truck_id,Ops_Status,Location FROM yfm_wp_AT_trucks where ID1 != '' and Ops_Status !='Retired' and Location LIKE '$contact_loc' ORDER BY id ASC";
		$trucks = $wpdb->get_results($queryTruck, OBJECT);
	}
	else{
		$queryTruck = "SELECT ID1,ID2,Truck_id,Ops_Status FROM yfm_wp_AT_trucks where ID1 != '' and Ops_Status !='Retired' ORDER BY id ASC";
		$trucks = $wpdb->get_results($queryTruck, OBJECT);
	}

	include( plugin_dir_path( __FILE__ ) .'../includes/include.php'); // include scripts and styles ?>

	<?php //echo "<prE>"; print_r($trucks1); echo "</pre>"; ?>

		<style type="text/css">
        .partsChosen, .chosen-container{
              width: 400px;
            }
    </style>


    <div class="login-panel">
        <div class="container">
            <div class="dashboard-sec overview-sec">
                <h4>Overview order</h4>
                <div class="overview-order-detail">
    	<div id="msg"></div>
      <?php
      $type = array("Body Repairs", "Client");
      if(!in_array($companyDel[0]->Type, $type)){
        echo '<div class="alert alert-danger text-center">You are not eligible to place order. Kindly contact administrator</div>';
      }else {?>
    	<form id="overviewOrder" name="overviewOrder" action="#" method="post">
    		<?php //echo "<pre>"; print_r($trucks->records); echo "</pre>"; ?>
    		<input type="hidden" name="action" value="overviewOrder">
    		<input type="hidden" name="hiddenTruckId" id="hiddenTruckId" value="">
    		<input type="hidden" name="user_name" id="user_name" value="<?php echo $user->data->airtable_ContactName;?>">
    		<input type="hidden" name="user_com" id="user_com" value="<?php echo $companyDel[0]->CompanyName;?>">
	    	<div class="row">
	    		<div class="col-md-12">
	        		<div class="form_panel">
						<div class="form-group">
							<div class="row">
								<div class="col-md-3">
									<label>Truck Identification</label>
								</div>
								<div class="col-md-9 truckfield">
									<select class="form-control chosen-select" name="truck_identification" id="truck_identification">
					                	<option value="">--Select Truck--</option>
							        	<?php
										/*for($i=0; $i<count($trucks->records)-1;$i++) {
											echo '<option value="'.$trucks->records[$i]->fields->ID1.'" data-id="'.$trucks->records[$i]->id.'">'.$trucks->records[$i]->fields->ID1.' - '.$trucks->records[$i]->fields->ID2.'</option>';
										}*/

											foreach ($trucks as $key => $truck) {
												echo '<option value="'.$truck->Truck_id.'" data-id="'.$truck->Truck_id.'">'.$truck->ID1.' - '.$truck->ID2.'</option>';
											}


										?>
					                </select>
								</div>
							</div>
						</div>
						<div class="form-group">
						 	<div class="row">
								<div class="col-md-3">
									<label>Purchase Order Number</label>
								</div>
								<div class="col-md-9">
									<input type="text" class="form-control" name="purchase_order_num" id="purchase_order_num" placeholder="" >
								</div>
							</div>
						</div>
						<div class="form-group">
						 	<div class="row">
								<div class="col-md-3">
									<label>Panel Selection</label>
								</div>
								<div class="col-md-9">
									<h5>Truck Body</h5>
									<div class="form-group panels-list">
					                	<label class="Check_box">Driver Side
					                      <input type="checkbox" name="panel_selection[]" class="panelchk" value="driver_side" panel_type="BD">
					                      <span class="checkmark"></span>
					                    </label>
					                    <label class="Check_box">Passenger Side
					                      <input type="checkbox" name="panel_selection[]" class="panelchk" value="passenger_side" panel_type="BP">
					                      <span class="checkmark"></span>
					                    </label>
					                    <label class="Check_box">Rear
					                      <input type="checkbox" name="panel_selection[]" class="panelchk" value="rear" panel_type="BR">
					                      <span class="checkmark"></span>
					                    </label>
					                    <label class="Check_box">Front
					                      <input type="checkbox" name="panel_selection[]" class="panelchk" value="front" panel_type="BF">
					                      <span class="checkmark"></span>
					                    </label>
					                    <label class="Check_box">Internal
					                      <input type="checkbox" name="panel_selection[]" class="panelchk" value="body_internal" panel_type="BI">
					                      <span class="checkmark"></span>
					                    </label>
					                    <h5>Truck Cabin</h5>
					                     <label class="Check_box">Driver Side
					                      <input type="checkbox" name="panel_selection[]" class="panelchk" value="cabin_driver" panel_type="CD">
					                      <span class="checkmark"></span>
					                    </label>
					                    <label class="Check_box">Passenger Side
					                      <input type="checkbox" name="panel_selection[]" class="panelchk" value="cabin_passenger" panel_type="CP">
					                      <span class="checkmark"></span>
					                    </label>
					                     <label class="Check_box">Front
					                      <input type="checkbox" name="panel_selection[]" class="panelchk" value="cabin_front" panel_type="CF">
					                      <span class="checkmark"></span>
					                    </label>
					              		<label class="Check_box">Internal
					                      <input type="checkbox" name="panel_selection[]" class="panelchk" value="cabin_internal" panel_type="CI">
					                      <span class="checkmark"></span>
					                    </label>

					                </div>
								</div>
							</div>
						</div>

						<!--div class="form-group order-image-section" style="display: none;">
						 	<div class="row image-remove">
								<div class="col-md-3">
									<label>Driver Photograph<sup>*</sup></label>
								</div>
								<div class="col-md-4">
									<input type="file" class="form-control" name="dynamicVal" id="damage_1"/>
					                <span>Attach Photograph</span>
								</div>
								<div class="col-md-1">
									<button type="button" class="btn btn-info" id="btn1">
									 <span class="glyphicon glyphicon-plus"></span>
									</button>
								</div>
							</div>
						</div-->
						<!--<div class="form-group">
						 	<div class="row">
								<div class="col-md-3">
									<label>Photograph damage-2<sup></sup></label>
								</div>
								<div class="col-md-9">
									<input type="file" class="form-control" name="damage_2" />
					                <span>Attach Photograph</span>
								</div>
							</div>
						</div> -->

						<div class="form-group sideparts" id="driver_side" style="display: none;">
						 	<div class="row">
								<div class="col-md-3">
									<label>Parts- Driver Side</label>
								</div>
								<div class="col-md-9 with-select">
									<select name="driver_side[]" id="driver_side_part" class="partsChosen" multiple="true">
									</select>
								</div>
							</div>
							<div class="form-group order-image-section">
							 	<div class="row">
									<div class="col-md-3">
										<label>Image 1<sup></sup></label>
									</div>
									<div class="col-md-4">
										<input type="file" class="form-control" name="driver_side_image[]"/>
						                <!-- <span>Attach Photograph</span> -->
									</div>
									<div class="col-md-1">
										<!-- <i class="far fa-plus-square addimage" id="driver_side_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded"></i> -->
										<button type="button" class="btn btn-info addimage" id="driver_side_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded">
										<span class="glyphicon glyphicon-plus"></span>
										</button>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group sideparts" id="passenger_side" style="display: none;">
						 	<div class="row">
								<div class="col-md-3">
									<label>Parts - Passenger Side</label>
								</div>
								<div class="col-md-9 with-select">
									<select name="passenger_side[]" id="passenger_side_part" class="partsChosen" multiple="true">
									</select>

								</div>
							</div>
							<div class="form-group order-image-section">
							 	<div class="row">
									<div class="col-md-3">
										<label>Image 1<sup></sup></label>
									</div>
									<div class="col-md-4">
										<input type="file" class="form-control" name="passenger_side_image[]"/>
						                <!-- <span>Attach Photograph</span> -->
									</div>
									<div class="col-md-1">
										<!-- <i class="far fa-plus-square addimage" id="passenger_side_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded"></i> -->
										<button type="button" class="btn btn-info addimage" id="passenger_side_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded">
										<span class="glyphicon glyphicon-plus"></span>
										</button>
									</div>
								</div>
							</div>
						</div>

				        <div class="form-group sideparts" id="rear" style="display: none;">
						 	<div class="row">
								<div class="col-md-3">
									<label>Parts - Rear</label>
								</div>
								<div class="col-md-9">
									<select name="rear[]" id="rear_part" class="partsChosen" multiple="true">
									</select>
								</div>
							</div>
							<div class="form-group order-image-section">
							 	<div class="row">
									<div class="col-md-3">
										<label>Image 1<sup></sup></label>
									</div>
									<div class="col-md-4">
										<input type="file" class="form-control" name="rear_image[]"/>
						                <!-- <span>Attach Photograph</span> -->
									</div>
									<div class="col-md-1">
										<!-- <i class="far fa-plus-square addimage" id="rear_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded"></i> -->
										<button type="button" class="btn btn-info addimage" id="rear_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded">
										<span class="glyphicon glyphicon-plus"></span>
										</button>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group sideparts" id="front" style="display: none;">
						 	<div class="row">
								<div class="col-md-3">
									<label>Parts- Front</label>
								</div>
								<div class="col-md-9">
									<select name="front" id="front_part" class="partsChosen" multiple="true">
									</select>
								</div>
							</div>
							<div class="form-group order-image-section">
							 	<div class="row">
									<div class="col-md-3">
										<label>Image 1<sup></sup></label>
									</div>
									<div class="col-md-4">
										<input type="file" class="form-control" name="front_image[]"/>
						                <!-- <span>Attach Photograph</span> -->
									</div>
									<div class="col-md-1">
										<!-- <i class="far fa-plus-square addimage" id="front_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded"></i> -->
										<button type="button" class="btn btn-info addimage" id="front_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded">
										<span class="glyphicon glyphicon-plus"></span>
										</button>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group sideparts" id="cabin_front" style="display: none;">
						 	<div class="row">
								<div class="col-md-3">
									<label>Parts - Cabin Front</label>
								</div>
								<div class="col-md-9">
									<select name="cabin_front" id="cabin_front_part" class="partsChosen" multiple="true">
									</select>
								</div>
							</div>
							<div class="form-group order-image-section">
							 	<div class="row">
									<div class="col-md-3">
										<label>Image 1<sup></sup></label>
									</div>
									<div class="col-md-4">
										<input type="file" class="form-control" name="cabin_front_image[]"/>
						               <!--  <span>Attach Photograph</span> -->
									</div>
									<div class="col-md-1">
										<!-- <i class="far fa-plus-square addimage" id="cabin_front_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded"></i> -->
										<button type="button" class="btn btn-info addimage" id="cabin_front_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded">
										<span class="glyphicon glyphicon-plus"></span>
										</button>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group sideparts" id="cabin_driver" style="display: none;">
						 	<div class="row">
								<div class="col-md-3">
									<label>Parts - Cabin Driver</label>
								</div>
								<div class="col-md-9">
									<select name="cabin_driver[]" id="cabin_driver_part" class="partsChosen" multiple="true">
									</select>
								</div>
							</div>
							<div class="form-group order-image-section">
							 	<div class="row">
									<div class="col-md-3">
										<label> Image 1<sup></sup></label>
									</div>
									<div class="col-md-4">
										<input type="file" class="form-control" name="cabin_driver_image[]"/>
						                <!-- <span>Attach Photograph</span> -->
									</div>
									<div class="col-md-1">
										<!-- <i class="far fa-plus-square addimage" id="cabin_driver_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded"></i> -->

										<button type="button" class="btn btn-info addimage" id="cabin_driver_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded">
										<span class="glyphicon glyphicon-plus"></span>
										</button>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group sideparts" id="cabin_passenger" style="display: none;">
						 	<div class="row">
								<div class="col-md-3">
									<label>Parts - Cabin Passenger</label>
								</div>
								<div class="col-md-9">
									<select name="cabin_passenger" id="cabin_passenger_part" class="partsChosen" multiple="true">
									</select>
								</div>
							</div>
							<div class="form-group order-image-section">
							 	<div class="row">
									<div class="col-md-3">
										<label>Image 1<sup></sup></label>
									</div>
									<div class="col-md-4">
										<input type="file" class="form-control" name="cabin_passenger_image[]"/>
						                <!-- <span>Attach Photograph</span> -->
									</div>
									<div class="col-md-1">
										<!-- <i class="far fa-plus-square addimage" id="cabin_passenger_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded"></i> -->
										<button type="button" class="btn btn-info addimage" id="cabin_passenger_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded">
										<span class="glyphicon glyphicon-plus"></span>
										</button>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group sideparts" id="cabin_internal" style="display: none;">
						 	<div class="row">
								<div class="col-md-3">
									<label>Parts - Cabin Internal</label>
								</div>
								<div class="col-md-9">
									<select name="cabin_internal[]" id="cabin_internal_part" class="partsChosen" multiple="true">
									</select>
								</div>
							</div>
							<div class="form-group order-image-section">
							 	<div class="row">
									<div class="col-md-3">
										<label>Image 1<sup></sup></label>
									</div>
									<div class="col-md-4">
										<input type="file" class="form-control" name="cabin_internal_image[]"/>
						                <!-- <span>Attach Photograph</span> -->
									</div>
									<div class="col-md-1">
										<!-- <i class="far fa-plus-square addimage" id="cabin_internal_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded"></i> -->
										<button type="button" class="btn btn-info addimage" id="cabin_internal_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded">
										<span class="glyphicon glyphicon-plus"></span>
										</button>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group sideparts" id="body_internal" style="display: none;">
						 	<div class="row">
								<div class="col-md-3">
									<label>Parts - Body Internal</label>
								</div>
								<div class="col-md-9">
									<select name="body_internal" id="body_internal_part" class="partsChosen" multiple="true">
									</select>
								</div>
							</div>
							<div class="form-group order-image-section">
							 	<div class="row">
									<div class="col-md-3">
										<label>Image 1<sup></sup></label>
									</div>
									<div class="col-md-4">
										<input type="file" class="form-control" name="body_internal_image[]"/>
						                <!-- <span>Attach Photograph</span> -->
									</div>
									<div class="col-md-1">
										<!-- <i class="far fa-plus-square addimage" id="body_internal_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded"></i> -->

										<button type="button" class="btn btn-info addimage" id="body_internal_image" data-toggle="tooltip" data-placement="top" title="Each panel max of 5 images can be uploaded">
										<span class="glyphicon glyphicon-plus"></span>
										</button>
									</div>
								</div>
							</div>
						</div>

				        <div class="form-group installation">
						 	<div class="row">
								<div class="col-md-3">
									<label>Do you require installation?</label>
								</div>
								<div class="col-md-9">
									<div class="radio radio-primary">
					                    <input type="radio" name="installation" id="radio1" value="yes">
					                    <label>Yes</label>
					                </div>
					                <div class="radio radio-primary">
					                    <input type="radio" name="installation" id="radio2" value="no">
					                    <label>No</label>
					                </div>
								</div>
							</div>
						</div>

						<!--div class="form-group">
						 	<div class="row">
								<div class="col-md-3">
									<label>Freight account<sup>*</sup></label>
								</div>
								<div class="col-md-9">
									<div class="radio radio-primary">
					                    <input type="radio" name="freightAccount" id="fa1" value="Use your own account">
					                    <label for="fa1">Use your own account</label>
					                </div>
					                <div class="radio radio-primary">
					                    <input type="radio" name="freightAccount" id="fa2" value="Use our (YFM) account">
					                    <label for="fa2">Use our (YFM) account</label>
					                </div>
					                <div class="radio radio-primary">
					                    <input type="radio" name="freightAccount" id="fa3" value="Pick up">
					                    <label for="fa3">Pick up</label>
					                </div>
								</div>
							</div>
						</div-->

						<div id="qtyhtml">

						</div>

						<div class="form-group text-center">
							<input type="submit" class="btn btn-orange overview-order" value="Submit" style="display:none;">
							<input type="button" class="btn btn-orange show-qty-popup" value="Submit">
						</div>
					</div>
	        	</div>
	       	</div>


	       	<!-- Modal -->
			<div id="qtyModal" class="modal fade" role="dialog">
			  <div class="modal-dialog">

			    <!-- Modal content-->
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title">Parts Quantity</h4>
			      </div>
			      <div class="modal-body">
			    		<div class="row">
				    		<div class="col-md-12">
				        		<div class="form_panel" id="partqtysection">
				        			<!--
				        			<h3> BP PArts </h3>
									<div class="form-group">
									 	<div class="row">
											<div class="col-md-6">
												<label>Tension Profile - Horizontal<sup>*</sup></label>
											</div>
											<div class="col-md-6">
												<input type="text" class="form-control" name="purchase_order_num" id="purchase_order_num" >
											</div>
										</div>
									</div>
									-->
								</div>
								<input type="hidden" name="hiddenpartslist" value="" id="hiddenpartslist">
				        	</div>
			       		</div>
			      </div>
			      <div class="modal-footer">
			        <!--button type="button" class="btn btn-default" data-dismiss="modal">Close</button-->
			        <input type="button" class="btn btn-orange btn-primary popup-submit" value="Submit">
			      </div>
			    </div>

			  </div>
			</div>
			<div id="loader" style="display: none;"></div>
    		<div id="overlay" style="display: none;"></div>
		</form>
  <?php } ?>
	</div>
</div>
</div>
</div>



	<?php /*}else{
		echo do_shortcode('[login_form]');
	}*/
 }
add_shortcode('order_form', 'orderForm_creation'); // EO Register Form shortcode

add_action('wp_ajax_parts_list', 'part_list');
add_action('wp_ajax_nopriv_parts_list', 'part_list');

function part_list(){
	//echo "<prE>"; print_r($_POST); echo "</prE>"; die;
	global $wpdb;
	$panel_type = $_POST['panel_type'];
	$truck = $_POST['truck'];

	$partListwhere = array();

	$queryTruck = "SELECT * FROM yfm_wp_AT_trucks WHERE Truck_id = '$truck'";
	$truck_data = $wpdb->get_results($queryTruck, OBJECT);

	$truckType = str_replace('["', '', $truck_data[0]->Truck_Type);
	$truckType = str_replace('"]', '', $truckType);

	$queryTruckType = "SELECT Truck_Type,$panel_type,Chassis,Bodies FROM yfm_wp_AT_truck_types WHERE Truck_Types_id = '$truckType'";
	$truckTypeData = $wpdb->get_results($queryTruckType, OBJECT);

	$partListwhere['panel_type']   = $panel_type;
	$partListwhere['Truck_Type']   = $truckTypeData[0]->Truck_Type;
	$partListwhere['Signage_Type'] = $truckTypeData[0]->$panel_type;

	#Get Chassis Data
	$truckChassis = str_replace('["', '', $truckTypeData[0]->Chassis);
	$truckChassis = str_replace('"]', '', $truckChassis);

	$queryTruckChassis = "SELECT * FROM yfm_wp_AT_chassis WHERE Chassis_id = '$truckChassis'";
	$truckChassisData = $wpdb->get_results($queryTruckChassis, OBJECT);

	$partListwhere['Chassis_code'] = $truckChassisData[0]->Chassis_code;

	#Get Boddy Data
	$truckBody = str_replace('["', '', $truckTypeData[0]->Bodies);
	$truckBody = str_replace('"]', '', $truckBody);

	$queryTruckBody = "SELECT * FROM yfm_wp_AT_bodies WHERE Bodies_id = '$truckBody'";
	$truckBodyData = $wpdb->get_results($queryTruckBody, OBJECT);

	$partListwhere['Body_Code'] = $truckBodyData[0]->Body_Code;

	#Get Company Data
	$truckCompany = str_replace('["', '', $truck_data[0]->Client_code);
	$truckCompany = str_replace('"]', '', $truckCompany);

	$queryCompanyBody = "SELECT * FROM yfm_wp_AT_companies WHERE company_id = '$truckCompany'";
	$truckCompanyData = $wpdb->get_results($queryCompanyBody, OBJECT);

	#Get Company Data
	$CompanyCode = str_replace('["', '', $truckCompanyData[0]->CompanyCode);
	$CompanyCode = str_replace('"]', '', $CompanyCode);

	$partListwhere['CompanyCode'] = $truckCompanyData[0]->CompanyCode;
	//$partListwhere['CompanyCode'] = substr($truckCompanyData[0]->CompanyCode, 0, 3);

	$whare = " (Panel = '".$panel_type."' OR Panel = 'GEN')";
	$whare .= " AND (Chassis = '".$partListwhere['Chassis_code']."' OR Chassis = 'GEN')";
	$whare .= " AND (Body_Type = '".$partListwhere['Body_Code']."' OR Body_Type = 'GEN')";
	$whare .= " AND (Signage_Type = '".$partListwhere['Signage_Type']."' OR Signage_Type = 'GEN')";
	$whare .= " AND (Client = '".substr($CompanyCode, 0, 3)."' OR Client = 'GEN')";

	// $querystr = "SELECT * FROM yfm_wp_AT_parts_list
	// WHERE Panel = '$panel_type' OR Panel = 'GEN'
	// GROUP BY `Item_Name`,`Profile_Mapping`
	// ORDER BY id DESC";
	//echo $whare;

	$queryParts1 = "SELECT * FROM yfm_wp_AT_parts_list WHERE $whare GROUP BY `Item_Name`,`Profile_Mapping` ORDER BY id DESC";
	$parts_data = $wpdb->get_results($queryParts1, OBJECT);

	$parts_data2 = array();
	if($partListwhere['Signage_Type'] == 'TRX'){
		$queryParts2 = "SELECT * FROM yfm_wp_AT_parts_list WHERE Signage_Type = 'TRXGEN' OR Signage_Type = 'TRXCNR' GROUP BY `Item_Name`,`Profile_Mapping` ORDER BY id DESC";
		$parts_data2 = $wpdb->get_results($queryParts2, OBJECT);
	}

	if($partListwhere['Signage_Type'] == 'TRXC'){
		//echo '------------';
		$queryParts2 = "SELECT * FROM yfm_wp_AT_parts_list WHERE Signage_Type = 'TRXGEN' GROUP BY `Item_Name`,`Profile_Mapping` ORDER BY id DESC";
		$parts_data2 = $wpdb->get_results($queryParts2, OBJECT);
	}

 	$res = array();
	$res['calData']   = $calData;
	$res['partsData'] = $parts_data;
	$res['partsData2'] = $parts_data2;
	$res['queryParts1'] = $queryParts1;
	$res['queryParts2'] = $queryParts2;

 	echo wp_json_encode($res);exit;
}

add_action('wp_ajax_overviewOrder', 'overviewOrder');
add_action('wp_ajax_nopriv_overviewOrder', 'overviewOrder');

function overviewOrder(){
	//echo "<prE>"; print_r($_POST); echo "<prE>"; die;
	if(!is_user_logged_in()){
		$loginLink = '<a href="'.site_url().'/login" target="_blank" > login </a>';
		$ajaxResponse['msg'] = __('Login Required. Clieck here to '.$loginLink);
		$ajaxResponse['status'] = __('fail');
		echo wp_json_encode($ajaxResponse); exit;
	}
    global $wpdb;
    $ajaxResponse = array();
    $ajaxResponse['status'] = 'success';
    $ponum = $_POST['purchase_order_num'];
    $panel_type = array();
    $taskImagesArr = [];
    $mailImageLinks = '';
    foreach ($_POST['panel_selection'] as $key => $value) {
    	if($value == 'driver_side'){
			$panel_type[] = "BD";
		}else if($value == 'passenger_side'){
			$panel_type[] = "BP";
		}else if($value == 'rear'){
			$panel_type[] = "BR";
		}else if($value == 'front'){
			$panel_type[] = "BF";
		}else if($value == 'cabin_front'){
			$panel_type[] = "CF";
		}else if($value == 'cabin_driver'){
			$panel_type[] = "CD";
		}else if($value == 'cabin_passenger'){
			$panel_type[] = "CP";
		}else if($value == 'cabin_internal'){
			$panel_type[] = "CI";
		}else if($value == 'body_internal'){
			$panel_type[] = "BI";
		}
		// Upload images
		$upload_overrides = array( 'test_form' => false );
		$panelVal =  $value;
		$imgfname =  $value.'_image';
		if(!empty($_FILES[$imgfname])){
			$files = $_FILES[$imgfname];
			foreach ($files['name'] as $key => $value) {
				if ($files['name'][$key]) {
					$file = array(
						'name'     => $files['name'][$key],
						'type'     => $files['type'][$key],
						'tmp_name' => $files['tmp_name'][$key],
						'error'    => $files['error'][$key],
						'size'     => $files['size'][$key]
					);
					//wp_handle_upload($file);
					$movefile = wp_handle_upload($file, $upload_overrides );
					if ( $movefile && ! isset( $movefile['error'] ) ) {
					    $url = stripslashes($movefile['url']);
					    $taskImagesArr[] = ['url'=>$url];
					    $mailImageLinks .= $panelVal.' : <a href="'.$url.'"> Image '.($key+1).'</a> <br>';
					} else {
					    //Error generated by _wp_handle_upload()
					    //@see _wp_handle_upload() in wp-admin/includes/file.php
					    //echo $movefile['error'];
					}
				}
			}
		}
	}

	$panel_type = implode(',', $panel_type); //die;
	$tid1 = $_POST['hiddenTruckId'];
	$queryTruck = "SELECT Truck_id,ID1,ID2,Truck_Type,D_Side_Art_code,P_Side_Art_code,Rear_Art_code,Client FROM yfm_wp_AT_trucks WHERE Truck_id = '".$tid1."'";
	$truck_data = $wpdb->get_results($queryTruck, OBJECT);
	if(empty($truck_data)){
		//echo 'logs inserted';
		$data['msg']= 'Trucks Table is empty. Kindly sync Trucks table first.';
		$data['status'] = 'fail';
		echo wp_json_encode($data); exit;
	}

	//echo "<pre>"; print_r($truck_data); die;
	$trukId1 = $truck_data[0]->ID1;
	$trukId2 = $truck_data[0]->ID2;

	$truckType = str_replace('["', '', $truck_data[0]->Truck_Type);
	$truckType = str_replace('"]', '', $truckType);

	$queryTruckType = "SELECT Truck_Type,$panel_type,Chassis,Bodies,Tare,GVM FROM yfm_wp_AT_truck_types WHERE Truck_Types_id = '$truckType'";
	$truckTypeData = $wpdb->get_results($queryTruckType, OBJECT);
	if(empty($truckTypeData)){
		$data['msg']= 'Truck Types Table is empty. Kindly sync Truck Types table first.';
		$data['status'] = 'fail';
		echo wp_json_encode($data); exit;
	}

	$partListwhere['panel_type']   = $panel_type;
	$partListwhere['Truck_Type']   = $truckTypeData[0]->Truck_Type;
	$partListwhere['Signage_Type'] = $truckTypeData[0]->$panel_type;

	//echo "<pre>"; print_r($truckTypeData); //die;
	#Get Chassis Data
	$truckChassis = str_replace('["', '', $truckTypeData[0]->Chassis);
	$truckChassis = str_replace('"]', '', $truckChassis);

	$queryTruckChassis = "SELECT * FROM yfm_wp_AT_chassis WHERE Chassis_id = '$truckChassis'";
	$truckChassisData = $wpdb->get_results($queryTruckChassis, OBJECT);
	if(empty($truckChassisData)){
		//echo 'logs inserted';
		$data['msg']= 'Chassis Table is empty. Kindly sync Chassis table first.';
		$data['status'] = 'fail';
		echo wp_json_encode($data); exit;
	}
	//echo "<pre>"; print_r($truckChassisData); die;
	$partListwhere['Chassis_code'] = $truckChassisData[0]->Chassis_code;

	#Get Boddy Data
	$truckBody = str_replace('["', '', $truckTypeData[0]->Bodies);
	$truckBody = str_replace('"]', '', $truckBody);
	$queryTruckBody = "SELECT * FROM yfm_wp_AT_bodies WHERE Bodies_id = '$truckBody'";
	$truckBodyData = $wpdb->get_results($queryTruckBody, OBJECT);
	// if(empty($truckBodyData))
	// {
	// 	//echo 'logs inserted';
	// 	$data['msg']= 'Bodies Table is empty. Kindly sync Bodies table first.';
	// 	$data['status'] = 'fail';
	// 	echo wp_json_encode($data); exit;
	// }
	//echo "<pre>"; print_r($truckBodyData); die('truckBodyData');
	$partListwhere['Body_Code'] = $truckBodyData[0]->Body_Code;

	#Get Company Data
	$truckCompany = str_replace('["', '', $truck_data[0]->Client);
	$truckCompany = str_replace('"]', '', $truckCompany);
	$queryCompanyBody = "SELECT * FROM yfm_wp_AT_companies WHERE CompanyName = '$truckCompany'";
	$truckCompanyData = $wpdb->get_results($queryCompanyBody, OBJECT);

	// if(empty($truckCompanyData))
	// {
	// 	//echo 'logs inserted';
	// 	$data['msg']= 'Companies Table is empty. Kindly sync Companies table first.';
	// 	$data['status'] = 'fail';
	// 	echo wp_json_encode($data); exit;
	// }
	//echo "<pre>"; print_r($truckCompanyData); die;

  /*22-01-2020 New Code....*/
  $usr = wp_get_current_user();
	$cName = "SELECT * FROM yfm_wp_AT_companies WHERE company_id = '$usr->airtable_company_id'";
  $cInfo = $wpdb->get_results($cName, OBJECT);
	$partListwhere['CompanyCode'] = $cInfo[0]->CompanyCode;
	$invoiceConpanyName = $cInfo[0]->CompanyName;
  $invoiceConpanyName = str_replace("&","&amp;",$invoiceConpanyName);
  $invoiceConpanyName = str_replace('"',"&quot;",$invoiceConpanyName);
  $invoiceConpanyName = str_replace("'","&apos;",$invoiceConpanyName);
  $invoiceConpanyName = str_replace("<","&lt;",$invoiceConpanyName);
  $invoiceConpanyName = str_replace('>',"&gt;",$invoiceConpanyName);
/*22-01-2020 End...*/


	#Get Side Art/Rear Art Data
	//if ($panel_type == 'BP' && $truck_data[0]->P_Side_Art_code != '""') {
		$partCodeId = str_replace('["', '', $truck_data[0]->P_Side_Art_code);
		$pSideArtCodeId = str_replace('"]', '', $partCodeId);
		$queryPSide = "SELECT Artwork_Code,Artwork,Artwork_Transition,Artwork_Transition FROM yfm_wp_AT_side_artwork WHERE Side_Artwork_id = '$pSideArtCodeId'";
		$truckPSideArtData = $wpdb->get_results($queryPSide, OBJECT);
		// if(empty($truckCompanyData))
		// {
		// 	//echo 'logs inserted';
		// 	$data['msg']= 'Side Artwork Table is empty. Kindly sync Side Artwork table first.';
		// 	$data['status'] = 'fail';
		// 	echo wp_json_encode($data); exit;
		// }
	//}

	//if ($panel_type == 'BD' && $truck_data[0]->D_Side_Art_code != '""') {
		$dartCodeId = str_replace('["', '', $truck_data[0]->D_Side_Art_code);
		$dSideArtCodeId = str_replace('"]', '', $dartCodeId);
		$queryDSide = "SELECT Artwork_Code,Artwork,Artwork_Transition,Artwork_Transition FROM yfm_wp_AT_side_artwork WHERE Side_Artwork_id = '$dSideArtCodeId'";
		$truckDSideArtData = $wpdb->get_results($queryDSide, OBJECT);
		// if(empty($truckCompanyData))
		// {
		// 	//echo 'logs inserted';
		// 	$data['msg']= 'Side Artwork Table is empty. Kindly sync Side Artwork table first.';
		// 	$data['status'] = 'fail';
		// 	echo wp_json_encode($data); exit;
		// }
	//}

	//if ($panel_type == 'BR' && $truck_data[0]->Rear_Art_code != '""') {
		$rartCodeId = str_replace('["', '', $truck_data[0]->Rear_Art_code);
		$rSideArtCodeId = str_replace('"]', '', $rartCodeId);
		$queryRSide = "SELECT Artwork_Code,Artwork,Artwork_Transition,Artwork_Transition FROM yfm_wp_AT_rear_artwork WHERE Rear_Artwork_id = '$rSideArtCodeId'";
		$truckRSideArtData = $wpdb->get_results($queryRSide, OBJECT);
		// if(empty($truckCompanyData))
		// {
		// 	//echo 'logs inserted';
		// 	$data['msg']= 'Rear Artwork Table is empty. Kindly sync Rear Artwork table first.';
		// 	$data['status'] = 'fail';
		// 	echo wp_json_encode($data); exit;
		// }
	//}




 /**************************************************************************************/


	#Calculation Data
	$calData = array();
	$calData['truckCode'] 		= $truckTypeData[0]->Truck_Type;
	$calData['D_Side_Art_code'] = $truckDSideArtData[0]->Artwork_Code;
	$calData['P_Side_Art_code'] = $truckPSideArtData[0]->Artwork_Code;
	$calData['Rear_Art_code']   = $truckRSideArtData[0]->Artwork_Code;

	//echo '<pre>';
	//print_r($calData);


	$ArtworkArray = array();
	if(!empty(json_decode($truckDSideArtData[0]->Artwork_Transition))){
		$newArtWorkId = json_decode($truckDSideArtData[0]->Artwork_Transition);
		$queryDSide2 = "SELECT Artwork_Code,Artwork,Artwork_Transition,Artwork_Transition FROM yfm_wp_AT_side_artwork WHERE Side_Artwork_id = '$newArtWorkId[0]'";
		$truckDSideArtData2 = $wpdb->get_results($queryDSide2, OBJECT);
		$ArtworkArray['D_Side_Art_Work'] = json_decode($truckDSideArtData2[0]->Artwork);
		$calData['D_Side_Art_code'] = $truckDSideArtData2[0]->Artwork_Code;

	}else{
		$ArtworkArray['D_Side_Art_Work'] = json_decode($truckDSideArtData[0]->Artwork);
	}
	if(!empty(json_decode($truckPSideArtData[0]->Artwork_Transition))){
		$newArtWorkId = json_decode($truckPSideArtData[0]->Artwork_Transition);
		$queryPSide2 = "SELECT Artwork_Code,Artwork,Artwork_Transition,Artwork_Transition FROM yfm_wp_AT_side_artwork WHERE Side_Artwork_id = '$newArtWorkId[0]'";
		$truckPSideArtData2 = $wpdb->get_results($queryPSide2, OBJECT);
		$ArtworkArray['P_Side_Art_Work'] = json_decode($truckPSideArtData2[0]->Artwork);
		$calData['P_Side_Art_code'] = $truckPSideArtData2[0]->Artwork_Code;
	}else{
		$ArtworkArray['P_Side_Art_Work'] = json_decode($truckPSideArtData[0]->Artwork);
	}
	if(json_decode($truckRSideArtData[0]->Artwork_Transition) == 'N' || json_decode($truckRSideArtData[0]->Artwork_Transition) == ''){
		$ArtworkArray['Rear_Art_Work']   = json_decode($truckRSideArtData[0]->Artwork);
	}else{
	 	$newArtWorkId = json_decode($truckRSideArtData[0]->Artwork_Transition);
	 	//echo $queryRSide2 = "SELECT Artwork_Code,Artwork,Artwork_Transition,Artwork_Transition FROM yfm_wp_AT_rear_artwork WHERE Artwork_Code = '$newArtWorkId[0]'";
	 	$queryRSide2 = "SELECT Artwork_Code,Artwork,Artwork_Transition,Artwork_Transition FROM yfm_wp_AT_rear_artwork WHERE Rear_Artwork_id = '$newArtWorkId[0]'";
	 	$truckRSideArtData2 = $wpdb->get_results($queryRSide2, OBJECT);
	 	$ArtworkArray['Rear_Art_Work']   = json_decode($truckRSideArtData2[0]->Artwork);
		//$calData['Rear_Art_code']   = $truckRSideArtData2[0]->Artwork_Code;
		//$ArtworkArray['Rear_Art_Work']   = $newArtWorkId;
		//$calData['Rear_Art_code']   = $newArtWorkId;
		////$calData['Rear_Art_code']   = $truckRSideArtData[0]->Artwork_Code;
	 }

	$artWorkImageLinks ='';
	foreach ($ArtworkArray as $artKey => $artValue) {
		if(isset($artValue[0])){
			//$artWorkImages[$artKey] = $artValue[0]->thumbnails->large->url;
			if(isset($artValue[0]->thumbnails)){
				$artWorkImageLinks .= $artKey.' : <a href="'.$artValue[0]->thumbnails->large->url.'"> '.$artKey.'</a> <br>';
			}else{
				$artWorkImageLinks .= $artKey.' : No image Found';
			}
		}
		// else{
		// 	$artWorkImageLinks .= $artKey.' : Image Not Found <br>';
		// }
	}
	#PanelCals
	$calculation = array();
	$panelCodes = [];
	foreach ($_POST['panel_selection'] as $key => $value) {
		$panelCode = '';
    	if($value == 'driver_side'){
			$panelCode = "BD";
		}else if($value == 'passenger_side'){
			$panelCode = "BP";
		}else if($value == 'rear'){
			$panelCode = "BR";
		}else if($value == 'front'){
			$panelCode = "BF";
		}else if($value == 'cabin_front'){
			$panelCode = "CF";
		}else if($value == 'cabin_driver'){
			$panelCode = "CD";
		}else if($value == 'cabin_passenger'){
			$panelCode = "CP";
		}else if($value == 'cabin_internal'){
			$panelCode = "CI";
		}else if($value == 'body_internal'){
			$panelCode = "BI";
		}
		$panelCodes[] = $panelCode;
		//$calData['panelCode']		= $panel_type;
		//$calData['signage'] 		= $truckTypeData[0]->$panel_type;
		$calData[$panelCode]['signage'] = $truckTypeData[0]->$panelCode;

		$l = $panelCode.'_Length';
		$h = $panelCode.'_Height';
		if(isset($truckBodyData[0]->$l) && isset($truckBodyData[0]->$h)){

			$calData[$panelCode][$l] = $truckBodyData[0]->$l;
			$calData[$panelCode][$h] = $truckBodyData[0]->$h;

			$queryPanelCals = "SELECT * FROM `yfm_wp_AT_panel_calcs` WHERE `Signage` LIKE '".$truckTypeData[0]->$panelCode."' ";
			$truckPanelCals = $wpdb->get_results($queryPanelCals, OBJECT);

			#PanelCals
			//$calculation = array();
			foreach ($truckPanelCals as $key => $value) {
				$panelCals[$value->Profile_Code] = $value->Calculation;
				$customeKey = $panelCode.$value->Profile_Code;
				#Calculation
				if($value->Profile_Code == 'TP'){
					$calculation[$customeKey] = $truckBodyData[0]->$l + $value->Calculation;
				}else if($value->Profile_Code == 'HBP'){
					$calculation[$customeKey] = $truckBodyData[0]->$l + $value->Calculation;
				}else if($value->Profile_Code == 'VBP'){
					$calculation[$customeKey] = $truckBodyData[0]->$h + $value->Calculation;
				}else if($value->Profile_Code == 'HTP'){
					$calculation[$customeKey] = $truckBodyData[0]->$l + $value->Calculation;
				}else if($value->Profile_Code == 'VTP'){
					$calculation[$customeKey] = $truckBodyData[0]->$h + $value->Calculation;
				}else if($value->Profile_Code == 'BL'){
					$calculation[$customeKey] = $truckBodyData[0]->$l + $value->Calculation;
				}else if($value->Profile_Code == 'BH'){
					$calculation[$customeKey] = $truckBodyData[0]->$h + $value->Calculation;
				}else{
					$calculation[$customeKey] = 'NA';
				}
			}
			//$calData[$panelCode]['calculation'] = $calculation;
			//echo "<pre>"; print_r($calculation); die;

		}else{
			$calculation[$customeKey] = array();
		}
	}

	/*
	// Create purchase order lines group by parts wharehouse
    $powhline = array();
    $hiddenpartslist = explode(',',$_POST['hiddenpartslist']);
    $partsList = array();
    foreach ($hiddenpartslist as $valuehpl) {
    	$hpl = explode('-',$valuehpl);
    	$queryparts = "SELECT * FROM yfm_wp_AT_parts_list WHERE `Parts_list_id` = '".$hpl[1]."'";
		$partsdata = $wpdb->get_results($queryparts, OBJECT);
		$partsList[$hpl[0]][] =  $partsdata[0]->Item_Name;
		$dimensions = '';
		$profileMapping = $partsdata[0]->Profile_Mapping;
		if($profileMapping != ''){
			$profileMapping = explode(',', $profileMapping);
			if(count($profileMapping) == 1){
				$dimensions = (isset($calculation[$hpl[0].$profileMapping[0]]))? $calculation[$hpl[0].$profileMapping[0]] : '';
				$lineDesc['Purchase_Description'] = str_replace('_____mm', $dimensions, $partsdata[0]->Purchase_Description);
			}else if(count($profileMapping) == 2){
				$profileMapping[0] = trim($profileMapping[0]);
				$profileMapping[1] = trim($profileMapping[1]);
				$dimensions1 = (isset($calculation[$hpl[0].$profileMapping[0]]))? $calculation[$hpl[0].$profileMapping[0]] : '';
				$dimensions2 = (isset($calculation[$hpl[0].$profileMapping[1]]))? $calculation[$hpl[0].$profileMapping[1]] : '';
				$dimensions = $dimensions1.'*'.$dimensions2.'mm';
				$lineDesc['Purchase_Description'] = str_replace('____x____mm', $dimensions, $partsdata[0]->Purchase_Description);
			}else{
				$lineDesc['Purchase_Description'] = $partsdata[0]->Purchase_Description;
			}
		}
		$lineDesc['Sales_Description'] = $partsdata[0]->Sales_Description;
		$lineDesc['qty'] = $_POST[$valuehpl];
		$lineDesc['unitAmount'] = 10;
		$powhline[$partsdata[0]->Warehouse]['lines'][] = $lineDesc;
    }*/

    #Upload Form Images
	if ( ! function_exists( 'wp_handle_upload' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
	}

	/*$upload_overrides = array( 'test_form' => false );
	if(!empty($_FILES['damage_1'])){
		$uploadedfile = $_FILES['damage_1'];
		$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
	}
	if(!empty($_FILES['damage_2'])){
		$uploadedfile2 = $_FILES['damage_2'];
		$movefile2 = wp_handle_upload( $uploadedfile2, $upload_overrides );
	}*/

	if ( $movefile && ! isset( $movefile['error'] ) ) {
	    //echo "File is valid, and was successfully uploaded.\n";
	    //var_dump( $movefile );
	} else {
	    // Error generated by _wp_handle_upload()
	    // @see _wp_handle_upload() in wp-admin/includes/file.php
	    //echo $movefile['error'];
	}

	// Wp Login user Data
	$wlud = wp_get_current_user();
	//$Freight_Account = $_POST['freightAccount'];
	$Freight_Account = 'Use our (YFM) account';
	$isInstallation = $_POST['installation'];
	if($isInstallation == 'yes'){
		$taskType = "Repairs (P&L)";
		$installer = '';
	}else{
		$installer = $wlud->data->airtable_id;
		$taskType = "Repairs (Parts)";
	}
	$url1 = isset($movefile['url'])? stripslashes($movefile['url']):'';
	$url2 = isset($movefile2['url'])? stripslashes($movefile['url']):'';
	$taskData =[];
	if($isInstallation == 'no' && $installer != ''){
		$taskData=[
		    'fields'=>[
		   	'Truck_ID1'=>[$truck_data[0]->Truck_id],
		   	'Task_Type'=>$taskType,
		   	/*'Images'=>[
		   		['url'=>$url1],
		        ['url'=>$url2]
		   	 ],*/
		   	'Images'=>$taskImagesArr,
		   	'Job_Type'=>'Order',
		    'Status'=>['Unassigned'],
		     'D_side_Artwork'=>$calData['D_Side_Art_code'],
		     'P_side_Artwork'=>$calData['P_Side_Art_code'],
		     'Rear_Artwork'=>$calData['Rear_Art_code'],
		     //"Panels"=>[$panelCode],
		     "Panels"=>$panelCodes,
		     // "Freight_Account"=>$Freight_Account,
		     "Installer"=> [$installer],
		     "Ordered_by"=> [$wlud->data->airtable_id]
			]
		];
	}else{
		$taskData=[
		    'fields'=>[
		   	'Truck_ID1'=>[$truck_data[0]->Truck_id],
		   	'Task_Type'=>$taskType,
		   	/*'Images'=>[
		   		['url'=>$url1],
		        ['url'=>$url2]
		   	 ],*/
		   	'Images'=>$taskImagesArr,
		   	'Job_Type'=>'Order',
		    'Status'=>['Unassigned'],
		     'D_side_Artwork'=>$calData['D_Side_Art_code'],
		     'P_side_Artwork'=>$calData['P_Side_Art_code'],
		     'Rear_Artwork'=>$calData['Rear_Art_code'],
		     //"Panels"=>[$panelCode],
		     "Panels"=>$panelCodes,
		     // "Freight_Account"=>$Freight_Account,
		     //"Installer"=> [$installer],
		     "Ordered_by"=> [$wlud->data->airtable_id]
			]
		];
	}

	//echo "<pre>"; print_r($taskData); //die;
	$curl = curl_init();
	$airtableTaskUrl = "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Tasks";
		curl_setopt_array($curl, array(
		CURLOPT_URL => $airtableTaskUrl,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		//CURLOPT_POSTFIELDS => "{\n  \"fields\": {\n    \"Truck_ID1\": [\n      \"rec0z32nKnCHIQhzu\"\n    ],\n    \"Task_Type\": \"Repairs (Parts & Labour)\",\n    \"Images\": [\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC.jpg\"\n      },\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC-1.jpg\"\n      }\n    ],\n    \"Job Type\": [\n      \"Order\"\n    ],\n    \"Status\": \"Client response?\"\n  }\n}",
		CURLOPT_POSTFIELDS => json_encode($taskData),
		CURLOPT_HTTPHEADER => array(
				"authorization: Bearer keyw3cJfiAFXmYjJf",
				"cache-control: no-cache",
				"content-type: application/json",
				"postman-token: d6aa4070-c2cd-df5f-cd62-0f25e59be5e2"
			),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	$airtableResponse = json_decode($response);
	//echo "<pre>"; print_r($airtableResponse); die;

	if ($err) {
		$request['url'] = $airtableTaskUrl;
		$request['data'] = $taskData;
		$ajaxResponse['log']['request'] = "Create Task";
		$ajaxResponse['log']['request_data'] = wp_json_encode($request);
		$ajaxResponse['log']['status'] = "fail";
		$ajaxResponse['log']['response'] = "Curl request fail.";
		$ajaxResponse['log']['response_data'] = wp_json_encode($err);
		$ajaxResponse['log']['sent_by'] = $wlud->data->user_email;
		$ajaxResponse['log']['log_reference'] = strtotime(date('Y-m-d h:i:s'));
		$ajaxResponse['log']['created_date'] = date('Y-m-d h:i:s');
		//Update error log
		$api = $wpdb->insert('ymf_wp_API_log', $ajaxResponse['log']);
		if($api){
			//echo 'logs inserted';
			$data['msg']= 'Something went wrong while synchronize AirTable. Contact with administrator with following reference : {'.$ajaxResponse['log']['log_reference'].'}';
			$data['status'] = 'fail';
		}
		echo wp_json_encode($data); exit;

	}else if(isset($airtableResponse->error)){

		$request['url'] = $airtableTaskUrl;
		$request['data'] = $taskData;

		$ajaxResponse['log']['request'] = 'Create Task';
		$ajaxResponse['log']['request_data'] = wp_json_encode($request);
		$ajaxResponse['log']['status'] = "fail";
		$ajaxResponse['log']['response'] = "Airtable request error";
		$ajaxResponse['log']['response_data'] = wp_json_encode($airtableResponse);
		$ajaxResponse['log']['sent_by'] = $wlud->data->user_email;
		$ajaxResponse['log']['log_reference'] = strtotime(date('Y-m-d h:i:s'));
		$ajaxResponse['log']['created_date'] = date('Y-m-d h:i:s');
		//Update error log
		$api = $wpdb->insert('ymf_wp_API_log', $ajaxResponse['log']);
       	if($api){
			//echo 'logs inserted';
			$data['msg']= 'something went wrong while getting response from AirTable. Contact with administrator with following reference : {'.$ajaxResponse['log']['log_reference'].'}';
			$data['status'] = 'fail';
		}
		echo wp_json_encode($data); exit;
	} else {
		$request['url'] = $airtableTaskUrl;
		$request['data'] = $taskData;

		$ajaxResponse['log']['request'] = "Create Task";
		$ajaxResponse['log']['request_data'] = wp_json_encode($request);
		$ajaxResponse['log']['status'] = "success";
		$ajaxResponse['log']['response'] = 'Task created. <a href="https://airtable.com/tbla2QBLhnUjtZ2nx/viw8im6rKKLvvdRMI/'.$airtableResponse->id.'" target="_blank"> click here </a>';
		$ajaxResponse['log']['response_data'] = wp_json_encode($airtableResponse);
		$ajaxResponse['log']['sent_by'] = $wlud->data->user_email;
		$ajaxResponse['log']['log_reference'] = strtotime(date('Y-m-d h:i:s'));
		$ajaxResponse['log']['created_date'] = date('Y-m-d h:i:s');
		//Update error log
		$api = $wpdb->insert('ymf_wp_API_log', $ajaxResponse['log']);
	}

	// Create Purchase order From Order Form
	//$poReference = $ponum.'#'.$calData['truckCode'].'#'.$truckCompanyData[0]->CompanyCode.'#'.$truck_data[0]->ID1.' '.$truck_data[0]->ID2.'#'.$wlud->data->airtable_ContactName."#".$truckCompanyData[0]->CompanyName;
	
	$poReference1 = $_POST['user_com'].' - '.$truckCompanyData[0]->CompanyName.' - '.strtoupper($truck_data[0]->ID1);
	//$poReference = $ponum;

	if($calData['D_Side_Art_code'] != ''){
		$poReference = $poReference."#".$calData['D_Side_Art_code'];
	}
	if($calData['P_Side_Art_code'] != ''){
		$poReference = $poReference."#".$calData['P_Side_Art_code'];
	}
	if($calData['Rear_Art_code'] != ''){
		$poReference = $poReference."#".$calData['Rear_Art_code'];
	}
	if(isset($airtableResponse->id)){
		// Create purchase order lines group by parts wharehouse
	    $powhline = array();
	    $hiddenpartslist = explode(',',$_POST['hiddenpartslist']);
	    $mailPanelsParts = '';
	    foreach ($hiddenpartslist as $valuehpl) {
	    	$hpl = explode('-',$valuehpl);
	    	// print_r($hpl);
	    	// die('$hpl');
	    	$queryparts = "SELECT * FROM yfm_wp_AT_parts_list WHERE `Parts_list_id` = '".$hpl[1]."'";
			$partsdata = $wpdb->get_results($queryparts, OBJECT);
			//Get Qty from post data
			if(isset($_POST[$valuehpl])){
				$partQty = $_POST[$valuehpl];
			}else{
				$partQty = 0;
			}
			$mailPanelsParts .= $hpl[0].' : '.$partsdata[0]->Item_Name.' (Qty : '.$partQty.')<br>';

			$dimensions = '';
			$lineDesc = array();
			$profileMapping = $partsdata[0]->Profile_Mapping;
			if($profileMapping != ''){
				$profileMapping = explode(',', $profileMapping);
				if (strpos($partsdata[0]->Purchase_Description, '_____mm') !== false) {
					$dimensions = (isset($calculation[$hpl[0].$profileMapping[0]]))? $calculation[$hpl[0].$profileMapping[0]] : '';
					$lineDesc['Purchase_Description'] = str_replace('_____mm', $dimensions." mm", $partsdata[0]->Purchase_Description);
				}else if (strpos($partsdata[0]->Purchase_Description, '____x____mm') !== false) {
					$profileMapping[0] = trim($profileMapping[0]);
					$profileMapping[1] = trim($profileMapping[1]);
					$dimensions1 = (isset($calculation[$hpl[0].$profileMapping[0]]))? $calculation[$hpl[0].$profileMapping[0]] : '';
					$dimensions2 = (isset($calculation[$hpl[0].$profileMapping[1]]))? $calculation[$hpl[0].$profileMapping[1]] : '';
					$dimensions = $dimensions1.'*'.$dimensions2.' mm';
					$lineDesc['Purchase_Description'] = str_replace('____x____mm', $dimensions, $partsdata[0]->Purchase_Description);
				}else if (strpos($partsdata[0]->Purchase_Description, 'T____GVM____') !== false) {
					$dimensions = 'T '.$truckTypeData[0]->Tare.' GVM '.$truckTypeData[0]->GVM;
					$lineDesc['Purchase_Description'] = str_replace('T____GVM____', $dimensions, $partsdata[0]->Purchase_Description);
				}else if (strpos($partsdata[0]->Purchase_Description, "Van 'Name' decal - _____") !== false) {
					$dimensions = "Van 'Name' decal - ".$_POST['truck_identification'];
					$lineDesc['Purchase_Description'] = str_replace("Van 'Name' decal - _____", $dimensions, $partsdata[0]->Purchase_Description);
				}else{
					$lineDesc['Purchase_Description'] = $partsdata[0]->Purchase_Description;
				}

			}else{
				$lineDesc['Purchase_Description'] = $partsdata[0]->Purchase_Description;
			}
			$lineDesc['Part_Code'] = $partsdata[0]->Part_Code;
			$lineDesc['Sales_Description'] = $partsdata[0]->Sales_Description;
			$lineDesc['qty'] = $_POST[$valuehpl];
			$lineDesc['unitAmount'] = 10;
			/* comment to resolve 500 error on 17-sep */
			//$lineDesc['itemdata'] = (array) $partsdata[0];
			//echo "*-*-*<pre>"; print_r($lineDesc);
			$powhline[$partsdata[0]->Warehouse]['lines'][] = $lineDesc;
	    }

		include( plugin_dir_path( __FILE__ ) . '../xero/purchaseOrder.php');
		/*print_r($xeroResponse);die('working....');
		if($xeroRes['status'] == 'success'){
			$poid= $xeroRes['po_id'] ;
		}*/

		// Xero error log entry
		if($xeroStatus == "fail"){
			$ajaxResponse['log']['request'] = "Xero po request";
			$ajaxResponse['log']['request_data'] = "";
			$ajaxResponse['log']['status'] = "fail";
			$ajaxResponse['log']['response'] = "Xero request fail";
			$ajaxResponse['log']['response_data'] = $xeroResponse['response'];
			$ajaxResponse['log']['sent_by'] = $wlud->data->user_email;
			$ajaxResponse['log']['log_reference'] = strtotime(date('Y-m-d h:i:s'));
			$ajaxResponse['log']['created_date'] = date('Y-m-d h:i:s');
			//Update error log
			$api = $wpdb->insert('ymf_wp_API_log', $ajaxResponse['log']);
			if($api){
				//echo 'logs inserted';
				$data['msg']= 'something went wrong with Xero PO Request. Contact with administrator with following reference : {'.$ajaxResponse['log']['log_reference'].'}';
				$data['status'] = 'fail';
			}
			echo wp_json_encode($data); exit;
		}else{
			$xeroLinks = '';
			foreach ($poLinks as $plkeys => $plvals) {
				$xeroLinks = $xeroLinks.' PO Link : <a href="https://go.xero.com/Accounts/Payable/PurchaseOrders/Edit/'.$plvals['id'].'"> '.$plvals['name'].' </a><br/>';
			}
			$xeroLinks = $xeroLinks.' Inv Link : <a href="https://go.xero.com/AccountsReceivable/Edit.aspx?InvoiceID='.$invLinks['id'].'"> '.$invLinks['name'].' </a><br/>';

			$ajaxResponse['log']['request'] = 'Xero po order request';
			$ajaxResponse['log']['request_data'] = '';
			$ajaxResponse['log']['status'] = "success";
			$ajaxResponse['log']['response'] = $xeroLinks;
			$ajaxResponse['log']['response_data'] = '';
			$ajaxResponse['log']['sent_by'] = $wlud->data->user_email;
			$ajaxResponse['log']['log_reference'] = strtotime(date('Y-m-d h:i:s'));
			$ajaxResponse['log']['created_date'] = date('Y-m-d h:i:s');
			//Update error log
			$api = $wpdb->insert('ymf_wp_API_log', $ajaxResponse['log']);
		}

		$Task_Type=$airtableResponse->fields->Task_Type;
		$Status=$airtableResponse->fields->Status;
		//$D_side_Artwork=$airtableResponse->fields->D_side_Artwork;
		//$P_side_Artwork=$airtableResponse->fields->P_side_Artwork;
		//$Rear_Artwork=$airtableResponse->fields->Rear_Artwork;
		//$Panels=$airtableResponse->fields->Panels;
		$Truck_ID1=wp_json_encode($airtableResponse->fields->Truck_ID1);
		$Truck_ID2=wp_json_encode($airtableResponse->fields->Truck_ID2);
		$Client=wp_json_encode($airtableResponse->fields->Client);
		$Location=wp_json_encode($airtableResponse->fields->Location);
		$Job_No=$airtableResponse->fields->Job_No;
		$Order_Date=$airtableResponse->fields->Order_Date;
		$Images=wp_json_encode($airtableResponse->fields->Images);
		$Freight_Account=$airtableResponse->fields->Freight_Account;
		$Task_id=wp_json_encode($airtableResponse->id);
		$Ordered_by=wp_json_encode($airtableResponse->fields->Ordered_by);
		$Truck_id=$airtableResponse->fields->Truck_ID1[0];;
		//echo "<pre>"; print_r($wpArray); echo "</pre>"; //die;
	  	$table_name = 'yfm_wp_AT_tasks';
		$checkDataInsert = $wpdb->insert($table_name, array(
		    'Job_No' => $Job_No,
		    'Truck_ID1' => $Truck_ID1,
			'Truck_ID2' => $Truck_ID2,
		    'Task_Type'=> $Task_Type,
		    'Status'=>$Status,
		    'Location'=> $Location,
		    'Client'=>$Client,
		    'Order_Date'=>$Order_Date,
		    'Images'=>$Images,
		    'Freight_Account'=> $Freight_Account,
		   	'Task_id'=>$Task_id,
		   	'Ordered_by'=> $Ordered_by,
		   	'Truck_id'=> $Truck_id
		));
		//echo $wpdb->last_query;
		if($checkDataInsert){
			$tastLink = '<a href="https://airtable.com/tbla2QBLhnUjtZ2nx/viw8im6rKKLvvdRMI/'.$airtableResponse->id.'"> click here </a>';
			$contactLink = '<a href="https://airtable.com/tblfJ923QFFjr89w3/viwAXRAip5DGKfT5M/'.$wlud->data->airtable_id.'"> click here </a>';
			$consignmentAddress = 'Consignment Address : '.$wlud->data->airtable_address.'.';
			$userCom= $_POST['user_com'];
			$userName= $_POST['user_name'];

			$subject = 'Your Fleet Media Order Request';
			$to = 'service@yourfleetmedia.com';
			//$to = 'yfmoperations@yopmail.com';

			$body = '<div style="width:100%;background:#eee;margin: 0;padding:50px 0;height:100%;">';
			$body .= '<table style="margin:0 auto;padding:20px;width: 700px;background:#fff;border: 1px solid #ddd;">';
			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">Order placed by :</td><td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;"> '.$userName.'</td></tr>';
			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">From :</td><td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;"> '.$userCom.'</td></tr>';
			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">Truck identification :</td><td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;"> '.$trukId1.' '.$trukId2.'</td></tr>';
			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">Purchase Order Number :</td><td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;"> '.$ponum.'</td></tr>';
			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">Installation required </td> <td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.ucfirst($isInstallation).'</td></tr>';
			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">Freight Account</td> <td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.ucfirst($Freight_Account).'</td></tr>';

			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">Panels :</td> <td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.$mailPanelsParts.'</td></tr>';

			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;"> Panel Images :</td> <td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.$mailImageLinks.'</td></tr>';

			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;"> Art Work Images :</td> <td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.$artWorkImageLinks.'</td></tr>';

			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;"> Po links :</td>';

			$body .= '<td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">';
			foreach ($poLinks as $plkeys => $plvals) {
				$body .= $plkeys.' PO Link : <a href="https://go.xero.com/Accounts/Payable/PurchaseOrders/Edit/'.$plvals['id'].'"> '.$plvals['name'].' </a><br/>';
			}
			$body .= '</td></tr>';
			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;"> Inv Link :</td>';
			$body .= '<td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;"> Inv Link :<a href="https://go.xero.com/AccountsReceivable/Edit.aspx?InvoiceID='.$invLinks['id'].'"> '.$invLinks['name'].' </a><br/></td></tr>';
			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">Task link :</td> <td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.$tastLink.'</td></tr>';
			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">Contact raised the request :</td><td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.$contactLink.'</td></tr>';
			//$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;">Consignment Address </td><td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;">'.$consignmentAddress.'</td></tr>';

			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 1px solid #ddd;"></td> <td style="float: left;padding: 10px;border-top: 1px solid #ddd;width:410px;"> </td></tr>';
			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 0px solid #ddd;"> Thanks, </td> <tr> ';
			$body .= '<tr><td style="width: 200px;float: left;padding: 10px;border-top: 0px solid #ddd;"> Your Fleet Media </td> <tr> ';
			$body .= '</table></div>';
			$headers = array('Content-Type: text/html; charset=UTF-8');
			$a = wp_mail( $to, $subject, $body, $headers);
			if($a){
				$data['msg']= 'Order placed successfully';
				$data['status'] = 'success';
				$_SESSION['form-message'] = $data;
				echo wp_json_encode($data); exit;
			}else{
				$request['mail']['request'] = "mail sending";
				$request['mail']['response'] = "issue with mail sending";
				$ajaxResponse['log']['request'] = wp_json_encode($request['mail']['request']);
				$ajaxResponse['log']['status'] = "fail";
				$ajaxResponse['log']['response'] = wp_json_encode($request['mail']['response']);
				$ajaxResponse['log']['sent_by'] = $wlud->data->user_email;
				$ajaxResponse['log']['log_reference'] = strtotime(date('Y-m-d h:i:s'));
				$ajaxResponse['log']['created_date'] = date('Y-m-d h:i:s');
				//Update error log
				$api = $wpdb->insert('ymf_wp_API_log', $ajaxResponse['log']);
				if($api){
					//echo 'logs inserted';
					$data['msg']= 'Opps! email not sent. Contact with administrator with following reference : {'.$ajaxResponse['log']['log_reference'].'}';
					$data['status'] = 'fail';
				}
				echo wp_json_encode($data); exit;
			}
		}else{
			$request['wp']['response'] = $wpdb->last_query;
			$request['wp']['request'] = "Wordpress task entry";
			$ajaxResponse['log']['request'] = wp_json_encode($request['wp']['request'] );
			$ajaxResponse['log']['status'] = "fail";
			$ajaxResponse['log']['response'] = wp_json_encode($request['wp']['response']);
			$ajaxResponse['log']['sent_by'] = $wlud->data->user_email;
			$ajaxResponse['log']['log_reference'] = strtotime(date('Y-m-d h:i:s'));
			$ajaxResponse['log']['created_date'] = date('Y-m-d h:i:s');
			//Update error log
			$api = $wpdb->insert('ymf_wp_API_log', $ajaxResponse['log']);
			if($api){
				//echo 'logs inserted';
				$data['msg']= 'Something went wrong while creating New TASK. Contact with administrator with following reference : {'.$ajaxResponse['log']['log_reference'].'}';
				$data['status'] = 'fail';
			}
			echo wp_json_encode($data); exit;
		}
	}
}
?>
