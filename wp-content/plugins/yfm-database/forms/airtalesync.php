<?php /**************************************************************************************
   *
   * This file consists of the code to create the shortcode that is used in this plugin to create the dasboard page.
   * It will further links to other forms after user logged in.
   *
   *********************************************************************************************/
   /********* Create apilogs Shortcode********/

   function airtable_sync(){
   	require_once(ABSPATH.'/wp-config.php');
   	require_once(ABSPATH.'/wp-load.php');
   	require_once(ABSPATH.'/wp-includes/wp-db.php');
   	require_once( ABSPATH.'wp-admin/includes/user.php' );
   	global $wpdb;
   	$userid= get_current_user_id();
   	$user = get_user_by('id',$userid);
   	$queryUsers = "SELECT * FROM yfm_wp_AT_contacts where Contact_id= '".$user->data->airtable_id."'";
   	$contact = $wpdb->get_results($queryUsers, OBJECT);
   	if(!empty($contact)){
   		$position = strtolower(str_replace(' ','',$contact[0]->Position));
   		if(strcmp($position,'newuser')==0){
   			 wp_logout();
   			 wp_redirect( site_url().'?acc=inprogress' );
   		}
   	}
   	if('yes' === get_user_meta( get_current_user_id(), sanitize_key( 'user_disabled' ), true ) ){
			$error_message = get_option( 'baba_locked_message' );
			echo "Your account is no longer active, Please contact the administrator.";
			wp_logout();
			wp_redirect( site_url().'?acc=disabled' );
   }else{
   	if(!is_user_logged_in()){ $loginLink = site_url()."/login"; ?>
			<script type="text/javascript"> window.location="<?php echo $loginLink;?>"; </script>
<?php }
   	if(is_user_logged_in()){
   		include( plugin_dir_path( __FILE__ ) .'../includes/include.php'); // include scripts and styles
  		global $wpdb;
  		$queryLogs = "SELECT * FROM ymf_wp_API_log ORDER BY id DESC";
  		$logs = $wpdb->get_results($queryLogs, OBJECT);
   		//echo "<prE>"; print_r($logs); ///die;
  ?>
<div class="login-panel">
   <div class="container">
      <div class="dashboard-sec">
         <h4>Airtable Tables</h4>
         <div class="row">
            <table id="sync" class="table table-striped table-bordered" style="width:100%">
               <thead>
                  <tr>
                     <th>Table Name</th>
                     <th>Actions</th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td>Companies (<?php echo $wpdb->get_var( "SELECT COUNT(*) FROM yfm_wp_AT_companies" );?>)</td>
                     <td><button class="btn btn-primary" ><a href="<?php echo site_url();?>/airtable/companies.php" target="_blank" id="">Sync Now</button></td>
                  </tr>
                  <tr>
                     <td>Locations (<?php echo $wpdb->get_var( "SELECT COUNT(*) FROM   yfm_wp_AT_locations" );?>)</td>
                     <td><button class="btn btn-primary" ><a href="<?php echo site_url();?>/airtable/locations.php" target="_blank" id="">Sync Now</button></td>
                  </tr>
                  <tr>
                     <td>Contacts (<?php echo $wpdb->get_var( "SELECT COUNT(*) FROM yfm_wp_AT_contacts" );?>)</td>
                     <td><button class="btn btn-primary" ><a href="<?php echo site_url();?>/airtable/contacts.php" target="_blank" id="">Sync Now</button></td>
                  </tr>
                  <tr>
                     <td>Trucks (<?php echo $wpdb->get_var( "SELECT COUNT(*) FROM yfm_wp_AT_trucks" );?>)</td>
                     <td><button class="btn btn-primary" ><a href="<?php echo site_url();?>/airtable/trucks.php" target="_blank" id="">Sync Now</button></td>
                  </tr>
                  <tr>
                     <td>Tasks (<?php echo $wpdb->get_var( "SELECT COUNT(*) FROM yfm_wp_AT_tasks" );?>)</td>
                     <td><button class="btn btn-primary" ><a href="<?php echo site_url();?>/airtable/tasks.php" target="_blank" id="">Sync Now</button></td>
                  </tr>
                  <tr>
                     <td>Truck Types (<?php echo $wpdb->get_var( "SELECT COUNT(*) FROM yfm_wp_AT_truck_types" );?>)</td>
                     <td><button class="btn btn-primary" ><a href="<?php echo site_url();?>/airtable/trucks_types.php" target="_blank" id="">Sync Now</button></td>
                  </tr>
                  <tr>
                     <td>Bodies (<?php echo $wpdb->get_var( "SELECT COUNT(*) FROM yfm_wp_AT_bodies" );?>)</td>
                     <td><button class="btn btn-primary" ><a href="<?php echo site_url();?>/airtable/bodies.php" target="_blank" id="">Sync Now</button></td>
                  </tr>
                  <tr>
                     <td>Chasis (<?php echo $wpdb->get_var( "SELECT COUNT(*) FROM yfm_wp_AT_chassis" );?>)</td>
                     <td><button class="btn btn-primary" ><a href="<?php echo site_url();?>/airtable/chassis.php" target="_blank" id="">Sync Now</button></td>
                  </tr>
                  <tr>
                     <td>Campaigns (<?php echo $wpdb->get_var( "SELECT COUNT(*) FROM yfm_wp_AT_campaign" );?>)</td>
                     <td><button class="btn btn-primary" ><a href="<?php echo site_url();?>/airtable/campaigns.php" target="_blank" id="">Sync Now</button></td>
                  </tr>
                  <tr>
                     <td>Side Artwork (<?php echo $wpdb->get_var( "SELECT COUNT(*) FROM yfm_wp_AT_side_artwork" );?>)</td>
                     <td><button class="btn btn-primary" ><a href="<?php echo site_url();?>/airtable/side_artwork.php" target="_blank" id="">Sync Now</button></td>
                  </tr>
                  <tr>
                     <td>Rear Artwork (<?php echo $wpdb->get_var( "SELECT COUNT(*) FROM yfm_wp_AT_rear_artwork" );?>)</td>
                     <td><button class="btn btn-primary"><a href="<?php echo site_url();?>/airtable/rear_artwork.php" target="_blank" id="">Sync Now</button></td>
                  </tr>
                  <tr>
                     <td>Panel Calcs (<?php echo $wpdb->get_var( "SELECT COUNT(*) FROM yfm_wp_AT_panel_calcs" );?>)</td>
                     <td><button class="btn btn-primary"><a href="<?php echo site_url();?>/airtable/panel_calcs.php" target="_blank" id="">Sync Now</button></td>
                  </tr>
                  <tr>
                     <td>Parts List (<?php echo $wpdb->get_var( "SELECT COUNT(*) FROM yfm_wp_AT_parts_list" );?>)</td>
                     <td><button class="btn btn-primary" ><a href="<?php echo site_url();?>/airtable/parts_list.php" target="_blank" id="">Sync Now</button></td>
                  </tr>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>
<?php }else{
   echo do_shortcode('[login_form]');
   } } ?>
<?php }
   add_shortcode('airtable_sync', 'airtable_sync'); // EO Register Form shortcode
   ?>
