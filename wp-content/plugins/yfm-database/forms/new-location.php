<?php /**************************************************************************************
*
* This file consists of the code to create the new location for the logged in user
*
*********************************************************************************************/

/********* Create Register Form Shortcode********/

function addLocationform_creation(){
	
	if(is_user_logged_in()){
	 $current_user = wp_get_current_user();
	
	//include( plugin_dir_path( __FILE__ ) . '../airtables/get-companies.php'); // get companies list
	global $wpdb;	
	$companyDataQry = "SELECT * FROM yfm_wp_AT_companies where company_id = '".$current_user->data->airtable_company_id."'";          
	$companyData = $wpdb->get_results($companyDataQry, OBJECT);

	//echo "<prE>"; print_r($companyData); die;

	//include( plugin_dir_path( __FILE__ ) .'../airtables/get-locations.php'); // get locations list 	
	//$locationDataQry = "SELECT Location_id,Fulladdress FROM yfm_wp_AT_locations";          
	//$locationData = $wpdb->get_results($locationDataQry, OBJECT);
	$locationData = array();
	include( plugin_dir_path( __FILE__ ) .'../includes/include.php'); // include scripts and styles   
	$current_user = wp_get_current_user();
?>



<div class="login-panel">
        <div class="container">
            <div class="row ">
                <div class="col-sm-5">
                    <div class="login-detail register">
                    	<h1>Add New Location</h1>
	<div id="msg"></div>
	<!-- <div class="notes">
		<h4>Fields marked with <sup>*</sup> are mandatory.</h4>
	</div> -->
	
<form name="newlocation" id="newlocation" method="post" action="#">
	<input type="hidden" name="action" value="register_form">  	
		<div class="form-group">			 
			<label>Username (email)</label>				
				<input type="email" name="user_email" id="user_email" value="<?php echo $current_user->user_email;?>" class="form-control success" placeholder="Email Address" readonly>					
		</div>

		<div class="form-group">		 
				<label>Company</label>	
				<input type="text" name="company" id="company" value="<?php echo $companyData[0]->CompanyName;?>" class="form-control success" placeholder="company" readonly>	

				<input type="hidden" name="company_id" id="company_id" value="<?php echo $current_user->data->airtable_company_id;?>" class="form-control success" placeholder="company" readonly>			
								
		</div>
		 
		
		<div class="location-data">
			<div class="form-group">			 	
						<label class="lbl-bold"><!-- Address Details <br>-->Street Address</label>					
						<input type="text" class="form-control" placeholder="Street Address" name="street_address1" id="street_address1">		                
						
			</div>
	        <div class="form-group">			 	
						<label class="lbl-bold"><span>Street Address Line 2</span></label>					
						<input type="text" class="form-control" placeholder="Street Address Line 2" id="street_address_2_1" name="street_address_2_1">		                 
						
			</div>
	        <div class="form-group">			 	
				<label class="lbl-bold">Suburb</label>					
		        <input type="text" class="form-control" placeholder="Suburb" name="suburb1" id="suburb1">
		    </div>
		    <div class="form-group">         		
		        <label class="lbl-bold">State</label>	
		        <select class="form-control" id="state1" name="state1">
		            <option value="">Select State</option>
		            <option value="SA">SA</option>
					<option value="NSW">NSW</option>
					<option value="VIC">VIC</option>
					<option value="WA">WA</option>
					<option value="NT">NT</option>
					<option value="QLD">QLD</option>
					<option value="TAS">TAS</option>
					<option value="ACT">ACT</option>		             
				</select>
		    </div>
		    <div class="form-group">             		
				<label class="lbl-bold">Post Code</label>                  
				<input type="text" class="form-control" placeholder="Post Code" name="post_code1" id="post_code1">
			</div>

		</div> <?php /*****EO location-data*******/?>
		<div class="form-group">		
				<label>Phone</label>			
				<input type="tel" name="phone" value="" maxlength="16" id="phone" class="form-control phoneNo success" placeholder="Mobile">					
		</div>
        <div class="form-group">		 
				<label>Phone</label>			
				<input type="tel" name="phone2" value="" maxlength="16" id="phone2" class="form-control phoneNo success" placeholder="Landline">
		</div>
		<div class="text-center">
			<input type="submit" name="wp-submit" id="wp-submit" class="btn btn-orange btn-primary addLocation" value="Add Location">
		</div>
		    
    <div id="loader" style="display: none;"></div>
    <div id="overlay" style="display: none;"></div>
</form>
</div>
</div>
<div class="col-sm-7">
                    <div class="register-right">
                        <img src="<?php echo get_stylesheet_directory_uri();?>/img/truck-img.jpg" alt="truck-img" />
                        <h2>Unleash the power of your fleet with high impact truck advertising campaigns</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
} else{
	echo do_shortcode('[login_form]');
}

}
add_shortcode('Add_NewLocation_form', 'addLocationform_creation'); // EO Add New Location Form shortcode

add_action('wp_ajax_newlocation_form', 'newlocation_form');
add_action('wp_ajax_nopriv_newlocation_form', 'newlocation_form');
function newlocation_form()
{	
	global $wpdb;
	
	// Create location at airtable
			$locationData =[];
			$locationData=[
			    'fields'=>[
			   	'ID1'=>$_POST['suburb1'],		   	
			   	'Street'=>$_POST['street_address1'].' '.$_POST['street_address_2_1'],
			    //'street_address_2'=>$_POST['street_address_2_1'],
			    'Suburb'=>$_POST['suburb1'],
			    'State'=> $_POST['state1'],
			    'Postcode'=> $_POST['post_code1'],
			    'Email'=> $_POST['user_email'],
			    'Phone1'=> $_POST['phone'],
			    'Phone2'=> $_POST['phone2'],
			    "Company"=>[$_POST['company']],
				]
			];


			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Locations",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			//CURLOPT_POSTFIELDS => "{\n  \"fields\": {\n    \"Truck_ID1\": [\n      \"rec0z32nKnCHIQhzu\"\n    ],\n    \"Task_Type\": \"Repairs (Parts & Labour)\",\n    \"Images\": [\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC.jpg\"\n      },\n      {\n        \"url\": \"http://docpoke.in/ymf/wp-content/uploads/2018/09/Vishal-sharma-PPC-1.jpg\"\n      }\n    ],\n    \"Job Type\": [\n      \"Order\"\n    ],\n    \"Status\": \"Client response?\"\n  }\n}",
			CURLOPT_POSTFIELDS => json_encode($locationData),
			CURLOPT_HTTPHEADER => array(
				"authorization: Bearer keyw3cJfiAFXmYjJf",
				"cache-control: no-cache",
				"content-type: application/json",
				"postman-token: d6aa4070-c2cd-df5f-cd62-0f25e59be5e2"
				),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);

			$airtableLocationResponse = json_decode($response);
			//echo "<pre>"; print_r($airtableLocationResponse); echo "</pre>"; die;
			$location_id = $airtableLocationResponse->id;

	if(!empty($location_id))
	{
		$data['msg'] = __('New Location added Successfully.');
		$data['status'] = __('success');
	}
	else{
		$data['msg'] = __('Error while adding new location');
		$data['status'] = __('danger');
	}
	
	return wp_send_json($data);
			

}