<?php /**************************************************************************************
*
* This file consists of the code to create the shortcode that is used in this plugin
* to create the login forms.
*
*********************************************************************************************/
/********* Create Login Form Shortcode********/

function loginform_creation(){
	require_once(ABSPATH.'/wp-config.php');
	require_once(ABSPATH.'/wp-load.php');
	require_once(ABSPATH.'/wp-includes/wp-db.php');

	global $wpdb;
	require_once( ABSPATH.'wp-admin/includes/user.php' );
include( plugin_dir_path( __FILE__ ) .'../includes/include.php'); // include scripts and styles
$queryUsers = "SELECT * FROM wp_users Where ID!='1' ORDER BY ID DESC";
$Users = $wpdb->get_results($queryUsers, OBJECT);
//echo "<pre>"; print_r($Users); ///die;
foreach ($Users as $user) {
	$CompanyDetail = "SELECT * FROM yfm_wp_AT_contacts Where Contact_id= '".$user->airtable_id."' ";
	$Details = $wpdb->get_results($CompanyDetail, OBJECT);
	if(!empty($Details))
	{
	
	}
	else{
		//$user = wp_delete_user($user->ID);
		// if($user)
		// {
		//   echo $user->email.' is deleted';
		// }
		// else{
		//     echo $user->email.' is not deleted';
		// }
	}
}

	if(!is_user_logged_in()){

		?>

<div class="login-panel">
    <div class="container">
        <div class="row ">
            <div class="col-sm-5">
                <div class="login-detail">
									<?php if(isset($_GET['acc']) && $_GET['acc']=='disabled')
														{
																echo '<div class="notice notice-danger">
																 Your account is no longer active, Please contact the administrator.
															</div>';
														}
									if(isset($_GET['acc']) && $_GET['acc']=='inprogress')
														{
																echo '<div class="notice notice-danger">
																 Your registration is in progress.
																</div>';
														}?>
                    <h1>Login Now</h1>
                    <!-- <form name="loginform" id="loginform" action="<?php echo esc_url(home_url('wp-login.php?wpe-login=true'));?>" method="post">
						<p>
							<label for="user_login">Username (email)<br>
							<input type="text" name="log" id="user_login" class="input" value="" placeholder="Username"></label>
						</p>
						<p>
							<label for="user_pass">Password<br>
							<input type="password" name="pwd" id="user_pass" class="input" value="" placeholder="Password"></label>
						</p>
							<p class="forgetmenot">
								<label for="rememberme">
									<input name="rememberme" type="checkbox" id="rememberme" value="forever"> <span class="label-text">Remember Me</span>
								</label>
							</p>
						<p class="submit form-group text-center">
							<input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="Log In">
							<input type="hidden" name="redirect_to" value="<?php echo esc_url(home_url('dashboard'));?>">
							<input type="hidden" name="testcookie" value="1">
						</p>
					</form> -->
					<?php echo do_shortcode('[theme-my-login]'); ?>




                </div>
            </div>
            <div class="col-sm-7">
                <div class="login-right">
                    <img src="<?php echo get_stylesheet_directory_uri();?>/img/truck-img.jpg" alt="truck-img" />
                    <h2>Unleash the power of your fleet with high impact truck advertising campaigns</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<?php	}
else{
	echo "You are already logged in! Click here to <a href='".wp_logout_url( home_url() )."'>logout</a>";
}


}
add_shortcode('login_form', 'loginform_creation'); // EO Register Form shortcode
