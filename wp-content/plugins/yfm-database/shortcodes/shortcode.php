<?php /**************************************************************************************
*
* This file consists of the shortcodes that is used in this plugin to create the forms.
* It will automatically creates the pages in wordpress admin.
*
*********************************************************************************************/

// require wp-load.php to use built-in WordPress functions
 require_once(ABSPATH."wp-load.php");
if (isset($_GET['activated']) && is_admin()){

/******* Create Login Page *********/

 $login_page = array(
	 'post_title' => 'Login',
	 'post_content' => '[login_form]',
	 'post_status' => 'publish',
	 'post_author' => 1,
	 'post_type' => 'page',
 );

 $page_check = get_page_by_title('Login');
 if(!isset($page_check->ID)){
		$new_page_id = wp_insert_post($login_page);
		
	}


/******* Create Register Page *********/

 $register_page = array(
 'post_title' => 'Register',
 'post_content' => '[register_form]',
 'post_status' => 'publish',
 'post_author' => 1,
 'post_type' => 'page',

 );
 
$page_check = get_page_by_title('Register');
 if(!isset($page_check->ID)){
		$new_page_id = wp_insert_post($register_page);
		
	}

/******* Create Forget Password Page *********/

 $register_page = array(
 'post_title' => 'Forget Password',
 'post_content' => '[forget_password]',
 'post_status' => 'publish',
 'post_author' => 1,
 'post_type' => 'page',

 );
 
$page_check = get_page_by_title('Forget Password');
 if(!isset($page_check->ID)){
		$new_page_id = wp_insert_post($register_page);
		
	}

/******* Create Dashboard Page *********/
 
 $xero_auth = array(
 'post_title' => 'Dashboard',
 'post_content' => '[dashboard]',
 'post_status' => 'publish',
 'post_author' => 1,
 'post_type' => 'page',

 );
 
$page_check = get_page_by_slug('xero-auth');
 if(!isset($page_check->ID)){
		$new_page_id = wp_insert_post($xero_auth);
		
	}

/******* Create Overview Order Page *********/
 
 $xero_auth = array(
 'post_title' => 'Overview Order',
 'post_content' => '[order_form]',
 'post_status' => 'publish',
 'post_author' => 1,
 'post_type' => 'page',

 );
 
$page_check = get_page_by_slug('overview-order');
 if(!isset($page_check->ID)){
		$new_page_id = wp_insert_post($xero_auth);
		
	}

/******* Create Truck Report Page *********/
 
 $xero_auth = array(
 'post_title' => 'Truck Report',
 'post_content' => '[truck_report_form]',
 'post_status' => 'publish',
 'post_author' => 1,
 'post_type' => 'page',

 );
 
$page_check = get_page_by_slug('truck-report');
 if(!isset($page_check->ID)){
		$new_page_id = wp_insert_post($xero_auth);
		
	}
/******* Create Job Completion Page *********/
 
 $xero_auth = array(
 'post_title' => 'Job Completion',
 'post_content' => '[job_completion]',
 'post_status' => 'publish',
 'post_author' => 1,
 'post_type' => 'page',

 );
 
$page_check = get_page_by_slug('job-completion');
 if(!isset($page_check->ID)){
		$new_page_id = wp_insert_post($xero_auth);
		
	}

/******* Create Xero Auth Page *********/
 
 $xero_auth = array(
 'post_title' => 'Xero Auth',
 'post_content' => '[xero_login_auth]',
 'post_status' => 'publish',
 'post_author' => 1,
 'post_type' => 'page',

 );


 /******* Api log page *********/
 
 $xero_auth = array(
 'post_title' => 'Logs',
 'post_content' => '[apilogs]',
 'post_status' => 'publish',
 'post_author' => 1,
 'post_type' => 'page',

 );
 
$page_check = get_page_by_slug('xero-auth');
	 if(!isset($page_check->ID)){
		$new_page_id = wp_insert_post($xero_auth);
		
	}

/******* Add New Location page *********/
 
 $new_location = array(
 'post_title' => 'New Location',
 'post_content' => '[Add_NewLocation_form]',
 'post_status' => 'publish',
 'post_author' => 1,
 'post_type' => 'page',

 );
 
$page_check = get_page_by_slug('new-location');
	 if(!isset($page_check->ID)){
		$new_page_id = wp_insert_post($new_location);
		
	}

}