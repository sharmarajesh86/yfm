
//jQuery('#company').searchableOptionList({ maxHeight: '300px', showSelectAll: true });
jQuery.noConflict();
jQuery(document).ready(function($) {
    $('#locationsList,#company').searchableOptionList({ maxHeight: '300px', showSelectAll: true });

});

jQuery('#company').change(function(){
	var company = jQuery(this).val();
	var selected = $(this).find('option:selected');
	var com_id = jQuery(selected).attr('data-id');
	var ajaxurl = '<?php echo get_stylesheet_directory_uri();?>"/locations.php"';
	if(company == 'other')
	{
		jQuery('.other-company').css({"display":"block"});
		jQuery('#locations').hide();
	}
	else{
		jQuery('.other-company').css('display','none');
		jQuery('#locations').show();

		console.log('com_id'+com_id);
	$.ajax({
    url: "https://api.airtable.com/v0/apprTuWAjlI7FBBPb/Companies/"+com_id,
    beforeSend: function(xhr) {
      xhr.setRequestHeader("Authorization", "Bearer " +"keyoA5KNgdkmd3iDG");
    },
    type: 'GET',
    dataType: 'json',
    contentType: 'application/json',
    processData: false,
    data: '{"foo":"bar"}',
    success: function (data) {
      //alert(JSON.stringify(data.fields.Locations));
      var location = data.fields.Locations;

      if(location.length >=2)
      {
      	$("#locations option").each(function() {
		    $(this).remove();
		});
      	for($i=0;$i<location.length;$i++)
     	{
	      	$.ajax({
			    url: "https://api.airtable.com/v0/apprTuWAjlI7FBBPb/Locations/"+location[$i],
			    beforeSend: function(xhr) {
			      xhr.setRequestHeader("Authorization", "Bearer " +"keyoA5KNgdkmd3iDG");
			    },
			    type: 'GET',
			    dataType: 'json',
			    contentType: 'application/json',
			    processData: false,
			    data: '{"foo":"bar"}',
			    success: function (data) {
			    	//alert((JSON.stringify(data.fields.Fulladdress)));
			    	var fulladdress = data.fields.Fulladdress;
			    	$('#locations select').append('<option value="'+location+'">'+fulladdress+'</option>')

		      	},
		      	error: function(){
			      alert("Cannot get location data");
			    }
			 });
      	}
      }
      else{

      	$.ajax({
		    url: "https://api.airtable.com/v0/apprTuWAjlI7FBBPb/Locations/"+location,
		    beforeSend: function(xhr) {
		      xhr.setRequestHeader("Authorization", "Bearer " +"keyoA5KNgdkmd3iDG");
		    },
		    type: 'GET',
		    dataType: 'json',
		    contentType: 'application/json',
		    processData: false,
		    data: '{"foo":"bar"}',
		    success: function (data) {
		    	//alert((JSON.stringify(data.fields.Fulladdress)));
		    	var fulladdress = data.fields.Fulladdress;
	      		$('#locations select').html('<option value="'+location+'">'+fulladdress+'</option>');
	      	},
	      	error: function(){
		      alert("Cannot get location data");
		    }
		 });

      }


    },
    error: function(){
      alert("Cannot get data");
    }
});
	}

});
jQuery(document).ready(function ()
{
	 jQuery('#registerform').validate({
   	submitHandler: function(form) {
            $(form).ajaxSubmit();
        },
    rules: {
        user_email: {
          required: true,
          email: true
        },
        credentials: {
          required: true,
          minlength: 8
        },
        full_name: {
          required: true,
        }
    }

  });
});

jQuery(document).ready(function()
{
	jQuery('.rgisterUser').click(function(){
		var firstName;
		var lastName;
		var user_email = $('#user_email').val();
		if(user_email != "")
		{
			var credentials = $('#credentials').val();
			var full_name 	= $('#full_name').val();
			if(full_name != "")
			{
				var ret = full_name.split(" ");
				var firstName = ret[0];
				var lastName = ret[1];
			}
			$.ajax({
				type:"POST",
				url:ajaxurl,
				data:{action: 'register_form', 'user_email':user_email,'credentials':credentials,'firname':firstName,'lasname':lastName},
				dataType:"json",
				success:function(response)
				{					
					if(response  != "")
					console.log('registered successfully');
					else
					console.log('error occured..');
				},
				error:function(){
					console.log('failed!');
				}
			});
		}
		else {
			$('#user_email').addClass('error-check');
			$('.error-email').remove();
			$('#user_email').after('<div class="error-email error-common">Enter email address</div>');
			// $('.error-email').remove(5000);
			return false;
		}
	});

/* Ravinder 14-08-2018 */
	jQuery(document).on('blur','#user_email',function(){
		var Obj = 	$(this);
		email 	= 	Obj.val();
		if(email != "")
		{
			if(!ValidateEmail(email))
			{
				$('#user_email').val('');
				$('#user_email').addClass('error-check');
				$('.error-email').remove();
				$('#user_email').after('<div class="error-email error-common">Enter valid email address</div>');
			}
			else
			{
				$('#user_email').removeClass('error-check');
				$('.error-email').remove();
			}
		}
	});

	jQuery(document).on('keyup blur', '.phoneNo', function(e)
	{
		e.preventDefault();
		var key  = e.charCode || e.keyCode || 0;
		if (key >= 65 && key <= 90)
		{
			this.value = this.value.replace(/[^\d]/g,'');
			return false;
		}
		$phone   = $(this);
		var val  = $phone.val();
		val 	 = val.replace(/\-+/g, '');
		var len = val.length;

		if( len <= 10 )
		{
			if ((key !== 8 && key !== 9))
			{
				var first  = val.substring(0, 3);
				var sec1   = val.substring(3, 6);
				var sec2   = val.substring(6, 10);
				var newPhone = first;
				if(sec1)
					newPhone = newPhone+'-'+sec1;
				if(sec2)
					newPhone = newPhone+'-'+sec2;
				$(this).val(newPhone);
			}
		}
		else if( len <= 11 )
		{
			if ((key !== 8 && key !== 9))
			{
				var first  = val.substring(0, 3);
				var sec1   = val.substring(3, 7);
				var sec2   = val.substring(7, 11);
				var newPhone = first;
				if(sec1)
					newPhone = newPhone+'-'+sec1;
				if(sec2)
					newPhone = newPhone+'-'+sec2;
				$(this).val(newPhone);
			}
		}
		else if( len <= 13 )
		{
			if ((key !== 8 && key !== 9))
			{
				var first  = val.substring(0, 3);
				var sec1   = val.substring(3, 6);
				var sec2   = val.substring(6, 9);
				var sec3   = val.substring(9, 13);
				var newPhone = first;
				if(sec1)
					newPhone = newPhone+'-'+sec1;
				if(sec2)
					newPhone = newPhone+'-'+sec2;
				if(sec3)
						newPhone = newPhone+'-'+sec3;
				$(this).val(newPhone);
			}
		}
		// Allow numeric (and tab, backspace, delete) keys only
		return (key == 8 ||
				key == 9 ||
				key == 46 ||
				(key >= 48 && key <= 57) ||
				(key >= 96 && key <= 105));
	});

});

function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
};
