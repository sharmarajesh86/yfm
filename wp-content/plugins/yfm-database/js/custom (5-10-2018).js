jQuery.noConflict();
jQuery(document).ready(function($) {
    jQuery('#locations,#company,#truck_identification').searchableOptionList({ maxHeight: '300px', showSelectAll: false });

});
var ajaxurl = document.location.origin+"/yfm/wp-admin/admin-ajax.php";
jQuery(document).ready(function()
{	
	/****** Login Form Validation *********/
	jQuery('.login').click(function(){		
		jQuery('#loginform').validate({
            rules: {
	            log: {
	               required: true	               
	               },
	            pwd: {
	               required: true	          
	               },	            
            },
            messages:{
            	log:"Kindly enter username!",
            	pwd:"Kindly enter password!"
            }
        });
       
    });

    /****** Forget Password Form Validation *********/

	jQuery('.forgetPass').click(function(){		
		
		jQuery('#lostpasswordform').validate({
            rules: {
	            user_login: {
	               required: true	               
	               },	                     
            },
            messages:{
            	user_login:"Kindly enter your Username or Email Address!",
            	
            },
           /* submitHandler: function(form) {
            	
            }*/
        });
       
    });
});

/****** Overview Order Form *********/

jQuery(document).ready(function()
{	

	// Get truck details	
	jQuery('.sol-label-text').click(function(){
		alert(jQuery(this).val());		
		alert(jQuery(this).attr('data-id'));	
	});

	// Get truck details	
	jQuery('#truck_identification').click(function(){
		//alert(jQuery(this).val());		
		//alert(jQuery(this).attr('data-id'));	
	});

	/****** Show Relevant Part Name********/
	//jQuery('input[type="checkbox"]').click(function(){
	jQuery('.panelchk').click(function(){
		//alert('hi');
	
		var truckID1 = jQuery('#truck_identification').val();	
		//alert(truckID1);		
		var show_data = jQuery(this).val();		
		//alert(show_data);
		jQuery('#'+show_data).toggleClass('hide');
		var panel_type = jQuery(this).attr('panel_type');
		//console.log('panel_type:'+panel_type);
		//alert('#'+show_data);
		if(jQuery(this).prop("checked") == false){
            jQuery('#'+show_data+' #'+show_data+'_part option').remove();            
        }else{		
			jQuery.ajax({
			    url:ajaxurl,
			    type: 'POST',
			    dataType: 'json',		   	 
				data:{action: 'parts_list','panel_type':panel_type,'truck':truckID1},
			    success: function (response) {
			    	//console.log(response.length);
			    	//console.log(response);
			    	//return false;
			    	var item_name = '';
			    	if(response.partsData.length !== 0){
				    	item_name += '<optgroup label="Panel Parts">';
				    	jQuery.each(response.partsData,function(index,value){ 
				    		//console.log(value);
				    		item_name += '<option data-Parts_list_id='+value.Parts_list_id+' data-Artwork='+value.Artwork+'>'+value.Item_Name+'</option>';
				    	});
				    	item_name += ' </optgroup>';	
			    	}	    	
			    	if(response.partsData2.length !== 0){
			    		item_name += ' <optgroup label="Additional Generic Parts">';
			    		jQuery.each(response.partsData2,function(index,value){ 
				    		//console.log(value);
				    		item_name += '<option data-Parts_list_id='+value.Parts_list_id+' data-Artwork='+value.Artwork+'>'+value.Item_Name+'</option>';
				    	});
				    	item_name += ' </optgroup>';
			    	}
			    	
			    	jQuery('#'+show_data+' #'+show_data+'_part').html(item_name);
			    	jQuery('#'+show_data+' #'+show_data+'_part').multipleSelect({
			            placeholder: "Select Parts"
			        });

			    	// jQuery();
			    	//jQuery('#caldata').html('<p> ---------------------------- </p>');
			    	/*
			    	jQuery.each(response.calData,function(key,data){ 
			    		jQuery('#caldata').append("<p> "+key+" : "+data+" </p>");
			    	});
			    	jQuery.each(response.panelCals,function(key,data){ 
			    		jQuery('#caldata').append("<p> "+key+" : "+data+" </p>");
			    	});
			    	jQuery.each(response.calculation,function(key,data){ 
			    		jQuery('#caldata').append("<p> "+key+" : "+data+" </p>");
			    	});
			    	*/
			    	
			    },
			    error: function(error){
				    console.log(error);			    
			    }
			});
		}
		

	}); /***** EO Part Name Code ****/


	jQuery('.show-qty-popup').click(function(){
		jQuery("#partqtysection").html('');

		var chkArray = [];	
		var hdnpartqtyname = [];
		var ptyrowhtml = '';
		var nopartcount = 1;
		alert('///');
		jQuery(".panelchk:checked").each(function() {
			//chkArray.push(jQuery(this).val());
			var panelName = jQuery(this).val();
			var panel_type = jQuery(this).attr('panel_type');
			ptyrowhtml = '<h3>'+panelName+'</h3>';
			//selecte part ids
			var spids = []; 
			var partcount = 0;		

			//alert(jQuery('#'+panelName+'_part').length);
			jQuery(jQuery('#'+panelName+'_part option')).size(); 

			jQuery('#'+panelName+'_part option:selected').each(function(){				
				partcount = 1;
				var partqtyname = panel_type+'-'+jQuery(this).data('parts_list_id');
				var partname = jQuery(this).val();
				alert(partname);
				hdnpartqtyname.push(partqtyname);
				ptyrowhtml = ptyrowhtml+'<div class="form-group"><div class="row"><div class="col-md-6"><label>'+partname+'<sup>*</sup></label></div><div class="col-md-6"><input type="number" min="1" max="20" class="form-control popup_qty_txt" name="'+partqtyname+'" value="1"></div></div></div>';
			});
										
			if(parseInt(partcount) == 0){
				nopartcount = 0;
				jQuery("#partqtysection").append('<p>'+ptyrowhtml+'No parts selected.Please select atleast one part.<p>');
			}else{
				jQuery("#partqtysection").append(ptyrowhtml);
			}
						
		});
		jQuery("#hiddenpartslist").val(hdnpartqtyname);
		//alert(hdnpartqtyname);
		if(parseInt(nopartcount) == 0){
			//jQuery(".popup-submit").hide();
		}else{
			jQuery(".popup-submit").show();
		}
		if(ptyrowhtml == ''){
			jQuery(".overview-order").trigger('click');
		}else{
			jQuery('#qtyModal').modal('toggle');	
		}
			
	});

	// Pop Quantity validation
	jQuery('.popup_qty_txt').blur(function(){
		if(parseInt(jQuery(this).val()) > 0 && parseInt(jQuery(this).val()) < 21 ){

		}else{
			//jQuery(this).addClass("has-error");
		}
	});

	
	jQuery('.popup-submit').click(function(){
		jQuery('#qtyModal').modal('hide');
		jQuery(".overview-order").trigger('click');
	});


	jQuery('.show-qty-popup-report').click(function(){
		jQuery("#partqtysection").html('');

		var chkArray = [];	
		jQuery(".panelchk:checked").each(function() {
			//chkArray.push(jQuery(this).val());
			var panelName = jQuery(this).val();
			var panel_type = jQuery(this).attr('panel_type');
			var ptyrowhtml = '<h3>'+panelName+'</h3>';
			//selecte part ids
			var spids = []; 
			jQuery('#'+panelName+'_part option:selected').each(function(){	
				var partqtyname = panel_type+'-'+jQuery(this).data('parts_list_id');
				var partname = jQuery(this).val();
				ptyrowhtml = ptyrowhtml+'<div class="form-group"><div class="row"><div class="col-md-6"><label>'+partname+'<sup>*</sup></label></div><div class="col-md-6"><input type="number" min="1" max="20" class="form-control popup_qty_txt" name="'+partqtyname+'" value="1"></div></div></div>';
			});
			jQuery("#partqtysection").append(ptyrowhtml);			
		});
		jQuery('#qtyModalReport').modal('toggle');
	});

	jQuery('.popup-submit-report').click(function(){
		jQuery('#qtyModalReport').modal('hide');
		jQuery(".truck-submit").trigger('click');
	});

	//Popup validation//
	jQuery(document).on('keyup','.popup_qty_txt', function(){
		if(jQuery(this).val() > 20){
			alert("No numbers above 20");
		    jQuery(this).val('20');
		}
		if(jQuery(this).val() < 1){
			alert("No numbers less than 1");
		    jQuery(this).val('1');
		}
	});

	/**************** Driver Side Part Names Select ***********************/

	//jQuery('#driver_side_part').change(function(e){
		
		//alert(this.options[e.target.selectedIndex].text);
		//var optionId = jQuery('option:selected', this).attr('data-parts_list_id');
		//alert(ddd);
		/*
		jQuery("#driver_side_parts").removeClass('hide');
		var optionId = jQuery('option:selected', this).attr('data-parts_list_id');
		//alert(optionId);
		var partLabel = this.options[e.target.selectedIndex].text;		
		var rowhtml = '<div class="row dpartqtyrow" id="'+optionId+'" ><div class="col-md-6"><label>'+partLabel+'<sup>*</sup></label></div><div class="col-md-3"><input type="text" class="form-control" name="" id=""></div></div>';
		jQuery("#driver_side_parts_qty").append(rowhtml);
		*/
		// Selecte option array
		/*
		var sdpids = []; 
		jQuery('#driver_side_part option:selected').each(function(){			
			var sid = jQuery(this).data('parts_list_id');
			sdpids.push(sid);
		});
		//alert(sdpids);
		// Qty row id check in selecte option
		jQuery('.dpartqtyrow').each(function(){
			if (jQuery.inArray(this.id, sdpids)!='-1') {
				var optionId = jQuery('option:selected', this).attr('data-parts_list_id');
				//alert(optionId);
				var partLabel = this.options[e.target.selectedIndex].text;		
				var rowhtml = '<div class="row dpartqtyrow" id="'+optionId+'" ><div class="col-md-6"><label>'+partLabel+'<sup>*</sup></label></div><div class="col-md-3"><input type="text" class="form-control" name="" id=""></div></div>';
				jQuery("#driver_side_parts_qty").append(rowhtml);
			}else{
				this.remove();
			}
		});
		*/
    //});
	

	/******  Form Validation *********/
	jQuery('.overview-order').click(function(){		

		jQuery('#overviewOrder').validate({
           
            rules: {
	            truck_identification:
	            {
	               required: true	               
	            },
	            purchase_order_num:
	            {
	               required: true	               
	            },
	            'panel_selection[]':
	            {
	            	required:true,
	            	//maxlength: 2
	            },
				/*
				damage_1:
	            {
	               required: true	               
	            },
				damage_2:
	            {
	               required: true	               
	            },
				*/
				installation:
	            {
	               required: true	               
	            },
			                     
            },

            submitHandler: function(form) {
            	
            	jQuery('#loader,#overlay').css('display','block');
				jQuery("#overlay").css({
				    position: "absolute",
				    width: "100%",
				    height: "100%",
				    top: 0,
				    left: 0,
				    background: "rgba(14, 10, 10, 0.5)"
				});
				

	            /* WP Insert */
	            
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!
				var yyyy = today.getFullYear();
				var date = yyyy+'-'+mm+'-'+dd;
				var radioVal 	= jQuery("input[name='installation']:checked").val();
				var fd = new FormData(form); 

				/*
					fd.append( 'truck', '#truck_identification'); 			
					fd.append( 'radioVal', radioVal); 
					fd.append( 'notes', '#ccomments');  
					fd.append( 'task_Type', 'Repairs(Parts only)');   
					fd.append( 'status', 'Unassigned');  
					fd.append( 'D_side_Artwork', 'Driver' );
					fd.append( 'P_side_Artwork', 'Passenger' );
					fd.append( 'Rear_Artwork', 'Artwork Code' );
					fd.append( 'Panels', 'Panel Codes of user selection' );
					fd.append( 'Install_Date', date );
					fd.append( 'action', 'overviewOrder' );
				*/
				
				console.log(fd); 

				jQuery.ajax({	
		            type:"POST",
		            url: ajaxurl,
		           	data: fd,
			        contentType : false,
			        cache       : false,
			        processData : false,
			        dataType    : "json",
		        	success: function(response){
		        		if(response.status='success'){
		        			jQuery( '#overviewOrder' ).each(function(){
								this.reset();								
								jQuery('.sol-selected-display-item-text').html("");									
								jQuery('#driver_side_part').val("");
							});
		        			jQuery('#loader,#overlay').css('display','none');
		        			jQuery('html, body').animate({scrollTop: jQuery('#msg').offset().top-150 }, 1000);
							setTimeout(function(){
								jQuery('#msg').append('<div class="alert alert-'+response.status+'">'+response.msg+'</div>');
							}, 2000);
		        		}else{
		        			alert('fail');
		        		}		                
		            }
		        });
	        
            }            

        });
       
    }); // EO Form Validation
});

/*Truck Report validation*/
	
		
jQuery('#truck-report').validate({
    rules: {
        
        truck_identification:
        {
           required: true	               
        },
        purchase_order_num:
        {
           required: true	               
        },
        'panel_selection[]':
        {
        	required:true,
        	//maxlength: 2
        },
		Photograph1:
        {
           required: true	               
        },
		Photograph2:
        {
           required: true	               
        },
		
		ccomments:
        {
           required: true	               
        },
	                  
    },
    messages: {
	    'panel_selection[]': {
	        required: "You must check at least 1 box",
	        maxlength: "Check no more than {0} boxes"
	    }
	},


	submitHandler: function(form) {
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		var date = yyyy+'-'+mm+'-'+dd;
		var fd = new FormData(form); 

		/*
			fd.append( 'truck', '#truck_identification');  
			fd.append( 'notes', '#ccomments');  
			fd.append( 'Task_Type', 'Repairs(Parts only)');   
			fd.append( 'Status', 'Unassigned');  
			fd.append( 'D_side_Artwork', 'Driver' );
			fd.append( 'P_side_Artwork', 'Passenger' );
			fd.append( 'Rear_Artwork', 'Artwork Code' );
			fd.append( 'Panels', 'Panel Codes of user selection' );
			fd.append( 'Installer', 'request initiator' );
			fd.append( 'Install_Date', date );
		*/

			jQuery('#loader,#overlay').css('display','block');
			jQuery("#overlay").css({
			    position: "absolute",
			    width: "100%",
			    height: "100%",
			    top: 0,
			    left: 0,
			    background: "rgba(14, 10, 10, 0.5)"
			});

		jQuery.ajax({	
		    type:"POST",
		    url: ajaxurl,
		    /*data:{action: 'truckReport', 'truck_identification':truck,'ccomments':notes,
		    'Photograph1':pic1,'Photograph2':pic2,'task_type':Task_Type,'Status1':Status,
			'D_side':D_side_Artwork,'P_side': P_side_Artwork,'Rear':Rear_Artwork,'Panels1':Panels,'Installer1':Installer,'install_date1':date},*/
			//data: formdata,
			//data        : new FormData(form),
			data: fd,
		    contentType : false,
		    cache       : false,
		    processData : false,
		    dataType    : "json",

			success: function(response){
		        if(response.status='success'){
		        	jQuery( '#truck-report' ).each(function(){
								this.reset();
							});
        			jQuery('#loader,#overlay').css('display','none');
        			jQuery('html, body').animate({scrollTop: jQuery('#msg').offset().top-150 }, 1000);
					setTimeout(function(){
						jQuery('#msg').append('<div class="alert alert-'+response.status+'">'+response.msg+'</div>');
					}, 2000);
        		}else{
        			alert('fail');
        		}	 
		    }
		});
	}
           
});

          
/*jQuery("#truck-report").submit(function(){

	return false;
});*/
  

// });




/****** Job Completion Form Validation *********/

jQuery(document).ready(function()
{

	jQuery('.job-completion').click(function(){		
		
		jQuery('#jobCompletion').validate({
			
            rules: {
	            truck_identification:
	            {
	               required: true	               
	            },
	            work_image:
	            {
	               required: true	               
	            },
	           			                     
            },           
                   
            submitHandler: function(form) {

				/******** Overview Order Information **********/	
				var truck_identification 			= jQuery('.sol-selected-display-item-text').val();			
				var purchase_order  				= jQuery('#purchase_order_num').val();
				var phone2 							= jQuery('#phone2').val();


            }
        });

    });
});


jQuery(document).on('click','#locationsection input[name="locations"]',function(){	
	var new_locid = jQuery(this).val();
	var sel_add = jQuery(this).next().text();	
 	jQuery('#location_id').val(new_locid);
 	jQuery('#locationsection .sol-selected-display-item-text').text(sel_add);
 	jQuery('#locationsection .success').removeClass('sol-active');
});
  
  jQuery('#searchdata').keydown(function(){
  	jQuery('#locationsection .sol-selection .sol-option').addClass('sol-active');
  });

jQuery('#company').change(function(){ 
	var company = jQuery(this).val();
	var selected = jQuery(this).find('option:selected');
	var com_id = jQuery(selected).attr('data-id');
	
	if(company == 'other')
	{
		jQuery('.other-company').css({"display":"block"});
		jQuery('#locationsection').hide();
		/*jQuery('#company_name').val('');
		jQuery('#website').val('');
		jQuery('#street_address').val('');
		jQuery('#street_address_2').val('');
		jQuery('#suburb').val('');
		jQuery('#post_code').val('');*/
	}
	else{
		jQuery('.other-company').css('display','none');
		jQuery('#locationsection').show();

		console.log('com_id'+com_id);
	jQuery.ajax({
    url: "https://api.airtable.com/v0/apprTuWAjlI7FBBPb/Companies/"+com_id,
    beforeSend: function(xhr) { 
      xhr.setRequestHeader("Authorization", "Bearer " +"keyoA5KNgdkmd3iDG"); 
    },
    type: 'GET',
    dataType: 'json',
    contentType: 'application/json',
    processData: false,
    data: '{"foo":"bar"}',
    success: function (data) {
      console.log(JSON.stringify(data));
      var location = data.fields.Locations;
      var web_link = data.fields.Website;
      if(web_link != null)
      {	
      	var weburl = data.fields.Website.replace(/(^\w+:|^)\/\//, '');
      	var arr = web_link.split(':');     	
     	if(arr[0] == 'http')
     	{
     		jQuery('.website-label select option[value="0"]').attr('selected',true);
     	}
     	else{
     		jQuery('.website-label select option[value="1"]').attr('selected',true);
     	}
      }else{
      	var weburl = data.fields.Website;
      }
      jQuery('#company_name').val(data.fields.CompanyName); // Company name
      jQuery('#company_code').val(data.fields.CompanyCode); // Company Code
      jQuery('#company_type').val(data.fields.Type); // Company Type
      jQuery('#company_comments').val(data.fields.Comments); // Company Comments
      jQuery('#company_contacts').val(data.fields.Contacts); // Company Contacts
      jQuery('#company_rating').val(data.fields.Rating); // Company Rating
      jQuery('#web_link_co_code').val(data.fields.Web_link_co_code); // Company web_link_no_code
      jQuery('#company_id').val(data.id); //Company ID
      jQuery('#website').val(weburl); // Website      
      jQuery('#location_id').val(data.fields.Locations[0]); // Location ID

      

      if(location.length >=2)
      {
      	jQuery("#locations option").each(function() {
		    jQuery(this).remove();
		});
		jQuery('#locationsection .sol-selection div.sol-option,.sol-option.show.sol-filtered-search').addClass('hide');
    /*  	jQuery('#locationsection .sol-selection div.sol-option').each(function(){
			    		jQuery(this).addClass('hide');
			    	});*/
		
		jQuery.ajax({
			    url: "https://api.airtable.com/v0/apprTuWAjlI7FBBPb/Locations/"+location[0],
			    beforeSend: function(xhr) { 
			      xhr.setRequestHeader("Authorization", "Bearer " +"keyoA5KNgdkmd3iDG"); 
			    },
			    type: 'GET',
			    dataType: 'json',
			    contentType: 'application/json',
			    processData: false,
			    data: '{"foo":"bar"}',
			    success: function (data) {	
			    	var fulladdress = data.fields.Fulladdress;
			    	jQuery('#locationsection .sol-current-selection .sol-selected-display-item-text').html(fulladdress);
		      	},
		      	error: function(){
			      console.log("Cannot get location data");
			    }
			 });


      	for($i=0;$i<location.length;$i++)
     	{
	      	jQuery.ajax({
			    url: "https://api.airtable.com/v0/apprTuWAjlI7FBBPb/Locations/"+location[$i],
			    beforeSend: function(xhr) { 
			      xhr.setRequestHeader("Authorization", "Bearer " +"keyoA5KNgdkmd3iDG"); 
			    },
			    type: 'GET',
			    dataType: 'json',
			    contentType: 'application/json',
			    processData: false,
			    data: '{"foo":"bar"}',
			    success: function (data) {	
			    	console.log((JSON.stringify(data)));
			    	jQuery('#street_address').val(data.fields.Street); // Street Address
      				jQuery('#street_address_2').val(''); // Street Line 2
     				jQuery('#suburb').val(data.fields.Suburb); // Suburb
      				jQuery('#state').val(data.fields.State); // State
      				jQuery('#post_code').val(data.fields.Postcode); // Postcode    			
				    
			    	var fulladdress = data.fields.Fulladdress;
			    	jQuery('#locationsection select').append('<option value="'+data.id+'">'+fulladdress+'</option>');

			    	jQuery("#locationsection .sol-selection div.sol-option label input").each(function() {
					    //alert(jQuery(this).val() );
					    if(jQuery(this).val() == data.id)
					    {
					    	jQuery(this).parent().parent().addClass('show');
					    	jQuery(this).parent().parent().removeClass('hide');
					    }
					    else
					    {
					    	
					    }
					});					

		      	},
		      	error: function(){
			      console.log("Cannot get location data");
			    }
			 });
      	}
      }
      else{

      	jQuery.ajax({
		    url: "https://api.airtable.com/v0/apprTuWAjlI7FBBPb/Locations/"+location,
		    beforeSend: function(xhr) { 
		      xhr.setRequestHeader("Authorization", "Bearer " +"keyoA5KNgdkmd3iDG"); 
		    },
		    type: 'GET',
		    dataType: 'json',
		    contentType: 'application/json',
		    processData: false,
		    data: '{"foo":"bar"}',
		    success: function (data) {	
		    	console.log((JSON.stringify(data)));
		    	jQuery('#street_address').val(data.fields.Street); // Street Address
      			jQuery('#street_address_2').val(''); // Street Line 2
     			jQuery('#suburb').val(data.fields.Suburb); // Suburb
      			jQuery('#state').val(data.fields.State); // State
      			jQuery('#post_code').val(data.fields.Postcode); // Postcode
      			
		    	var fulladdress = data.fields.Fulladdress;
	      		jQuery('#locationsection select').html('<option value="'+location+'">'+fulladdress+'</option>');
	      		jQuery('#locationsection .sol-selection').html('<div class="sol-option"><label class="sol-label"><input type="radio" class="sol-radio" name="locations" value="'+data.id+'"><div class="sol-label-text">'+fulladdress+'</div></label></div>');
	      		jQuery('#locationsection .sol-current-selection .sol-selected-display-item-text').html(fulladdress);
	      	},
	      	error: function(){
		      console.log("Cannot get location data");
		    }
		 });
    
      }
      
      	
    },
    error: function(){
      console.log("Cannot get company list");
    }
});
	}
	
});




/********** Register Form Validation and submit js*************/
jQuery(document).ready(function()
{	
	jQuery('.rgisterUser-OLD').click(function(){	
		if(jQuery('#website').val()!="")
		{	if(!ValidateUrl(jQuery('#website').val()))
			{
				//jQuery('#website').val('');
				jQuery('#website').addClass('error-check');
				jQuery('.error-website').remove();
				jQuery('#website').after('<div class="error-website error-common">Please enter valid url.</div>');
			}
			else{
				jQuery('#website').removeClass('error-check');
				jQuery('.error-website').remove();
			}
			
			
		}
		
		jQuery('#registerform-OLD').validate({
            rules: {
	            user_email: {
	               required: true,
	               email:true
	               },
	            credentials: {
	               required: true,
	               minlength:8
	               },
	            full_name: {
	               required: true
	               } ,
	            company:{
	            	required:true
	            }, 
	            company_name:{
	            	required:true
	            }, 
	            /*Website:{	            	
	            	url:true
	            }, */
	            street_address:{
	            	required:true
	            }, 
	            suburb:{
	            	required:true
	            }, 
	            state:{
	            	required:true
	            },  
	            post_code:{
	            	required:true
	            },
	            locations:{
	            	required:true
	            },
	            phone:{
	            	required:true,
	            	minlength:10
	            },
	            phone21:{
	            	minlength:10
	            }, 
            },
       
            submitHandler: function(form) {


				/***** User Information **********/
				var firstName;
				var lastName;

				var user_email 			= jQuery('#user_email').val();
					
				var phone 				= jQuery('#phone').val();
				var phone2 				= jQuery('#phone2').val();

				/******* Company info added by user **********/
				var searchdata 			= jQuery('.sol-radio').val();
				var locations 			= jQuery('#location_id').val();

				var company_name 		= jQuery('#company_name').val(); // Company name
				var company_type		= jQuery('#company_type').val(); // Company Type
		      	var company_comments	= jQuery('#company_comments').val(); // Company Comments
		      	var company_contacts	= jQuery('#company_contacts').val(); // Company Contacts
		      	var company_rating		= jQuery('#company_rating').val(); // Company Rating
		      	var Web_link_co_code 	= jQuery('#web_link_co_code').val(); // Company web_link_no_code
				var company_id			= jQuery('#company_id').val(); //Company ID
				var company_code		= jQuery('#company_code').val(); //Company ID

				var website 			= jQuery('#website').val(); // Website
				var link_type 			= jQuery('#link_type option:checked').data('title'); // Website Type i.e http:// or https://

				/******** Company Location Info ***********/

				var street_address 		= jQuery('#street_address').val(); // Street Address
				var street_address_2	= jQuery('#street_address_2').val(); // Street Line 2
				var suburb 				= jQuery('#suburb').val(); // Suburb
				var state 				= jQuery('#state').val(); // State
				var post_code 			= jQuery('#post_code').val(); // Postcode
				if(website !="")
				{	
					var web_url = link_type+'://'+website;
				}
				else{
					var web_url = '';
				}
					
		
		
				if(user_email != "")
				{	
					if(!ValidateEmail(user_email))
					{
						jQuery('#user_email').val('');
						jQuery('#user_email').addClass('error-check');
						jQuery('.error-email').remove();
						jQuery('#user_email').after('<div class="error-email error-common">Enter valid email address</div>');
					}
					else
					{	
						
						jQuery('#user_email').removeClass('error-check');
						jQuery('.error-email').remove();
						var credentials 		= jQuery('#credentials').val();
						var full_name 			= jQuery('#full_name').val();	
						if(full_name != "")
						{
							var ret = full_name.split(" ");
							var firstName = ret[0];
							var lastName = ret[1];
						}

						if(company_id !="")
						{	
							jQuery('.rgisterUser').attr('disabled',true);
							jQuery('#loader,#overlay').css('display','block');
							jQuery("#overlay").css({
							    position: "absolute",
							    width: "100%",
							    height: "100%",
							    top: 0,
							    left: 0,
							    background: "rgba(14, 10, 10, 0.5)"
							});

							jQuery.ajax({
								type:"POST",
								url:ajaxurl,
								data:{action: 'register_form', 'user_email':user_email,'credentials':credentials,'firname':firstName,'lasname':lastName,'phone':phone,'phone2':phone2},
								dataType:"json",
								success:function(response)
								{	console.log('response'+response);
									//console.log(wp_send_json_success());				
									if(response  != "")
									{	
										jQuery('.rgisterUser').attr('disabled',false);
										jQuery('#loader,#overlay').css('display','none');
										console.log('registered successfully');
										setTimeout(function(){
											jQuery('#msg').append('<div class="alert alert-'+response.status+'">'+response.msg+'</div>');
										}, 2000);

										jQuery( '#registerform' ).each(function(){
										    this.reset();
										});
										jQuery('.rgisterUser').closest('form').find("input[type=hidden]").val("");

										jQuery('.sol-selected-display-item-text').html('');
										/******** Add User to airtable Contacts Database *********/
										jQuery.ajax({
										    url: "https://api.airtable.com/v0/apprTuWAjlI7FBBPb/Contacts",
										    beforeSend: function(xhr) { 
										      xhr.setRequestHeader("Authorization", "Bearer "+"keyoA5KNgdkmd3iDG"); 
										    },
										    type: 'POST',
										    dataType: 'json',
										    contentType: 'application/json',
										    processData: true,
										    data: '{"fields": {"ContactName": "'+full_name+'","Locations": ["'+locations+'"],"Position": "New User","Phone": "'+phone+'","Email": "'+user_email+'","Companies":["'+company_id+'"]}}',
										    success: function (data) {	
												console.log(JSON.stringify(data));

												var contac_id = data.id;
												console.log('contac_id:'+contac_id);

											      /********* Save logs************/
											      jQuery.ajax({
														type:"POST",
														url:ajaxurl,
														data:{action: 'register_form','user_emails':'"'+user_email+'"', 'send_request':'add user data to contacts table,update company table and loctaions table with new created contact in airtable','received_request':'new contact inserted: '+contac_id},
														dataType:"json",
														success:function(response)
														{
															console.log("logs updated!");
														},
														error: function(xhr, status, error){
														    console.log("Cannot update logs!");
														     var err = eval("(" + xhr.responseText + ")");
		  													//alert(err.Message);
														}
													});


												},
									      	error: function(){
										      console.log("Cannot add contacts data");
										      /********* Save logs************/
											      jQuery.ajax({
														type:"POST",
														url:ajaxurl,
														data:{action: 'register_form','user_emails':'"'+user_email+'"', 'send_request':'add user data to contacts table,update company table and loctaions table with new created contact in airtable','received_reqest':'error in insertion'},
														dataType:"json",
														success:function(response)
																{
																	console.log("logs updated!");
																},
																error: function(){
																    console.log("Cannot update logs!");
																}
													});
										    }
										 });
										 

								
									} // EO response for user registration if part
									else
									{	jQuery( '#registerform' ).each(function(){
										    this.reset();
										});
										jQuery('.rgisterUser').closest('form').find("input[type=hidden]").val("");
										jQuery('.sol-selected-display-item-text').html('');
										jQuery('.rgisterUser').attr('disabled',false);
										jQuery('#loader,#overlay').css('display','none');
										console.log('error occured..');
										setTimeout(function(){
											jQuery('#msg').append('<div class="alert alert-danger">'+user_email+': Email already exists!</div>');
										}, 2000);
									}		// EO response for user registration else part			
									
								},
								error:function(){
									jQuery( '#registerform' ).each(function(){
										    this.reset();
										});
									jQuery('.rgisterUser').closest('form').find("input[type=hidden]").val("");
									jQuery('.sol-selected-display-item-text').html('');
									jQuery('.rgisterUser').attr('disabled',false);
									jQuery('#loader,#overlay').css('display','none');
									console.log('failed!');
									//console.log(wp_send_json_error());
									setTimeout(function(){
										jQuery('#msg').append('<div class="alert alert-danger">User Registration failed!</div>');
									}, 2000);
								}
							}); // EO user registration

						} // EO If Company Id not empty
						else{
							jQuery('.rgisterUser').attr('disabled',true);
							jQuery('#loader,#overlay').css('display','block');
							jQuery("#overlay").css({
							    position: "absolute",
							    width: "100%",
							    height: "100%",
							    top: 0,
							    left: 0,
							    background: "rgba(14, 10, 10, 0.5)"
							});

							jQuery.ajax({
								type:"POST",
								url:ajaxurl,
								data:{action: 'register_form', 'user_email':user_email,'credentials':credentials,'firname':firstName,'lasname':lastName,'phone':phone,'phone2':phone2},
								dataType:"json",
								success:function(response)
								{	console.log('response'+response);
									//console.log(wp_send_json_success());				
									if(response  != "")
									{	jQuery( '#registerform' ).each(function(){
										    this.reset();
										});
										jQuery('.rgisterUser').closest('form').find("input[type=hidden]").val("");
										jQuery('.sol-selected-display-item-text').html('');
										jQuery('.rgisterUser').attr('disabled',true);
										jQuery('#loader,#overlay').css('display','none');
										console.log('registered successfully');
										setTimeout(function(){
											jQuery('#msg').append('<div class="alert alert-'+response.status+'">User and Company Created Successfully!</div>');
										}, 2000);

										/******** Add Location to airtable Locations Database *********/
										jQuery.ajax({
										    url: "https://api.airtable.com/v0/apprTuWAjlI7FBBPb/Locations",
										    beforeSend: function(xhr) { 
										      xhr.setRequestHeader("Authorization", "Bearer "+"keyoA5KNgdkmd3iDG"); 
										    },
										    type: 'POST',
										    dataType: 'json',
										    contentType: 'application/json',
										    processData: true,
										    data: '{"fields": {"ID1":"'+suburb+'","Street":"'+street_address+' '+street_address_2+'","Suburb":"'+suburb+'","State":"'+state+'","Postcode":"'+post_code+'","Email":"'+user_email+'","Phone1":"'+phone+'","Phone2":"'+phone2+'"}}',
										    success: function (data) {	
												console.log(JSON.stringify(data));
												var at_locid= data.id;
												 /********* Save logs************/
											      jQuery.ajax({
														type:"POST",
														url:ajaxurl,
														data:{action: 'register_form', 'user_emails':user_email,'send_request':'add location data to location table in airtable','received_request':'new location inserted with id: '+at_locid},
														dataType:"json",
														success:function(response)
														{
															console.log("Location logs updated!");
														},
														error: function(){
														    console.log("Cannot update location logs!");
														}
													});		

											     /****** EO Logs ********/

											    /******** Add Company to airtable Companies Database *********/
												jQuery.ajax({
													url: "https://api.airtable.com/v0/apprTuWAjlI7FBBPb/Companies",
													beforeSend: function(xhr) { 
													    xhr.setRequestHeader("Authorization", "Bearer "+"keyoA5KNgdkmd3iDG"); 
													},
													type: 'POST',
													dataType: 'json',
													contentType: 'application/json',
													processData: true,
													data: '{"fields": {"CompanyCode": "Pending Company","CompanyName": "'+company_name+'","Locations":["'+at_locid+'"],"Type": "NA","Website": "'+web_url+'"}}',
													success: function (data) {	
														console.log(JSON.stringify(data));
														var at_comid = data.id;
														/********* Save logs************/
														jQuery.ajax({
															type:"POST",
															url:ajaxurl,
															data:{action: 'register_form', 'user_emails':user_email,'send_request':'add new company data to company table in airtable','company_id':at_comid,'received_request':'new company inserted with id: '+at_comid},
															dataType:"json",
															success:function(response)
															{
																console.log("Company logs updated!");
															},
															error: function(){
															    console.log("Cannot update company logs!");
															}
														});
														/***** EO Logs*****/

														/******* Add Contacts to airtable Contacts Database ********/
														jQuery.ajax({
															url: "https://api.airtable.com/v0/apprTuWAjlI7FBBPb/Contacts",
															beforeSend: function(xhr) { 
															    xhr.setRequestHeader("Authorization", "Bearer "+"keyoA5KNgdkmd3iDG"); 
															},
															type: 'POST',
															dataType: 'json',
															contentType: 'application/json',
															processData: true,
															data: '{"fields": {"ContactName": "'+full_name+'","Locations": ["'+at_locid+'"],"Position": "New User","Companies":["'+at_comid+'"],"Phone": "'+phone+'","Email": "'+user_email+'"}}',
															success: function (data) {	
																console.log(JSON.stringify(data));
																console.log('Contacts added to airtable');
																var contac_id = data.id;
																//alert('contac_id:'+contac_id);
																/********* Save logs************/
																jQuery.ajax({
																	type:"POST",
																	url:ajaxurl,
																	data:{action: 'register_form', 'user_emails':user_email,'send_request':'add new contact in airtable','received_request':'new contact added to airtable with id: '+contac_id},
																	dataType:"json",
																	success:function(response)
																	{
																		console.log("Contacts logs updated!");
																	},
																	error: function(){
																	    console.log("Cannot update contacts logs!");
																	}
																});														
															},
															error: function(){
																alert("Cannot add contacts data");
																	/********* Save logs************/
																	jQuery.ajax({
																		type:"POST",
																		url:ajaxurl,
																		data:{action: 'register_form', 'user_emails':user_email,'send_request':'add new contact data to contacts table in airtable','received_request':'new contact not inserted'},
																		dataType:"json",
																		success:function(response)
																		{
																			console.log("logs updated!");
																		},
																		error: function(){
																		    console.log("Cannot update logs!");
																		}
																	});
																}
															}); // EO Add Contacts	
													},												
												    error: function(){
													    console.log("Cannot add company data");
													    /********* Save logs************/
														jQuery.ajax({
															type:"POST",
															url:ajaxurl,
															data:{action: 'register_form', 'user_emails':user_email,'send_request':'add company data to companies table in airtable','received_request':'new company not inserted'},
															dataType:"json",
															success:function(response)
																{
																	console.log("logs updated!");
																},
																error: function(){
																    console.log("Cannot update logs!");
																}
														});
													}
												}); // EO Add Company
											},
											error: function(){
												console.log("Cannot add location data");
												/********* Save logs************/
												jQuery.ajax({
													type:"POST",
													url:ajaxurl,
													data:{action: 'register_form', 'user_emails':user_email,'send_request':'add location data to location table in airtable','received_request':'new location not inserted'},
													dataType:"json",
													success:function(response)
													{
														console.log("Location logs updated!");
													},
													error: function(){
													    console.log("Cannot update location logs!");
													}
												});	
												/****** EO Logs ********/
											}
										}); //EO Add Location
										

			
							

									} // EO response for user registration if part
									else
									{	jQuery( '#registerform' ).each(function(){
										    this.reset();
										});
										jQuery('.rgisterUser').closest('form').find("input[type=hidden]").val("");
										jQuery('.sol-selected-display-item-text').html('');	
										jQuery('.rgisterUser').attr('disabled',false);
										jQuery('#loader,#overlay').css('display','none');
										console.log('error occured..');
										setTimeout(function(){
											jQuery('#msg').append('<div class="alert alert-danger">'+user_email+': Email already exists!</div>');
										}, 2000);
									}		// EO response for user registration else part			
									
								},
								error:function(){
									jQuery( '#registerform' ).each(function(){
										    this.reset();
										});
									jQuery('.rgisterUser').closest('form').find("input[type=hidden]").val("");
									jQuery('.sol-selected-display-item-text').html('');
									jQuery('.rgisterUser').attr('disabled',false);
									jQuery('#loader,#overlay').css('display','none');
									console.log('failed!');
									//console.log(wp_send_json_error());
									setTimeout(function(){
										jQuery('#msg').append('<div class="alert alert-danger">User and Company Registration failed!</div>');
									}, 2000);
								}
							}); // EO user registration
						}// EO Else part if company 
								
						} // EO Else of email Validate
					
				} // EO if user Email is not empty
			
				return false;
            },
         });
	}); // EO click funtion


	jQuery('.rgisterUser').click(function(){
	
		if(jQuery('#website').val()!="")
		{	if(!ValidateUrl(jQuery('#website').val()))
			{
				//jQuery('#website').val('');
				jQuery('#website').addClass('error-check');
				jQuery('.error-website').remove();
				jQuery('#website').after('<div class="error-website error-common">Please enter valid url.</div>');
			}
			else{
				jQuery('#website').removeClass('error-check');
				jQuery('.error-website').remove();
			}
			
			
		}
		
		jQuery('#registerform').validate({
            rules: {
	            user_email: {
	               required: true,
	               email:true
	               },
	            credentials: {
	               required: true,
	               minlength:8
	               },
	            full_name: {
	               required: true
	               } ,
	            company:{
	            	required:true
	            }, 
	            company_name:{
	            	required:true
	            }, 
	            /*Website:{	            	
	            	url:true
	            }, */
	            street_address:{
	            	required:true
	            }, 
	            suburb:{
	            	required:true
	            }, 
	            state:{
	            	required:true
	            },  
	            post_code:{
	            	required:true
	            },
	            locations:{
	            	required:true
	            },
	            phone:{
	            	required:true,
	            	minlength:10
	            },
	            phone21:{
	            	minlength:10
	            }, 
            },
       
            submitHandler: function(form) {
		
				var fd = new FormData(form); 

				jQuery('#loader,#overlay').css('display','block');
				jQuery("#overlay").css({
				    position: "absolute",
				    width: "100%",
				    height: "100%",
				    top: 0,
				    left: 0,
				    background: "rgba(14, 10, 10, 0.5)"
				});
				
				jQuery.ajax({	
		            type:"POST",
		            url: ajaxurl,
		           	data: fd,
			        contentType : false,
			        cache       : false,
			        processData : false,
			        dataType    : "json",
		        	success: function(response){		        		
		        		if(response.status='success'){
							jQuery('#loader,#overlay').css('display','none');
							setTimeout(function(){
								jQuery('#msg').append('<div class="alert alert-'+response.status+'">'+response.msg+'</div>');
							}, 2000);

							jQuery( '#registerform' ).each(function(){
							    this.reset();
							});
							jQuery('.rgisterUser').closest('form').find("input[type=hidden]").val("");

							jQuery('.sol-selected-display-item-text').html('');
		        		}else{
		        			alert('fail');
		        		}		                
		            }
		        });		

            },
         });
	}); // EO click funtion


}); // EO ready funtion


jQuery( "#full_name").keydown(function (e) {

    if (e.ctrlKey || e.altKey) 
    {
        e.preventDefault();

    } else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
            e.preventDefault();
        }
    }
});

jQuery(document).on('keyup blur', '.phoneNo', function(e){
		e.preventDefault();
		var key  = e.charCode || e.keyCode || 0;
		if (key >= 65 && key <= 90)
		{
			this.value = this.value.replace(/[^\d]/g,'');
			return false;
		}
		$phone   = jQuery(this);
		var val  = $phone.val();
		val 	 = val.replace(/\-+/g, '');
		var len = val.length;

		if( len <= 10 )
		{
			if ((key !== 8 && key !== 9))
			{
				var first  = val.substring(0, 3);
				var sec1   = val.substring(3, 6);
				var sec2   = val.substring(6, 10);
				var newPhone = first;
				if(sec1)
					newPhone = newPhone+'-'+sec1;
				if(sec2)
					newPhone = newPhone+'-'+sec2;
				jQuery(this).val(newPhone);
			}
		}
		else if( len <= 11 )
		{
			if ((key !== 8 && key !== 9))
			{
				var first  = val.substring(0, 3);
				var sec1   = val.substring(3, 7);
				var sec2   = val.substring(7, 11);
				var newPhone = first;
				if(sec1)
					newPhone = newPhone+'-'+sec1;
				if(sec2)
					newPhone = newPhone+'-'+sec2;
				jQuery(this).val(newPhone);
			}
		}
		else if( len <= 13 )
		{
			if ((key !== 8 && key !== 9))
			{
				var first  = val.substring(0, 3);
				var sec1   = val.substring(3, 6);
				var sec2   = val.substring(6, 9);
				var sec3   = val.substring(9, 13);
				var newPhone = first;
				if(sec1)
					newPhone = newPhone+'-'+sec1;
				if(sec2)
					newPhone = newPhone+'-'+sec2;
				if(sec3)
						newPhone = newPhone+'-'+sec3;
				jQuery(this).val(newPhone);
			}
		}
		// Allow numeric (and tab, backspace, delete) keys only
		return (key == 8 ||
				key == 9 ||
				key == 46 ||
				(key >= 48 && key <= 57) ||
				(key >= 96 && key <= 105));
});


function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
}

function ValidateUrl(url) {
        var re = /(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/;
        return re.test(url);
}


jQuery(document).ready(function(){
	jQuery(document).on('click','.panelchk',function(){
	var dynamicVal	 = jQuery(this).val();
	
	jQuery('#damage_1').attr('name', dynamicVal); 
 	jQuery('.order-image-section').show();
	 
	addDynamicPhoto(dynamicVal);
	});

function addDynamicPhoto(dynamicVal)
{
	//alert(dynamicVal);
	var testt = dynamicVal;
	//alert(testt);
	var count = 1;	

	jQuery("#btn1").click(function(){
		
		if(count <= 4)
		{
			 jQuery(".order-image-section").append("<div class='row image-remove'><div class='col-md-3'><label>Driver Photograph <sup>*</sup></label></div><div class='col-md-4'><input type='file' class='form-control' name='"+ testt +"' id='damage_1'/><span>Attach Photograph</span></div><div class='col-md-1'><button type='button' class='btn btn-danger' id='minus'> <span class='glyphicon glyphicon-remove'></span></button></div>	</div>");
		}
		count++; 
	});

}
	
	jQuery(document).on('click','#minus',function(){
		jQuery(this).closest('div').parents('div.image-remove').remove();
	

	});

});