jQuery.noConflict();
jQuery(document).ready(function($) {
    jQuery('#locationsList,#company').searchableOptionList({ maxHeight: '300px', showSelectAll: true });

});

jQuery('#company').change(function(){ 
	var company = jQuery(this).val();
	var selected = jQuery(this).find('option:selected');
	var com_id = jQuery(selected).attr('data-id');
	var ajaxurl = '<?php echo get_stylesheet_directory_uri();?>"/locations.php"';
	if(company == 'other')
	{
		jQuery('.other-company').css({"display":"block"});
		jQuery('#locations').hide();
	}
	else{
		jQuery('.other-company').css('display','none');
		jQuery('#locations').show();

		console.log('com_id'+com_id);
	jQuery.ajax({
    url: "https://api.airtable.com/v0/apprTuWAjlI7FBBPb/Companies/"+com_id,
    beforeSend: function(xhr) { 
      xhr.setRequestHeader("Authorization", "Bearer " +"keyoA5KNgdkmd3iDG"); 
    },
    type: 'GET',
    dataType: 'json',
    contentType: 'application/json',
    processData: false,
    data: '{"foo":"bar"}',
    success: function (data) {
      alert(JSON.stringify(data));
      var location = data.fields.Locations;
      jQuery('#company_name').val(data.fields.CompanyName); // Company name
      jQuery('#company_code').val(data.fields.CompanyCode); // Company Code
      jQuery('#company_type').val(data.fields.Type); // Company Type
      jQuery('#company_comments').val(data.fields.Comments); // Company Comments
      jQuery('#company_contacts').val(data.fields.Contacts); // Company Contacts
      jQuery('#company_rating').val(data.fields.Rating); // Company Rating
      jQuery('#web_link_co_code').val(data.fields.Web_link_co_code); // Company web_link_no_code
      jQuery('#company_id').val(data.id); //Company ID
      jQuery('#website').val(data.fields.Website); // Website      
      jQuery('#location_id').val(data.fields.Locations); // Location ID


      if(location.length >=2)
      {
      	jQuery("#locations option").each(function() {
		    jQuery(this).remove();
		});
      	for($i=0;$i<location.length;$i++)
     	{
	      	jQuery.ajax({
			    url: "https://api.airtable.com/v0/apprTuWAjlI7FBBPb/Locations/"+location[$i],
			    beforeSend: function(xhr) { 
			      xhr.setRequestHeader("Authorization", "Bearer " +"keyoA5KNgdkmd3iDG"); 
			    },
			    type: 'GET',
			    dataType: 'json',
			    contentType: 'application/json',
			    processData: false,
			    data: '{"foo":"bar"}',
			    success: function (data) {	
			    	alert((JSON.stringify(data)));
			    	jQuery('#street_address').val(data.fields.Street); // Street Address
      				jQuery('#street_address_2').val(''); // Street Line 2
     				jQuery('#suburb').val(data.fields.Suburb); // Suburb
      				jQuery('#state').val(data.fields.State); // State
      				jQuery('#post_code').val(data.fields.Postcode); // Postcode
			    	var fulladdress = data.fields.Fulladdress;
			    	jQuery('#locations select').append('<option value="'+data.id+'">'+fulladdress+'</option>')

		      	},
		      	error: function(){
			      alert("Cannot get location data");
			    }
			 });
      	}
      }
      else{

      	jQuery.ajax({
		    url: "https://api.airtable.com/v0/apprTuWAjlI7FBBPb/Locations/"+location,
		    beforeSend: function(xhr) { 
		      xhr.setRequestHeader("Authorization", "Bearer " +"keyoA5KNgdkmd3iDG"); 
		    },
		    type: 'GET',
		    dataType: 'json',
		    contentType: 'application/json',
		    processData: false,
		    data: '{"foo":"bar"}',
		    success: function (data) {	
		    	alert((JSON.stringify(data)));
		    	jQuery('#street_address').val(data.fields.Street); // Street Address
      			jQuery('#street_address_2').val(''); // Street Line 2
     			jQuery('#suburb').val(data.fields.Suburb); // Suburb
      			jQuery('#state').val(data.fields.State); // State
      			jQuery('#post_code').val(data.fields.Postcode); // Postcode
		    	var fulladdress = data.fields.Fulladdress;
	      		jQuery('#locations select').html('<option value="'+location+'">'+fulladdress+'</option>');
	      	},
	      	error: function(){
		      alert("Cannot get location data");
		    }
		 });
    
      }
      
      	
    },
    error: function(){
      alert("Cannot get data");
    }
});
	}
	
});


/*jQuery(document).ready(function ()
{
	 jQuery('#registerform').validate({
   	submitHandler: function(form) {
            jQuery(form).ajaxSubmit();
        },
    rules: {
        user_email: {
          required: true,
          email: true
        },
        credentials: {
          required: true,
          minlength: 8
        },
        full_name: {
          required: true,        
        }
    },

  });
});
*/



jQuery(document).ready(function()
{
	jQuery('.rgisterUser').click(function(){

		/***** User Information **********/
		var firstName;
		var lastName;

		var user_email 			= jQuery('#user_email').val();
		var credentials 		= jQuery('#credentials').val();
		var full_name 			= jQuery('#full_name').val();		
		var phone 				= jQuery('#phone').val();
		var phone2 				= jQuery('#phone2').val();

		/******* Company info added by user **********/
		var searchdata 			= jQuery('.sol-radio').val();
		var locations 			= jQuery('#location_id').val();

		var company_name 		= jQuery('#company_name').val(); // Company name
		var company_type		= jQuery('#company_type').val(data.fields.Type); // Company Type
      	var company_comments	= jQuery('#company_comments').val(data.fields.Comments); // Company Comments
      	var company_contacts	= jQuery('#company_contacts').val(data.fields.Contacts); // Company Contacts
      	var company_rating		= jQuery('#company_rating').val(data.fields.Rating); // Company Rating
      	var Web_link_co_code 	= jQuery('#web_link_co_code').val(data.fields.Web_link_co_code); // Company web_link_no_code
		var company_id			= jQuery('#company_id').val(); //Company ID
		var company_code		= jQuery('#company_code').val(); //Company ID
		var website 			= jQuery('#website').val(); // Website

		/******** Company Location Info ***********/

		var street_address 		= jQuery('#street_address').val(); // Street Address
		var street_address_2	= jQuery('#street_address_2').val(); // Street Line 2
		var suburb 				= jQuery('#suburb').val(); // Suburb
		var state 				= jQuery('#state').val(); // State
		var post_code 			= jQuery('#post_code').val(); // Postcode
		//var location_id 		= jQuery('#location_id').val(); // Location ID

		var ajaxurl = "http://localhost/ymf/index.php/wp-admin/admin-ajax.php";

		if(user_email != "")
		{	
			if(!ValidateEmail(user_email))
			{
				jQuery('#user_email').val('');
				jQuery('#user_email').addClass('error-check');
				jQuery('.error-email').remove();
				jQuery('#user_email').after('<div class="error-email error-common">Enter valid email address</div>');
			}
			else
			{
				jQuery('#user_email').removeClass('error-check');
				jQuery('.error-email').remove();
				
			}
		}
		else {
			jQuery('#user_email').addClass('error-check');
			jQuery('.error-email').remove();
			jQuery('#user_email').after('<div class="error-email error-common">Enter email address</div>');
			// $('.error-email').remove(5000);
			return false;
		}
/*
		if(credentials!="")
		{
				alert('credentials not empty');	
		}
		else{
			jQuery('#credentials').addClass('error-check');
			jQuery('.error-credentials').remove();
			jQuery('#credentials').after('<div class="error-credentials error-common">Enter password</div>');
				// $('.error-email').remove(5000);
			return false;
		}

			if(full_name !="")
			{
				var ret = full_name.split(" ");
				var firstName = ret[0];
				var lastName = ret[1];
			}
			else{
				jQuery('#full_name').addClass('error-check');
				jQuery('.error-full_name').remove();
				jQuery('#full_name').after('<div class="error-full_name error-common">Enter Full Name</div>');
				// $('.error-email').remove(5000);
				return false;
			}

		if(searchdata !="")
		{
			
		}
		else{
			jQuery('#searchdata').addClass('error-check');
			jQuery('.error-searchdata').remove();
			jQuery('#searchdata').after('<div class="error-searchdata error-common">Select Company</div>');
			// $('.error-email').remove(5000);
			return false;
		}

		if(locations!="")
		{
			
		}
		else{
			jQuery('#locations').addClass('error-check');
			jQuery('.error-locations').remove();
			jQuery('#locations').after('<div class="error-locations error-common">Select locations</div>');
			// $('.error-email').remove(5000);
			return false;
		}

		if(phone!="")
		{
			
		}
		else{
			jQuery('#phone').addClass('error-check');
			jQuery('.error-phone').remove();
			jQuery('#phone').after('<div class="error-phone error-common">Enter Phone Number</div>');
			// $('.error-email').remove(5000);
			return false;
		}
		if(phone2!="")
		{
					
		}
		else{
			jQuery('#phone2').addClass('error-check');
			jQuery('.error-phone2').remove();
			jQuery('#phone2').after('<div class="error-phone2 error-common">Enter Phone Number</div>');
				// $('.error-email').remove(5000);
			return false;
		}
*/
		
		jQuery.ajax({
					type:"POST",
					url:ajaxurl,
					data:{action: 'register_form', 'user_email':user_email,'credentials':credentials,'firname':firstName,'lasname':lastName,'company_type':company_type,'company_comments':company_comments,'company_contacts':company_contacts,'company_rating':company_rating,'web_link_co_code':web_link_co_code,'company_code':company_code,'locations':locations,'phone':phone,'phone2':phone2,'company_name':company_name,'company_id':company_id,'website':website,'street_address':street_address,'street_address_2':street_address_2,'suburb':suburb,'state':state,'post_code':post_code},
					dataType:"json",
					success:function(response)
					{	console.log(response);
						//console.log(wp_send_json_success());				
						if(response  != "")
						{
							console.log('registered successfully');
							setTimeout(function(){
								jQuery('#msg').append('<div class="alert alert-'+response.status+'">'+response.msg+'</div>');
							}, 2000);

						}
						else
						{
							console.log('error occured..');
							setTimeout(function(){
								jQuery('#msg').append('<div class="alert alert-'+response.status+'">'+response.msg+'</div>');
							}, 2000);
						}
						
						
					},
					error:function(){
						console.log('failed!');
						//console.log(wp_send_json_error());
						/*setTimeout(function(){
							jQuery('#msg').append('<div class="alert alert-danger">User Registration failed!</div>');
						}, 2000);*/
					}
				});
				
	});

});


jQuery( "#full_name").keyup(function (e) {
          if (e.shiftKey || e.ctrlKey || e.altKey) {
              e.preventDefault();
          } else {
              var key = e.keyCode;
              if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                  e.preventDefault();
              }
          }
      });
	jQuery(document).on('keyup blur', '.phoneNo', function(e)
	{
		e.preventDefault();
		var key  = e.charCode || e.keyCode || 0;
		if (key >= 65 && key <= 90)
		{
			this.value = this.value.replace(/[^\d]/g,'');
			return false;
		}
		$phone   = jQuery(this);
		var val  = $phone.val();
		val 	 = val.replace(/\-+/g, '');
		var len = val.length;

		if( len <= 10 )
		{
			if ((key !== 8 && key !== 9))
			{
				var first  = val.substring(0, 3);
				var sec1   = val.substring(3, 6);
				var sec2   = val.substring(6, 10);
				var newPhone = first;
				if(sec1)
					newPhone = newPhone+'-'+sec1;
				if(sec2)
					newPhone = newPhone+'-'+sec2;
				jQuery(this).val(newPhone);
			}
		}
		else if( len <= 11 )
		{
			if ((key !== 8 && key !== 9))
			{
				var first  = val.substring(0, 3);
				var sec1   = val.substring(3, 7);
				var sec2   = val.substring(7, 11);
				var newPhone = first;
				if(sec1)
					newPhone = newPhone+'-'+sec1;
				if(sec2)
					newPhone = newPhone+'-'+sec2;
				jQuery(this).val(newPhone);
			}
		}
		else if( len <= 13 )
		{
			if ((key !== 8 && key !== 9))
			{
				var first  = val.substring(0, 3);
				var sec1   = val.substring(3, 6);
				var sec2   = val.substring(6, 9);
				var sec3   = val.substring(9, 13);
				var newPhone = first;
				if(sec1)
					newPhone = newPhone+'-'+sec1;
				if(sec2)
					newPhone = newPhone+'-'+sec2;
				if(sec3)
						newPhone = newPhone+'-'+sec3;
				jQuery(this).val(newPhone);
			}
		}
		// Allow numeric (and tab, backspace, delete) keys only
		return (key == 8 ||
				key == 9 ||
				key == 46 ||
				(key >= 48 && key <= 57) ||
				(key >= 96 && key <= 105));
	});



function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
};

/* Ravinder 14-08-2018 */
	/*jQuery(document).on('blur','#user_email',function(){
		var Obj = 	jQuery(this);
		email 	= 	Obj.val();
		if(email != "")
		{
			if(!ValidateEmail(email))
			{
				jQuery('#user_email').val('');
				jQuery('#user_email').addClass('error-check');
				jQuery('.error-email').remove();
				jQuery('#user_email').after('<div class="error-email error-common">Enter valid email address</div>');
			}
			else
			{
				jQuery('#user_email').removeClass('error-check');
				jQuery('.error-email').remove();
			}
		}
	});*/

	