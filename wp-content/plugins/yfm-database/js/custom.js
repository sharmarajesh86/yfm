jQuery.noConflict();

function getHomeUrl() {
	var href = window.location.href;
	var index = href.indexOf('yfm');
	var homeUrl = href.substring(0, index);
	return homeUrl+'yfm';
}

function getXMLToArray(xml){
    var thisArray = new Array();
    if(jQuery(xml).children().length > 0){
    jQuery(xml).children().each(function(){
        if(jQuery(xml).find(this.nodeName).children().length > 0){
        var NextNode = jQuery(xml).find(this.nodeName);
        thisArray[this.nodeName] = getXMLToArray(NextNode);
        } else {
        thisArray[this.nodeName] = jQuery(xml).find(this.nodeName).text();
        }
    });
    }
    return thisArray;
}

jQuery(document).ready(function($) {
	//var ajaxurl = document.location.origin+"/wp-admin/admin-ajax.php"; // PATH ERROR 20-12-19
	jQuery("#veryfyMobile").on('click',function(){
		var count = 0;
		var _len = jQuery("#credentials").val().length;
		if(_len < 8 ){
			count++;
		}

		jQuery(".req").each(function(){
			if(jQuery(this).val() == ''){
				jQuery(this).css('border-color','red');
				count++;
			}else{
				jQuery(this).css('border-color','#d1d1d1');
			}
		});
		var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!regex.test(jQuery("#user_email").val())) {
		count++;
			alert("Please enter a valid email");
			return false;
		}
		jQuery.ajax({
		url : ajaxurl,
		type : 'post',
		data : {
          action : 'validateMail',
          usrMail : jQuery("#user_email").val()
      	},
      	success : function( response ) {
          	if(response != 'success'){
				jQuery(".emailAE").show();
				count++;
			}
      	}
    });
		if(count == 0){
			var _randNum = Math.floor((Math.random() * 999999) + 000001);
			localStorage.setItem("verifymobi", _randNum);
			var _msg = "Your OTP is: "+_randNum;
			var _mobi = jQuery("#phone").val();
			var flag_title  = jQuery('.selected-flag').attr('title');
			var exactNumber ;
			if(flag_title != ""){
				var pieces 		= flag_title.split(/[\s,]+/);
				var countryCode = pieces[pieces.length-1];
				exactNumber = countryCode+_mobi;
			}
			console.log(exactNumber);
			//return false;						
			jQuery.ajax({
				url: "https://api-mapper.clicksend.com/http/v2/send.php?method=http&username=tom@yourfleetmedia.com&key=EA602148-E67A-A61D-C7DE-EB946C5B72F5&to="+exactNumber+"&message="+_msg,
				success: function(resP){
					var xml = jQuery.parseXML(resP);
					var xmlArray = getXMLToArray(xml);
					var _arr = xmlArray.xml.messages.message;				
					if(_arr.errortext != 'Success'){
						var counter = 10;
						var interval = setInterval(function() {
							counter--;
							if (counter == 0) {
								clearInterval(interval);
							}
							jQuery(".otpIssue").html('<b>'+_arr.errortext+'</b> hide in '+ counter+'sec');
						}, 1000);
						jQuery('.maskArea').delay(1200000).fadeOut('slow');
						jQuery(".otpIssue").html('');
					}else{
						jQuery(".otpIssue").text('');
					}
					console.log(_arr);
					jQuery(".maskArea").show();
					jQuery("#verify_otp").focus();
			  }
			});
		}else{
			alert("Please fill all fields. Password must be greater then 8 character");
			return false;
		}
	});

	jQuery("#verify_now").click(function(){
		var _otp = jQuery("#verify_otp").val();
		jQuery(".invalidOtp").hide();
		if(_otp == ''){
			jQuery(".optEmpty").show();
		}else{
			jQuery(".optEmpty").hide();
			if(_otp == localStorage.getItem("verifymobi")){
				sessionStorage.removeItem("verifymobi");
				jQuery(".maskArea").hide();
				/*jQuery(".submitBtn").html('<input type="submit" name="wp-submit" id="wp-submit" class="btn btn-orange btn-primary rgisterUser" value="Register">');*/
				jQuery("#registerform").submit();
			}else{
				jQuery(".invalidOtp").show();
				return false;
			}
		}
	});

	jQuery('#example').DataTable();
	jQuery('.chosen-select').chosen({width: "100% !important"});
	jQuery('.chosen-select').chosen({width: "100% !important"});
	jQuery(".partsChosen").chosen({
        //max_selected_options: 2,
    	placeholder_text_multiple: "Select panel parts"
    });
	jQuery.validator.setDefaults({ ignore: ":hidden:not(select)" });
	/*jQuery(".chosen-select").chosen({
        //max_selected_options: 2,
   		placeholder_text_multiple: "Please select ypur company"
    });*/
    if ($("select.chosen-select").length > 0) {
	    $("select.chosen-select").each(function() {
	        if ($(this).attr('required') !== undefined) {
	            $(this).on("change", function() {
	                $(this).valid();
	            });
	        }
	    });
	}

	jQuery('.ViewErrorLog').click(function(){
		//alert(jQuery(this).attr('data-id'));
		var errorLogRow = '.span-'+jQuery(this).attr('data-id');
		console.log(errorLogRow);
		var errorLog = jQuery(errorLogRow).html();
		jQuery('.errorlog-modal-body').html(errorLog);
		jQuery('#erroLogModal').modal('toggle');
	});

    jQuery('#locations123,#company123,#truck_identification123').searchableOptionList({
    	maxHeight: '300px', showSelectAll: false
 	});
    //jQuery('#locations,#company,#truck_identification').customselect();
});

jQuery('.enable_user').click(function(){
		jQuery('#loaders,#overlays').css('display','block');
		jQuery('#loaders').css("top","0");
		jQuery("#overlays").css({
			position: "absolute",
			width: "100%",
			height: "100%",
			top: 0,
			left: 0,
			background: "rgba(14, 10, 10, 0.5)"
		});
		var id = jQuery(this).data('id');
		var status = jQuery(this).data('status');
		jQuery.ajax({
			type:"POST",
			url:ajaxurl,
			data:{action: 'user_status', 'uid':id,'status':status},
			dataType:"json",
			success:function(response){
				jQuery('#loaders,#overlays').css('display','none');
				window.location.href = document.location.origin+'/yfm/users/';
				console.log(response.msg);
			},
			 error: function(error){
				    console.log(error);
			    }
		});
	});
jQuery(document).ready(function(){
	/****** Login Form Validation *********/
	jQuery('.login').click(function(){
		jQuery('#loginform').validate({
      rules: {
	      log: {
         required: true
        },
        pwd: {
        	required: true
        },
      },
      messages:{
      	log:"Kindly enter username!",
      	pwd:"Kindly enter password!"
      }
    });
  });

  /****** Forget Password Form Validation *********/

	jQuery('.forgetPass').click(function(){
		jQuery('#lostpasswordform').validate({
      rules: {
        user_login: {
           required: true
           },
      },
      messages:{
      	user_login:"Kindly enter your Username or Email Address!",
      },
     /* submitHandler: function(form) {
      }*/
  	});
	});
});

/****** Overview Order Form *********/

jQuery(document).ready(function(){
	// Get truck details
	jQuery('.sol-label-text').click(function(){
		//alert(jQuery(this).val());
	});
	// Get truck details
	jQuery('#truck_identification').change(function(){
		//jQuery('.sol-selected-display-item-text').show();
		jQuery('#hiddenTruckId').val(jQuery(this).find(':selected').attr('data-id'));
		jQuery('#hiddenTaskId').val(jQuery(this).find(':selected').attr('data-taskid'));
		jQuery('.truckErrorMsg').hide();
		jQuery(".panelchk:checked").each(function() {
			jQuery(this).prop("checked", false);
			var panelName = jQuery(this).val();
			//jQuery('#'+panelName).toggleClass('show');
			jQuery('#'+panelName).hide();
			jQuery('#'+panelName+'_part').empty();
		});
	});

	/****** Show Relevant Part Name********/
	//jQuery('input[type="checkbox"]').click(function(){
	jQuery('.panelchk-old').click(function(){
		//alert('hi');
		var truckID1 = jQuery('#truck_identification').val();
		//alert(truckID1);
		var show_data = jQuery(this).val();
		//alert(show_data);
		//jQuery('#'+show_data).toggleClass('hide');
		jQuery('#'+show_data).show();
		var panel_type = jQuery(this).attr('panel_type');
		if(jQuery(this).prop("checked") == false){
            jQuery('#'+show_data).hide();
        }else{
			jQuery.ajax({
		    url:ajaxurl,
		    type: 'POST',
		    dataType: 'json',
				data:{action: 'parts_list','panel_type':panel_type,'truck':truckID1},
		    success: function (response) {
		    	var item_name = '';
		    	if(response.partsData.length !== 0){
			    	item_name += '<optgroup label="Panel Parts">';
			    	jQuery.each(response.partsData,function(index,value){
			    		//console.log(value);
			    		item_name += '<option data-Parts_list_id='+value.Parts_list_id+' data-Artwork='+value.Artwork+'>'+value.Item_Name+'</option>';
			    	});
			    	item_name += ' </optgroup>';
		    	}
		    	if(response.partsData2.length !== 0){
		    		item_name += ' <optgroup label="Additional Generic Parts">';
		    		jQuery.each(response.partsData2,function(index,value){
			    		//console.log(value);
			    		item_name += '<option data-Parts_list_id='+value.Parts_list_id+' data-Artwork='+value.Artwork+'>'+value.Item_Name+'</option>';
			    	});
			    	item_name += ' </optgroup>';
		    	}
		    	jQuery('#'+show_data+' #'+show_data+'_part').html(item_name);
		    	jQuery('#'+show_data+' #'+show_data+'_part').multipleSelect();

		    	if(item_name == ''){
		    		jQuery('#'+show_data).find('.order-image-section').hide();
		    	}else{
		    		jQuery('#'+show_data).find('.order-image-section').show();
		    	}

			  },
			  error: function(error){
				  console.log(error);
			  }
			});
		}
	});

	jQuery('.panelchk').click(function(){
		var truckID1 = jQuery('#truck_identification').val();
		var show_data = jQuery(this).val();
		//jQuery('#'+show_data).toggleClass('hide');
		jQuery('#'+show_data).show();
		//jQuery('#'+show_data).find('.chosen-container').style(' width=400px');
		var panel_type = jQuery(this).attr('panel_type');
		if(jQuery(this).prop("checked") == false){
      jQuery('#'+show_data).hide();
    }else{
			jQuery.ajax({
			  url:ajaxurl,
			  type: 'POST',
			  dataType: 'json',
				data:{action: 'parts_list','panel_type':panel_type,'truck':truckID1},
			  success: function (response) {
			    var item_name = '';
			    if(response.partsData.length !== 0){
			    	item_name += '<optgroup label="Panel Parts">';
				  	jQuery.each(response.partsData,function(index,value){
				  		console.log(value);
				  		item_name += '<option data-Parts_list_id='+value.Parts_list_id+' data-Artwork='+value.Artwork+'>'+value.Item_Name+'</option>';
				  	});
				    	item_name += ' </optgroup>';
			    }
			    if(response.partsData2.length !== 0){
			    	item_name += ' <optgroup label="Additional Generic Parts">';
			    	jQuery.each(response.partsData2,function(index,value){
				    	item_name += '<option data-Parts_list_id='+value.Parts_list_id+' data-Artwork='+value.Artwork+'>'+value.Item_Name+'</option>';
				    });
				    item_name += ' </optgroup>';
			    }
					if(item_name == ''){
						//item_name += '<option>No Parts Found</option>';
						jQuery('#'+show_data+'_part_chosen ul').html('<li class="search-field">No Parts Found</li>');
						jQuery('#'+show_data+'_part').attr('disabled', true).trigger("chosen:updated");
					}else{
						jQuery('#'+show_data+'_part').html(item_name);
					}
		    	jQuery('#'+show_data+'_part').trigger("chosen:updated");
			    	if(item_name == ''){
			    		jQuery('#'+show_data).find('.order-image-section').hide();
			    	}else{
			    		jQuery('#'+show_data).find('.order-image-section').show();
			    	}
			    },
			    error: function(error){
				    console.log(error);
			    }
			});
		}
	});
	/***** EO Part Name Code ****/

	jQuery('.show-qty-popup').click(function(){
		jQuery("#partqtysection").html('');
		var chkArray = [];
		var hdnpartqtyname = [];
		var ptyrowhtml = '';
		var cansubmit = 'yes';
		jQuery(".panelchk:checked").each(function() {
			//chkArray.push(jQuery(this).val());
			var panelName = jQuery(this).val();
			var panel_type = jQuery(this).attr('panel_type');
			ptyrowhtml = '<h3>'+panelName.replace(/_/g, ' ')+'</h3>';
			//selecte part ids
			var spids = [];
			var partcount = 0;
			if(parseInt(jQuery('#'+panelName+'_part option').length) == 0 ){
				partcount = 1;
				ptyrowhtml = ptyrowhtml+'<p>'+'No matches found.</p>';
			}else{
				jQuery('#'+panelName+'_part option:selected').each(function(){
					partcount = 1;
					var partqtyname = panel_type+'-'+jQuery(this).data('parts_list_id');
					var partname = jQuery(this).val();
					hdnpartqtyname.push(partqtyname);
					ptyrowhtml = ptyrowhtml+'<div class="form-group"><div class="row"><div class="col-md-6"><label>'+partname+'<sup>*</sup></label></div><div class="col-md-6"><input type="number" min="1" max="20" class="form-control popup_qty_txt" name="'+partqtyname+'" value="1"></div></div></div>';
				});
			}

			if(parseInt(partcount) == 0){
				cansubmit = 'no';
				jQuery("#partqtysection").append(ptyrowhtml+'<p class="text-danger" >No parts selected.Please select atleast one part.</p>');
			}else{
				jQuery("#partqtysection").append(ptyrowhtml);
			}

		});
		jQuery("#hiddenpartslist").val(hdnpartqtyname);
		if(cansubmit == 'no'){
			jQuery('.popup-submit').hide();
		}else{
			jQuery('.popup-submit').show();
		}
		if(ptyrowhtml == ''){
			jQuery(".overview-order").trigger('click');
		}else{
			jQuery('#qtyModal').modal('toggle');
		}
	});

	// Pop Quantity validation
	jQuery('.popup_qty_txt').blur(function(){
		if(parseInt(jQuery(this).val()) > 0 && parseInt(jQuery(this).val()) < 21 ){

		}else{
			//jQuery(this).addClass("has-error");
		}
	});

	jQuery('.popup-submit').click(function(){
		jQuery('#qtyModal').modal('hide');
		jQuery(".overview-order").trigger('click');
	});

	jQuery('.show-qty-popup-report').click(function(){
		jQuery("#partqtysection").html('');
		var chkArray = [];
		jQuery(".panelchk:checked").each(function() {
			//chkArray.push(jQuery(this).val());
			var panelName = jQuery(this).val();
			var panel_type = jQuery(this).attr('panel_type');
			var ptyrowhtml = '<h3>'+panelName+'</h3>';
			//selecte part ids
			var spids = [];
			jQuery('#'+panelName+'_part option:selected').each(function(){
				var partqtyname = panel_type+'-'+jQuery(this).data('parts_list_id');
				var partname = jQuery(this).val();
				ptyrowhtml = ptyrowhtml+'<div class="form-group"><div class="row"><div class="col-md-6"><label>'+partname+'<sup>*</sup></label></div><div class="col-md-6"><input type="number" min="1" max="20" class="form-control popup_qty_txt" name="'+partqtyname+'" value="1"></div></div></div>';
			});
			jQuery("#partqtysection").append(ptyrowhtml);
		});
		jQuery('#qtyModalReport').modal('toggle');
	});

	jQuery('.popup-submit-report').click(function(){
		jQuery('#qtyModalReport').modal('hide');
		jQuery(".truck-submit").trigger('click');
	});

	//Popup validation//
	jQuery(document).on('keyup','.popup_qty_txt', function(){
		if(jQuery(this).val() > 20){
			alert("No numbers above 20");
		    jQuery(this).val('20');
		}
		if(jQuery(this).val() < 1){
			alert("No numbers less than 1");
		    jQuery(this).val('1');
		}
	});

	/**************** Driver Side Part Names Select ***********************/

	//jQuery('#driver_side_part').change(function(e){

		//alert(this.options[e.target.selectedIndex].text);
		//var optionId = jQuery('option:selected', this).attr('data-parts_list_id');
		//alert(ddd);
		/*
		jQuery("#driver_side_parts").removeClass('hide');
		var optionId = jQuery('option:selected', this).attr('data-parts_list_id');
		//alert(optionId);
		var partLabel = this.options[e.target.selectedIndex].text;
		var rowhtml = '<div class="row dpartqtyrow" id="'+optionId+'" ><div class="col-md-6"><label>'+partLabel+'<sup>*</sup></label></div><div class="col-md-3"><input type="text" class="form-control" name="" id=""></div></div>';
		jQuery("#driver_side_parts_qty").append(rowhtml);
		*/
		// Selecte option array
		/*
		var sdpids = [];
		jQuery('#driver_side_part option:selected').each(function(){
			var sid = jQuery(this).data('parts_list_id');
			sdpids.push(sid);
		});
		//alert(sdpids);
		// Qty row id check in selecte option
		jQuery('.dpartqtyrow').each(function(){
			if (jQuery.inArray(this.id, sdpids)!='-1') {
				var optionId = jQuery('option:selected', this).attr('data-parts_list_id');
				//alert(optionId);
				var partLabel = this.options[e.target.selectedIndex].text;
				var rowhtml = '<div class="row dpartqtyrow" id="'+optionId+'" ><div class="col-md-6"><label>'+partLabel+'<sup>*</sup></label></div><div class="col-md-3"><input type="text" class="form-control" name="" id=""></div></div>';
				jQuery("#driver_side_parts_qty").append(rowhtml);
			}else{
				this.remove();
			}
		});
		*/
    //});


	/******  Form Validation *********/

		jQuery('#overviewOrder').validate({
			errorClass   : "has-error",
		    highlight    : function(element, errorClass) {
		        jQuery(element).parents('.form-group').addClass(errorClass);
		    },
		    unhighlight  : function(element, errorClass, validClass) {
		        jQuery(element).parents('.form-group').removeClass(errorClass);
		    },
        rules: {
          truck_identification:
          {
             required: true
          },
          purchase_order_num:
          {
             required: true
          },
          'panel_selection[]':
          {
          	required:true,
          	//maxlength: 2
          },
					/*
					damage_1:
			          {
			             required: true
			          },
					damage_2:
			          {
			             required: true
			          },
					*/
					installation:
          {
             required: true
          },
          freightAccount:
          {
             required: true
          },
        },
		    messages: {
			    'panel_selection[]': {
			        required: "You must check at least 1 box",
			        maxlength: "Check no more than {0} boxes"
			    }
			},
        submitHandler: function(form) {
      		jQuery('#loader,#overlay').css('display','block');
				jQuery("#overlay").css({
			    position: "absolute",
			    width: "100%",
			    height: "100%",
			    top: 0,
			    left: 0,
			    background: "rgba(14, 10, 10, 0.5)"
				});

	        /* WP Insert */
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!
				var yyyy = today.getFullYear();
				var date = yyyy+'-'+mm+'-'+dd;
				var radioVal 	= jQuery("input[name='installation']:checked").val();
				var fd = new FormData(form);
				/*
					fd.append( 'truck', '#truck_identification');
					fd.append( 'radioVal', radioVal);
					fd.append( 'notes', '#ccomments');
					fd.append( 'task_Type', 'Repairs(Parts only)');
					fd.append( 'status', 'Unassigned');
					fd.append( 'D_side_Artwork', 'Driver' );
					fd.append( 'P_side_Artwork', 'Passenger' );
					fd.append( 'Rear_Artwork', 'Artwork Code' );
					fd.append( 'Panels', 'Panel Codes of user selection' );
					fd.append( 'Install_Date', date );
					fd.append( 'action', 'overviewOrder' );
				*/

				jQuery.ajax({
					type:"POST",
					url: ajaxurl,
					data: fd,
					contentType : false,
					cache       : false,
					processData : false,
					dataType    : "json",
        			success: function(response){
        				jQuery('#loader,#overlay').css('display','none');
		        		jQuery('html, body').animate({scrollTop: jQuery('#msg').offset().top-150 }, 1000);
		        		if(response.status == 'success'){
		        			jQuery( '#overviewOrder' ).each(function(){
								this.reset();
							});
							jQuery('.image-remove').each(function(){
								this.remove();
							});
							jQuery('.sideparts').each(function(){
								jQuery("#"+this.id).hide();
								jQuery("#"+this.id+'_part').empty();
							});
							jQuery('.chosen-select').trigger('chosen:updated');
							jQuery('#msg').html('<div class="alert alert-'+response.status+'">'+response.msg+'</div>');	
							jQuery("#msg").show().delay(3000).fadeOut();
		        		}else{
							jQuery('#msg').show();
			        			setTimeout(function(){
									jQuery('#msg').html('<div class="alert alert-danger">'+response.msg+'</div>');
							}, 1000);
				        }
		      		}
		    	});
      		},
			errorPlacement: function (errorClass, element) {
		        if ( element.is(":radio") ){
		          errorClass.appendTo( element.parents('.container') );
		        }else if (element.is("select.chosen-select")) {
	          		// placement for chosen
	          		element.next("div.chosen-container").append(errorClass);
        		} else {
		          // standard placement
		          errorClass.insertAfter(element);
		        }
		        if ( element.prop( "type" ) === "checkbox" ) {
		          errorClass.insertAfter( ".panels-list");
		       	} else {
		          errorClass.insertAfter( element );
		        }
    		}
  		});// EO Form Validation
	});

  // Chosen validation
	//jQuery('.chosen-select').chosen();
	jQuery.validator.setDefaults({ ignore: ":hidden:not(select)" });

	// validation of chosen on change
	if (jQuery("select.chosen-select").length > 0) {
    jQuery("select.chosen-select").each(function() {
      if (jQuery(this).attr('required') !== undefined) {
        jQuery(this).on("change", function() {
          jQuery(this).valid();
        });
      }
    });
	}
	/*Truck Report validation*/
	jQuery('#truckReport').validate({
		errorClass   : "has-error",
	    highlight    : function(element, errorClass) {
	    	console.log(element);
	    	console.log(errorClass);
        jQuery(element).parents('.form-group').addClass(errorClass);
	  },
	  unhighlight  : function(element, errorClass, validClass) {
	    jQuery(element).parents('.form-group').removeClass(errorClass);
	  },
    rules: {
      truck_identification:
      {
         required: true
      },
      'panel_selection[]':
      {
      	required:true
      },
			Photograph1:
        {
           required: true
        }
	  },
    messages: {
	    'panel_selection[]': {
	        required: "You must check at least 1 box",
	        maxlength: "Check no more than {0} boxes"
	    }
		},

		submitHandler: function(form) {
			if(jQuery('.sol-selected-display-item-text').html() == '--Select Truck--'){
				jQuery('.sol-selected-display-item-text').addClass('error');
				//jQuery('.truckfield').append('<label for="truck_identification" class="error customerror">This field is required.</label>');
				return false;
			}
			var fd = new FormData(form);
			jQuery('#loader,#overlay').css('display','block');
			jQuery("#overlay").css({
		    position: "absolute",
		    width: "100%",
		    height: "100%",
		    top: 0,
		    left: 0,
		    background: "rgba(14, 10, 10, 0.5)"
			});
			jQuery.ajax({
			  type:"POST",
			  url: ajaxurl,
			  /*data:{action: 'truckReport', 'truck_identification':truck,'ccomments':notes,
			  'Photograph1':pic1,'Photograph2':pic2,'task_type':Task_Type,'Status1':Status,
				'D_side':D_side_Artwork,'P_side': P_side_Artwork,'Rear':Rear_Artwork,'Panels1':Panels,'Installer1':Installer,'install_date1':date},*/
				//data: formdata,
				//data        : new FormData(form),
				data: fd,
			    contentType : false,
			    cache       : false,
			    processData : false,
			    dataType    : "json",

				success: function(response){
	        jQuery('#loader,#overlay').css('display','none');
      		jQuery('html, body').animate({scrollTop: jQuery('#msg').offset().top-150 }, 1000);
      		if(response.status == 'success'){
      			jQuery( '#truckReport' ).each(function(){
							this.reset();
						});
						jQuery('.image-remove').each(function(){
							this.remove();
						});
						jQuery('.chosen-select').trigger('chosen:updated');
						jQuery('#msg').html('<div class="alert alert-'+response.status+'">'+response.msg+'</div>');
						jQuery("#msg").show().delay(3000).fadeOut();
      		}else{
      			jQuery('#msg').show();
      			setTimeout(function(){
						jQuery('#msg').html('<div class="alert alert-danger">'+response.msg+'</div>');
						}, 1000);
	        }
			  }
			});
		},
		errorPlacement: function (errorClass, element) {
	  	if (element.is("select.chosen-select")) {
		    console.log("placement for chosen");
		    // placement for chosen
		    element.next("div.chosen-container").append(errorClass);
		  } else {
		    // standard placement
		    console.log('select else');
		    errorClass.insertAfter(element);
		  }

		  if ( element.prop( "type" ) === "checkbox" ) {
        errorClass.insertAfter( ".panels-list");
	    } else {
	     	console.log('elsss');
        errorClass.insertAfter( element );
    	}

	        // if ( element.prop( "type" ) === "checkbox" ) {
         //                errorClass.insertAfter( ".panels-list");
         //    }else if (element.is("select.chosen-select")) {
		       //      //console.log("placement for chosen");
		       //      // placement for chosen
		       //      element.next("div.chosen-container").append(errorClass);
		       //  }  else {
         //    	//console.log('elsss');
         //        errorClass.insertAfter( element );
         //    }
	  }
	});

	jQuery(".truck_report_image").each(function(){
	    jQuery(this).rules('remove');
	    jQuery(this).rules('add', {
	            required: true,
	            messages: {
	                required: "Photograph 1 is required."
	            },
	     });
	});

	/*jQuery("#truck-report").submit(function(){
		return false;
	});*/
	// });




	/****** Job Completion Form Validation *********/

jQuery(document).ready(function(){
	jQuery('#jobCompletion').validate({
		errorClass   : "has-error",
	  highlight    : function(element, errorClass) {
	    jQuery(element).parents('.form-group').addClass(errorClass);
	  },
	  unhighlight  : function(element, errorClass, validClass) {
      jQuery(element).parents('.form-group').removeClass(errorClass);
    },
    rules: {
      truck_identification:
      {
         required: true
      },
      'work_image[]':
      {
         required: true
      },
    },
    messages: {
   		'work_image[]': {
	    	required: "You must upload at least 1 Image",
	    	maxlength: "Check no more than {0} Image"
  		}
		},
		submitHandler: function(form) {
				var fd = new FormData(form);
				jQuery('#loader2,#overlay').css('display','block');
				jQuery("#overlay").css({
			    position: "absolute",
			    width: "100%",
			    height: "100%",
			    top: 0,
			    left: 0,
			    background: "rgba(14, 10, 10, 0.5)"
				});
				jQuery.ajax({
					type:"POST",
					url: ajaxurl,
					data: fd,
					contentType : false,
					cache       : false,
					processData : false,
					dataType    : "json",
        	success: function(response){
        		jQuery('#loader2,#overlay').css('display','none');
        		jQuery('html, body').animate({scrollTop: jQuery('#msg').offset().top-150 }, 1000);
        		if(response.status == 'success'){
							jQuery( '#jobCompletion' ).each(function(){
								this.reset();
							});
							jQuery('.image-remove').each(function(){
								this.remove();
							});
							jQuery('.chosen-select').trigger('chosen:updated');
							jQuery('#msg').html('<div class="alert alert-'+response.status+'">'+response.msg+'</div>');
							jQuery("#msg").show().delay(3000).fadeOut();
        		}else{
        			setTimeout(function(){
								jQuery('#msg').html('<div class="alert alert-danger">'+response.msg+'</div>');
							}, 1000);
        		}
          }
		    });
			},
			errorPlacement: function (errorClass, element) {
		    if (element.is("select.chosen-select")) {
		      console.log("placement for chosen");
		      // placement for chosen
		      element.next("div.chosen-container").append(errorClass);
		    } else {
	      	// standard placement
	        errorClass.insertAfter(element);
		    }
		  }
    });
});


/*jQuery(document).on('click','#locationsection input[name="locations"]',function(){
	var new_locid = jQuery(this).val();
	var sel_add = jQuery(this).next().text();
 	jQuery('#location_id').val(new_locid);
 	jQuery('#locationsection .sol-selected-display-item-text').text(sel_add);
 	jQuery('#locationsection .success').removeClass('sol-active');
});*/

  jQuery('#searchdata').keydown(function(){
  	jQuery('#locationsection .sol-selection .sol-option').addClass('sol-active');
  });
jQuery('#company').change(function(){
	var company = jQuery(this).val();
	var selected = jQuery(this).find('option:selected');
	var com_id = jQuery(selected).attr('data-id');
	if(company == 'other'){
		jQuery('.other-company').css({"display":"block"});
		jQuery('#locationsection,.location-data').hide();
		jQuery('#company_name').val('');
		jQuery('#website').val('');
		jQuery('#street_address').val('');
		jQuery('#street_address_2').val('');
		jQuery('#suburb').val('');
		jQuery('#post_code').val('');
	} else {
		jQuery('#locationsection').html('<label>Location</label><select class="form-control success chosen-select" name="locations" id="locations"> <!-- chosen-select-location --><option>--Select Location--</option></select><input type="hidden" name="location_id" id="location_id" value="">');
		jQuery('#location_id').html('');
		jQuery('.location-data').css({"display":"none"});
		jQuery('.other-company').css('display','none');
		jQuery('#locationsection').show();
		jQuery('.chosen-select').chosen({width: "100% !important"});
		jQuery.ajax({
	    url: "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Companies/"+com_id,
	    beforeSend: function(xhr) {
	      xhr.setRequestHeader("Authorization", "Bearer " +"keyw3cJfiAFXmYjJf");
	    },
	    type: 'GET',
	    dataType: 'json',
	    contentType: 'application/json',
	    processData: false,
	    data: '{"foo":"bar"}',
	    success: function (data) {
				var location = data.fields.Locations;
	      var web_link = data.fields.Website;
	      if(web_link != null){
	      	var weburl = data.fields.Website.replace(/(^\w+:|^)\/\//, '');
	      	var arr = web_link.split(':');
	     	if(arr[0] == 'http'){
	     		jQuery('.website-label select option[value="0"]').attr('selected',true);
	     	} else{
	     		jQuery('.website-label select option[value="1"]').attr('selected',true);
	     	}
	      }else{
	      	var weburl = data.fields.Website;
	      }
	      jQuery('#company_name').val(data.fields.CompanyName); // Company name
	      jQuery('#company_code').val(data.fields.CompanyCode); // Company Code
	      jQuery('#company_type').val(data.fields.Type); // Company Type
	      jQuery('#company_comments').val(data.fields.Comments); // Company Comments
	      jQuery('#company_contacts').val(data.fields.Contacts); // Company Contacts
	      jQuery('#company_rating').val(data.fields.Rating); // Company Rating
	      jQuery('#web_link_co_code').val(data.fields.Web_link_co_code); // Company web_link_no_code
	      jQuery('#company_id').val(data.id); //Company ID
	      jQuery('#website').val(weburl); // Website
	      jQuery('#location_id').val(data.fields.Locations[0]); // Location ID
	      if(location.length >=2){
					//jQuery('.location-data').css({"display":"block"});
	      	jQuery("#locations option").each(function() {
			    	jQuery(this).remove();
					});
				//jQuery('.chosen-select').trigger('chosen:updated');
	      	for($i=0;$i<location.length;$i++){
	     			jQuery('#location_id').val(''); // Location ID
	     			var locationCount = 0;
	     			var fulladdress = '';
		      	jQuery.ajax({
					    url: "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Locations/"+location[$i],
					    beforeSend: function(xhr) {
					      xhr.setRequestHeader("Authorization", "Bearer " +"keyw3cJfiAFXmYjJf");
				    	},
					    type: 'GET',
					    dataType: 'json',
					    contentType: 'application/json',
					    processData: false,
					    data: '{"foo":"bar"}',
				    	success: function (data) {
					    	//console.log((JSON.stringify(data)));
								var _select = '';
					    	jQuery('#street_address').val(data.fields.Street); // Street Address
		      			jQuery('#street_address_2').val(''); // Street Line 2
		     				jQuery('#suburb').val(data.fields.Suburb); // Suburb
		      			jQuery('#state').val(data.fields.State); // State
		      			jQuery('#post_code').val(data.fields.Postcode); // Postcode
					    	//var fulladdress = data.fields.Fulladdress;
					    	var fulladdress = data.fields.Suburb;
					    	// console.log(fulladdress);
								// console.log(locationCount);
								if(locationCount == 0){
									_select = 'selected';
								}else{
									_select = '';
								}
								jQuery('#locationsection select').append('<option value="'+data.id+'" '+ _select +'>'+fulladdress+'</option>');
								//jQuery('#locations_chosen ul').append('<li>'+locationCount+'</li>');
			      		jQuery('#locations_chosen ul').append('<li class="active-result" data-option-array-index="'+locationCount+'" data-type="'+location+'">'+fulladdress+'</li>');
			      		locationCount = (locationCount+1);
		      			//jQuery('#locationsection select').addClass('chosen-select-location');
		  	 				jQuery('.chosen-select').chosen({width: "100% !important"});
								jQuery('.chosen-select').trigger('chosen:updated');
				      },
				      error: function(){
					      console.log("Cannot get location data");
					    }
						});
	      	}
	      	jQuery('#locationsection select').html('<option value="other">Other - Add a new location</option>');
	      	//jQuery('.chosen-select').trigger('chosen:updated');
	      }else{
	      	//jQuery('#locationsection select').removeClass('chosen-select-location');
					jQuery('.chosen-select').trigger('chosen:updated');
	      	jQuery.ajax({
			    url: "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Locations/"+location,
			    beforeSend: function(xhr) {
			      xhr.setRequestHeader("Authorization", "Bearer " +"keyw3cJfiAFXmYjJf");
			    },
			    type: 'GET',
			    dataType: 'json',
			    contentType: 'application/json',
			    processData: false,
			    data: '{"foo":"bar"}',
			    success: function (data) {
						//console.log((JSON.stringify(data)));
						jQuery('#street_address').val(data.fields.Street); // Street Address
						jQuery('#street_address_2').val(''); // Street Line 2
						jQuery('#suburb').val(data.fields.Suburb); // Suburb
						jQuery('#state').val(data.fields.State); // State
						jQuery('#post_code').val(data.fields.Postcode); // Postcode
			    	//var fulladdress = data.fields.Fulladdress; [19-12-2019 changed to bottom line ]
			    	var fulladdress = data.fields.Suburb;
	      		//jQuery('#locationsection select').html('<option value="'+location+'">'+fulladdress+'</option>');
	      		//jQuery('#locationsection .sol-selection').html('<div class="sol-option"><label class="sol-label"><input type="radio" class="sol-radio" name="locations" value="'+data.id+'"><div class="sol-label-text">'+fulladdress+'</div></label></div>');
	      		//jQuery('#locations_chosen').html('<a class="chosen-single" tabindex="-1"><span>'+fulladdress+'</span><div><b></b></div></a>');
		      		jQuery('#locationsection select').html('<option value="'+location+'">'+fulladdress+'</option>');
		      		jQuery('#locationsection select').append('<option value="other">Other - Add a new location</option>');
		      		//jQuery('#locationsection .sol-selection').html('<div class="sol-option"><label class="sol-label"><input type="radio" class="sol-radio" name="locations" value="'+data.id+'"><div class="sol-label-text">'+fulladdress+'</div></label></div>');
		      		jQuery('#locations_chosen').html('<a class="chosen-single" tabindex="-1"><span>'+fulladdress+'</span></a>');
		      		jQuery('#locations_chosen').append('<div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off"></div><ul class="chosen-results"></ul></div>');
		      		jQuery('#locations_chosen ul').append('<li class="active-result result-selected highlighted" data-option-array-index="0" data-type="'+location+'" style="">'+fulladdress+'</li>');
		      		jQuery('#locations_chosen ul').append('<li class="active-result" data-option-array-index="1" style="" data-type="other" id="locationid">Other - Add a new location</li>');
		      		jQuery('#locationsection select').addClass('chosen-select-location');
	      			jQuery('.chosen-select').chosen({width: "100% !important"});
	      			//jQuery('.chosen-select').trigger('chosen:updated');
		      		//jQuery('#locationsection .sol-current-selection .sol-selected-display-item-text').html(fulladdress);
		      	},
		      	error: function(){
			      console.log("Cannot get location data");
			    }
			 });
      }
			/*jQuery('#locationsection select').addClass('chosen-select-location');
			jQuery('.chosen-select').chosen({width: "100% !important"});*/
	    },
	    error: function(){
	      console.log("Cannot get company list");
	    }
		});
	}
});

/*
*	change location data
*/
jQuery(document).on('change','#locations',function(){
	//alert(jQuery('#locations').val());
	var loc = jQuery('#locations').val();
	if(loc =='other'){
		jQuery('.location-data').css('display','block');
		jQuery('#location_id').val('');
	}else{
		jQuery('.location-data').css('display','none');
		jQuery('#location_id').val(loc);
		jQuery.ajax({
			url: "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Locations/"+loc,
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Authorization", "Bearer " +"keyw3cJfiAFXmYjJf");
			},
			type: 'GET',
			dataType: 'json',
			contentType: 'application/json',
			processData: false,
			data: '{"foo":"bar"}',
			success: function (data) {
				//console.log((JSON.stringify(data)));
				jQuery('#street_address').val(data.fields.Street); // Street Address
					jQuery('#street_address_2').val(''); // Street Line 2
				jQuery('#suburb').val(data.fields.Suburb); // Suburb
					jQuery('#state').val(data.fields.State); // State
					//jQuery('#state option["'+data.fields.State+'"]').attr('selected',true); // State
					jQuery('#post_code').val(data.fields.Postcode); // Postcode
					//jQuery('.chosen-select').trigger('chosen:updated');
					//jQuery('#locationsection .sol-current-selection .sol-selected-display-item-text').html(fulladdress);
			},
			error: function(){
				console.log("Cannot get location data");
			}
		});
	}
});

jQuery(document).on('click','#locations_chosen li',function(){
	var loca = jQuery(this).data('type');
	var data= jQuery(this).text();
	if(loca ==undefined){
		var loca = jQuery('#locations').val();
	}
	jQuery('#locationsection div').removeClass('chosen-with-drop');
	if(loca =='other'){
		jQuery('.location-data').css('display','block');
		jQuery('#location_id').val('');
		jQuery('#locationsection .chosen-single span').html('Other - Add a new location');
		jQuery('#locationsection .chosen-results li:nth-child:(2)').addClass('result-selected ');
	}else{	//alert('else');
		jQuery('.location-data').css('display','none');
		jQuery('#locationsection .chosen-single span').html(data);
		jQuery('#location_id').val(loca);
		jQuery.ajax({
			url: "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Locations/"+loca,
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Authorization", "Bearer " +"keyw3cJfiAFXmYjJf");
			},
			type: 'GET',
			dataType: 'json',
			contentType: 'application/json',
			processData: false,
			data: '{"foo":"bar"}',
			success: function (data) {
				//console.log((JSON.stringify(data)));
				jQuery('#street_address').val(data.fields.Street); // Street Address
				jQuery('#street_address_2').val(''); // Street Line 2
				jQuery('#suburb').val(data.fields.Suburb); // Suburb
				jQuery('#state').val(data.fields.State); // State
				//jQuery('#state option["'+data.fields.State+'"]').attr('selected',true); // State
				jQuery('#post_code').val(data.fields.Postcode); // Postcode
				//jQuery('.chosen-select').trigger('chosen:updated');
				//jQuery('#locationsection .sol-current-selection .sol-selected-display-item-text').html(fulladdress);
			},
			error: function(){
				console.log("Cannot get location data");
			}
		});
	}
});
/*
*	EO change location data
*/

jQuery('#company-NEW').change(function(){
	var company = jQuery(this).val();
	var selected = jQuery(this).find('option:selected');
	var com_id = jQuery(selected).attr('data-id');
	if(company == 'other'){
		jQuery('.other-company').css({"display":"block"});
		jQuery('#locationsection').hide();
		jQuery('#company_name').val('');
		jQuery('#website').val('');
		jQuery('#street_address').val('');
		jQuery('#street_address_2').val('');
		jQuery('#suburb').val('');
		jQuery('#post_code').val('');
	}	else{
		jQuery('.other-company').css('display','none');
		jQuery('#locationsection').show();
	}

	jQuery.ajax({
		url:ajaxurl,
		type: 'POST',
		dataType: 'json',
		data:{action: 'find_locations','companyid':com_id},
    success: function (response) {
    	var item_name = '';
    	if(response.partsData.length !== 0){
	    	item_name += '<optgroup label="Panel Parts">';
	    	jQuery.each(response.partsData,function(index,value){
	    		item_name += '<option data-Parts_list_id='+value.Parts_list_id+' data-Artwork='+value.Artwork+'>'+value.Item_Name+'</option>';
	    	});
	    	item_name += ' </optgroup>';
    	}
    	if(response.partsData2.length !== 0){
    		item_name += ' <optgroup label="Additional Generic Parts">';
    		jQuery.each(response.partsData2,function(index,value){
	    		item_name += '<option data-Parts_list_id='+value.Parts_list_id+' data-Artwork='+value.Artwork+'>'+value.Item_Name+'</option>';
	    	});
	    	item_name += ' </optgroup>';
    	}
    	jQuery('#'+show_data+' #'+show_data+'_part').html(item_name);
    	jQuery('#'+show_data+' #'+show_data+'_part').multipleSelect();
    	if(item_name == ''){
    		jQuery('#'+show_data).find('.order-image-section').hide();
    	}else{
    		jQuery('#'+show_data).find('.order-image-section').show();
    	}
    },
	    error: function(error){
		    console.log(error);
	    }
	});
});

/********** Register Form Validation and submit js*************/
jQuery(document).ready(function(){
	jQuery('.rgisterUser-OLD').click(function(){
		if(jQuery('#website').val()!=""){
			if(!ValidateUrl(jQuery('#website').val())){
				//jQuery('#website').val('');
				jQuery('#website').addClass('error-check');
				jQuery('.error-website').remove();
				jQuery('#website').after('<div class="error-website error-common">Please enter valid url.</div>');
			}else{
				jQuery('#website').removeClass('error-check');
				jQuery('.error-website').remove();
			}
		}
		jQuery('#registerform-OLD').validate({
            rules: {
	            user_email: {
	               required: true,
	               email:true
	               },
	            credentials: {
	               required: true,
	               minlength:8
	               },
	            full_name: {
	               required: true
	               } ,
	            company:{
	            	required:true
	            },
	            company_name:{
	            	required:true
	            },
	            /*Website:{
	            	url:true
	            }, */
	            street_address:{
	            	required:true
	            },
	            suburb:{
	            	required:true
	            },
	            state:{
	            	required:true
	            },
	            post_code:{
	            	required:true
	            },
	            locations:{
	            	required:true
	            },
	            phone:{
	            	required:true,
	            	minlength:10
	            },
	            phone21:{
	            	minlength:10
	            },
            },

            submitHandler: function(form) {


				/***** User Information **********/
				var firstName;
				var lastName;

				var user_email 			= jQuery('#user_email').val();

				var phone 				= jQuery('#phone').val();
				var phone2 				= jQuery('#phone2').val();

				/******* Company info added by user **********/
				var searchdata 			= jQuery('.sol-radio').val();
				var locations 			= jQuery('#location_id').val();

				var company_name 		= jQuery('#company_name').val(); // Company name
				var company_type		= jQuery('#company_type').val(); // Company Type
		      	var company_comments	= jQuery('#company_comments').val(); // Company Comments
		      	var company_contacts	= jQuery('#company_contacts').val(); // Company Contacts
		      	var company_rating		= jQuery('#company_rating').val(); // Company Rating
		      	var Web_link_co_code 	= jQuery('#web_link_co_code').val(); // Company web_link_no_code
				//var company_id			= jQuery('#company_id').val(); //Company ID
				var company_id			= 'recDfkIe5B7BD2B4y'; //Company ID
				//var company_code		= jQuery('#company_code').val(); //Company ID
				var company_code		= 'AIDGUI'; //Company ID

				var website 			= jQuery('#website').val(); // Website
				var link_type 			= jQuery('#link_type option:checked').data('title'); // Website Type i.e http:// or https://

				/******** Company Location Info ***********/

				var street_address 		= jQuery('#street_address').val(); // Street Address
				var street_address_2	= jQuery('#street_address_2').val(); // Street Line 2
				var suburb 				= jQuery('#suburb').val(); // Suburb
				var state 				= jQuery('#state').val(); // State
				var post_code 			= jQuery('#post_code').val(); // Postcode

				/******** Company Location Info if Other location selected ***********/

				var street_address_1 		= jQuery('#street_address1').val(); // Street Address
				var street_address_2_1		= jQuery('#street_address_2_1').val(); // Street Line 2
				var suburb_1 				= jQuery('#suburb1').val(); // Suburb
				var state_1 				= jQuery('#state1').val(); // State
				var post_code_1 			= jQuery('#post_code1').val(); // Postcode


				if(website !="")
				{
					var web_url = link_type+'://'+website;
				}
				else{
					var web_url = '';
				}



				if(user_email != "")
				{
					if(!ValidateEmail(user_email))
					{
						jQuery('#user_email').val('');
						jQuery('#user_email').addClass('error-check');
						jQuery('.error-email').remove();
						jQuery('#user_email').after('<div class="error-email error-common">Enter valid email address</div>');
					}
					else
					{

						jQuery('#user_email').removeClass('error-check');
						jQuery('.error-email').remove();
						var credentials 		= jQuery('#credentials').val();
						var full_name 			= jQuery('#full_name').val();
						if(full_name != "")
						{
							var ret = full_name.split(" ");
							var firstName = ret[0];
							var lastName = ret[1];
						}

						if(company_id !="")
						{
							jQuery('.rgisterUser').attr('disabled',true);
							jQuery('#loader,#overlay').css('display','block');
							jQuery("#overlay").css({
							    position: "absolute",
							    width: "100%",
							    height: "100%",
							    top: 0,
							    left: 0,
							    background: "rgba(14, 10, 10, 0.5)"
							});

							jQuery.ajax({
								type:"POST",
								url:ajaxurl,
								data:{action: 'register_form', 'user_email':user_email,'credentials':credentials,'firname':firstName,'lasname':lastName,'phone':phone,'phone2':phone2},
								dataType:"json",
								success:function(response)
								{	console.log('response'+response);
									//console.log(wp_send_json_success());
									if(response  != "")
									{
										jQuery('.rgisterUser').attr('disabled',false);
										jQuery('#loader,#overlay').css('display','none');
										console.log('registered successfully');
										setTimeout(function(){
											jQuery('#msg').html('<div class="alert alert-'+response.status+'">'+response.msg+'</div>');
										}, 2000);

										jQuery( '#registerform' ).each(function(){
										    this.reset();
										});
										jQuery('.rgisterUser').closest('form').find("input[type=hidden]").val("");

										jQuery('.sol-selected-display-item-text').html('');
										/******** Add Location to airtable Locations Database *********/
										jQuery.ajax({
										    url: "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Locations",
										    beforeSend: function(xhr) {
											    xhr.setRequestHeader("Authorization", "Bearer "+"keyw3cJfiAFXmYjJf");
											},
											type: 'POST',
											dataType: 'json',
											contentType: 'application/json',
											processData: true,
											data: '{"fields": {"ID1":"'+suburb_1+'","Street":"'+street_address_1+' '+street_address_2_1+'","Suburb":"'+suburb_1+'","State":"'+state_1+'","Postcode":"'+post_code_1+'","Email":"'+user_email+'","Phone1":"'+phone+'","Phone2":"'+phone2+'","Company":["'+company_id+'"]}}',
												success: function (data) {
												alert(JSON.stringify(data));
											},
											error: function(){
												console.log("Cannot add location data");
											}
										}); //EO Add Location
										/******** Add User to airtable Contacts Database *********/
										jQuery.ajax({
										    url: "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Contacts",
										    beforeSend: function(xhr) {
										      xhr.setRequestHeader("Authorization", "Bearer "+"keyw3cJfiAFXmYjJf");
										    },
										    type: 'POST',
										    dataType: 'json',
										    contentType: 'application/json',
										    processData: true,
										    data: '{"fields": {"ContactName": "'+full_name+'","Locations": ["'+locations+'"],"Position": "New User","Phone": "'+phone+'","Email": "'+user_email+'","Companies":["'+company_id+'"]}}',
										    success: function (data) {
												console.log(JSON.stringify(data));

												var contac_id = data.id;
												console.log('contac_id:'+contac_id);

											      /********* Save logs************/
											      jQuery.ajax({
														type:"POST",
														url:ajaxurl,
														data:{action: 'register_form','user_emails':'"'+user_email+'"', 'send_request':'add user data to contacts table,update company table and loctaions table with new created contact in airtable','received_request':'new contact inserted: '+contac_id},
														dataType:"json",
														success:function(response)
														{
															console.log("logs updated!");
														},
														error: function(xhr, status, error){
														    console.log("Cannot update logs!");
														     var err = eval("(" + xhr.responseText + ")");
		  													//alert(err.Message);
														}
													});


												},
									      	error: function(){
										      console.log("Cannot add contacts data");
										      /********* Save logs************/
											      jQuery.ajax({
														type:"POST",
														url:ajaxurl,
														data:{action: 'register_form','user_emails':'"'+user_email+'"', 'send_request':'add user data to contacts table,update company table and loctaions table with new created contact in airtable','received_reqest':'error in insertion'},
														dataType:"json",
														success:function(response)
																{
																	console.log("logs updated!");
																},
																error: function(){
																    console.log("Cannot update logs!");
																}
													});
										    }
										 });



									} // EO response for user registration if part
									else
									{	jQuery( '#registerform' ).each(function(){
										    this.reset();
										});
										jQuery('.rgisterUser').closest('form').find("input[type=hidden]").val("");
										jQuery('.sol-selected-display-item-text').html('');
										jQuery('.rgisterUser').attr('disabled',false);
										jQuery('#loader,#overlay').css('display','none');
										console.log('error occured..');
										setTimeout(function(){
											jQuery('#msg').html('<div class="alert alert-danger">'+user_email+': Email already exists!</div>');
										}, 2000);
									}		// EO response for user registration else part

								},
								error:function(){
									jQuery( '#registerform' ).each(function(){
										    this.reset();
										});
									jQuery('.rgisterUser').closest('form').find("input[type=hidden]").val("");
									jQuery('.sol-selected-display-item-text').html('');
									jQuery('.rgisterUser').attr('disabled',false);
									jQuery('#loader,#overlay').css('display','none');
									console.log('failed!');
									//console.log(wp_send_json_error());
									setTimeout(function(){
										jQuery('#msg').html('<div class="alert alert-danger">User Registration failed!</div>');
									}, 2000);
								}
							}); // EO user registration

						} // EO If Company Id not empty
						else{
							jQuery('.rgisterUser').attr('disabled',true);
							jQuery('#loader,#overlay').css('display','block');
							jQuery("#overlay").css({
							    position: "absolute",
							    width: "100%",
							    height: "100%",
							    top: 0,
							    left: 0,
							    background: "rgba(14, 10, 10, 0.5)"
							});

							jQuery.ajax({
								type:"POST",
								url:ajaxurl,
								data:{action: 'register_form', 'user_email':user_email,'credentials':credentials,'firname':firstName,'lasname':lastName,'phone':phone,'phone2':phone2},
								dataType:"json",
								success:function(response)
								{	console.log('response'+response);
									//console.log(wp_send_json_success());
									if(response  != "")
									{	jQuery( '#registerform' ).each(function(){
										    this.reset();
										});
										jQuery('.rgisterUser').closest('form').find("input[type=hidden]").val("");
										jQuery('.sol-selected-display-item-text').html('');
										jQuery('.rgisterUser').attr('disabled',true);
										jQuery('#loader,#overlay').css('display','none');
										console.log('registered successfully');
										setTimeout(function(){
											jQuery('#msg').html('<div class="alert alert-'+response.status+'">User and Company Created Successfully!</div>');
										}, 2000);

										/******** Add Location to airtable Locations Database *********/
										jQuery.ajax({
										    url: "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Locations",
										    beforeSend: function(xhr) {
										      xhr.setRequestHeader("Authorization", "Bearer "+"keyw3cJfiAFXmYjJf");
										    },
										    type: 'POST',
										    dataType: 'json',
										    contentType: 'application/json',
										    processData: true,
										    data: '{"fields": {"ID1":"'+suburb+'","Street":"'+street_address+' '+street_address_2+'","Suburb":"'+suburb+'","State":"'+state+'","Postcode":"'+post_code+'","Email":"'+user_email+'","Phone1":"'+phone+'","Phone2":"'+phone2+'"}}',
										    success: function (data) {
												console.log(JSON.stringify(data));
												var at_locid= data.id;
												 /********* Save logs************/
											      jQuery.ajax({
														type:"POST",
														url:ajaxurl,
														data:{action: 'register_form', 'user_emails':user_email,'send_request':'add location data to location table in airtable','received_request':'new location inserted with id: '+at_locid},
														dataType:"json",
														success:function(response)
														{
															console.log("Location logs updated!");
														},
														error: function(){
														    console.log("Cannot update location logs!");
														}
													});

											     /****** EO Logs ********/

											    /******** Add Company to airtable Companies Database *********/
												jQuery.ajax({
													url: "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Companies",
													beforeSend: function(xhr) {
													    xhr.setRequestHeader("Authorization", "Bearer "+"keyw3cJfiAFXmYjJf");
													},
													type: 'POST',
													dataType: 'json',
													contentType: 'application/json',
													processData: true,
													data: '{"fields": {"CompanyCode": "Pending Company","CompanyName": "'+company_name+'","Locations":["'+at_locid+'"],"Type": "NA","Website": "'+web_url+'"}}',
													success: function (data) {
														console.log(JSON.stringify(data));
														var at_comid = data.id;
														/********* Save logs************/
														jQuery.ajax({
															type:"POST",
															url:ajaxurl,
															data:{action: 'register_form', 'user_emails':user_email,'send_request':'add new company data to company table in airtable','company_id':at_comid,'received_request':'new company inserted with id: '+at_comid},
															dataType:"json",
															success:function(response)
															{
																console.log("Company logs updated!");
															},
															error: function(){
															    console.log("Cannot update company logs!");
															}
														});
														/***** EO Logs*****/

														/******* Add Contacts to airtable Contacts Database ********/
														jQuery.ajax({
															url: "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Contacts",
															beforeSend: function(xhr) {
															    xhr.setRequestHeader("Authorization", "Bearer "+"keyw3cJfiAFXmYjJf");
															},
															type: 'POST',
															dataType: 'json',
															contentType: 'application/json',
															processData: true,
															data: '{"fields": {"ContactName": "'+full_name+'","Locations": ["'+at_locid+'"],"Position": "New User","Companies":["'+at_comid+'"],"Phone": "'+phone+'","Email": "'+user_email+'"}}',
															success: function (data) {
																console.log(JSON.stringify(data));
																console.log('Contacts added to airtable');
																var contac_id = data.id;
																//alert('contac_id:'+contac_id);
																/********* Save logs************/
																jQuery.ajax({
																	type:"POST",
																	url:ajaxurl,
																	data:{action: 'register_form', 'user_emails':user_email,'send_request':'add new contact in airtable','received_request':'new contact added to airtable with id: '+contac_id},
																	dataType:"json",
																	success:function(response)
																	{
																		console.log("Contacts logs updated!");
																	},
																	error: function(){
																	    console.log("Cannot update contacts logs!");
																	}
																});
															},
															error: function(){
																alert("Cannot add contacts data");
																	/********* Save logs************/
																	jQuery.ajax({
																		type:"POST",
																		url:ajaxurl,
																		data:{action: 'register_form', 'user_emails':user_email,'send_request':'add new contact data to contacts table in airtable','received_request':'new contact not inserted'},
																		dataType:"json",
																		success:function(response)
																		{
																			console.log("logs updated!");
																		},
																		error: function(){
																		    console.log("Cannot update logs!");
																		}
																	});
																}
															}); // EO Add Contacts
													},
												    error: function(){
													    console.log("Cannot add company data");
													    /********* Save logs************/
														jQuery.ajax({
															type:"POST",
															url:ajaxurl,
															data:{action: 'register_form', 'user_emails':user_email,'send_request':'add company data to companies table in airtable','received_request':'new company not inserted'},
															dataType:"json",
															success:function(response)
																{
																	console.log("logs updated!");
																},
																error: function(){
																    console.log("Cannot update logs!");
																}
														});
													}
												}); // EO Add Company
											},
											error: function(){
												console.log("Cannot add location data");
												/********* Save logs************/
												jQuery.ajax({
													type:"POST",
													url:ajaxurl,
													data:{action: 'register_form', 'user_emails':user_email,'send_request':'add location data to location table in airtable','received_request':'new location not inserted'},
													dataType:"json",
													success:function(response)
													{
														console.log("Location logs updated!");
													},
													error: function(){
													    console.log("Cannot update location logs!");
													}
												});
												/****** EO Logs ********/
											}
										}); //EO Add Location



									} // EO response for user registration if part
									else
									{	jQuery( '#registerform' ).each(function(){
										    this.reset();
										});
										jQuery('.rgisterUser').closest('form').find("input[type=hidden]").val("");
										jQuery('.sol-selected-display-item-text').html('');
										jQuery('.rgisterUser').attr('disabled',false);
										jQuery('#loader,#overlay').css('display','none');
										console.log('error occured..');
										setTimeout(function(){
											jQuery('#msg').html('<div class="alert alert-danger">'+user_email+': Email already exists!</div>');
										}, 2000);
									}		// EO response for user registration else part

								},
								error:function(){
									jQuery( '#registerform' ).each(function(){
										    this.reset();
										});
									jQuery('.rgisterUser').closest('form').find("input[type=hidden]").val("");
									jQuery('.sol-selected-display-item-text').html('');
									jQuery('.rgisterUser').attr('disabled',false);
									jQuery('#loader,#overlay').css('display','none');
									console.log('failed!');
									//console.log(wp_send_json_error());
									setTimeout(function(){
										jQuery('#msg').html('<div class="alert alert-danger">User and Company Registration failed!</div>');
									}, 2000);
								}
							}); // EO user registration
						}// EO Else part if company

						} // EO Else of email Validate

				} // EO if user Email is not empty

				return false;
            },
         });
	}); // EO click funtion

	jQuery('.rgisterUser').click(function(){
		if(jQuery('#website').val()!=""){
			if(!ValidateUrl(jQuery('#website').val())){
				jQuery('#website').addClass('error-check');
				jQuery('.error-website').remove();
				jQuery('#website').after('<div class="error-website error-common">Please enter valid url.</div>');
			} else {
				jQuery('#website').removeClass('error-check');
				jQuery('.error-website').remove();
			}
		}
		jQuery('#registerform').validate({
			errorClass : "has-error",
	        highlight : function(element, errorClass) {
						jQuery(element).parents('.form-group').addClass(errorClass);
	        },
	        unhighlight  : function(element, errorClass, validClass) {
	           jQuery(element).parents('.form-group').removeClass(errorClass);
	        },
          rules: {
            user_email: {
               required: true,
               email:true
               },
            credentials: {
               required: true,
               minlength:8
               },
            full_name: {
               required: true
               } ,
            /*company:{
            	required:true
            }, */
            company_name:{
            	required:true
            },
            /*Website:{
            	url:true
            }, */
            street_address:{
            	required:true
            },
            suburb:{
            	required:true
            },
            state:{
            	required:true
            },
            post_code:{
            	required:true
            },
            locations:{
            	required:true
            },
            phone:{
            	required:true,
            	minlength:10
            },
            phone21:{
            	minlength:10
            },
          },

          submitHandler: function(form) {
						var fd = new FormData(form);
						jQuery('#loader,#overlay').css('display','block');
						jQuery("#overlay").css({
						    position: "absolute",
						    width: "100%",
						    height: "100%",
						    top: 0,
						    left: 0,
						    background: "rgba(14, 10, 10, 0.5)"
						});

						jQuery.ajax({
				        type:"POST",
				        url: ajaxurl,
				        data: fd,
					      contentType : false,
					      cache       : false,
					      processData : false,
					      dataType    : "json",
				        success: function(response){
									if(response.status=='success'){
										jQuery('#loader,#overlay').css('display','none');
										setTimeout(function(){
											jQuery('#msg').html('<div class="alert alert-'+response.status+'">'+response.msg+'</div>');
										}, 2000);
										jQuery( '#registerform' ).each(function(){
										    this.reset();
										});
										jQuery('.chosen-select').trigger('chosen:updated');
										jQuery('#locations_chosen a span').html('--Select Location--');
										jQuery('.rgisterUser').closest('form').find("input[type=hidden]").val("");
										jQuery('.sol-selected-display-item-text').html('');

				        	}else if(response.status=='danger'){
										jQuery('#loader,#overlay').css('display','none');
										setTimeout(function(){
											jQuery('#msg').html('<div class="alert alert-'+response.status+'">'+response.msg+'</div>');
										}, 200);

										jQuery( '#registerform' ).each(function(){
										    this.reset();
										});
										jQuery('.chosen-select').trigger('chosen:updated');
										jQuery('#locations_chosen a span').html('--Select Location--');
										jQuery('.rgisterUser').closest('form').find("input[type=hidden]").val("");
										jQuery('.sol-selected-display-item-text').html('');
				        	}else{
				        		alert('fail');
				        	}
				        }
				  	});
					},
					errorPlacement: function (errorClass, element) {
		        if (element.is("select.chosen-select")) {
		          // placement for chosen
		          element.next("div.chosen-container").append(errorClass);
		        } else {
		          //standard placement
		          errorClass.insertAfter(element);
		        }
		    	}
				}); //EO Validation
	}); // EO click funtion

	jQuery('.addLocation').click(function(){
		jQuery('#newlocation').validate({
      rules: {
        street_address1:{
        	required:true
        },
        suburb1:{
        	required:true
        },
        state1:{
        	required:true
        },
        post_code1:{
        	required:true
        },
        phone:{
        	required:true
        },

      },

			submitHandler: function(form) {
      	jQuery('.addLocation').attr('disabled',true);
				jQuery('#loader,#overlay').css('display','block');
				jQuery("#overlay").css({
				    position: "absolute",
				    width: "100%",
				    height: "100%",
				    top: 0,
				    left: 0,
				    background: "rgba(14, 10, 10, 0.5)"
				});
				var user_email 			= jQuery('#user_email').val();
				var company 			= jQuery('#company_id').val();
				var street_address1 	= jQuery('#street_address1').val();
				var street_address_2_1 	= jQuery('#street_address_2_1').val();
				var suburb1 			= jQuery('#suburb1').val();
				var state1 				= jQuery('#state1').val();
				var post_code1 			= jQuery('#post_code1').val();
				var phone 				= jQuery('#phone').val();
				var phone2 				= jQuery('#phone2').val();
				jQuery.ajax({
					type:"POST",
					url:ajaxurl,
					data:{action: 'newlocation_form', 'user_email':user_email,'company':company,'street_address1':street_address1,'street_address_2_1':street_address_2_1,'suburb1':suburb1,'state1':state1,'post_code1':post_code1,'phone':phone,'phone2':phone2},
					dataType:"json",
					success:function(response){
						console.log('response'+response);
						if(response  != ""){
							jQuery('.addLocation').attr('disabled',false);
							jQuery('#loader,#overlay').css('display','none');
							console.log('new location added successfully');
							setTimeout(function(){
								jQuery('#msg').html('<div class="alert alert-'+response.status+'">'+response.msg+'</div>');
							}, 2000);
							jQuery( '#newlocation' ).each(function(){
							    this.reset();
							});
						}
					},
					error: function(){
						console.log("Cannot add new location");
					}
				});
			}
		});
	});// EO click function
}); // EO ready funtion

jQuery( "#full_name").keydown(function (e) {
  if (e.ctrlKey || e.altKey){
      e.preventDefault();
  } else {
      var key = e.keyCode;
      if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
          e.preventDefault();
      }
  }
});

jQuery(document).on('keyup blur', '.phoneNo', function(e){
		e.preventDefault();
		var key  = e.charCode || e.keyCode || 0;
		if (key >= 65 && key <= 90){
			this.value = this.value.replace(/[^\d]/g,'');
			return false;
		}
		$phone   = jQuery(this);
		var val  = $phone.val();
		val 	 = val.replace(/\-+/g, '');
		var len = val.length;
		if( len <= 10 ){
			if ((key !== 8 && key !== 9)){
				var first  = val.substring(0, 4);
				var sec1   = val.substring(4, 7);
				var sec2   = val.substring(7, 10);
				var newPhone = first;
				if(sec1)
					newPhone = newPhone+'-'+sec1;
				if(sec2)
					newPhone = newPhone+'-'+sec2;
				jQuery(this).val(newPhone);
			}
		} else if( len <= 11 ) {
			if ((key !== 8 && key !== 9)){
				var first  = val.substring(0, 4);
				var sec1   = val.substring(4, 7);
				var sec2   = val.substring(7, 11);
				var newPhone = first;
				if(sec1)
					newPhone = newPhone+'-'+sec1;
				if(sec2)
					newPhone = newPhone+'-'+sec2;
				jQuery(this).val(newPhone);
			}
		} else if( len <= 13 ) {
			if ((key !== 8 && key !== 9)) {
				var first  = val.substring(0, 4);
				var sec1   = val.substring(4, 7);
				var sec2   = val.substring(7, 10);
				var sec3   = val.substring(10, 13);
				var newPhone = first;
				if(sec1)
					newPhone = newPhone+'-'+sec1;
				if(sec2)
					newPhone = newPhone+'-'+sec2;
				if(sec3)
						newPhone = newPhone+'-'+sec3;
				jQuery(this).val(newPhone);
			}
		}
		// Allow numeric (and tab, backspace, delete) keys only
		return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
});

function ValidateEmail(email) {
  var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  return expr.test(email);
}

function ValidateUrl(url) {
  var re = /(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/;
  return re.test(url);
}

/*
jQuery(document).ready(function(){
	jQuery(document).on('click','.panelchk',function(){
	var dynamicVal	 = jQuery(this).val();

	jQuery('#damage_1').attr('name', dynamicVal);
 	jQuery('.order-image-section').show();

	addDynamicPhoto(dynamicVal);
	});

function addDynamicPhoto(dynamicVal)
{
	//alert(dynamicVal);
	var testt = dynamicVal;
	//alert(testt);
	var count = 1;

	jQuery("#btn1").click(function(){

		if(count <= 4)
		{
			 jQuery(".order-image-section").append("<div class='row image-remove'><div class='col-md-3'><label>Driver Photograph <sup>*</sup></label></div><div class='col-md-4'><input type='file' class='form-control' name='"+ testt +"' id='damage_1'/><span>Attach Photograph</span></div><div class='col-md-1'><button type='button' class='btn btn-danger' id='minus'> <span class='glyphicon glyphicon-remove'></span></button></div>	</div>");
		}
		count++;
	});

}

jQuery(document).on('click','#minus',function(){
	jQuery(this).closest('div').parents('div.image-remove').remove();
});

*/

jQuery(document).ready(function(){
		/*
	jQuery(document).on('click','.panelchk',function(){
	var dynamicVal	 = jQuery(this).val()+'_image[]';

	jQuery('#damage_1').attr('name', dynamicVal);
 	jQuery('.order-image-section').show();

	addDynamicPhoto(dynamicVal);
	});

function addDynamicPhoto(dynamicVal)
{
	//alert(dynamicVal);
	var testt = dynamicVal;
	//alert(testt);
	var count = 1;

	//jQuery("#btn1").click(function(){
	jQuery(".addimage").click(function(){
		var asd = jQuery(this).parents('.order-image-section').find('.row').length;
		if(asd <= 4)
		{
			//jQuery(".order-image-section").append("<div class='row image-remove'><div class='col-md-3'><label>Driver Photograph <sup>*</sup></label></div><div class='col-md-4'><input type='file' class='form-control' name='"+ testt +"' id='damage_1'/><span>Attach Photograph</span></div><div class='col-md-1'><button type='button' class='btn btn-danger' id='minus'> <span class='glyphicon glyphicon-remove'></span></button></div>	</div>");
			jQuery(this).parents('.order-image-section').append("<div class='row image-remove'><div class='col-md-3'><label>Driver Photograph <sup>*</sup></label></div><div class='col-md-4'><input type='file' class='form-control' name='"+ testt +"' id='damage_1'/><span>Attach Photograph</span></div><div class='col-md-1'><button type='button' class='btn btn-danger' id='minus'> <span class='glyphicon glyphicon-remove'></span></button></div>	</div>");
		}
		count++;
	});

}*/

	jQuery(".addimage").click(function(){
		var fname = jQuery(this).attr('id');
		var count = jQuery(this).parents('.order-image-section').find('.row').length;
		if(count <= 4){
			jQuery(this).parents('.order-image-section').append("<div class='row image-remove'><div class='col-md-3'><label>Image "+ parseInt(count+1)+" <sup></sup></label></div><div class='col-md-4'><input type='file' class='form-control' name='"+ fname +"[]'/></div><div class='col-md-1'><i class='glyphicon glyphicon-minus btn btn-info addimage' id='minus'></i></div>	</div>");
		}
		count++;
	});

	jQuery(".addimagejob").click(function(){
		var count = jQuery(this).parents('#job_completion_images').find('.row').length;
		if(count <= 4){
			jQuery(this).parents('#job_completion_images').append("<div class='row image-remove'><div class='col-md-3'><label>Image "+ parseInt(count+1)+" <sup></sup></label></div><div class='col-md-4'><input type='file' class='form-control' name='work_image[]'/></div><div class='col-md-1'><i class='glyphicon glyphicon-minus btn btn-info addimage' id='minus'></i></div>	</div>");
		}
		count++;
	});

	jQuery(document).on('click','#minus',function(){
		jQuery(this).closest('div').parents('div.image-remove').remove();
	});


	jQuery(document).on('keyup blur', '.mbCheckNm', function(e){
		e.preventDefault();
		var key  = e.charCode || e.keyCode || 0;
		if (key >= 65 && key <= 90){
			this.value = this.value.replace(/[^\d]/g,'');
			return false;
		}
	});

	var input = document.querySelector("#phone"),
    errorMsg = document.querySelector("#error-msg"),
    validMsg = document.querySelector("#valid-msg");

	// Error messages based on the code returned from getValidationError
	var errorMap = [ "Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number","Invalid number"];

	// Initialise plugin
	var intl = window.intlTelInput(input, {
		initialCountry: "au",	  
		utilsScript: "utils-js"
	});

	var reset = function() {
		input.classList.remove("error");
		errorMsg.innerHTML = "";
		errorMsg.classList.add("hide");
		validMsg.classList.add("hide");
	};

	// Validate on blur event
	input.addEventListener('blur', function() {
		reset();
		if(input.value.trim()){
			if(intl.isValidNumber()){
				validMsg.classList.remove("hide");
			}else{
				input.classList.add("error");
				var errorCode = intl.getValidationError();
				console.log(errorCode);
				errorMsg.innerHTML = errorMap[errorCode];
				errorMsg.classList.remove("hide");
			}
		}
	});

	// Reset on keyup/change event
	input.addEventListener('change', reset);
	input.addEventListener('keyup', reset);


});
