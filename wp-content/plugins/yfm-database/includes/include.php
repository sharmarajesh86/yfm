<?php /**************************************************************************************
*
* This file consists of the scripts and styles that is used in this plugin.
*
*********************************************************************************************/

/***** enqueue scripts on form *****/

	wp_enqueue_script( 'jquery.min', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', array('jquery') );
	wp_enqueue_script( 'bootstrap.min', plugins_url().'/yfm-database/js/bootstrap.min.js');
	wp_enqueue_script( 'jquery.form-validator', '//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js', array('jquery') );
	wp_enqueue_script( 'jquery-validator', 'https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js', array('jquery') );
	wp_enqueue_script( 'additional-method', plugins_url().'/yfm-database/js/additional-method.js', array('jquery') );
	wp_enqueue_script( 'intlTelInput-js', plugins_url().'/yfm-database/js/intlTelInput.js');
	wp_enqueue_script( 'custom-js', plugins_url().'/yfm-database/js/custom.js');	
	wp_enqueue_script( 'multiple-select-js', plugins_url().'/yfm-database/js/multiple-select.js');
	wp_enqueue_script( 'chosen.jquery.js', plugins_url().'/yfm-database/js/chosen.jquery.min.js');
	wp_enqueue_script( 'sol', plugins_url().'/yfm-database/js/sol.js');
	wp_enqueue_script( 'datatables', plugins_url().'/yfm-database/js/datatables.js');
	wp_enqueue_script( 'utils-js', plugins_url().'/yfm-database/js/utils.js');
	// declare the URL to the file that handles the AJAX request (wp-admin/admin-ajax.php)
	wp_localize_script( 'make-register-ajax', 'MRAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

	/***** enqueue styles on  form *****/
	wp_register_style( 'bootstrap', plugins_url().'/yfm-database/css/bootstrap.min.css' );
	wp_register_style( 'multiple-select', plugins_url().'/yfm-database/css/multiple-select.css');
    wp_enqueue_style( 'multiple-select' );
    wp_register_style( 'chosen', plugins_url().'/yfm-database/css/chosen.min.css');
    wp_enqueue_style( 'chosen' );
    wp_enqueue_style( 'bootstrap' );

    wp_register_style( 'custom-style', plugins_url().'/yfm-database/css/custom-style.css');
    wp_enqueue_style( 'custom-style' );

    wp_register_style( 'font-awesome', plugins_url().'/yfm-database/css/font-awesome.min.css' );
    wp_enqueue_style( 'font-awesome' );


    wp_register_style( 'sol', plugins_url().'/yfm-database/css/sol.css' );
    wp_enqueue_style( 'sol' );

    wp_register_style( 'datatables', plugins_url().'/yfm-database/css/datatables.css' );
	wp_enqueue_style( 'datatables' );
	
	wp_register_style( 'intlTelInput', plugins_url().'/yfm-database/css/intlTelInput.css');
    wp_enqueue_style( 'intlTelInput' );
