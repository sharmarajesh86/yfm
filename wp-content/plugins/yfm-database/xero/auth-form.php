<?php /**************************************************************************************
*
* This file consists of the shortcodes that is used in this plugin to create the forms for 
* xero authentication.
*
*********************************************************************************************/

/********* Create Xero Login Button Shortcode********/

function xeroLogin_creation(){
	include( plugin_dir_path( __FILE__ ) . 'public.php'); // get locations list 
	//include( plugin_dir_path( __FILE__ ) . 'private.php'); // get locations list 
	
}
add_shortcode('xero_login_auth', 'xeroLogin_creation'); // EO Xero Login Button shortcode

