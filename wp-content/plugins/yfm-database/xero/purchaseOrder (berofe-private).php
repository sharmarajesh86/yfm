<?php
if (!class_exists('XeroOAuth'))
{
    require 'lib/XeroOAuth.php';
}

/**
 * Define for file includes
 */
define ( 'BASE_PATH', dirname(__FILE__) );

/**
 * Define which app type you are using:
 * Private - private app method
 * Public - standard public app method
 * Public - partner app method
 */
define ( "XRO_APP_TYPE", "Public" );

/**
 * Set a user agent string that matches your application name as set in the Xero developer centre
 */
//$useragent = "Xero-OAuth-PHP Public";
$useragent = "kdlocalapp";

/**
 * Set your callback url or set 'oob' if none required
 * Make sure you've set the callback URL in the Xero Dashboard
 * Go to https://api.xero.com/Application/List and select your application
 * Under OAuth callback domain enter localhost or whatever domain you are using.
 */
define ( "OAUTH_CALLBACK", site_url().'/xero-auth' );

/**
 * Application specific settings
 * Not all are required for given application types
 * consumer_key: required for all applications
 * consumer_secret: for partner applications, set to: s (cannot be blank)
 * rsa_private_key: application certificate private key - not needed for public applications
 * rsa_public_key: application certificate public cert - not needed for public applications
 */

if (!function_exists('testLinks'))
{
    include 'tests/testRunner.php';
}


$signatures = array (
        'consumer_key' => 'DWSGSXDEW4IWHDLUPTGBQURQTTKMQ2',
        'shared_secret' => 'X9XKEX5YNKYVWX6JVYUCMQRDYM4KQ3',
        //'consumer_key' => 'BKG6SHPMJLYLVFSMSMQMO4VIQRV0ML',
        //'shared_secret' => 'DPOTWVDDFAA6IEKTNEUNH0E4FLH4HR',
        // API versions
        'core_version' => '2.0',
        'payroll_version' => '1.0',
        'file_version' => '1.0' 
);

if (XRO_APP_TYPE == "Private" || XRO_APP_TYPE == "Partner") {
    $signatures ['rsa_private_key'] = BASE_PATH . '/certs/privatekey.pem';
    $signatures ['rsa_public_key'] = BASE_PATH . '/certs/publickey.cer';
}
if (XRO_APP_TYPE == "Partner") {
    $signatures ['curl_ssl_cert'] = BASE_PATH . '/certs/entrust-cert-RQ3.pem';
    $signatures ['curl_ssl_password'] = '1234';
    $signatures ['curl_ssl_key'] = BASE_PATH . '/certs/entrust-private-RQ3.pem';
}

$XeroOAuth = new XeroOAuth ( array_merge ( array (
        'application_type' => XRO_APP_TYPE,
        'oauth_callback' => OAUTH_CALLBACK,
        'user_agent' => $useragent 
), $signatures ) );

$initialCheck = $XeroOAuth->diagnostics ();

//echo "<pre>"; print_r($_POST); die('---');

$checkErrors = count ( $initialCheck );
if ($checkErrors > 0)
{
    // you could handle any config errors here, or keep on truckin if you like to live dangerously
    foreach ( $initialCheck as $check )
    {
        echo 'Error: ' . $check . PHP_EOL;
    }
    $_SESSION["tpa_purchase_xero_error"] = 1;
}
else
{
    session_start ();
    if (!isset($_SESSION['session_handle']))
    {
        $_SESSION['session_handle'] = "";
    }
    $oauthSession = retrieveSession ();

    //echo "<pre>"; print_r($oauthSession); die('---');

    if(empty($oauthSession)){
                
    }

    $XeroOAuth->config['access_token']  = $oauthSession['oauth_token'];
    $XeroOAuth->config['access_token_secret'] = $oauthSession['oauth_token_secret'];
    $XeroOAuth->config['session_handle'] = $oauthSession['oauth_session_handle'];

    // var_dump($checkBrandTpa);
    // echo "stringadd";

    //echo "<pre>"; print_r($powhline); die;

    // All wharehouse
    $totalInvociceAmt = 0;
    $randNumber = rand(10,1000);
    foreach ($powhline as $whkey => $wh) {
        
        $addCustomerXml =  "<Contacts>
                            <Contact>
                                <Name>".$whkey."</Name>
                                <FirstName>".$whkey."</FirstName>
                            </Contact>
                        </Contacts>";
        $response = $XeroOAuth->request('POST', $XeroOAuth->url('Contacts', 'core'), array(), $addCustomerXml);
        if ($XeroOAuth->response['code'] == 200){
            $contact = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
            if (count($contact->Contacts[0])>0)
            {
                $wpcontact['ContactID'] = $ContactID = $contact->Contacts[0]->Contact->ContactID;
                $xeroResponse['contact']['status'] = 'success'; 
                $xeroResponse['contact']['response'] = $XeroOAuth->response['response'];      
                $xeroStatus = "success"; 
            }
        } else {
            $xeroStatus = "fail";
            $xeroResponse['contact']['status'] = 'fail';  
            $xeroResponse['contact']['response'] = $XeroOAuth->response['response'];    
        }
        



        $poItemListXml = '';
        $invItemListXml = '';
        foreach ($wh['lines'] as $line) {
            $totalInvociceAmt = $totalInvociceAmt + ($line["qty"]*$line["unitAmount"]);
                        
            $poItemListXml    .=  "<LineItem>
                <ItemCode>".$line["Part_Code"]."</ItemCode>
                <Description>".$line["Purchase_Description"]."</Description>
                <Quantity>".$line["qty"]."</Quantity> 
                <AccountCode>300</AccountCode>               
            </LineItem>";

            $invItemListXml    .=  "<LineItem>
                        <ItemCode>".$line["Part_Code"]."</ItemCode>
                        <Description>".$line["Sales_Description"]."</Description>
                        <Quantity>".$line["qty"]."</Quantity>
                        <AccountCode>200</AccountCode>
                    </LineItem>";
                                       

        }
      
        $ponumber =  $randNumber."-PO-".$whkey;
        $purchaseOrderXml = "<PurchaseOrder>
                            <PurchaseOrderNumber>".$ponumber."</PurchaseOrderNumber>                             
                            <Reference>".$poReference."</Reference>                             
                            <Contact>
                                <ContactID>".$wpcontact['ContactID']."</ContactID>
                            </Contact>
                            <Date>".date('Y-m-d')."</Date>
                            <LineItems>".$poItemListXml ."</LineItems>
                        </PurchaseOrder>";

    
        $response = $XeroOAuth->request('POST', $XeroOAuth->url('PurchaseOrders', 'core'), array(), $purchaseOrderXml);
      
        if ($XeroOAuth->response['code'] == 200)
        {
            $purchaseOrder = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
            $po_id = $purchaseOrder->PurchaseOrders->PurchaseOrder->PurchaseOrderID;
            $xeroResponse['po'][$whkey]['status'] = 'success';
            $xeroResponse['po'][$whkey]['po_id'] =  $po_id;
            $xeroResponse['po'][$whkey]['response']    =  $XeroOAuth->response['response'];     
            $xeroStatus = "success";            
           
        }
        else
        { 
            $xeroResponse['po'][$whkey]['status']      = 'fail';
            $xeroResponse['po'][$whkey]['response']    =  $XeroOAuth->response['response'];
            $xeroStatus = "fail";
        }  

    }

    // If Instalation Required
    if($isInstallation == 'yes'){

         $addCustomerXml =  "<Contacts>
                            <Contact>
                                <Name>Placeholder Installer</Name>
                                <FirstName>Installation</FirstName>
                            </Contact>
                        </Contacts>";

        $response = $XeroOAuth->request('POST', $XeroOAuth->url('Contacts', 'core'), array(), $addCustomerXml);
        if ($XeroOAuth->response['code'] == 200){
            $contact = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
            if (count($contact->Contacts[0])>0)
            {
                $installer['ContactID'] = $ContactID = $contact->Contacts[0]->Contact->ContactID;
                $xeroResponse['Installer']['contact']['status'] = 'success'; 
                $xeroResponse['Installer']['contact']['response'] = $XeroOAuth->response['response'];      
                #Update Contact Id In Wp Admin
                //echo "<prE>"; print_r($XeroOAuth->response); echo "</pre>";
                //die('po_contact_created');
            }
        } else {
            // outputError($XeroOAuth);
            $_SESSION["yfm_po_contact_create_errro"] = 1;
            //echo "<prE>"; print_r($XeroOAuth->response); echo "</pre>";
            $xeroResponse['Installer']['contact']['status'] = 'fail';  
            $xeroResponse['Installer']['contact']['response'] = $XeroOAuth->response['response'];    
        }


        $instalationCharge = 100;
        $totalInvociceAmt = $totalInvociceAmt + $instalationCharge;
        $ponumber =  $randNumber."-PO-Installation";

        $purchaseOrderXml = "<PurchaseOrder>
                            <PurchaseOrderNumber>".$ponumber."</PurchaseOrderNumber>                             
                             <Reference>".$poReference."</Reference>                                 
                            <Contact>
                                <ContactID>".$installer['ContactID']."</ContactID>
                            </Contact>
                            <Date>".date('Y-m-d')."</Date>
                            <LineItems>
                                <LineItem>
                                    <Description>Installation Charge</Description>
                                    <Quantity>1</Quantity>
                                    <UnitAmount>".$instalationCharge."</UnitAmount>
                                </LineItem>
                            </LineItems>
                        </PurchaseOrder>";

         $invItemListXml    .=  "<LineItem>
                                        <Description>Installation Charge</Description>
                                        <Quantity>1</Quantity>
                                        <UnitAmount>$instalationCharge</UnitAmount>
                                        <AccountCode>200</AccountCode>
                                    </LineItem>";

        $response = $XeroOAuth->request('POST', $XeroOAuth->url('PurchaseOrders', 'core'), array(), $purchaseOrderXml);
        if ($XeroOAuth->response['code'] == 200)
        {
            $purchaseOrder = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
            $po_id = $purchaseOrder->PurchaseOrders->PurchaseOrder->PurchaseOrderID;
            $xeroResponse['PO-Installation']['status'] = 'success';
            $xeroResponse['PO-Installation']['po_id'] =  $po_id;
            $xeroResponse['PO-Installation']['response']    =  $XeroOAuth->response['response'];
            //die('po_created');
            $xeroStatus = "success";
                     
        }
        else
        {
            //outputError($XeroOAuth);
            //echo "<prE>"; print_r($response->response); echo "</pre>";
            //die('create_po_error');  
            $xeroResponse['PO-Installation']['status']      = 'fail';
            $xeroResponse['PO-Installation']['response']    =  $XeroOAuth->response['response'];  
            $xeroStatus = "fail";
        }    
    }
     

    // Invocie create 

    $addCustomerXml =  "<Contacts>
                            <Contact>
                                <Name>".$wlud->data->airtable_ContactName."</Name>
                                <FirstName>".$wlud->data->airtable_ContactName."</FirstName>
                            </Contact>
                        </Contacts>";
                        
        $response = $XeroOAuth->request('POST', $XeroOAuth->url('Contacts', 'core'), array(), $addCustomerXml);
        if ($XeroOAuth->response['code'] == 200){
            $contact = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
            if (count($contact->Contacts[0])>0)
            {
                $invoiceContact['ContactID'] = $ContactID = $contact->Contacts[0]->Contact->ContactID;
                $xeroResponse['invociee']['contact']['status'] = 'success'; 
                $xeroResponse['invociee']['contact']['response'] = $XeroOAuth->response['response'];      
                #Update Contact Id In Wp Admin
                //echo "<prE>"; print_r($XeroOAuth->response); echo "</pre>";
                //die('po_contact_created');
            }
        } else {
            // outputError($XeroOAuth);
            $_SESSION["yfm_po_contact_create_errro"] = 1;
            //echo "<prE>"; print_r($XeroOAuth->response); echo "</pre>";
            $xeroResponse['invociee']['contact']['status'] = 'fail';  
            $xeroResponse['invociee']['contact']['response'] = $XeroOAuth->response['response'];    
        }


    $invoiceNumber = $randNumber."-INV";
    $xeroxmldata = "<Invoices>
                        <Invoice>
                            <Type>ACCREC</Type>
                            <Contact>
                                <ContactID>".$invoiceContact['ContactID']."</ContactID>
                            </Contact>
                            <Reference>".$poReference."</Reference>     
                            <Date>" . date('Y-m-d') . "T00:00:00</Date>
                            <DueDate>" . date('Y-m-d') . "T00:00:00</DueDate>
                            <InvoiceNumber>" . $invoiceNumber . "</InvoiceNumber>
                            <LineAmountTypes>Inclusive</LineAmountTypes>
                            <SubTotal>" .  $totalInvociceAmt . "</SubTotal>
                            <TotalTax>0</TotalTax>
                            <Total>" . $totalInvociceAmt . "</Total>
                            <LineItems>
                                ". $invItemListXml."
                            </LineItems>
                        </Invoice>
                    </Invoices>";

    $response = $XeroOAuth->request('POST', $XeroOAuth->url('Invoices', 'core'), array(), $xeroxmldata);
    if ($XeroOAuth->response['code'] == 200) {
        $xeroResponse['invoice']['status']      = 'success';
        $xeroResponse['invoice']['response']    =  $XeroOAuth->response['response'];    
    }
    else {
        $xeroResponse['invoice']['status']      = 'fail';
        $xeroResponse['invoice']['response']    =  $XeroOAuth->response['response'];    
    }
        

    //echo "<prE>"; print_r( $xeroResponse ); echo "</pre>"; die;

}