<?php

    define ( 'BASE_PATH', dirname(__FILE__) );

    include BASE_PATH . '/private.php';

    $totalInvociceAmt = 0;
    $randNumber = $airtableResponse->fields->Job_No;
    $poLinks = array();
    $invItemListXml = '';
    foreach ($powhline as $whkey => $wh) {
        $addCustomerXml =  "<Contacts>
                            <Contact>
                                <Name>".$whkey."</Name>
                                <FirstName>".$whkey."</FirstName>
                            </Contact>
                        </Contacts>";
        $response = $XeroOAuth->request('POST', $XeroOAuth->url('Contacts', 'core'), array(), $addCustomerXml);
        $poContact = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
        if ($XeroOAuth->response['code'] == 200){
            $contact = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
            if (count($contact->Contacts[0])>0)
            {
                $wpcontact['ContactID'] = $ContactID = $contact->Contacts[0]->Contact->ContactID;
                $xeroResponse['status']['contact'] = 'success';
                $xeroResponse['response']['contact'] = $poContact;
                $xeroStatus = "success";
            }
        } else {
            $xeroStatus = "fail";
            $xeroResponse['status']['contact'] = 'fail';
            $xeroResponse['response']['contact'] = $poContact;
        }

        $poItemListXml = '';
        foreach ($wh['lines'] as $line) {

            /* // $addItemXml =  "<Item>
            //                         <Code>".$line['itemdata']->Part_Code."</Code>
            //                         <Name>".$line['itemdata']->Item_Name."</Name>
            //                         <Description>".$line['itemdata']->Sales_Description."</Description>
            //                         <PurchaseDescription>".$line['itemdata']->Purchase_Description."</PurchaseDescription>
            //                         <PurchaseDetails>
            //                             <UnitPrice>10</UnitPrice>
            //                         </PurchaseDetails>
            //                         <SalesDetails>
            //                             <UnitPrice>10</UnitPrice>
            //                         </SalesDetails>
            //                     </Item>";


            // $response = $XeroOAuth->request('POST', $XeroOAuth->url('Items', 'core'), array(), $addItemXml);
            // $itemResponce = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);

            // if ($XeroOAuth->response['code'] == 200)
            // {

            // } */



            $totalInvociceAmt = $totalInvociceAmt + ($line["qty"]*$line["unitAmount"]);
            $poItemListXml    .=  "<LineItem>
                <ItemCode>".$line["Part_Code"]."</ItemCode>
                <Description>".$line["Purchase_Description"]."</Description>
                <Quantity>".$line["qty"]."</Quantity>
            </LineItem>";
            /* <AccountCode>300</AccountCode> */

            $invItemListXml    .=  "<LineItem>
                        <ItemCode>".$line["Part_Code"]."</ItemCode>
                        <Description>".$line["Sales_Description"]."</Description>
                        <Quantity>".$line["qty"]."</Quantity>
                    </LineItem>";
            /* <AccountCode>200</AccountCode>*/
        }

        $ponumber =  $randNumber."-PO-".$whkey;
        /* <PurchaseOrderNumber>".$ponumber."</PurchaseOrderNumber>*/
        $poReference1 = str_replace("&","&amp;",$poReference1);
        $poReference1 = str_replace('"',"&quot;",$poReference1);
        $poReference1 = str_replace("'","&apos;",$poReference1);
        $poReference1 = str_replace("<","&lt;",$poReference1);
        $poReference1 = str_replace('>',"&gt;",$poReference1);
        $purchaseOrderXml = "<PurchaseOrder>
                            <Reference>".$poReference1."</Reference>
                            <Contact>
                                <ContactID>".$wpcontact['ContactID']."</ContactID>
                            </Contact>
                            <Date>".date('Y-m-d')."</Date>
                            <BrandingThemeID>c7a479f1-8c30-41df-a27a-80c0e4a2fb37</BrandingThemeID>
                            <LineItems>".$poItemListXml ."</LineItems>
                        </PurchaseOrder>";
        $response = $XeroOAuth->request('POST', $XeroOAuth->url('PurchaseOrders', 'core'), array(), $purchaseOrderXml);
        $poRes = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);

        if ($XeroOAuth->response['code'] == 200)
        {
            $purchaseOrder = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
            $po_id = $purchaseOrder->PurchaseOrders->PurchaseOrder->PurchaseOrderID;
            $po_name = $purchaseOrder->PurchaseOrders->PurchaseOrder->PurchaseOrderNumber;
            $xeroResponse['status']['po'][$whkey] = 'success';
            $xeroResponse['po_id']['po'][$whkey] =  $po_id;
            $xeroResponse['response']['po'][$whkey]  =  $poRes;
            $xeroStatus = "success";
           	$poLinks[$whkey]['id'] = $po_id;
           	$poLinks[$whkey]['name'] = $po_name;
    
        }else{
            $xeroResponse['status']['po'][$whkey]      = 'fail';
            $xeroResponse['response']['po'][$whkey]    =  $poRes;
            $xeroStatus = "fail";
        }

    }

    /* If Instalation Required */
    if($isInstallation == 'yes'){
         $addCustomerXml =  "<Contacts>
                            <Contact>
                                <Name>Placeholder Installer</Name>
                                <FirstName>Installation</FirstName>
                            </Contact>
                        </Contacts>";

        $response = $XeroOAuth->request('POST', $XeroOAuth->url('Contacts', 'core'), array(), $addCustomerXml);
        $poInstalContactRes = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
        if ($XeroOAuth->response['code'] == 200){
            $contact = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
            if (count($contact->Contacts[0])>0)
            {
                $ContactID = $contact->Contacts[0]->Contact->ContactID;
                $xeroResponse['status']['Installer']['contact'] = 'success';
                $xeroResponse['response']['Installer']['contact'] = $poInstalContactRes;
            }else{
                $ContactID = $contact->Contacts->Contact->ContactID;
                $xeroResponse['status']['Installer']['contact'] = 'success';
                $xeroResponse['response']['Installer']['contact'] = $poInstalContactRes;
            }
        } else {
            // outputError($XeroOAuth);
            $_SESSION["yfm_po_contact_create_errro"] = 1;
            $xeroResponse['status']['Installer']['contact'] = 'fail';
            $xeroResponse['response']['Installer']['contact'] = $poInstalContactRes;
        }

        $instalationCharge = 100;
        $totalInvociceAmt = $totalInvociceAmt + $instalationCharge;
        $ponumber =  $randNumber."-PO-Installation";
        /*<PurchaseOrderNumber>".$ponumber."</PurchaseOrderNumber> */
        $purchaseOrderXml = "<PurchaseOrder>
                             <Reference>".$poReference1."</Reference>
                            <Contact>
                                <ContactID>".$ContactID."</ContactID>
                            </Contact>
                            <Date>".date('Y-m-d')."</Date>
                            <BrandingThemeID>26032054-9bbc-4af4-b3d6-f273ef5b619a</BrandingThemeID>
                            <LineItems>
                                <LineItem>
                                    <Description>Installation Charge</Description>
                                    <Quantity>1</Quantity>
                                    <UnitAmount>".$instalationCharge."</UnitAmount>
                                    <AccountCode>312</AccountCode>
                                </LineItem>
                            </LineItems>
                        </PurchaseOrder>";

         $invItemListXml    .=  "<LineItem>
                                        <Description>Installation Charge</Description>
                                        <Quantity>1</Quantity>
                                        <UnitAmount>$instalationCharge</UnitAmount>
                                        <AccountCode>312</AccountCode>
                                    </LineItem>";
                                    //<AccountCode>200</AccountCode>

        $response = $XeroOAuth->request('POST', $XeroOAuth->url('PurchaseOrders', 'core'), array(), $purchaseOrderXml);
        $poInstalRes = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
        if ($XeroOAuth->response['code'] == 200)
        {
            $purchaseOrder = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
            $po_id = $purchaseOrder->PurchaseOrders->PurchaseOrder->PurchaseOrderID;
            $po_name = $purchaseOrder->PurchaseOrders->PurchaseOrder->PurchaseOrderNumber;
            $xeroResponse['status']['po']['PO-Installation'] = 'success';
            $xeroResponse['po_id']['po']['PO-Installation'] =  $po_id;
            $xeroResponse['response']['po']['PO-Installation']    =  $poInstalRes;
            $xeroStatus = "success";

            $poLinks['Installation']['id'] = $po_id;
            $poLinks['Installation']['name'] = $po_name;

        }
        else
        {
            $xeroResponse['status']['po']['PO-Installation']     = 'fail';
            $xeroResponse['response']['po']['PO-Installation']    =  $poInstalRes;
            $xeroStatus = "fail";
        }
    }

    /* Invocie create */
    $addCustomerXml =  "<Contacts>
                            <Contact>
                                <Name>".$invoiceConpanyName."</Name>
                                <FirstName>".$invoiceConpanyName."</FirstName>
                            </Contact>
                        </Contacts>";

        $response = $XeroOAuth->request('POST', $XeroOAuth->url('Contacts', 'core'), array(), $addCustomerXml);
        $invoiceContactRes = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);

        if ($XeroOAuth->response['code'] == 200){
            $contact = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
            if (count($contact->Contacts[0])>0)
            {
                $invoiceContact['ContactID'] = $ContactID = $contact->Contacts[0]->Contact->ContactID;
                $xeroResponse['status']['invoice']['contact'] = 'success';
                $xeroResponse['response']['invoice']['contact'] = $invoiceContactRes;
            }
        } else {
            $_SESSION["yfm_po_contact_create_errro"] = 1;
            $xeroResponse['status']['invoice']['contact'] = 'fail';
            $xeroResponse['response']['invoice']['contact'] = $invoiceContactRes;
        }

    $invoiceNumber = $randNumber."-INV";
    $invDate =date('Y-m-d');
    $invDueDate = date('Y-m-d', strtotime('+1 month'));
    $invRef = explode(' - ', $poReference1);
    array_shift($invRef);
    $InvReference = implode(' - ', $invRef);
    $xeroxmldata = "<Invoices>
                        <Invoice>
                            <Type>ACCREC</Type>
                            <Contact>
                                <ContactID>".$invoiceContact['ContactID']."</ContactID>
                            </Contact>
                            <Reference>".'PO '.$ponum.' - '.$InvReference."</Reference>
                            <Date>" . $invDate . "T00:00:00</Date>
                            <DueDate>" . $invDueDate . "T00:00:00</DueDate>
                            <LineAmountTypes>Exclusive</LineAmountTypes>
                            <SubTotal>" .  $totalInvociceAmt . "</SubTotal>
                            <Total>" . $totalInvociceAmt . "</Total>
                            <LineItems>
                                ". $invItemListXml."
                            </LineItems>
                        </Invoice>
                    </Invoices>";
                    /* <InvoiceNumber>" . $invoiceNumber . "</InvoiceNumber> */
    $response = $XeroOAuth->request('POST', $XeroOAuth->url('Invoices', 'core'), array(), $xeroxmldata);
    $invoiceRes = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
    if ($XeroOAuth->response['code'] == 200) {
        $xeroResponse['status']['invoice']      = 'success';
        $xeroResponse['response']['invoice']    =  $invoiceRes;
        $invLinks['id'] = $invoiceRes->Invoices->Invoice->InvoiceID;
        $invLinks['name'] = $invoiceRes->Invoices->Invoice->InvoiceNumber;

    } else {
        $xeroResponse['status']['invoice']     = 'fail';
        $xeroResponse['response']['invoice']    =  $invoiceRes;
    }