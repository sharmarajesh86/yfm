<?php
if (!class_exists('XeroOAuth'))
{
    require 'lib/XeroOAuth.php';
}

/**
 * Define for file includes
 */
define ( 'BASE_PATH', dirname(__FILE__) );

/**
 * Define which app type you are using:
 * Private - private app method
 * Public - standard public app method
 * Public - partner app method
 */
define ( "XRO_APP_TYPE", "Public" );

/**
 * Set a user agent string that matches your application name as set in the Xero developer centre
 */
//$useragent = "Xero-OAuth-PHP Public";
$useragent = "kdlocalapp";

/**
 * Set your callback url or set 'oob' if none required
 * Make sure you've set the callback URL in the Xero Dashboard
 * Go to https://api.xero.com/Application/List and select your application
 * Under OAuth callback domain enter localhost or whatever domain you are using.
 */
define ( "OAUTH_CALLBACK", site_url().'/dashboard' );

/**
 * Application specific settings
 * Not all are required for given application types
 * consumer_key: required for all applications
 * consumer_secret: for partner applications, set to: s (cannot be blank)
 * rsa_private_key: application certificate private key - not needed for public applications
 * rsa_public_key: application certificate public cert - not needed for public applications
 */

if (!function_exists('testLinks'))
{
    include 'tests/testRunner.php';
}


$signatures = array (
        'consumer_key' => 'DWSGSXDEW4IWHDLUPTGBQURQTTKMQ2',
        'shared_secret' => 'X9XKEX5YNKYVWX6JVYUCMQRDYM4KQ3',
        // API versions
        'core_version' => '2.0',
        'payroll_version' => '1.0',
        'file_version' => '1.0' 
);

if (XRO_APP_TYPE == "Private" || XRO_APP_TYPE == "Partner") {
    $signatures ['rsa_private_key'] = BASE_PATH . '/certs/privatekey.pem';
    $signatures ['rsa_public_key'] = BASE_PATH . '/certs/publickey.cer';
}
if (XRO_APP_TYPE == "Partner") {
    $signatures ['curl_ssl_cert'] = BASE_PATH . '/certs/entrust-cert-RQ3.pem';
    $signatures ['curl_ssl_password'] = '1234';
    $signatures ['curl_ssl_key'] = BASE_PATH . '/certs/entrust-private-RQ3.pem';
}

$XeroOAuth = new XeroOAuth ( array_merge ( array (
        'application_type' => XRO_APP_TYPE,
        'oauth_callback' => OAUTH_CALLBACK,
        'user_agent' => $useragent 
), $signatures ) );

$initialCheck = $XeroOAuth->diagnostics ();

//echo "<pre>"; print_r($_POST); die('---');

$checkErrors = count ( $initialCheck );
if ($checkErrors > 0)
{
    // you could handle any config errors here, or keep on truckin if you like to live dangerously
    foreach ( $initialCheck as $check )
    {
        echo 'Error: ' . $check . PHP_EOL;
    }
    $_SESSION["tpa_purchase_xero_error"] = 1;
}
else
{
    //session_start ();
    if (!isset($_SESSION['session_handle']))
    {
        $_SESSION['session_handle'] = "";
    }
    $oauthSession = retrieveSession ();

    //echo "<pre>"; print_r($oauthSession); die('---');

    
}