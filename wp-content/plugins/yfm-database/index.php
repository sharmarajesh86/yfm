<?php /*
* Plugin Name: YFM Database
* Author: 1wayit (RJ)
* Version 1.0
* Description: This plugin is used to get the data from Airtable into wp database using Airtable API.
* 				It also uses the Xero API.
*/
defined( 'ABSPATH' ) or die( 'Hacking Not Allowed!' );
session_start();
function _remove_script_version( $src ){
$parts = explode( '?', $src );
return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );


include( plugin_dir_path( __FILE__ ) . '/shortcodes/shortcode.php'); // Create Pages with shortcode in wordpress admin.
include( plugin_dir_path( __FILE__ ) . '/forms/registration-form.php'); // Create Registration form and save form data to create a new user in wordpress.
include( plugin_dir_path( __FILE__ ) . '/forms/login-form.php'); // Create Login form
include( plugin_dir_path( __FILE__ ) . '/forms/dashboard.php'); // Create Dashboard
include( plugin_dir_path( __FILE__ ) . '/forms/forget-password.php'); // Create Forget Password form
include( plugin_dir_path( __FILE__ ) . '/forms/overview-order-form.php'); // Create Overview Order Form
include( plugin_dir_path( __FILE__ ) . '/forms/truck-report.php'); // Create Truck Report Form
include( plugin_dir_path( __FILE__ ) . '/forms/job-completion.php'); // Create Job Completion Form
include( plugin_dir_path( __FILE__ ) . '/xero/auth-form.php'); // Create Xero Auth form
include( plugin_dir_path( __FILE__ ) . '/forms/apilogs.php'); // Create API Logs
include( plugin_dir_path( __FILE__ ) . '/forms/users.php'); // Create users Listing
include( plugin_dir_path( __FILE__ ) . '/forms/new-location.php'); // Create users
include( plugin_dir_path( __FILE__ ) . '/forms/airtalesync.php'); // Create Airtable sync

/*
include( plugin_dir_path( __FILE__ ) . '/airtables/get-companies.php'); // get companies list
include( plugin_dir_path( __FILE__ ) .'/airtables/get-locations.php'); // get locations list
include( plugin_dir_path( __FILE__ ) .'/airtables/get-contacts.php'); // get contacts list
*/

// function register_custom_menu_page() {
//     add_menu_page('custom menu title', 'Api Logs', 'add_users', 'custompage', '_custom_menu_page', null, 6);
// }
// add_action('admin_menu', 'register_custom_menu_page');

function _custom_menu_page(){
   include( plugin_dir_path( __FILE__ ) . '/forms/apilogs.php'); // Api logs
}

?>
