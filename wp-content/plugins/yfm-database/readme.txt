=== YFM Database ===
Contributors: ritika janjua
Tags: airtable, xero
Tested on: 4.9.7
Stable tag: 4.0.8
License: GPLv2 or later

This plugin is used to get the data from Airtable into wp database using Airtable API. It also uses the Xero API. 

== Description ==

This plugin is used to get the data from Airtable into wp database using Airtable API. It also uses the Xero API. 

Major features in YFM Database include:

* Fetch data from Airtable database to forms.
* Shortcode to the Register Form which get data from the Airtable
* Xero API Auth verification

PS: You'll need an [Airtable API key, Xero Consumer Key and Secret Key] to use it. Please ensure you have curl enable on your server.

== Installation ==

Upload the YFM Database plugin to your blog, Activate it, then enter your [Airtable API key, Xero Consumer Key and Secret Key].

