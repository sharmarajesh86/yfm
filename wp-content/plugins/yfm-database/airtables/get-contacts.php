<?php 
// This file is used to fetch Contacts from airtable

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "https://api.airtable.com/v0/appwQYmk5VhwJ4AeM/Contacts");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

$headers = array();
$headers[] = "Authorization: Bearer key9pmq64RydJYxDD";
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}

$result_contacts = curl_exec($ch);

$get_contacts =json_decode($result_contacts);

//print_r($get_contacts->records);
curl_close ($ch);
