<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/**************************************************************
*
*
*********************************************************************/
#require_once($_SERVER['DOCUMENT_ROOT'].'/yfm/wp-config.php');
#require_once($_SERVER['DOCUMENT_ROOT'].'/yfm/wp-load.php');
#require_once($_SERVER['DOCUMENT_ROOT'].'/yfm/wp-includes/wp-db.php');

global $wpdb;
#require_once(__DIR__.'/../wp-admin/includes/user.php' );
?>

 <script src="https://code.jquery.com/jquery-1.12.2.min.js"></script>
        <script src="airtable.browser.js"></script>

<script>
    var Airtable = require('airtable');
    var ajaxurl = document.location.origin+"/yfm/wp-admin/admin-ajax.php";
    var base = new Airtable({ apiKey: 'keyoA5KNgdkmd3iDG' }).base('appwQYmk5VhwJ4AeM');
</script>
<?php
/************** Truncate Companies and Locations Table****************/

$_GET['table'] = 'tasks';

if($_GET['table'] == 'all' )
{
    $link = "<a href='http://docpoke.in/yfm/airtable-sync' > Sync Aritable  </a>";
    echo "This link has been expired. Please click here $link  to sync airtable";
    die('');
}


if($_GET['table'] == 'tasks' )

{    
$date = date('Y/m/d h:i:s', time());;
//echo $date;die;
$servername = "localhost";
$username = "root";
//$password = "admin786";
$dbname = "yfm";


if($_SERVER['HTTP_HOST'] == 'localhost'){
	$password = "admin786";
}else{
	$password = "Server@987";
}
	// echo '<pre>';
	// print_r($_POST);
	//die;

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
		$sql = "INSERT INTO test (date)	VALUES ('$date')";
	if ($conn->query($sql) === TRUE) {
	echo "Inserted successfully";
	} else {
		echo "Error: " . $sql . "<br>" . $conn->error;
	}
	// $arr = array('asdf','t4re','asdf');
	// print_r(json_encode($arr)); die();

   #$tasks_del = $wpdb->query("TRUNCATE TABLE yfm_wp_AT_tasks"); ?>

    <script type="text/javascript">
        /******** Insert Tasks data **********/

    base('Tasks').select({
        sort: [
            {field: 'Job_No', direction: 'asc'},

        ],
        //filterByFormula: 'AND(CREATED_TIME() > "2019-03-14T01:18:59.000Z")'


    }).eachPage(function page(records, fetchNextPage) {
      console.log(fetchNextPage+"   ss ");
        records.forEach(function(record) {
            console.log('Retrieved ', record.getId());
            console.log('offset ', record.get('offset'));
            $.ajax({
                type:"POST",
                url:'insert_tasks.php',
                data:{'Task_id':record.getId(),'Job_No':record.get('Job_No'),'Ordered_by':record.get('Ordered_by'),'Order_Date':record.get('Order_Date'),'Task_Type':record.get('Task_Type'),'Status':record.get('Status'),'Truck_ID1':record.get('Truck_ID1'),'Truck_ID2':record.get('Truck_ID2'),'D_side_Artwork':record.get('D_side_Artwork'),'P_side_Artwork':record.get('P_side_Artwork'),'Rear_Artwork':record.get('Rear_Artwork'),'Panels':record.get('Panels'),'Location':record.get('Location'),'Installer':record.get('Installer'),'Install_Co':record.get('Install_Co'),'Install_Date':record.get('Install_Date'),'Notes_YFM':record.get('Notes(YFM)'),'Images':record.get('Images'),'Client':record.get('Client'),'Notes':record.get('Notes'),'Order_Number':record.get('Order_Number'),'Pan_Flat':record.get('Pan_Flat')},
                dataType:"json",
                success:function(response)
                {   console.log(response);
                    console.log("Tasks inserted");
                },
                error: function(xhr, status, error){
                    console.log("Cannot insert Tasks");

                }
            });

        });

        fetchNextPage();
    }, function done(error) {
        console.log(error);
        setTimeout(function(){
        	jQuery('body').append('<h2>Tasks inserted successfully!!!</h2>');
        }, 3000);

    });
    </script>
<?php }

if($_GET['table'] == 'contacts' )
{

    $contact_del = $wpdb->query("TRUNCATE TABLE yfm_wp_AT_contacts");
    //echo '<div class="process"><span></span></div>'; ?>
    <script type="text/javascript">
        /******** insert contacts **********/
        jQuery('body').append('<h2>Processing...</h2>');
    base('Contacts').select({
        sort: [
            {field: 'ContactName', direction: 'asc'},

        ],

    }).eachPage(function page(records, fetchNextPage) {
        records.forEach(function(record) {
            //console.log('Retrieved ', record.getId());

            $.ajax({
                type:"POST",
                url:'insert_contacts.php',
                data:{'Contact_id':record.getId(),'ContactName':record.get('ContactName'),'Position':record.get('Position'),'Companies':record.get('Company'),'Locations':record.get('Locations'),'Rating':record.get('Rating'),'Installer_info':record.get('Installer_info'),'Notes':record.get('Notes'),'Phone':record.get('Phone'),'Email':record.get('Email'),'Tasks':record.get('Tasks'),'Orders':record.get('Orders')},
                dataType:"json",
                success:function(response)
                {   //console.log(response);
                    //console.log("Contacts inserted");

                },
                error: function(xhr, status, err){
                  console.log(err);
                    // jQuery('body').append('<h2>Contacts not inserted!!!</h2>');
                    // jQuery('body').append('<p>'.err.'</p>');
                    //console.log("Cannot insert contacts");

                }

            });

        });

        fetchNextPage();
    }, function done(error) {
    	setTimeout(function(){
        	jQuery('body').append('<h2>Contacts inserted successfully!!!</h2>');
        }, 3000);

        console.log(error);
    });
    </script>
<?php
}

if($_GET['table'] == 'companies' )
{
    $com_del = $wpdb->query("TRUNCATE TABLE yfm_wp_AT_companies"); ?>
    <script>
        /******* insert  Companies ***********/
    base('Companies').select({
        sort: [
            {field: 'CompanyCode', direction: 'asc'},

        ],

    }).eachPage(function page(records, fetchNextPage) {
        records.forEach(function(record) {
            console.log('Retrieved ', record.get('CompanyCode'));

            $.ajax({
                type:"POST",
                url:'insert_company.php',
                data:{'CompanyCode':record.get('CompanyCode'),'CompanyName':record.get('CompanyName'),'Type':record.get('Type'),'Website':record.get('Website'),'Comments':record.get('Comments'),'Locations':record.get('Locations'),'Contacts':record.get('Contacts'),'Rating':record.get('Rating'),'Web_link_co_code':record.get('Web_link_co_code'),'company_id':record.getId()},
                dataType:"json",
                success:function(response)
                {   console.log(response);
                    console.log("company inserted");
                },
                error: function(xhr, status, error){
                    console.log("Cannot insert company");

                }
            });

        });

        fetchNextPage();
    }, function done(error) {
        console.log(error);
        setTimeout(function(){
        	jQuery('body').append('<h2>Companies inserted successfully!!!</h2>');
        }, 3000);

    });
    </script>
<?php }
if($_GET['table'] == 'locations' )
{
    $loc_del = $wpdb->query("TRUNCATE TABLE yfm_wp_AT_locations"); ?>
    <script type="text/javascript">
        /******** insert Locations ***********/

    base('Locations').select({
        sort: [
            {field: 'ID1', direction: 'asc'},

        ],

    }).eachPage(function page(records, fetchNextPage) {
        records.forEach(function(record) {
            console.log('Retrieved ', record.get('ID1'));

            $.ajax({
                type:"POST",
                url:'insert_locations.php',
                data:{'ID1':record.get('ID1'),'ID2':record.get('ID2'),'Contacts':record.get('Contacts'),'Company':record.get('Company'),'CompanyName':record.get('CompanyName'),'LocationType':record.get('LocationType'),'Street':record.get('Street'),'Suburb':record.get('Suburb'),'State':record.get('State'),'Postcode':record.get('Postcode'),'Fulladdress':record.get('Fulladdress'),'Phone1':record.get('Phone1'),'Phone2':record.get('Phone2'),'Email':record.get('Email'),'Trucks':record.get('Trucks'),'CompanyType':record.get('CompanyType'),'Location_id':record.getId()},
                dataType:"json",
                success:function(response)
                {   console.log(response);
                    console.log("location inserted");
                },
                error: function(xhr, status, error){
                    console.log("Cannot insert location");

                }
            });

        });

        fetchNextPage();
    }, function done(error) {
        console.log(error);
        setTimeout(function(){
        	jQuery('body').append('<h2>Locations inserted successfully!!!</h2>');
        }, 3000);

    });

    </script>

<?php }
if($_GET['table'] == 'trucks' )
{
    $trucks_del = $wpdb->query("TRUNCATE TABLE yfm_wp_AT_trucks"); ?>
    <script type="text/javascript">
         /******** Insert Trucks data **********/

    base('Trucks').select({
        sort: [
            {field: 'ID1', direction: 'asc'},

        ],

    }).eachPage(function page(records, fetchNextPage) {
        records.forEach(function(record) {
            console.log('Retrieved ', record.getId());
            $.ajax({
                type:"POST",
                url:'insert_trucks.php',
                data:{'Truck_id':record.getId(),'ID1':record.get('ID1'),'ID2':record.get('ID2'),'Ops_Status':record.get('Ops_Status'),'Comments':record.get('Comments'),'Info':record.get('Info'),'Lease_End':record.get('Lease_End'),'Truck_Type':record.get('Truck_Type'),'D_Side_Art_code':record.get('D_Side_Art_code'),'P_Side_Art_code':record.get('P_Side_Art_code'),'Rear_Art_code':record.get('Rear_Art_code'),'Location':record.get('Location'),'Client':record.get('Client code'),'Tasks':record.get('Tasks'),'Images':record.get('Images')},
                dataType:"json",
                success:function(response)
                {   console.log(response);
                    console.log("Trucks inserted");
                },
                error: function(xhr, status, error){
                    console.log("Cannot insert trucks");

                }
            });

        });

        fetchNextPage();
    }, function done(error) {
        console.log(error);
        setTimeout(function(){
        	jQuery('body').append('<h2>Trucks inserted successfully!!!</h2>');
        }, 3000);

    });
    </script>

<?php
}


if($_GET['table'] == 'chasis' or $_GET['table']== 'all')
{
    $chassis_del = $wpdb->query("TRUNCATE TABLE yfm_wp_AT_chassis"); ?>
        <script type="text/javascript">
            /******** Insert Chassis data **********/

    base('Chassis').select({
        sort: [
            {field: 'Chassis_code', direction: 'asc'},

        ],

    }).eachPage(function page(records, fetchNextPage) {
        records.forEach(function(record) {
            console.log('Retrieved ', record.getId());
            $.ajax({
                type:"POST",
                url:'insert_chassis.php',
                data:{'Chassis_id':record.getId(),'Chassis_code':record.get('Chassis_code'),'Make':record.get('Make'),'Model':record.get('Model'),'Description':record.get('Description'),'Truck_Types':record.get('Truck_Types')},
                dataType:"json",
                success:function(response)
                {   console.log(response);
                    console.log("Chassis inserted");
                },
                error: function(xhr, status, error){
                    console.log("Cannot insert Chassis");

                }
            });

        });

        fetchNextPage();
    }, function done(error) {
        console.log(error);
        setTimeout(function(){
        	jQuery('body').append('<h2>Chasiss inserted successfully!!!</h2>');
        }, 3000);

    });
        </script>
<?php }
if($_GET['table'] == 'bodies' )
{
    $bodies_del = $wpdb->query("TRUNCATE TABLE yfm_wp_AT_bodies"); ?>
    <script type="text/javascript">
        /******** Insert Bodies data **********/

    base('Bodies').select({
        sort: [
            {field: 'Body_Code', direction: 'asc'},

        ],

    }).eachPage(function page(records, fetchNextPage) {
        records.forEach(function(record) {
            console.log('Retrieved ', record.getId());
            $.ajax({
                type:"POST",
                url:'insert_bodies.php',
                data:{'Bodies_id':record.getId(),'Body_Code':record.get('Body_Code'),'Manufacturer':record.get('Manufacturer'),'Truck_Types':record.get('Truck_Types'),'BP_Length':record.get('BP_Length'),'BP_Height':record.get('BP_Height'),'BD_Length':record.get('BD_Length'),'BD_Height':record.get('BD_Height'),'BR_Length':record.get('BR_Length'),'BR_Height':record.get('BR_Height')},
                dataType:"json",
                success:function(response)
                {   console.log(response);
                    console.log("Bodies inserted");
                },
                error: function(xhr, status, error){
                    console.log("Cannot insert Bodies");

                }
            });

        });

        fetchNextPage();
    }, function done(error) {
        console.log(error);
        setTimeout(function(){
        	jQuery('body').append('<h2>Bodies inserted successfully!!!</h2>');
        }, 3000);

    });
    </script>
<?php }
if($_GET['table'] == 'side_artwork' )
{
    $side_art_del = $wpdb->query("TRUNCATE TABLE yfm_wp_AT_side_artwork"); ?>
    <script type="text/javascript">
        /******** Insert Side_Artwork data **********/

    base('Side_Artwork').select({
        sort: [
            {field: 'Artwork_Code', direction: 'asc'},

        ],

    }).eachPage(function page(records, fetchNextPage) {
        records.forEach(function(record) {
            console.log('Retrieved ', record.getId());
            $.ajax({
                type:"POST",
                url:'insert_side_artwork.php',
                data:{'Side_Artwork_id':record.getId(),'Artwork_Code':record.get('Artwork_Code'),'Campaign':record.get('Campaign'),'Version':record.get('Version'),'Artwork':record.get('Artwork'),'Artwork_Transition':record.get('Artwork_Transition'),'Date_supplied':record.get('Date_supplied'),'on_Driver_sides_of':record.get('on_Driver_sides_of'),'on_Passenger_sides_of':record.get('on_Passenger_sides_of')},
                dataType:"json",
                success:function(response)
                {   console.log(response);
                    console.log("Side Artwork inserted");
                },
                error: function(xhr, status, error){
                    console.log("Cannot insert Side Artwork");

                }
            });

        });

        fetchNextPage();
    }, function done(error) {
        console.log(error);
        setTimeout(function(){
        	jQuery('body').append('<h2>Side Artwork inserted successfully!!!</h2>');
        }, 3000);

    });
    </script>
<?php }
if($_GET['table'] == 'rear_artwork' )
{
    $rear_art_del = $wpdb->query("TRUNCATE TABLE yfm_wp_AT_rear_artwork"); ?>
    <script type="text/javascript">
        /******** Insert Rear Artwork data **********/

    base('Rear_Artwork').select({
        sort: [
            {field: 'Artwork_code', direction: 'asc'},

        ],

    }).eachPage(function page(records, fetchNextPage) {
        records.forEach(function(record) {
            console.log('Retrieved ', record.getId());
            $.ajax({
                type:"POST",
                url:'insert_rear_artwork.php',
                data:{'Rear_Artwork_id':record.getId(),'Artwork_Code':record.get('Artwork_code'),'Campaign':record.get('Campaign'),'Version':record.get('Version'),'Artwork':record.get('Artwork'),'Artwork_Transition':record.get('Artwork_Transition'),'Date_supplied':record.get('Date_supplied'),'on_Rears_of':record.get('on_Rears_of')},
                dataType:"json",
                success:function(response)
                {   console.log(response);
                    console.log("Rear Artwork inserted");
                },
                error: function(xhr, status, error){
                    console.log("Cannot insert Rear Artwork");

                }
            });

        });

        fetchNextPage();
    }, function done(error) {
        console.log(error);
        setTimeout(function(){
        	jQuery('body').append('<h2>Rear Artwork inserted successfully!!!</h2>');
        }, 3000);

    });
    </script>
<?php }

if($_GET['table'] == 'campaigns' )
{
    $campaings_del = $wpdb->query("TRUNCATE TABLE yfm_wp_AT_campaign"); ?>
    <script type="text/javascript">
        /******** Insert Campaigns data **********/

    base('Campaigns').select({
        sort: [
            {field: 'Campaign_code', direction: 'asc'},

        ],

    }).eachPage(function page(records, fetchNextPage) {
        records.forEach(function(record) {
            console.log('Retrieved ', record.getId());
            $.ajax({
                type:"POST",
                url:'insert_campaigns.php',
                data:{'Campaign_id':record.getId(),'Campaign_code':record.get('Campaign_code'),'Campaign_Name':record.get('Campaign_Name'),'Side_artwork_versions':record.get('Side_artwork_versions'),'Rear_artwork_versions':record.get('Rear_artwork_versions')},
                dataType:"json",
                success:function(response)
                {   console.log(response);
                    console.log("Campaigns inserted");
                },
                error: function(xhr, status, error){
                    console.log("Cannot insert Campaigns");

                }
            });

        });

        fetchNextPage();
    }, function done(error) {
        console.log(error);
        setTimeout(function(){
        	jQuery('body').append('<h2>Campaigns inserted successfully!!!</h2>');
        }, 3000);

    });
    </script>

<?php }

if($_GET['table'] == 'panel_calcs' )
{
    $panel_calcs_del = $wpdb->query("TRUNCATE TABLE yfm_wp_AT_panel_calcs"); ?>
    <script type="text/javascript">
        /******** Insert Panel Calcs data **********/

    base('Panel_Calcs').select({
        sort: [
            {field: 'Profile_Code', direction: 'asc'},

        ],

    }).eachPage(function page(records, fetchNextPage) {
        records.forEach(function(record) {
            console.log('Retrieved ', record.getId());
            $.ajax({
                type:"POST",
                url:'insert_panel_calcs.php',
                data:{'Panel_Calcs_id':record.getId(),'Profile_Code':record.get('Profile_Code'),'Signage':record.get('Signage'),'Part_Name':record.get('Part_Name'),'Calculation':record.get('Calculation')},
                dataType:"json",
                success:function(response)
                {   console.log(response);
                    console.log("Panel Calcs inserted");
                },
                error: function(xhr, status, error){
                    console.log("Cannot insert Panel Calcs");

                }
            });

        });

        fetchNextPage();
    }, function done(error) {
        console.log(error);
        setTimeout(function(){
        	jQuery('body').append('<h2>Panel calcs inserted successfully!!!</h2>');
        }, 3000);

    });
    </script>
<?php }
if($_GET['table'] == 'truck_types' )
{
    $truck_types_del = $wpdb->query("TRUNCATE TABLE yfm_wp_AT_truck_types"); ?>
    <script type="text/javascript">
        /******** Insert Truck Types data **********/

    base('Truck_Types').select({
        sort: [
            {field: 'Truck_Type', direction: 'asc'},

        ],

    }).eachPage(function page(records, fetchNextPage) {
        records.forEach(function(record) {
            console.log('Retrieved ', record.getId());
            $.ajax({
                type:"POST",
                url:'insert_truck_types.php',
                data:{'Truck_Types_id':record.getId(),'Truck_Type':record.get('Truck_Type'),'Chassis':record.get('Chassis'),'Bodies':record.get('Bodies'),'Images':record.get('Images'),'Line_Drawings_with_Specs':record.get('Line_Drawings_with_Specs'),'Line_Drawings_no_specs':record.get('Line_Drawings_no_specs'),'Cabin':record.get('Cabin'),'BP':record.get('BP'),'BD':record.get('BD'),'BR':record.get('BR'),'BF':record.get('BF'),'BI':record.get('BI'),'CF':record.get('CF'),'CD':record.get('CD'),'CP':record.get('CP'),'CI':record.get('CI'),'Tare':record.get('Tare'),'GVM':record.get('GVM'),'Trucks':record.get('Trucks')},
                dataType:"json",
                success:function(response)
                {   console.log(response);
                    console.log("Truck Types inserted");
                },
                error: function(xhr, status, error){
                    console.log("Cannot insert Truck Types");

                }
            });

        });

        fetchNextPage();
    }, function done(error) {
        console.log(error);
        setTimeout(function(){
        	jQuery('body').append('<h2>Truck Type inserted successfully!!!</h2>');
        }, 3000);

    });
    </script>
<?php }
if($_GET['table'] == 'parts_list' )
{
    $parts_list_del = $wpdb->query("TRUNCATE TABLE yfm_wp_AT_parts_list"); ?>
    <script type="text/javascript">
        /******** Insert Parts List data **********/

    base('Parts_List').select({
        sort: [
            {field: 'Part_Code', direction: 'asc'},

        ],
    }).eachPage(function page(records, fetchNextPage) {
        records.forEach(function(record) {
            console.log('Retrieved ', record.get('Part_Code'));
            $.ajax({
                type:"POST",
                url:'insert_parts_list.php',
                data:{'Parts_list_id':record.getId(),'Parts':record.get('Part_Code'),'Part_Type':record.get('Part_Type'),'Specific_to':record.get('Specific_to'),'Item_Name':record.get('Item_Name'),'Chassis':record.get('Chassis'),'Body_Type':record.get('Body_Type'),'Signage_Type':record.get('Signage_Type'),'Client':record.get('Client'),'Panel':record.get('Panel'),'Warehouse':record.get('Warehouse'),'Artwork':record.get('Artwork'),'Purchase_Description':record.get('Purchase_Description'),'Sales_Description':record.get('Sales_Description'),'CommentTo_Discuss':record.get('CommentTo_Discuss'),'Profile_Mapping':record.get('Profile_Mapping')},
                dataType:"json",
                success:function(response)
                {   console.log(response);
                    console.log("Parts List inserted");
                },
                error: function(xhr, status, error){
                    console.log("Cannot insert Parts List");

                }
            });

        });

        fetchNextPage();
    }, function done(error) {
        console.log(error);
        setTimeout(function(){
        	jQuery('body').append('<h2>Parts List inserted successfully!!!</h2>');
        }, 3000);

    });
    </script>
<?php }?>
