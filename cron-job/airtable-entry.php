<?php ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/**************************************************************
*
*	This file is used to enter the data from Airtable to our local database.
*
*********************************************************************/
require_once($_SERVER['DOCUMENT_ROOT'].'/ymf/wp-config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/ymf/wp-load.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/ymf/wp-includes/wp-db.php');

global $wpdb;

/************** Truncate Companies and Locations Table****************/

/*$com_del = $wpdb->query("TRUNCATE TABLE yfm_wp_AT_companies");
$loc_del = $wpdb->query("TRUNCATE TABLE yfm_wp_AT_locations");
$contact_del = $wpdb->query("TRUNCATE TABLE yfm_wp_AT_contacts");*/
?>
 <script src="https://code.jquery.com/jquery-1.12.2.min.js"></script>
        <script src="airtable.browser.js"></script>
         <body>
        <h1>Artists</h1>
        <div id="artists"></div>

        <hr/>
        <button id='create'>Create</button>
        <div id="created"></div>
    </body>

    <script>
	var Airtable = require('airtable');
	var ajaxurl = document.location.origin+"/ymf/wp-admin/admin-ajax.php";
    var base = new Airtable({ apiKey: 'keyoA5KNgdkmd3iDG' }).base('apprTuWAjlI7FBBPb');
    base('Companies').select({
        sort: [
            {field: 'CompanyCode', direction: 'asc'}
        ]
    }).eachPage(function page(records, fetchNextPage) {
        records.forEach(function(record) {
            console.log('Retrieved ', record.get('CompanyCode'));
             var $artistInfo = $('<div>');
            $artistInfo.append($('<h3>').text(record.get('CompanyCode')));
            $artistInfo.append($('<div>').text(record.get('CompanyName')));
            var x = $('<button>').text('Delete').click(function() {
                deleteArtist(record);
            });
            $artistInfo.append(x)
            $artistInfo.attr('data-record-id', record.getId());

            $('#artists').append($artistInfo);
            /********* insert ************/
			jQuery.ajax({
				type:"POST",
				url:ajaxurl,
				data:{action: 'insert_company','CompanyCode':'"'+record.get('CompanyCode')+'"','CompanyName':'"'+record.get('CompanyName')+'"','Type':'"'+record.get('Type')+'"','Website':'"'+record.get('Website')+'"','Comments':'"'+record.get('Comments')+'"','Locations':'"'+record.get('Locations')+'"','Contacts':'"'+record.get('Contacts')+'"','Rating':'"'+record.get('Rating')+'"','Web_link_co_code':'"'+record.get('Web_link_co_code')+'"'},
				dataType:"json",
				success:function(response)
				{	console.log(response);
					console.log("company inserted");
				},
				error: function(xhr, status, error){	
				    console.log("Cannot insert company");
				  
				}
			});
         
        });

        fetchNextPage();
    }, function done(error) {
        console.log(error);
    });

</script>

<?php 
	function insert_company()
	{	
				
		echo $CompanyCode = $_POST['CompanyName'];
		/*if(!empty($_POST['Type']))
		{
			$Type = $_POST['Type'];
		}else{
			$Type = '';
		}

		if(!empty($_POST['Website']))
		{
			$Website = $_POST['Website'];
		}
		else{
			$Website = '';
		}
		if(!empty($_POST['Comments']))
		{
			$Comments = $_POST['Comments'];
		}
		else{
			$Comments = '';
		}
		
		$Locations = $_POST['Locations'];	
		$Contacts = $_POST['Contacts'];
		if(!empty($_POST['Rating']))
		{
			$Rating = $_POST['Rating'];
		}
		else{
			$Rating = '';
		}
		if(!empty($_POST['Web_link_co_code']))
		{
			$Web_link_co_code = $_POST['Web_link_co_code'];
		}
		else{
			$Web_link_co_code = '';
		}
		
		$company_id =  $data->records[$i]->id;
	
		//error with the query 
		$sql = $wpdb->insert('yfm_wp_AT_companies', array(
	    'CompanyCode' => $CompanyCode,
	    'CompanyName' => $CompanyName,
	    'Type' => $Type, 
	    'Website'=> $Website,
	    'Comments'=>$Comments,
	    'Locations'=>wp_json_encode($Locations),
	    'Contacts'=>wp_json_encode($Contacts),
	    'Rating'=>$Rating,
	    'Web_link_co_code'=>$Web_link_co_code,
	    'company_id'=>$company_id
		));
       
		if($sql) 
		{
			$data['msg']= 'company inserted';
		}
		else
		{
			$data['msg'] 'company not inserted';
		}
    	
    	echo json_encode($data);*/
		
	}
?>

        <?php


/*if($com_del && $loc_del && $contact_del)
{
	************ Insert query in Companies table *********************

for($i=0; $i<count($data->records)-1;$i++) {
				
	$CompanyCode = $data->records[$i]->fields->CompanyCode;
	$CompanyName = $data->records[$i]->fields->CompanyName;
	if(!empty($data->records[$i]->fields->Type))
	{
		$Type = $data->records[$i]->fields->Type;
	}else{
		$Type = '';
	}

	if(!empty($data->records[$i]->fields->Website))
	{
		$Website = $data->records[$i]->fields->Website;
	}
	else{
		$Website = '';
	}
	if(!empty($data->records[$i]->fields->Comments))
	{
		$Comments = $data->records[$i]->fields->Comments;
	}
	else{
		$Comments = '';
	}
	
	$Locations = $data->records[$i]->fields->Locations;	
	$Contacts = $data->records[$i]->fields->Contacts;
	if(!empty($data->records[$i]->fields->Rating))
	{
		$Rating = $data->records[$i]->fields->Rating;
	}
	else{
		$Rating = '';
	}
	if(!empty($data->records[$i]->fields->Web_link_co_code))
	{
		$Web_link_co_code = $data->records[$i]->fields->Web_link_co_code;
	}
	else{
		$Web_link_co_code = '';
	}
	
	$company_id =  $data->records[$i]->id;

	
		//error with the query 
		$sql = $wpdb->insert('yfm_wp_AT_companies', array(
	    'CompanyCode' => $CompanyCode,
	    'CompanyName' => $CompanyName,
	    'Type' => $Type, 
	    'Website'=> $Website,
	    'Comments'=>$Comments,
	    'Locations'=>wp_json_encode($Locations),
	    'Contacts'=>wp_json_encode($Contacts),
	    'Rating'=>$Rating,
	    'Web_link_co_code'=>$Web_link_co_code,
	    'company_id'=>$company_id
		));
       
		if($sql) 
		{
			echo 'company inserted'.$i;
		}
		else
		{
			echo 'company not inserted'.$i;
		}
    
	}	

echo '<br><br><br>';

/************ Insert query in Locations table *********************

			
for($i=0; $i<count($get_locations->records)-1;$i++) {
				
	if(!empty($get_locations->records[$i]->fields->ID1))
	{
		$ID1 = $get_locations->records[$i]->fields->ID1;
	}
	else
	{
		$ID1 = '';
	}
	
	if(!empty($get_locations->records[$i]->fields->ID2))
	{
		$ID2 = $get_locations->records[$i]->fields->ID2;
	}
	else
	{
		$ID2 = '';
	}
	
	if(!empty($get_locations->records[$i]->fields->Contacts))
	{
		$Contacts = $get_locations->records[$i]->fields->Contacts;
	}
	else
	{
		$Contacts = '';
	}
	if(!empty($get_locations->records[$i]->fields->Company))
	{
		$Company = $get_locations->records[$i]->fields->Company;
	}
	else
	{
		$Company = '';
	}
	if(!empty($get_locations->records[$i]->fields->CompanyName))
	{
		$CompanyName = $get_locations->records[$i]->fields->CompanyName;
	}
	else
	{
		$CompanyName = '';
	}
	if(!empty($get_locations->records[$i]->fields->LocationType))
	{
		$LocationType = $get_locations->records[$i]->fields->LocationType;	
	}
	else
	{
		$LocationType = '';	
	}
	
	if(!empty($get_locations->records[$i]->fields->Street))
	{
		$Street = $get_locations->records[$i]->fields->Street;
	}
	else
	{
		$Street = '';
	}

	if(!empty($get_locations->records[$i]->fields->Suburb))
	{
		$Suburb = $get_locations->records[$i]->fields->Suburb;
	}
	else
	{
		$Suburb = '';
	}
	
	if(!empty($get_locations->records[$i]->fields->State))
	{
		$State = $get_locations->records[$i]->fields->State;
	}
	else
	{
		$State = '';
	}
	if(!empty($get_locations->records[$i]->fields->Postcode))
	{
		$Postcode = $get_locations->records[$i]->fields->Postcode;
	}
	else
	{
		$Postcode = '';
	}
	if(!empty($get_locations->records[$i]->fields->Fulladdress))
	{
		$Fulladdress = $get_locations->records[$i]->fields->Fulladdress;
	}
	else
	{
		$Fulladdress = '';
	}
	
	if(!empty($get_locations->records[$i]->fields->Phone1))
	{
		$Phone1 =  $get_locations->records[$i]->fields->Phone1;
	}
	else
	{
		$Phone1 =  '';
	}
	
	if(!empty($get_locations->records[$i]->fields->Phone2))
	{
		$Phone2 =  $get_locations->records[$i]->fields->Phone2;	
	}
	else
	{
		$Phone2 =  '';
	}
	if(!empty($get_locations->records[$i]->fields->Email))
	{
		$Email =  $get_locations->records[$i]->fields->Email;
	}
	else
	{
		$Email =  '';
	}
	
	if(!empty($get_locations->records[$i]->fields->Trucks))
	{
		$Trucks =  $get_locations->records[$i]->fields->Trucks;
	}
	else
	{
		$Trucks =  '';
	}
	
	if(!empty($get_locations->records[$i]->fields->CompanyType))
	{
		$CompanyType =  $get_locations->records[$i]->fields->CompanyType;
	}
	else
	{
		$CompanyType =  '';
	}
	
	$Location_id =  $get_locations->records[$i]->id;

	
	
		//error with the query 
		$loc = $wpdb->insert('yfm_wp_AT_locations', array(
	    'ID1' => $ID1,
	    'ID2' => $ID2,
	    'Contacts' => wp_json_encode($Contacts), 
	    'Company'=> wp_json_encode($Company),
	    'CompanyName'=>wp_json_encode($CompanyName),
	    'LocationType'=>wp_json_encode($LocationType),
	    'Street'=>$Street,
	    'Suburb'=>$Suburb,
	    'State'=>$State,
	    'Postcode'=>$Postcode,
	    'Fulladdress'=>$Fulladdress,
	    'Phone1'=>$Phone1,
	    'Phone2'=>$Phone2,
	    'Email'=>$Email,
	    'Trucks'=>wp_json_encode($Trucks),
	    'CompanyType'=>wp_json_encode($CompanyType),
	    'Location_id'=>$Location_id
		));
      
        if($loc) 
        {
            echo 'Locations inserted'.$i;
		}
		else
		{
			echo 'Locations not inserted'.$i;
		}
	}

	echo '<br><br><br>';

/************ Insert query in Contacts table *********************

			
	for($i=0; $i<count($get_contacts->records)-1;$i++) {
			
		if(!empty($get_contacts->records[$i]->id))
		{
			$Contact_id = $get_contacts->records[$i]->id;
		}
		else
		{
			$Contact_id = '';
		}
		
		if(!empty($get_contacts->records[$i]->fields->ContactName))
		{
			$ContactName = $get_contacts->records[$i]->fields->ContactName;
		}
		else
		{
			$ContactName = '';
		}
		
		if(!empty($get_contacts->records[$i]->fields->Position))
		{
			$Position = $get_contacts->records[$i]->fields->Position;
		}
		else
		{
			$Position = '';
		}
		if(!empty($get_contacts->records[$i]->fields->Companies))
		{
			$Companies = $get_contacts->records[$i]->fields->Companies;
		}
		else
		{
			$Companies = '';
		}
		if(!empty($get_contacts->records[$i]->fields->Locations))
		{
			$Locations = $get_contacts->records[$i]->fields->Locations;
		}
		else
		{
			$Locations = '';
		}
		if(!empty($get_contacts->records[$i]->fields->Rating))
		{
			$Rating = $get_contacts->records[$i]->fields->Rating;	
		}
		else
		{
			$Rating = '';	
		}
		
		if(!empty($get_contacts->records[$i]->fields->Installer_Info))
		{
			$Installer_info = $get_contacts->records[$i]->fields->Installer_Info;
		}
		else
		{
			$Installer_info	 = '';
		}

		if(!empty($get_contacts->records[$i]->fields->Notes))
		{
			$Notes = $get_contacts->records[$i]->fields->Notes;
		}
		else
		{
			$Notes = '';
		}
		
		if(!empty($get_contacts->records[$i]->fields->Phone))
		{
			$Phone = $get_contacts->records[$i]->fields->Phone;
		}
		else
		{
			$Phone = '';
		}
		if(!empty($get_contacts->records[$i]->fields->Email))
		{
			$Email = $get_contacts->records[$i]->fields->Email;
		}
		else
		{
			$Email = '';
		}
		if(!empty($get_contacts->records[$i]->fields->Tasks))
		{
			$Tasks = $get_contacts->records[$i]->fields->Tasks;
		}
		else
		{
			$Tasks = '';
		}
		
		if(!empty($get_contacts->records[$i]->fields->Orders))
		{
			$Orders =  $get_contacts->records[$i]->fields->Orders;
		}
		else
		{
			$Orders =  '';
		}
		
		
			//error with the query 
			$loc = $wpdb->insert('yfm_wp_AT_contacts', array(
		    'Contact_id' => $Contact_id,
		    'ContactName' => $ContactName,
		    'Position' => $Position, 
		    'Companies'=> wp_json_encode($Companies),
		    'Locations'=>wp_json_encode($Locations),
		    'Rating'=>$Rating,
		    'Installer_info'=>wp_json_encode($Installer_info),
		    'Notes'=>$Notes,
		    'Phone'=>$Phone,
		    'Email'=>$Email,
		    'Tasks'=>wp_json_encode($Tasks),
		    'Orders'=>wp_json_encode($Orders)		    
			));
	      
	        if($loc) 
	        {
	            echo 'Contacts inserted'.$i;
			}
			else
			{
				echo 'Contacts not inserted'.$i;
			}
	}

} */
?>
