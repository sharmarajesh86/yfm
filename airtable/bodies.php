<?php
require_once('dbconfig.php');
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
// Simple example to setup and retrieve all data from a table

// If using Composer
require 'vendor/autoload.php';

/* if not using composer, uncomment this
include('../src/Airtable.php');
include('../src/Request.php');
include('../src/Response.php');
*/

use TANIOS\Airtable\Airtable;

$airtable = new Airtable(array(
    'api_key'   => 'keyw3cJfiAFXmYjJf',
    'base'      => 'appwQYmk5VhwJ4AeM',
));

$del = "TRUNCATE TABLE yfm_wp_AT_bodies";
$delresult=mysqli_query($conn,$del);

if($delresult)
{
	$request = $airtable->getContent( 'Bodies' );
}
do {
    $response = $request->getResponse();
    // echo '<pre>';
    // var_dump( $response[ 'records' ] ); die;
		$data = $response['records'];
		$iserror = 0;
		foreach ($response['records'] as $data) {
			$Body_Code = $data->fields->Body_Code;
			if(!empty($data->fields->Manufacturer))
			{
				$Manufacturer = $data->fields->Manufacturer;
			}else{
				$Manufacturer = '';
			}
			if(!empty($data->fields->BP_Length))
			{
				$BP_Length = $data->fields->BP_Length;
			}
			else{
				$BP_Length = '0';
			}
			if(!empty($data->fields->BP_Height))
			{
				$BP_Height = $data->fields->BP_Height;
			}
			else{
				$BP_Height = '0';
			}
			if(!empty($data->fields->BD_Length))
			{
				$BD_Length = $data->fields->BD_Length;
			}
			else{
				$BD_Length = '0';
			}
			if(!empty($data->fields->BD_Height))
			{
				$BD_Height = $data->fields->BD_Height;
			}
			else{
				$BD_Height = '0';
			}
			if(!empty($data->fields->BR_Length))
			{
				$BR_Length = $data->fields->BR_Length;
			}
			else{
				$BR_Length = '0';
			}
			if(!empty($data->fields->BR_Height))
			{
				$BR_Height = $data->fields->BR_Height;
			}
			else{
				$BR_Height = '0';
			}
			if(!empty($data->fields->Truck_Types))
			{
				$Truck_Types = json_encode($data->fields->Truck_Types);
			}
			else{
				$Truck_Types = json_encode('');
			}

			$Bodies_id =  $data->id;

			$sel ="SELECT Bodies_id from yfm_wp_AT_bodies WHERE Bodies_id='$Bodies_id'";
			$result=mysqli_query($conn,$sel);
			if ($result)
			{
				$get = mysqli_num_rows($result);
				//echo '/'.$get.'<br>';

				if($get==1)
				{
					$sql = "UPDATE yfm_wp_AT_bodies SET Bodies_id='$Bodies_id',Body_Code='$Body_Code',Manufacturer='$Manufacturer',BP_Length='$BP_Length',BP_Height='$BP_Height',BD_Length='$BD_Length',BD_Height='$BD_Height',BR_Length='$BR_Length',BR_Height='$BR_Height',Truck_Types='$Truck_Types' WHERE Bodies_id='$Bodies_id'";
					if ($conn->query($sql) === TRUE) {
					echo "Updated successfully<br>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}
				}
				else{
					$sql = "INSERT INTO yfm_wp_AT_bodies (Bodies_id,Body_Code,Manufacturer,BP_Length,BP_Height,BD_Length,BD_Height,BR_Length,BR_Height,Truck_Types)
					VALUES ('$Bodies_id','$Body_Code','$Manufacturer','$BP_Length','$BP_Height','$BD_Length','$BD_Height','$BR_Length','$BR_Height','$Truck_Types')";

					if ($conn->query($sql) === TRUE) {
					    //echo "Bodies inserted successfully<br>";


					} else {
						$iserror=1;
					    echo "Error: " . $sql . "<br>" . $conn->error;
					}
			}
		}
	}
}
while( $request = $response->next() );
if($iserror==0)
{
	echo "Inserted successfully<br>";
}
