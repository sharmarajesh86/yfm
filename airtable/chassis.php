<?php
require_once('dbconfig.php');
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
// Simple example to setup and retrieve all data from a table

// If using Composer
require 'vendor/autoload.php';

/* if not using composer, uncomment this
include('../src/Airtable.php');
include('../src/Request.php');
include('../src/Response.php');
*/

use TANIOS\Airtable\Airtable;

$airtable = new Airtable(array(
    'api_key'   => 'keyw3cJfiAFXmYjJf',
    'base'      => 'appwQYmk5VhwJ4AeM',
));

$del = "TRUNCATE TABLE yfm_wp_AT_chassis";
$delresult=mysqli_query($conn,$del);

if($delresult)
{
	$request = $airtable->getContent( 'Chassis' );
}
do {
    $response = $request->getResponse();
    // echo '<pre>';
    // var_dump( $response[ 'records' ] ); die;
		$data = $response['records'];
		$iserror = 0;
		foreach ($response['records'] as $data) {
			if(!empty($data->fields->Make))
			{
				$Chassis_code = $data->fields->Chassis_code;
			}
			else{
				$Chassis_code ='';
			}
			if(!empty($data->fields->Make))
			{
				$Make = $data->fields->Make;
			}else{
				$Make = '';
			}
			if(!empty($data->fields->Model))
			{
				$Model = $data->fields->Model;
			}
			else{
				$Model = '';
			}
			if(!empty($data->fields->Description))
			{
				$Description = $data->fields->Description;
			}
			else{
				$Description = '';
			}
			if(!empty($data->fields->Truck_Types))
			{
				$Truck_Types = json_encode($data->fields->Truck_Types);
			}
			else{
				$Truck_Types = json_encode('');
			}

			$Chassis_id =  $data->id;

			$sel ="SELECT Chassis_id from yfm_wp_AT_chassis WHERE Chassis_id='$Chassis_id'";
			$result=mysqli_query($conn,$sel);
			if ($result)
			{
				$get = mysqli_num_rows($result);
				//echo '/'.$get.'<br>';

				if($get==1)
				{
					$sql = "UPDATE yfm_wp_AT_chassis SET Chassis_id='$Chassis_id',Chassis_code='$Chassis_code',Make='$Make',Model='$Model',Description='$Description',Truck_Types='$Truck_Types' WHERE Chassis_id='$Chassis_id'";
					if ($conn->query($sql) === TRUE) {
					echo "Updated successfully<br>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}
				}
				else{
					$sql = "INSERT INTO yfm_wp_AT_chassis (Chassis_id,Chassis_code, Make,Model,Description,Truck_Types)
					VALUES ('$Chassis_id','$Chassis_code','$Make','$Model','$Description','$Truck_Types')";

					if ($conn->query($sql) === TRUE) {
					    //echo "Chassis inserted successfully<br>";


					} else {
						$iserror=1;
					    echo "Error: " . $sql . "<br>" . $conn->error;
					}
			}
		}
	}
}
while( $request = $response->next() );
if($iserror==0)
{
	echo "Inserted successfully<br>";
}
