<?php
require_once('dbconfig.php');
// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
// Simple example to setup and retrieve all data from a table

// If using Composer
require 'vendor/autoload.php';

/* if not using composer, uncomment this
include('../src/Airtable.php');
include('../src/Request.php');
include('../src/Response.php');
*/

use TANIOS\Airtable\Airtable;

$airtable = new Airtable(array(
    'api_key'   => 'keyw3cJfiAFXmYjJf',
    'base'      => 'appwQYmk5VhwJ4AeM',
));

$del = "TRUNCATE TABLE yfm_wp_AT_locations";
$delresult=mysqli_query($conn,$del);
if($delresult)
{
	$request = $airtable->getContent( 'Locations' );
}

do {
    $response = $request->getResponse();
    // echo '<pre>';
    // var_dump( $response[ 'records' ] ); die;
		$data = $response['records'];
		$iserror=0;
		foreach ($response['records'] as $data) {
			if(!empty($data->fields->ID1))
				{
					$ID1 = $data->fields->ID1;
				}
				else
				{
					$ID1 = '';
				}

				if(!empty($data->fields->ID2))
				{
					$ID2 = $data->fields->ID2;
				}
				else
				{
					$ID2 = '';
				}

				if(!empty($data->fields->Contacts))
				{
					$Contacts = json_encode($data->fields->Contacts);
				}
				else
				{
					$Contacts = json_encode('');
				}
				if(!empty($data->fields->Company))
				{
					$Company = json_encode($data->fields->Company);
				}
				else
				{
					$Company = json_encode('');
				}
				if(!empty($data->fields->CompanyName))
				{
					$CompanyName = json_encode(mysqli_real_escape_string($conn,str_replace("'", '`', $data->fields->CompanyName[0])));
				}
				else
				{
					$CompanyName = json_encode('');
				}
				if(!empty($data->fields->LocationType))
				{
					$LocationType = json_encode($data->fields->LocationType);
				}
				else
				{
					$LocationType = json_encode('');
				}

				if(!empty($data->fields->Street))
				{
					$Street = str_replace("'", '`', $data->fields->Street);
				}
				else
				{
					$Street = '';
				}

				if(!empty($data->fields->Suburb))
				{
					$Suburb = str_replace("'", '`', $data->fields->Suburb);
				}
				else
				{
					$Suburb = '';
				}

				if(!empty($data->fields->State))
				{
					$State = str_replace("'", '`', $data->fields->State);
				}
				else
				{
					$State = '';
				}
				if(!empty($data->fields->Postcode))
				{
					$Postcode = $data->fields->Postcode;
				}
				else
				{
					$Postcode = '';
				}
				if(!empty($data->fields->Fulladdress))
				{
					$Fulladdress = str_replace("'", '`', $data->fields->Fulladdress);
				}
				else
				{
					$Fulladdress = '';
				}

				if(!empty($data->fields->Phone1))
				{
					$Phone1 =  $data->fields->Phone1;
				}
				else
				{
					$Phone1 =  '';
				}

				if(!empty($data->fields->Phone2))
				{
					$Phone2 =  $data->fields->Phone2;
				}
				else
				{
					$Phone2 =  '';
				}
				if(!empty($data->fields->Email))
				{
					$Email =  $data->fields->Email;
				}
				else
				{
					$Email =  '';
				}

				if(!empty($data->fields->Trucks))
				{
					$Trucks =  json_encode($data->fields->Trucks);
				}
				else
				{
					$Trucks =  json_encode('');
				}

				if(!empty($data->fields->CompanyType))
				{
					$CompanyType =  json_encode($data->fields->CompanyType);
				}
				else
				{
					$CompanyType =  json_encode('');
				}

				$Location_id =  $data->id;

			$sel ="SELECT Location_id from yfm_wp_AT_locations WHERE Location_id='$Location_id'";
			$result=mysqli_query($conn,$sel);


			if ($result)
			{
				$get = mysqli_num_rows($result);
				//echo '/'.$get.'<br>';

				if($get==1)
				{
					$sql = "UPDATE yfm_wp_AT_locations SET ID1='$ID1',ID2='$ID2',Contacts='$Contacts',Company='$Company',CompanyName='$CompanyName',LocationType='$LocationType',Street='$Street',Suburb='$Suburb',State='$State',Postcode='$Postcode',Fulladdress='$Fulladdress',Phone1='$Phone1',Phone2='$Phone2',Email='$Email',Trucks='$Trucks',CompanyType='$CompanyType',Location_id='$Location_id' WHERE Location_id='$Location_id'";
					if ($conn->query($sql) === TRUE) {
					echo "Updated successfully<br>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}
				}
				else{
					$loc = "INSERT INTO yfm_wp_AT_locations (ID1,ID2, Contacts,Company,CompanyName,LocationType,Street,Suburb,State,Postcode,Fulladdress,Phone1,Phone2,Email,Trucks,CompanyType,Location_id)
				VALUES ('$ID1','$ID2','$Contacts','$Company','$CompanyName','$LocationType','$Street','$Suburb','$State','$Postcode','$Fulladdress','$Phone1','$Phone2','$Email','$Trucks','$CompanyType','$Location_id')";

				if ($conn->query($loc) === TRUE) {
				    //echo "Inserted successfully<br>";

				} else {
					$iserror=1;
				    echo "Error: " . $loc . "<br>" . $conn->error;
				}
			}
		}
	}
}
while( $request = $response->next() );
if($iserror==0)
{
	echo "Inserted successfully<br>";
}
