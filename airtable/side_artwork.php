<?php
require_once('dbconfig.php');
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
// Simple example to setup and retrieve all data from a table

// If using Composer
require 'vendor/autoload.php';

/* if not using composer, uncomment this
include('../src/Airtable.php');
include('../src/Request.php');
include('../src/Response.php');
*/

use TANIOS\Airtable\Airtable;

$airtable = new Airtable(array(
    'api_key'   => 'keyw3cJfiAFXmYjJf',
    'base'      => 'appwQYmk5VhwJ4AeM',
));
$del = "TRUNCATE TABLE yfm_wp_AT_side_artwork";
$delresult=mysqli_query($conn,$del);

if($delresult)
{
	$request = $airtable->getContent( 'Side_Artwork' );
}
do {
    $response = $request->getResponse();
    // echo '<pre>';
    // var_dump( $response[ 'records' ] ); die;
		$data = $response['records'];
		$iserror=0;
		foreach ($response['records'] as $data) {
			$Artwork_Code = $data->fields->Artwork_Code;
			if(!empty($data->fields->Campaign))
			{
				$Campaign = json_encode($data->fields->Campaign);
			}else{
				$Campaign = json_encode('');
			}
			if(!empty($data->fields->Version))
			{
				$Version = $data->fields->Version;
			}
			else{
				$Version = '0';
			}
			if(!empty($data->fields->Artwork))
			{
				$Artwork = json_encode($data->fields->Artwork);
			}
			else{
				$Artwork = json_encode('');
			}
			if(!empty($data->fields->Artwork_Transition))
			{
				$Artwork_Transition = json_encode($data->fields->Artwork_Transition);
			}
			else{
				$Artwork_Transition = json_encode('');
			}
			if(!empty($data->fields->Date_supplied))
			{
				$Date_supplied = $data->fields->Date_supplied;
			}
			else{
				$Date_supplied = '';
			}
			if(!empty($data->fields->on_Driver_sides_of))
			{
				$on_Driver_sides_of = json_encode($data->fields->on_Driver_sides_of);
			}
			else{
				$on_Driver_sides_of = json_encode('');
			}
			if(!empty($data->fields->on_Passenger_sides_of))
			{
				$on_Passenger_sides_of = json_encode($data->fields->on_Passenger_sides_of);
			}
			else{
				$on_Passenger_sides_of = json_encode('');
			}

			$Side_Artwork_id =  $data->id;

			$sel ="SELECT Side_Artwork_id from yfm_wp_AT_side_artwork WHERE Side_Artwork_id='$Side_Artwork_id'";
			$result=mysqli_query($conn,$sel);
			if ($result)
			{
				$get = mysqli_num_rows($result);
				//echo '/'.$get.'<br>';

				if($get==1)
				{
					$sql = "UPDATE yfm_wp_AT_side_artwork SET Side_Artwork_id='$Side_Artwork_id',Artwork_Code='$Artwork_Code',Campaign='$Campaign',Version='$Version',Artwork='$Artwork',Artwork_Transition='$Artwork_Transition',Date_supplied='$Date_supplied',on_Driver_sides_of='$on_Driver_sides_of',on_Passenger_sides_of='$on_Passenger_sides_of' WHERE Side_Artwork_id='$Side_Artwork_id'";
					if ($conn->query($sql) === TRUE) {
					echo "Updated successfully<br>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}
				}
				else{
					$sql = "INSERT INTO yfm_wp_AT_side_artwork (Side_Artwork_id,Artwork_Code,Campaign,Version,Artwork,Artwork_Transition,Date_supplied,on_Driver_sides_of,on_Passenger_sides_of)
					VALUES ('$Side_Artwork_id','$Artwork_Code','$Campaign','$Version','$Artwork','$Artwork_Transition','$Date_supplied','$on_Driver_sides_of','$on_Passenger_sides_of')";

					if ($conn->query($sql) === TRUE) {
					    //echo "Side Artwork inserted successfully<br>";


					} else {
						$iserror=1;
					    echo "Error: " . $sql . "<br>" . $conn->error;
					}
			}
		}
	}
}
while( $request = $response->next() );
if($iserror==0)
{
	echo "Inserted successfully<br>";
}
