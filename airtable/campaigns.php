<?php
require_once('dbconfig.php');
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
// Simple example to setup and retrieve all data from a table

// If using Composer
require 'vendor/autoload.php';

/* if not using composer, uncomment this
include('../src/Airtable.php');
include('../src/Request.php');
include('../src/Response.php');
*/

use TANIOS\Airtable\Airtable;

$airtable = new Airtable(array(
    'api_key'   => 'keyw3cJfiAFXmYjJf',
    'base'      => 'appwQYmk5VhwJ4AeM',
));

$del = "TRUNCATE TABLE yfm_wp_AT_campaign";
$delresult=mysqli_query($conn,$del);

if($delresult)
{
	$request = $airtable->getContent( 'Campaigns' );
}
do {
    $response = $request->getResponse();
    // echo '<pre>';
    // var_dump( $response[ 'records' ] ); die;
		$data = $response['records'];
		$iserror = 0;
		foreach ($response['records'] as $data) {
			$Campaign_code = $data->fields->Campaign_code;
			$Campaign_id =  $data->id;

			if(!empty($data->fields->Campaign_Name))
			{
				$Campaign_Name = str_replace("'", '`', $data->fields->Campaign_Name);
			}else{
				$Campaign_Name = '';
			}

			if(!empty($data->fields->Side_artwork_versions))
			{
				$Side_artwork_versions = json_encode($data->fields->Side_artwork_versions);
			}
			else{
				$Side_artwork_versions = json_encode('');
			}

			if(!empty($data->fields->Rear_artwork_versions))
			{
				$Rear_artwork_versions = json_encode($data->fields->Rear_artwork_versions);
			}
			else{
				$Rear_artwork_versions = json_encode('');
			}

			$sel ="SELECT Campaign_id from yfm_wp_AT_campaign WHERE Campaign_id='$Campaign_id'";
			$result=mysqli_query($conn,$sel);
			if ($result)
			{
				$get = mysqli_num_rows($result);
				//echo '/'.$get.'<br>';

				if($get==1)
				{
					$sql = "UPDATE yfm_wp_AT_campaign SET Campaign_id='$Campaign_id',Campaign_code='$Campaign_code',Campaign_Name='$Campaign_Name',Side_artwork_versions='$Side_artwork_versions',Rear_artwork_versions='$Rear_artwork_versions' WHERE Campaign_id='$Campaign_id'";
					if ($conn->query($sql) === TRUE) {
					echo "Updated successfully<br>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}
				}
				else{
					$sql = "INSERT INTO yfm_wp_AT_campaign (Campaign_id, Campaign_code,Campaign_Name,Side_artwork_versions,Rear_artwork_versions)
					VALUES ('$Campaign_id','$Campaign_code','$Campaign_Name','$Side_artwork_versions','$Rear_artwork_versions')";

					if ($conn->query($sql) === TRUE) {
					    //echo "Campaigns inserted successfully<br>";


					} else {
						$iserror=1;
					    echo "Error: " . $sql . "<br>" . $conn->error;
					}
			}
		}
	}
}
while( $request = $response->next() );
if($iserror==0)
{
	echo "Inserted successfully<br>";
}
