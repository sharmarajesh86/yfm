<?php
require_once('dbconfig.php');
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
// Simple example to setup and retrieve all data from a table

// If using Composer
require 'vendor/autoload.php';

/* if not using composer, uncomment this
include('../src/Airtable.php');
include('../src/Request.php');
include('../src/Response.php');
*/

use TANIOS\Airtable\Airtable;

$airtable = new Airtable(array(
    'api_key'   => 'keyw3cJfiAFXmYjJf',
    'base'      => 'appwQYmk5VhwJ4AeM',
));

$del = "TRUNCATE TABLE yfm_wp_AT_tasks";
$delresult=mysqli_query($conn,$del);

if($delresult)
{
	$request = $airtable->getContent( 'Tasks' );

}

do {

    $response = $request->getResponse();
    // echo '<pre>';
    // var_dump( $response[ 'records' ] );
		$data = $response['records'];
		$iserror=0;
		foreach ($response['records'] as $data) {
			//echo '<br>';
			 $Job_No = $data->fields->Job_No;

			if(!empty($data->fields->Ordered_by))
			{
				$Ordered_by = json_encode($data->fields->Ordered_by);
			}
			else{
				$Ordered_by = json_encode('');
			}
			if(!empty($data->fields->Order_Date))
			{
				$Order_Date = $data->fields->Order_Date;
			}else{
				$Order_Date = '';
			}
			if(!empty($data->fields->Task_Type))
			{
				$Task_Type = $data->fields->Task_Type;
			}
			else{
				$Task_Type = '';
			}

			if(!empty($data->fields->Status))
			{
				$Status = $data->fields->Status[0];
			}
			else{
				$Status = '';
			}
			if(!empty($data->fields->Truck_ID1))
			{
				$Truck_ID1 = json_encode($data->fields->Truck_ID1);
				$Truck_id  = $data->fields->Truck_ID1[0];
			}
			else{
				$Truck_ID1 = json_encode('');
			}
			if(!empty($data->fields->Truck_ID2))
			{
				$Truck_ID2 = json_encode($data->fields->Truck_ID2);
			}
			else{
				$Truck_ID2 = json_encode('');
			}
			if(!empty($data->fields->D_side_Artwork))
			{
				$D_side_Artwork = json_encode($data->fields->D_side_Artwork);
			}
			else{
				$D_side_Artwork = json_encode('');
			}
			if(!empty($data->fields->P_side_Artwork))
			{
				$P_side_Artwork = json_encode($data->fields->P_side_Artwork);
			}
			else{
				$P_side_Artwork = json_encode('');
			}
			if(!empty($data->fields->Rear_Artwork))
			{
				$Rear_Artwork = json_encode($data->fields->Rear_Artwork);
			}
			else{
				$Rear_Artwork = json_encode('');
			}
			if(!empty($data->fields->Panels))
			{
				$Panels = json_encode($data->fields->Panels);
			}
			else{
				$Panels = json_encode('');
			}
			if(!empty($data->fields->Location))
			{
				$Location = json_encode($data->fields->Location);
			}
			else{
				$Location = json_encode('');
			}
			if(!empty($data->fields->Installer))
			{
				$Installer = json_encode($data->fields->Installer);
			}
			else{
				$Installer = json_encode('');
			}
			if(!empty($data->fields->Install_Co))
			{
				$Install_Co = json_encode(str_replace("'", '`', $data->fields->Install_Co));
				$Install_Co_id  = str_replace("'", '`', $data->fields->Install_Co[0]);
			}
			else{
				$Install_Co = json_encode('');
			}
			if(!empty($data->fields->Install_Date))
			{
				$Install_Date = $data->fields->Install_Date;
			}
			else{
				$Install_Date = '';
			}
			if(!empty($data->fields->Notes_YFM))
			{
				$Notes = $data->fields->Notes_YFM;
			}
			else{
				$Notes = '';
			}
			if(!empty($data->fields->Images))
			{
				$Images = json_encode($data->fields->Images);
			}
			else{
				$Images = json_encode('');
			}
			if(!empty($data->fields->Client))
			{
				$Client = json_encode($data->fields->Client);
			}
			else{
				$Client = json_encode('');
			}
			if(!empty($data->fields->Notes))
			{
				$Notes2 = $data->fields->Notes;
			}
			else{
				$Notes2 = '';
			}
			if(!empty($data->fields->Order_Number))
			{
				$Order_Number = json_encode($data->fields->Order_Number);
			}
			else{
				$Order_Number = json_encode('');
			}
			if(!empty($data->fields->Pan_Flat))
			{
				$Pan_Flat = json_encode($data->fields->Pan_Flat);
			}
			else{
				$Pan_Flat = json_encode('');
			}

			$Task_id =  $data->id;


				$sel ="SELECT Task_id from yfm_wp_AT_tasks WHERE Task_id='$Task_id'";
				$result=mysqli_query($conn,$sel);
				if ($result)
				{
					$get = mysqli_num_rows($result);
				//	echo '/'.$get.'<br>$iserror';

					if($get==1)
					{
						$sql = "UPDATE yfm_wp_AT_tasks SET Task_id='$Task_id',Job_No='$Job_No', Ordered_by='$Ordered_by',Order_Date='$Order_Date',Task_Type='$Task_Type',Status='$Status',Truck_ID1='$Truck_ID1',Truck_ID2='$Truck_ID2',D_side_Artwork='$D_side_Artwork',P_side_Artwork='$P_side_Artwork',Rear_Artwork='$Rear_Artwork',Panels='$Panels',Location='$Location',Installer='$Installer',Install_Co='$Install_Co',Install_Date='$Install_Date',Notes_YFM='$Notes',Client='$Client',Images='$Images',Notes='$Notes2',Order_Number='$Order_Number',Pan_Flat='$Pan_Flat',Truck_id='$Truck_id',Install_Co_id='$Install_Co_id' WHERE Task_id='$Task_id'";
						if ($conn->query($sql) === TRUE) {
						echo "Tasks updated successfully<br>";
						} else {
							echo "Error: " . $sql . "<br>" . $conn->error;
						}
					}
					else{
						$sql = "INSERT INTO yfm_wp_AT_tasks (Task_id,Job_No, Ordered_by,Order_Date,Task_Type,Status,Truck_ID1,Truck_ID2,D_side_Artwork,P_side_Artwork,Rear_Artwork,Panels,Location,Installer,Install_Co,Install_Date,Notes_YFM,Client,Images,Notes,Order_Number,Pan_Flat,Truck_id,Install_Co_id)
						VALUES ('$Task_id','$Job_No','$Ordered_by','$Order_Date','$Task_Type','$Status','$Truck_ID1','$Truck_ID2','$D_side_Artwork','$P_side_Artwork','$Rear_Artwork','$Panels','$Location','$Installer','$Install_Co','$Install_Date','$Notes','$Client','$Images','$Notes2','$Order_Number','$Pan_Flat','$Truck_id','$Install_Co_id')";
						if ($conn->query($sql) === TRUE) {
						//echo "Tasks inserted successfully<br>";
						} else {
							$iserror=1;
							echo "<br>Error: " . $sql . "<br>" . $conn->error;
						}
					}
				}



		}

}
while( $request = $response->next() );
if($iserror==0)
{
	echo "Inserted successfully<br>";
}
