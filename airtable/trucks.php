<?php
require_once('dbconfig.php');
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
// Simple example to setup and retrieve all data from a table

// If using Composer
require 'vendor/autoload.php';

/* if not using composer, uncomment this
include('../src/Airtable.php');
include('../src/Request.php');
include('../src/Response.php');
*/

use TANIOS\Airtable\Airtable;

$airtable = new Airtable(array(
    'api_key'   => 'keyw3cJfiAFXmYjJf',
    'base'      => 'appwQYmk5VhwJ4AeM',
));
$del = "TRUNCATE TABLE yfm_wp_AT_trucks";
$delresult=mysqli_query($conn,$del);

if($delresult)
{
	$request = $airtable->getContent( 'Trucks' );
}
do {
    $response = $request->getResponse();
    // echo '<pre>';
    // var_dump( $response[ 'records' ] ); die;
		$data = $response['records'];
		$iserror=0;
		foreach ($response['records'] as $data) {
			if(!empty($data->fields->ID1))
			{
				$ID1 = $data->fields->ID1;
			}
			else{
				$ID1 = '';
			}
			if(!empty($data->fields->ID2))
			{
				$ID2 = $data->fields->ID2;
			}
			else{
				$ID2 = '';
			}
			if(!empty($data->fields->Ops_Status))
			{
				$Ops_Status = $data->fields->Ops_Status;
			}else{
				$Ops_Status = '';
			}
				if(!empty($data->fields->Comments))
			{
				$Comments = $data->fields->Comments;
			}
			else{
				$Comments = '';
			}
			if(!empty($data->fields->Info))
			{
				$Info = $data->fields->Info;
			}
			else{
				$Info = '';
			}
			if(!empty($data->fields->Lease_End))
			{
				$Lease_End = $data->fields->Lease_End;
			}
			else{
				$Lease_End = '';
			}

			if(!empty($data->fields->Truck_Type))
			{
				$Truck_Type = json_encode($data->fields->Truck_Type);
			}
			else{
				$Truck_Type = json_encode('');
			}
		;
			if(!empty($data->fields->D_Side_Art_code))
			{
				$D_Side_Art_code = json_encode($data->fields->D_Side_Art_code);
			}
			else{
				$D_Side_Art_code = json_encode('');
			}
			if(!empty($data->fields->P_Side_Art_code))
			{
				$P_Side_Art_code = json_encode($data->fields->P_Side_Art_code);
			}
			else{
				$P_Side_Art_code = json_encode('');
			}
			if(!empty($data->fields->Rear_Art_code))
			{
				$Rear_Art_code = json_encode($data->fields->Rear_Art_code);
			}
			else{
				$Rear_Art_code = json_encode('');
			}
			if(!empty($data->fields->Location))
			{
				$Location = $data->fields->Location[0];
			}
			else{
				$Location = '';
			}

			if(!empty($data->fields->Client_code))
			{
				$Client_code = $data->fields->Client_code[0];
			}
			else{
				$Client_code = '';
			}
			if(!empty($data->fields->Client_code))
			{
				$Client = json_encode($data->fields->Client);
			}
			else{
				$Client = json_encode('');
			}
			if(!empty($data->fields->Tasks))
			{
				$Tasks = json_encode($data->fields->Tasks);
			}
			else{
				$Tasks = json_encode('');
			}
			if(!empty($data->fields->Images))
			{
				$Images = json_encode($data->fields->Images);
			}
			else{
				$Images = json_encode('');
			}

			$Truck_id =  $data->id;

			$sel ="SELECT Truck_id from yfm_wp_AT_trucks WHERE Truck_id='$Truck_id'";
			$result=mysqli_query($conn,$sel);
			if ($result)
			{
				$get = mysqli_num_rows($result);
				//echo '/'.$get.'<br>';

				if($get==1)
				{
					$sql = "UPDATE yfm_wp_AT_trucks SET Truck_id='$Truck_id',ID1='$ID1',ID2='$ID2',Ops_Status='$Ops_Status',Comments='$Comments',Info='$Info',Lease_End='$Lease_End',Truck_Type='$Truck_Type',D_Side_Art_code='$D_Side_Art_code',P_Side_Art_code='$P_Side_Art_code',Rear_Art_code='$Rear_Art_code',Location='$Location',Client_code='$Client_code',Client='$Client',Tasks='$Tasks',Images='$Images' WHERE Truck_id='$Truck_id'";
					if ($conn->query($sql) === TRUE) {
					echo "Updated successfully<br>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}
				}
				else{
					$sql = "INSERT INTO yfm_wp_AT_trucks (Truck_id,ID1, ID2,Ops_Status,Comments,Info,Lease_End,Truck_Type,D_Side_Art_code,P_Side_Art_code,Rear_Art_code,Location,Client_code,Client,Tasks,Images)
					VALUES ('$Truck_id','$ID1','$ID2','$Ops_Status','$Comments','$Info','$Lease_End','$Truck_Type','$D_Side_Art_code','$P_Side_Art_code','$Rear_Art_code','$Location','$Client_code','$Client','$Tasks','$Images')";

					if ($conn->query($sql) === TRUE) {
					    //echo "Trucks inserted successfully<br>";
					} else {
						$iserror=1;
					    echo "Error: " . $sql . "<br>" . $conn->error;
					}
			}
		}
	}
}
while( $request = $response->next() );
if($iserror==0)
{
	echo "Inserted successfully<br>";
}
