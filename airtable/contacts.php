<?php
require_once('dbconfig.php');
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
// Simple example to setup and retrieve all data from a table

// If using Composer
require 'vendor/autoload.php';

require_once('../wp-blog-header.php');
/* if not using composer, uncomment this
include('../src/Airtable.php');
include('../src/Request.php');
include('../src/Response.php');
*/


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


use TANIOS\Airtable\Airtable;

$airtable = new Airtable(array(
    'api_key'   => 'keyw3cJfiAFXmYjJf',
    'base'      => 'appwQYmk5VhwJ4AeM',
));
 // $del = "TRUNCATE TABLE yfm_wp_AT_contacts";
 // $delresult=mysqli_query($conn,$del);
// if($delresult){
// 	$request = $airtable->getContent( 'Contacts' );
// }

$Crequest = $airtable->getContent( 'Companies' );
$companyIdCode = [];
do{
	$Cresponse = $Crequest->getResponse();
	foreach ($Cresponse['records'] as $cdata) {
		$companyIdCode[$cdata->fields->CompanyCode] = $cdata->id;
	}
}
while( $Crequest = $Cresponse->next() );
/*echo "<pre>";print_r($companyIdCode);
die('working...');*/
$ddARE = [];
$request = $airtable->getContent( 'Contacts' );
do {

    $response = $request->getResponse();
		$data = $response['records'];
		$newarr[] = $data;
		$ddARE[] = $data;
		$_SESSION['total'][] = $response['records'] ;

		$iserror=0;
		foreach ($response['records'] as $data) {
			if(!empty($data->id)){
				$Contact_id = $data->id;
			} else {
				$Contact_id = '';
			}
			if(!empty($data->fields->ContactName)){
				$ContactName = str_replace("'", '`',$data->fields->ContactName);
			} else {
				$ContactName = '';
			}
			if(!empty($data->fields->Position)){
				$Position = $data->fields->Position;
			} else {
				$Position = '';
			}
			$dd = $data->fields;
			$ddcom = (isset($dd->CurrentCompanyId)) ? $dd->CurrentCompanyId : '';
			if($ddcom != ''){
				if(array_key_exists($ddcom, $companyIdCode)){
					if($data->fields->Email){
						$userByEmail = get_user_by( 'email', $data->fields->Email );
					}
					$usrID = (isset($userByEmail->ID)) ? $userByEmail->ID : '';
					$Companies = $companyIdCode[$ddcom];
					if($usrID !=''){
		 				$qry = "UPDATE `wp_users` SET `airtable_company_id` = '$Companies' WHERE `ID` =".$usrID;
		 				if ($conn->query($qry) === TRUE) {
							//echo "Updated successfully<br>";
						} else {
							echo "Error: " . $qry . "<br>" . $conn->error;
						}
		 			}
				}
			}else{
				if(!empty($data->fields->Company)){
					$Companies = json_encode($data->fields->Company);
				}else{
					$Companies = json_encode('');
				}
			}
			if(!empty($data->fields->Locations)){
				$Locations = json_encode($data->fields->Locations);
			}else{
				$Locations = json_encode('');
			}
			if(!empty($data->fields->Rating)){
				$Rating = $data->fields->Rating;
			}else{
				$Rating = '0';
			}
			if(!empty($data->fields->Installer_Info)){
				$Installer_info = json_encode($data->fields->Installer_Info);
			}else{
				$Installer_info	 = json_encode('');
			}
			if(!empty($data->fields->Notes)){
				$Notes = str_replace("'", '`',$data->fields->Notes);
			}else{
				$Notes = '';
			}
			if(!empty($data->fields->Phone)){
				$Phone =$data->fields->Phone;
			}else{
				$Phone = '';
			}
			if(!empty($data->fields->Email)){
				$Email = $data->fields->Email;
			}else{
				$Email = '';
			}
			if(!empty($data->fields->Tasks)){
				$Tasks = json_encode($data->fields->Tasks);
			}else{
				$Tasks = json_encode('');
			}
			if(!empty($data->fields->Orders)){
				$Orders =  json_encode($data->fields->Orders);
			}else{
				$Orders =  json_encode('');
			}

			if(!empty($data->fields->TestField)){ //d/m/y G:i:s:e');
				$LastModify = gmdate("Y-m-d H:i:s", strtotime($data->fields->TestField));
			}else{
				//$LastModify =  gmdate("Y-m-d H:i:s");
				$LastModify =  gmdate("Y-m-d H:i:s");
			}
			$allContacts ="SELECT `Contact_id`,`LastModify` from yfm_wp_AT_contacts WHERE Contact_id='$Contact_id'";
			$allResult=mysqli_query($conn,$allContacts);
			$contactInfo = mysqli_fetch_array($allResult);
			$numRows = mysqli_num_rows($allResult);
			if($numRows){
				if(($LastModify !='') &&  $contactInfo['LastModify'] < $LastModify){
					$newarr[$ContactName] = $LastModify;
					$sel ="SELECT Contact_id from yfm_wp_AT_contacts WHERE Contact_id='$Contact_id' AND `LastModify` < '$LastModify'";
					$result=mysqli_query($conn,$sel);
					if ($result){
						$get = mysqli_num_rows($result);
						if($get==1 ){
							$sql = "UPDATE yfm_wp_AT_contacts SET `Contact_id`='" . $Contact_id ."', ContactName='" . $ContactName ."', `Position`= '" . $Position . "', `Companies`='" . $Companies . "', `Locations`= '" . $Locations . "', `Rating` ='". $Rating . "', `Installer_info` = '" . $Installer_info . "', `Notes` = '" . $Notes . "', `Phone`='" . $Phone . "', `Email`='" . $Email . "', `Tasks`='" . $Tasks . "', `Orders`='". $Orders . "', `LastModify` ='" . $LastModify . "' WHERE `Contact_id`='" . $Contact_id . "'";
							if ($conn->query($sql) === TRUE) {
								//echo "Updated successfully<br>";
							} else {
								echo "Error: " . $sql . "<br>" . $conn->error;
							}
						}
					}
				}
			}else{
				$contacts = "INSERT INTO yfm_wp_AT_contacts (Contact_id,ContactName, Position,Companies,Locations,Rating,Installer_info,Notes,Phone,Email,Tasks,Orders, LastModify)
				 VALUES ('$Contact_id','$ContactName','$Position','$Companies','$Locations','$Rating','$Installer_info','$Notes','$Phone','$Email','$Tasks','$Orders', '$LastModify')";
				if ($conn->query($contacts) === TRUE) {
						//echo "Contacts inserted successfully<br>";
				} else {
					$iserror=1;
						echo "<br>Error: " . $contacts . "<br>" . $conn->error;
				}
			}
		}
	}
	while( $request = $response->next() );
	if($iserror==0){
		echo "<br>Inserted successfully<br>";
		echo "<pre>";
			print_r($ddARE);
		echo "</pre>";
	}
