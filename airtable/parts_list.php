<?php
require_once('dbconfig.php');
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
// Simple example to setup and retrieve all data from a table

// If using Composer
require 'vendor/autoload.php';

/* if not using composer, uncomment this
include('../src/Airtable.php');
include('../src/Request.php');
include('../src/Response.php');
*/

use TANIOS\Airtable\Airtable;

$airtable = new Airtable(array(
    'api_key'   => 'keyw3cJfiAFXmYjJf',
    'base'      => 'appwQYmk5VhwJ4AeM',
));

$del = "TRUNCATE TABLE yfm_wp_AT_parts_list";
$delresult=mysqli_query($conn,$del);

if($delresult)
{
	$request = $airtable->getContent( 'Parts_List' );
}
do {
    $response = $request->getResponse();
    // echo '<pre>';
    // var_dump( $response[ 'records' ] ); die;
		$data = $response['records'];
		$iserror= 0;
		foreach ($response['records'] as $data) {
			$Parts_list_id =  $data->id;
			if(!empty($data->fields->Part_Code))
			{
				$Part_Code = $data->fields->Part_Code;
			}else{
				$Part_Code = '';
			}

			if(!empty($data->fields->Part_Type))
			{
				$Part_Type = $data->fields->Part_Type;
			}else{
				$Part_Type = '';
			}

			if(!empty($data->fields->Specific_to))
			{
				$Specific_to = $data->fields->Specific_to;
			}
			else{
				$Specific_to = '';
			}

			if(!empty($data->fields->Item_Name))
			{
				$Item_Name = str_replace("'", '`', $data->fields->Item_Name);
			}
			else{
				$Item_Name = '';
			}
			if(!empty($data->fields->Chassis))
			{
				$Chassis = $data->fields->Chassis;
			}
			else{
				$Chassis = '';
			}
			if(!empty($data->fields->Body_Type))
			{
				$Body_Type = $data->fields->Body_Type;
			}
			else{
				$Body_Type = '';
			}
			if(!empty($data->fields->Signage_Type))
			{
				$Signage_Type = $data->fields->Signage_Type;
			}
			else{
				$Signage_Type = '';
			}
			if(!empty($data->fields->Client))
			{
				$Client = $data->fields->Client;
			}
			else{
				$Client = '';
			}
			if(!empty($data->fields->Panel))
			{
				$Panel = $data->fields->Panel;
			}
			else{
				$Panel = '';
			}
			if(!empty($data->fields->Warehouse))
			{
				$Warehouse = $data->fields->Warehouse;
			}
			else{
				$Warehouse = '';
			}
			if(!empty($data->fields->Artwork))
			{
				$Artwork = $data->fields->Artwork;
			}
			else{
				$Artwork = '';
			}
			if(!empty($data->fields->Purchase_Description))
			{
				$Purchase_Description = str_replace("'", '`', $data->fields->Purchase_Description);
			}
			else{
				$Purchase_Description = '';
			}
			if(!empty($data->fields->Sales_Description))
			{
				$Sales_Description = str_replace("'", '`', $data->fields->Sales_Description);
			}
			else{
				$Sales_Description = '';
			}
			if(!empty($data->fields->CommentTo_Discuss))
			{
				$CommentTo_Discuss = $data->fields->CommentTo_Discuss;
			}
			else{
				$CommentTo_Discuss = '';
			}
			if(!empty($data->fields->Profile_Mapping))
			{
				$Profile_Mapping = $data->fields->Profile_Mapping;
			}
			else{
				$Profile_Mapping = '';
			}

			$sel ="SELECT Parts_list_id from yfm_wp_AT_parts_list WHERE Parts_list_id='$Parts_list_id'";
			$result=mysqli_query($conn,$sel);
			if ($result)
			{
				$get = mysqli_num_rows($result);
				//echo '/'.$get.'<br>';

				if($get==1)
				{
					$sql = "UPDATE yfm_wp_AT_parts_list SET Parts_list_id='$Parts_list_id',Part_Code='$Part_Code',Part_Type='$Part_Type',Specific_to='$Specific_to',Item_Name='$Item_Name',Chassis='$Chassis',Body_Type='$Body_Type',Signage_Type='$Signage_Type',Client='$Client',Panel='$Panel',Warehouse='$Warehouse',Artwork='$Artwork',Purchase_Description='$Purchase_Description',Sales_Description='$Sales_Description',CommentTo_Discuss='$CommentTo_Discuss',Profile_Mapping='$Profile_Mapping' WHERE Parts_list_id='$Parts_list_id'";
					if ($conn->query($sql) === TRUE) {
					echo "Updated successfully<br>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}
				}
				else{
					$sql = "INSERT INTO yfm_wp_AT_parts_list (Parts_list_id,Part_Code,Part_Type,Specific_to,Item_Name,Chassis,Body_Type,Signage_Type,Client,Panel,Warehouse,Artwork,Purchase_Description,Sales_Description,CommentTo_Discuss,Profile_Mapping)
					VALUES ('$Parts_list_id','$Part_Code','$Part_Type','$Specific_to','$Item_Name','$Chassis','$Body_Type','$Signage_Type','$Client','$Panel','$Warehouse','$Artwork','$Purchase_Description','$Sales_Description','$CommentTo_Discuss','$Profile_Mapping')";

					if ($conn->query($sql) === TRUE) {
					    //echo "Parts List inserted successfully<br>";


					} else {
						$iserror=1;
					    echo "Error: " . $sql . "<br>" . $conn->error;
					}
			}
		}
	}
}
while( $request = $response->next() );

if($iserror==0)
{
	echo "Parts List inserted successfully<br>";
}
