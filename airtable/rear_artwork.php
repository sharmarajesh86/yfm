<?php
require_once('dbconfig.php');
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
// Simple example to setup and retrieve all data from a table

// If using Composer
require 'vendor/autoload.php';

/* if not using composer, uncomment this
include('../src/Airtable.php');
include('../src/Request.php');
include('../src/Response.php');
*/

use TANIOS\Airtable\Airtable;

$airtable = new Airtable(array(
    'api_key'   => 'keyw3cJfiAFXmYjJf',
    'base'      => 'appwQYmk5VhwJ4AeM',
));

$del = "TRUNCATE TABLE yfm_wp_AT_rear_artwork";
$delresult=mysqli_query($conn,$del);

if($delresult)
{
	$request = $airtable->getContent( 'Rear_Artwork' );
}
do {
    $response = $request->getResponse();
    // echo '<pre>';
    // var_dump( $response[ 'records' ] ); die;
		$data = $response['records'];
		$iserror=0;
		foreach ($response['records'] as $data) {
			$Artwork_Code = $data->fields->Artwork_code;
			$Rear_Artwork_id =  $data->id;

			if(!empty($data->fields->Campaign))
			{
				$Campaign = json_encode($data->fields->Campaign);
			}else{
				$Campaign = json_encode('');
			}

			if(!empty($data->fields->Version))
			{
				$Version = $data->fields->Version;
			}
			else{
				$Version = '0';
			}

			if(!empty($data->fields->Artwork))
			{
				$Artwork = json_encode($data->fields->Artwork);
			}
			else{
				$Artwork = json_encode('');
			}

			if(!empty($data->fields->Artwork_Transition))
			{
				$Artwork_Transition = json_encode($data->fields->Artwork_Transition);
			}
			else{
				$Artwork_Transition = json_encode('');
			}

			if(!empty($data->fields->Date_supplied))
			{
				$Date_supplied = $data->fields->Date_supplied;
			}
			else{
				$Date_supplied = '';
			}

			if(!empty($data->fields->on_Rears_of))
			{
				$on_Rears_of = json_encode($data->fields->on_Rears_of);
			}
			else{
				$on_Rears_of = json_encode('');
			}

			$sel ="SELECT Rear_Artwork_id from yfm_wp_AT_rear_artwork WHERE Rear_Artwork_id='$Rear_Artwork_id'";
			$result=mysqli_query($conn,$sel);
			if ($result)
			{
				$get = mysqli_num_rows($result);
				//echo '/'.$get.'<br>';

				if($get==1)
				{
					$sql = "UPDATE yfm_wp_AT_rear_artwork SET Rear_Artwork_id='$Rear_Artwork_id',Artwork_Code='$Artwork_Code',Campaign='$Campaign',Version='$Version',Artwork='$Artwork',Artwork_Transition='$Artwork_Transition',Date_supplied='$Date_supplied',on_Rears_of='$on_Rears_of' WHERE Rear_Artwork_id='$Rear_Artwork_id'";
					if ($conn->query($sql) === TRUE) {
					echo "Updated successfully<br>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}
				}
				else{
					$sql = "INSERT INTO yfm_wp_AT_rear_artwork (Rear_Artwork_id, Artwork_Code,Campaign,Version,Artwork,Artwork_Transition,Date_supplied,on_Rears_of)
					VALUES ('$Rear_Artwork_id','$Artwork_Code','$Campaign','$Version','$Artwork','$Artwork_Transition','$Date_supplied','$on_Rears_of')";

					if ($conn->query($sql) === TRUE) {
					    //echo "Rear Artwork inserted successfully<br>";


					} else {
						$iserror=1;
					    echo "Error: " . $sql . "<br>" . $conn->error;
					}
			}
		}
	}
}
while( $request = $response->next() );
if($iserror==0)
{
	echo "Inserted successfully<br>";
}
