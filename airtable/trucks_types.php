<?php
require_once('dbconfig.php');
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
// Simple example to setup and retrieve all data from a table

// If using Composer
require 'vendor/autoload.php';

/* if not using composer, uncomment this
include('../src/Airtable.php');
include('../src/Request.php');
include('../src/Response.php');
*/

use TANIOS\Airtable\Airtable;

$airtable = new Airtable(array(
    'api_key'   => 'keyw3cJfiAFXmYjJf',
    'base'      => 'appwQYmk5VhwJ4AeM',
));

$del = "TRUNCATE TABLE yfm_wp_AT_truck_types";
$delresult=mysqli_query($conn,$del);

if($delresult)
{
	$request = $airtable->getContent( 'Truck_Types' );
}
do {
    $response = $request->getResponse();
    // echo '<pre>';
    // var_dump( $response[ 'records' ] ); die;
		$data = $response['records'];
		$iserror=0;
		foreach ($response['records'] as $data) {
			$Truck_Type = $data->fields->Truck_Type;
			$Truck_Types_id =  $data->id;

			if(!empty($data->fields->Chassis))
			{
				$Chassis = json_encode($data->fields->Chassis);
			}else{
				$Chassis = json_encode('');
			}

			if(!empty($data->fields->Bodies))
			{
				$Bodies = json_encode($data->fields->Bodies);
			}
			else{
				$Bodies = json_encode('');
			}

			if(!empty($data->fields->Images))
			{
				$Images = json_encode($data->fields->Images);
			}
			else{
				$Images = json_encode('');
			}
			if(!empty($data->fields->Line_Drawings_with_Specs))
			{
				$Line_Drawings_with_Specs = json_encode($data->fields->Line_Drawings_with_Specs);
			}
			else{
				$Line_Drawings_with_Specs = json_encode('');
			}
			if(!empty($data->fields->Line_Drawings_no_specs))
			{
				$Line_Drawings_no_specs = json_encode($data->fields->Line_Drawings_no_specs);
			}
			else{
				$Line_Drawings_no_specs = json_encode('');
			}
			if(!empty($data->fields->Cabin))
			{
				$Cabin = $data->fields->Cabin;
			}
			else{
				$Cabin = '';
			}
			if(!empty($data->fields->BP))
			{
				$BP = $data->fields->BP;
			}
			else{
				$BP = '';
			}
			if(!empty($data->fields->BD))
			{
				$BD = $data->fields->BD;
			}
			else{
				$BD = '';
			}
			if(!empty($data->fields->BR))
			{
				$BR = $data->fields->BR;
			}
			else{
				$BR = '';
			}
			if(!empty($data->fields->BF))
			{
				$BF = $data->fields->BF;
			}
			else{
				$BF = '';
			}
			if(!empty($data->fields->BI))
			{
				$BI = $data->fields->BI;
			}
			else{
				$BI = '';
			}
			if(!empty($data->fields->CF))
			{
				$CF = $data->fields->CF;
			}
			else{
				$CF = '';
			}
			if(!empty($data->fields->CD))
			{
				$CD = $data->fields->CD;
			}
			else{
				$CD = '';
			}
			if(!empty($data->fields->CP))
			{
				$CP = $data->fields->CP;
			}
			else{
				$CP = '';
			}
			if(!empty($data->fields->CI))
			{
				$CI = $data->fields->CI;
			}
			else{
				$CI = '';
			}
			if(!empty($data->fields->Tare))
			{
				$Tare = $data->fields->Tare;
			}
			else{
				$Tare = '';
			}
			if(!empty($data->fields->GVM))
			{
				$GVM = $data->fields->GVM;
			}
			else{
				$GVM = '';
			}
			if(!empty($data->fields->Trucks))
			{
				$Trucks = json_encode($data->fields->Trucks);
			}
			else{
				$Trucks = json_encode('');
			}

			$sel ="SELECT Truck_Types_id from yfm_wp_AT_truck_types WHERE Truck_Types_id='$Truck_Types_id'";
			$result=mysqli_query($conn,$sel);
			if ($result)
			{
				$get = mysqli_num_rows($result);
				//echo '/'.$get.'<br>';

				if($get==1)
				{
					$sql = "UPDATE yfm_wp_AT_truck_types SET Truck_Types_id='$Truck_Types_id',Truck_Type='$Truck_Type',Chassis='$Chassis',Bodies='$Bodies',Images='$Images',Line_Drawings_with_Specs='$Line_Drawings_with_Specs',Line_Drawings_no_specs='$Line_Drawings_no_specs',Cabin='$Cabin',BP='$BP',BD='$BD',BR='$BR',BF='$BF',BI='$BI',CF='$CF',CD='$CD',CP='$CP',CI='$CI',Tare='$Tare',GVM='$GVM',Trucks='$Trucks' WHERE Truck_Types_id='$Truck_Types_id'";
					if ($conn->query($sql) === TRUE) {
					echo "Updated successfully<br>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}
				}
				else{
					$sql = "INSERT INTO yfm_wp_AT_truck_types (Truck_Types_id, Truck_Type,Chassis,Bodies,Images,Line_Drawings_with_Specs,Line_Drawings_no_specs,Cabin,BP,BD,BR,BF,BI,CF,CD,CP,CI,Tare,GVM,Trucks)
					VALUES ('$Truck_Types_id','$Truck_Type','$Chassis','$Bodies','$Images','$Line_Drawings_with_Specs','$Line_Drawings_no_specs','$Cabin','$BP','$BD','$BR','$BF','$BI','$CF','$CD','$CP','$CI','$Tare','$GVM','$Trucks')";

					if ($conn->query($sql) === TRUE) {
					    //echo "Truck Types inserted successfully<br>";


					} else {
						$iserror=1;
					    echo "Error: " . $sql . "<br>" . $conn->error;
					}
			}
		}
	}
}
while( $request = $response->next() );
if($iserror==0)
{
	echo "Inserted successfully<br>";
}
