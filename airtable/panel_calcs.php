<?php
require_once('dbconfig.php');
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
// Simple example to setup and retrieve all data from a table

// If using Composer
require 'vendor/autoload.php';

/* if not using composer, uncomment this
include('../src/Airtable.php');
include('../src/Request.php');
include('../src/Response.php');
*/

use TANIOS\Airtable\Airtable;

$airtable = new Airtable(array(
    'api_key'   => 'keyw3cJfiAFXmYjJf',
    'base'      => 'appwQYmk5VhwJ4AeM',
));

$del = "TRUNCATE TABLE yfm_wp_AT_panel_calcs";
$delresult=mysqli_query($conn,$del);

if($delresult)
{
	$request = $airtable->getContent( 'Panel_Calcs' );
}
do {
    $response = $request->getResponse();
    // echo '<pre>';
    // var_dump( $response[ 'records' ] ); die;
		$data = $response['records'];
		$iserror=0;
		foreach ($response['records'] as $data) {
			$Profile_Code = $data->fields->Profile_Code;
			$Panel_Calcs_id =  $data->id;

			if(!empty($data->fields->Signage))
			{
				$Signage = $data->fields->Signage;
			}else{
				$Signage = '';
			}

			if(!empty($data->fields->Part_Name))
			{
				$Part_Name = $data->fields->Part_Name;
			}
			else{
				$Part_Name = '';
			}

			if(!empty($data->fields->Calculation))
			{
				$Calculation = $data->fields->Calculation;
			}
			else{
				$Calculation = '';
			}

			$sel ="SELECT Panel_Calcs_id from yfm_wp_AT_panel_calcs WHERE Panel_Calcs_id='$Panel_Calcs_id'";
			$result=mysqli_query($conn,$sel);
			if ($result)
			{
				$get = mysqli_num_rows($result);
				//echo '/'.$get.'<br>';

				if($get==1)
				{
					$sql = "UPDATE yfm_wp_AT_panel_calcs SET Panel_Calcs_id='$Panel_Calcs_id',Profile_Code='$Profile_Code',Signage='$Signage',Part_Name='$Part_Name',Calculation='$Calculation' WHERE Panel_Calcs_id='$Panel_Calcs_id'";
					if ($conn->query($sql) === TRUE) {
					echo "Updated successfully<br>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}
				}
				else{
					$sql = "INSERT INTO yfm_wp_AT_panel_calcs (Panel_Calcs_id, Profile_Code,Signage,Part_Name,Calculation)
						VALUES ('$Panel_Calcs_id','$Profile_Code','$Signage','$Part_Name','$Calculation')";

						if ($conn->query($sql) === TRUE) {
						    //echo "Panel Calcs inserted successfully<br>";


						} else {
							$iserror=1;
						    echo "Error: " . $sql . "<br>" . $conn->error;
						}
			}
		}
	}
}
while( $request = $response->next() );
if($iserror==0)
{
	echo "Inserted successfully<br>";
}
