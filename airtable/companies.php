<?php
require_once('dbconfig.php');
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
// Simple example to setup and retrieve all data from a table

// If using Composer
require 'vendor/autoload.php';

/* if not using composer, uncomment this
include('../src/Airtable.php');
include('../src/Request.php');
include('../src/Response.php');
*/

use TANIOS\Airtable\Airtable;

$airtable = new Airtable(array(
    'api_key'   => 'keyw3cJfiAFXmYjJf', //keyoA5KNgdkmd3iDG
    'base'      => 'appwQYmk5VhwJ4AeM',
));

$del = "TRUNCATE TABLE yfm_wp_AT_companies";
$delresult=mysqli_query($conn,$del);

if($delresult)
{
	$request = $airtable->getContent( 'Companies' );
}
do {
    $response = $request->getResponse();
    // echo '<pre>';
    // var_dump( $response[ 'records' ] ); die;
		$data = $response['records'];
		$iserror = 0;
		foreach ($response['records'] as $data) {
			//$CompanyCode = $data->fields->CompanyCode;
			if(!empty($data->fields->CompanyCode))
		 		{
		 			$CompanyCode = $data->fields->CompanyCode;
		 		}
		 		else{
		 			$CompanyCode = '';
		 		}

		  //$CompanyName = str_replace("'", '`', $data->fields->CompanyName);
		  if(!empty($data->fields->CompanyName))
		 		{
		 			$CompanyName = str_replace("'", '`', $data->fields->CompanyName);
		 		}
		 		else{
		 			$CompanyName = '';
		 		}

		 		if(!empty($data->fields->Type))
		 		{
		 			$Type = $data->fields->Type;
		 		}else{
		 			$Type = '';
		 		}
		 			if(!empty($data->fields->Website))
		 		{
		 			$Website = $data->fields->Website;
		 		}
		 		else{
		 			$Website = '';
		 		}
		 		if(!empty($data->fields->Comments))
		 		{
		 			$Comments = str_replace("'", '`', $data->fields->Comments);
		 		}
		 		else{
		 			$Comments = '';
		 		}
		 		if(!empty($data->fields->Locations))
		 		{
		 			$Locations = json_encode($data->fields->Locations);
		 		}
		 		else{
		 			$Locations = json_encode('');
		 		}

		 		if(!empty($data->fields->Contacts))
		 		{
		 			$Contacts = json_encode($data->fields->Contacts);
		 		}
		 		else{
		 			$Contacts = json_encode('');
		 		}
		 	;
		 		if(!empty($data->fields->Rating))
		 		{
		 			$Rating = $data->fields->Rating;
		 		}
		 		else{
		 			$Rating = '0';
		 		}
		 		if(!empty($data->fields->Web_link_co_code))
		 		{
		 			$Web_link_co_code = $data->fields->Web_link_co_code;
		 		}
		 		else{
		 			$Web_link_co_code = '';
		 		}

		 		$company_id =  $data->id;

			$sel ="SELECT company_id from yfm_wp_AT_companies WHERE company_id='$company_id'";
			$result=mysqli_query($conn,$sel);
			if ($result)
			{
				$get = mysqli_num_rows($result);
				//echo '/'.$get.'<br>';

				if($get==1)
				{
					$sql = "UPDATE yfm_wp_AT_companies SET CompanyCode='$CompanyCode',CompanyName='$CompanyName',Type='$Type',Website='$Website',Comments='$Comments',Locations='$Locations',Contacts='$Contacts',Rating='$Rating',Web_link_co_code='$Web_link_co_code',company_id='$company_id' WHERE company_id='$company_id'";
					if ($conn->query($sql) === TRUE) {
					echo "Updated successfully<br>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}
				}
				else{
					$sql = "INSERT INTO yfm_wp_AT_companies (CompanyCode,CompanyName, Type,Website,Comments,Locations,Contacts,Rating,Web_link_co_code,company_id)
					VALUES ('$CompanyCode','$CompanyName','$Type','$Website','$Comments','$Locations','$Contacts','$Rating','$Web_link_co_code','$company_id')";

					if ($conn->query($sql) === TRUE) {
					    //echo "Inserted successfully<br>";

					} else {
						$iserror=1;
					    echo "Error: " . $sql . "<br>" . $conn->error;
					}
				}
			}
		}

}
while( $request = $response->next() );
if($iserror==0)
{
	echo "Inserted successfully<br>";
}
